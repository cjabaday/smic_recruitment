<?php
$params = ['soap_version'=>SOAP_1_2,
           'trace'=>1,
           'exceptions'=>1,
           'location'=>'http://csorepo.apc.edu.ph/smic_recruitment/recruitment_soap_server.php',
           // 'location'=>'http://localhost/smic_recruitment/recruitment_soap_server.php',
           'uri'=>'urn://smic_recruitment'];

$client = new SoapClient(null,$params);

//the export_prf is the method to use to export data from iHRIS to hosted recruitment
//this method expects an array that consists of the following:

//data for the set up tables
$arr_company        = array('company_id' => 1, 'official_name' => 'SM Investments Corporation');
$arr_department     = array('department_id' => 1, 'name' => 'Human Resources');
$arr_position       = array('position_id' => 1, 'title' => 'Compensation and Benefits');
$arr_rank           = array('rank_id' => 1, 'name' => 'Rank and File');
$arr_classification = array('classification_id' => 1, 'name' => 'Rank and File');

//export_prf will check if the data passed above already exist in the database. If yes, insert command will be skipped
// otherwise new records will be inserted

//data for the iprf_staffrequest table
$arr_prf_details = array(
                    'iprf_staffrequest_id'            => 2, //change this if iprf_staffrequest_id error appears
                    'PRF_No'                          => 'PRF-2018-00001',
                    'company_id'                      => 1,
                    'department_id'                   => 1,
                    'position_id'                     => 1,
                    'rank_id'                         => 1,
                    'classification_id'               => 1,
                    'status'                          => 'In progress',
                    'duration'                        => 'Sample Duration Data',
                    'responsibility'                  => 'Collects and process compensation and benefits of employees',
                    'general_duties'                  => 'Collects and process compensation and benefits of employees in general',
                    'detailed_duties'                 => 'Collects and process compensation and benefits of employees in detailed',
                    'education'                       => "Must have be college graduate",
                    'professional_eligibility_skills' => "Must clerical and attentive",
                    'experience'                      => "Nothing in Particular.",
                    'skills'                          => 'N/A',
                    'date_needed'                     => '2018-11-01',
                    'date_filed'                      => '2018-10-20'
);

//combine all arrays into a single array

$arr_prf = array(
                    'company'        => $arr_company,
                    'department'     => $arr_department,
                    'position'       => $arr_position,
                    'rank'           => $arr_rank,
                    'classification' => $arr_classification,
                    'prf_details'    => $arr_prf_details,
                );

$response = $client->export_prf($arr_prf);


//notification code only
if($response == 'duplicate')
{

        echo "PRF_staffrequest_id already used, please change the testing data";
}
else
{
    echo "Staff request successfully imported to SMIC Recruitment Module.";
    echo "<br>";
    echo 'You can check the staff request here by accessing this url: '.$response.'';
}
