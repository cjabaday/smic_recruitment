<?php
require_once 'path.php';
init_cobalt('ALLOW_ALL',FALSE);
$html = new html;
$html->draw_header('', $message, $message_type, FALSE);

if(ENABLE_SIDEBAR)
{
    echo '
    <script>
    if (top.location == location)
    {
        window.location.replace("start.php");
    }
    </script>
    ';
}

if(DEBUG_MODE)
{
    // $html->display_error('System is running in DEBUG MODE. Please contact the system administrator ASAP.');
}


if(check_link('Applicant Access'))
{

    //get applicant info
    $applicant_id = $_SESSION['applicant_id'];
    $dbh = cobalt_load_class('user');
    $dbh->set_where('applicant_id = ?');
    $dbh->stmt_bind_param($applicant_id);
    $dbh->stmt_prepare();
    $personal_email = $dbh->stmt_fetch('single')->dump['username'];

    //get applicant info
    $dbh = cobalt_load_class('applicant');
    $dbh->set_where('applicant_id = ?');
    $dbh->stmt_bind_param($applicant_id);
    $dbh->stmt_prepare();
    $dbh->stmt_fetch('single');

    extract($dbh->dump);

    // check applicant child tables
    // require_once "components/check_applicant_child_tables.php";
    $comp = cobalt_load_component("check_applicant_child_tables");
    $comp->inherit($applicant_id);
    $string = $comp->go();

    $temp_date = explode('-',$date_of_birth);
    if(count($temp_date) == 3)
    {
        $date_of_birth_year = $temp_date[0];
        $date_of_birth_month = $temp_date[1];
        $date_of_birth_day = $temp_date[2];

    }
    $full_name = "$last_name, $first_name $middle_name";
    // $province_address = "$provincial_address_line_1 $provincial_address_line_2 $provincial_address_barangay $provincial_address_city_municipality $provincial_address_province";

    if($date_of_birth)
    {
        $string_date = date('F d, Y',strtotime($date_of_birth));
        require_once 'components/compute_age.php';

    }
    else
    {
        $string_date = "&nbsp;";
        $age = "&nbsp;";
    }

    ?>
    <style>
    .name_class {
        font-size:30px;
        font-weight: bold;
    }

    .divider {
        margin-top:20px;
    }

    .container_content fieldset {
        border-bottom:0px;
        border-left:0px;
        border-right:0px;
        border-radius:0px;
        margin:20px;

    }

    .detail_view {
        border:solid 1px;
        width:300px;
        font-weight:bold;
        line-height:2;
        padding-left:5px;
    }
    </style>
    <?php
    $applicant_html = cobalt_load_class('applicant_html');
    $applicant_html->detail_view = TRUE;
    $arr_additional_applicant_info = [
                                        ['field_name' => 'string_date', 'field_value' =>$string_date],
                                        ['field_name' => 'age', 'field_value' =>$age],
                                        ['field_name' => 'applicant_id', 'field_value' =>$applicant_id],
                                        ['field_name' => 'full_name', 'field_value' =>$full_name],
                                        ['field_name' => 'image', 'field_value' =>$image],
                                        ['field_name' => 'personal_email', 'field_value' =>$personal_email]

    ];
        // brpt();
        $height .= " cm.";
        $weight .= " kg.";

    if($string == "")
    {
        // do nothing..
    }
    else
    {
        $applicant_html->display_message("Please fill out the following info: <br> $string");
    }
    $applicant_html->draw_applicant_details($arr_additional_applicant_info);
}
else
{

    $menu_links = array();
    $d = new data_abstraction;
    $d->set_fields('a.link_id, a.descriptive_title, a.target, a.description, c.passport_group, a.icon as link_icon, c.icon as `group_icon`');
    $d->set_table('user_links a, user_passport b, user_passport_groups c');
    $d->set_where("a.link_id=b.link_id AND b.username = ? AND a.passport_group_id=c.passport_group_id AND a.show_in_tasklist='Yes' AND a.status='On'");
    $d->set_order('c.priority DESC, c.passport_group, a.priority DESC, a.descriptive_title');
    $d->stmt_bind_param($_SESSION['user']);
    $d->stmt_prepare();
    $data = $d->stmt_fetch('rowdump')->dump;
    $num_rows = $d->num_rows;
    for($a = 0; $a < $num_rows; ++$a)
    {
        extract($data[$a]);
        $menu_links[$passport_group]['title'][]       = $descriptive_title;
        $menu_links[$passport_group]['target'][]      = $target;
        $menu_links[$passport_group]['link_id'][]     = $link_id;
        $menu_links[$passport_group]['description'][] = $description;
        $menu_links[$passport_group]['link_icon'][]   = $link_icon;
        $menu_links[$passport_group]['group_icon'][]  = $group_icon;
    }
    unset($d);

    if(isset($_SESSION['control_center_columns']) && $_SESSION['control_center_columns'] > 0)
    {
        $columns_per_row = $_SESSION['control_center_columns'];
    }
    elseif(defined('CONTROL_CENTER_COLUMNS'))
    {
        $columns_per_row = CONTROL_CENTER_COLUMNS;
    }
    else
    {
        $columns_per_row = 3; //just an arbitrary default value based on historical Cobalt setting
    }

    $cntr          = 0;
    $cntr_limit    = $columns_per_row - 1; //subtraction needed due to 0-based counter
    $column_width  = (100 / $columns_per_row);
    $current_group = '';
    if(is_array($menu_links))
    {
        $target_frame='';
        if(ENABLE_SIDEBAR)
        {
            $target_frame = 'target="content_frame"';
        }

        echo '<fieldset class="container">';
        foreach($menu_links as $group => $link_info)
        {
            if($current_group=='')
            {
                $current_group = $group;
                menuGroupWindowHeader($group, $link_info['group_icon'][0]);
            }

            $num_links = count($link_info['title']);
            for($a=0; $a<$num_links; ++$a)
            {
                if($current_group!= $group)
                {
                    echo '</tr></table></div>';
                    $cntr=0;
                    menuGroupWindowFooter();
                    menuGroupWindowHeader($group, $link_info['group_icon'][$a]);
                    $current_group = $group;
                }

                if($cntr==0)
                {
                    echo '<div class="container_icons_CC">';
                    echo '<table width = "100%">';
                    echo '<tr>';
                }
                elseif($cntr > $cntr_limit)
                {
                    echo '</tr></table>';
                    echo '</div><div class="container_icons_CC">';
                    echo '<table width = "100%">';
                    echo '<tr>';
                    $cntr = 0;
                }
                ++$cntr;
                echo '<td width="' . $column_width . '%" valign="top">
                        <a href="/' . BASE_DIRECTORY . '/' . $link_info['target'][$a] . '" $target_frame class="linkCC">
                            <img src="images/' . $_SESSION['icon_set'] . '/' . $link_info['link_icon'][$a] . '"><br>' . $link_info['title'][$a] . '
                        </a>
                      </td>';

            }

            //Just to be sure we have three columns before closing the table
            for($z = $cntr; $z<=$cntr_limit; ++$z)
            {
                echo '<td width="'. $column_width . '%"> &nbsp; </td>';
            }
        }
        echo '</tr></table></div>';
        echo '</fieldset>';
    }
    else
    {
        $html->display_error("You have no Control Center privileges in your account. Please contact your system administrator.");
    }
}

menuGroupWindowFooter();

function menuGroupWindowHeader($group, $icon)
{
    echo '<fieldset class="top">';
    echo "<img src='images/" . $_SESSION['icon_set'] . "/$icon' height='60px' width='40px'> $group";
    echo '</fieldset>';
    echo '<fieldset class="middle">';
}

function menuGroupWindowFooter()
{
    echo '</fieldset>';
}
$html->draw_footer();
