<?php
$params = ['soap_version'=>SOAP_1_1,
           'trace'=>1,
           'exceptions'=>1,
           'location'=>'http://csorepo.apc.edu.ph/smic_recruitment/recruitment_soap_server.php',
           // 'location'=>'http://localhost/smic_recruitment/recruitment_soap_server.php',
           'uri'=>'urn://smic_recruitment'];

$client = new SoapClient('http://csorepo.apc.edu.ph/smic_recruitment/recruitment_soap.wsdl',$params);

//the export_prf is the method to use to export data from iHRIS to hosted recruitment
//this method expects an array that consists of the following:


//data for the set up tables
$company_id = 1;
$company = 'SM Investments Corporation';

$department_id = 1;
$department = 'Human Resources';

$position_id = 2;
$position = 'Compensation and Benefits Staff';

$rank_id = 1;
$rank = "Rank and File";

$classification_id = 1;
$classification = "Rank and File";
//export_prf will check if the data passed above already exist in the database. If yes, insert command will be skipped
// otherwise new records will be inserted
$response =  $client->send_department($department, $department_id);

// var_dump($response);
echo $response;
$response =  $client->send_rank($rank, $rank_id);
echo $response;
$response =  $client->send_classification($classification, $classification_id);
echo $response;
$response =  $client->send_company($company, $company_id);
echo $response;

$response =  $client->send_position($position, $position_id);
echo $response;

//data for the iprf_staffrequest table

$iprf_staffrequest_id            = 3; //change these if iprf_staffrequest_id error appears
$PRF_No                          = 'PRF-2018-00002';
$status                          = 'In progress';
$duration                        = 'Sample Duration Data';
$responsibility                  = 'Collects and process compensation and benefits of employees';
$general_duties                  = 'Collects and process compensation and benefits of employees in general';
$detailed_duties                 = 'Collects and process compensation and benefits of employees in detailed';
$education                       = "Must have be college graduate";
$professional_eligibility_skills = "Must clerical and attentive";
$experience                      = "Nothing in Particular.";
$skills                          = 'N/A';
$date_needed                     = '2018-11-01';
$date_filed                      = '2018-10-20';

$response = $client->send_prf($iprf_staffrequest_id, $PRF_No, $company_id, $department_id, $position_id, $rank_id, $classification_id, $status, $duration, $responsibility, $general_duties, $detailed_duties, $education, $professional_eligibility_skills, $experience, $skills, $date_needed, $date_filed);

// code below not necessary, for notification only
if($response == 'duplicate')
{

        echo "PRF_staffrequest_id already used, please change the testing data";
}
else
{
    echo "Staff request successfully imported to SMIC Recruitment Module.";
    echo "<br>";
    echo 'You can check the staff request here by accessing this url: '.$response.'';
}
