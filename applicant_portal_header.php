<div class="class.header" style="background-color:white; width100%; height:7%; padding:1%;">
    <img style="display: inline-block; width:12%; vertical-align: middle; "src="images_applicant_portal/smic-logo.jpg">

        <div class="headerdetails" style="display: inline-block; text-align:right; float:right;">
            <div class="headertext" style="display: inline-block;">
            <h3 style="color:#3ca2d1;"><i>Welcome to SMIC Applicant Portal</i></h3>
            <p style="font-size:70%;"><span id ="day"></span> | <span id ="date_today"> </span> | <span id ="time"></span> | </p>
            <!-- <img style="width:7%; vertical-align:middle;" src="images_applicant_portal/cloud.png"> -->
            </div>
            <img src="images_applicant_portal/anniv.png" style="padding-left:3%; width:10%; display:inline-block; float:right;">
        </div>
</div>
<div class="header_menu" style="width: 100%; height:2%; border-bottom: 2px solid white; background-color:#073784; color:white; padding: 1% 0; text-align:center; vertical-align:middle;">
    <ul>
    <li><a href="applicant_home.php">Home</a></li>
    <li>Our Company</li>
    <li><a href="career_page.php">Career </a></li>
    <li>Contact Us</li>
    <li><a href="applicant_portal.php">Login</a></li>
    </ul>
</div>

<script>

// Get Day, Date, and Time
var date = new Date();
var day = date.getDay();
var weekday = new Array(7);
var months = new Array(12);
weekday = ["Sunday", "Monday","Tuesday", "Wednesday", "Thursday","Friday","Saturday"];
months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var month_string = months[date.getMonth()];
var date_today = month_string + " " + date.getDate() + " " + date.getFullYear();
var t = date.toLocaleTimeString();
var time = setInterval(timeAMPM, 1000);

function timeAMPM() {
    var date = new Date();
    var t = date.toLocaleTimeString();
    document.getElementById("time").innerHTML = t;
}

document.getElementById("day").innerHTML = weekday[day];
document.getElementById("date_today").innerHTML = date_today;

document.getElementById("time").innerHTML = t;
</script>
