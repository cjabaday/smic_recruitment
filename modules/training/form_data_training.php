<?php
require 'components/get_listview_referrer.php';

require 'subclasses/training.php';
$dbh_training = new training;
$dbh_training->set_where("training_id = ?");
$dbh_training->stmt_bind_param($training_id);

$data = $dbh_training->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);

    $data = explode('-',$training_date);
    if(count($data) == 3)
    {
        $training_date_year = $data[0];
        $training_date_month = $data[1];
        $training_date_day = $data[2];
    }
}

