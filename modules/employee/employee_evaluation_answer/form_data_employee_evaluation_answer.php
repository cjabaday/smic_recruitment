<?php
require 'components/get_listview_referrer.php';

require 'subclasses/employee_evaluation_answer.php';
$dbh_employee_evaluation_answer = new employee_evaluation_answer;
$dbh_employee_evaluation_answer->set_where("employee_evaluation_answer_id = ?");
$dbh_employee_evaluation_answer->stmt_bind_param($employee_evaluation_answer_id);

$data = $dbh_employee_evaluation_answer->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}

