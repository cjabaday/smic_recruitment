<?php
require 'components/get_listview_referrer.php';

require 'subclasses/employee_education.php';
$dbh_employee_education = new employee_education;
$dbh_employee_education->set_where("employee_education_id = ?");
$dbh_employee_education->stmt_bind_param($employee_education_id);

$data = $dbh_employee_education->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}

