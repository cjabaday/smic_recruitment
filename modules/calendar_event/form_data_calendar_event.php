<?php
require 'components/get_listview_referrer.php';

require 'subclasses/calendar_event.php';
$dbh_calendar_event = new calendar_event;
$dbh_calendar_event->set_where("calendar_event_id = ?");
$dbh_calendar_event->stmt_bind_param($calendar_event_id);

$data = $dbh_calendar_event->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);

    $data = explode('-',$start_date);
    if(count($data) == 3)
    {
        $start_date_year = $data[0];
        $start_date_month = $data[1];
        $start_date_day = $data[2];
    }
    $data = explode('-',$end_date);
    if(count($data) == 3)
    {
        $end_date_year = $data[0];
        $end_date_month = $data[1];
        $end_date_day = $data[2];
    }
}

