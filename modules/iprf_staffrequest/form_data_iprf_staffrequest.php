<?php
require 'components/get_listview_referrer.php';

require 'subclasses/iprf_staffrequest.php';
$dbh_iprf_staffrequest = new iprf_staffrequest;
$dbh_iprf_staffrequest->set_where("iprf_staffrequest_id = ?");
$dbh_iprf_staffrequest->stmt_bind_param($iprf_staffrequest_id);

$data = $dbh_iprf_staffrequest->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);

    $data = explode('-',$date_needed);
    if(count($data) == 3)
    {
        $date_needed_year = $data[0];
        $date_needed_month = $data[1];
        $date_needed_day = $data[2];
    }
    $data = explode('-',$date_filed);
    if(count($data) == 3)
    {
        $date_filed_year = $data[0];
        $date_filed_month = $data[1];
        $date_filed_day = $data[2];
    }
}

require_once 'subclasses/iprf_staffrequest_applicants.php';
$dbh_iprf_staffrequest_applicants = new iprf_staffrequest_applicants;
$dbh_iprf_staffrequest_applicants->set_fields('iprf_staffrequest_applicant_id, applicant_id, date_applied');
$dbh_iprf_staffrequest_applicants->set_where("iprf_staffrequest_id = ?");
$dbh_iprf_staffrequest_applicants->stmt_bind_param($iprf_staffrequest_id);

$data = $dbh_iprf_staffrequest_applicants->stmt_prepare()->stmt_fetch('rowdump')->dump;
$num_iprf_staffrequest_applicants = count($data);
$a = 0;
foreach($data as $column)
{
        $cf_iprf_staffrequest_applicants_iprf_staffrequest_applicant_id[$a] = $column['iprf_staffrequest_applicant_id'];
        $cf_iprf_staffrequest_applicants_applicant_id[$a] = $column['applicant_id'];
        $cf_iprf_staffrequest_applicants_date_applied[$a] = $column['date_applied'];
++$a;
}

