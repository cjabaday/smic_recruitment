<?php
require 'components/get_listview_referrer.php';

require 'subclasses/calendar_event_type.php';
$dbh_calendar_event_type = new calendar_event_type;
$dbh_calendar_event_type->set_where("calendar_event_type_id = ?");
$dbh_calendar_event_type->stmt_bind_param($calendar_event_type_id);

$data = $dbh_calendar_event_type->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}

