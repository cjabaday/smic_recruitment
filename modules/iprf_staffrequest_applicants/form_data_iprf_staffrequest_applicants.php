<?php
require 'components/get_listview_referrer.php';

require 'subclasses/iprf_staffrequest_applicants.php';
$dbh_iprf_staffrequest_applicants = new iprf_staffrequest_applicants;
$dbh_iprf_staffrequest_applicants->set_where("iprf_staffrequest_applicant_id = ?");
$dbh_iprf_staffrequest_applicants->set_where($iprf_staffrequest_applicant_id);

$data = $dbh_iprf_staffrequest_applicants->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}
