<?php
require 'components/get_listview_referrer.php';

require 'subclasses/applicant_attachments.php';
$dbh_applicant_attachments = new applicant_attachments;
$dbh_applicant_attachments->set_where("applicant_attachment_id = ?");
$dbh_applicant_attachments->stmt_bind_param($applicant_attachment_id);

$data = $dbh_applicant_attachments->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}

