<?php
require 'components/get_listview_referrer.php';

require 'subclasses/languages.php';
$dbh_languages = new languages;
$dbh_languages->set_where("language_id = ?");
$dbh_languages->stmt_bind_param($language_id);

$data = $dbh_languages->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}

