<?php
require 'components/get_listview_referrer.php';

require 'subclasses/personnel_requisition.php';
$dbh_personnel_requisition = new personnel_requisition;
$dbh_personnel_requisition->set_where("personnel_requisition_id = ?");
$dbh_personnel_requisition->stmt_bind_param($personnel_requisition_id);

$data = $dbh_personnel_requisition->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);

    $data = explode('-',$requisition_date);
    if(count($data) == 3)
    {
        $requisition_date_year = $data[0];
        $requisition_date_month = $data[1];
        $requisition_date_day = $data[2];
    }
}

