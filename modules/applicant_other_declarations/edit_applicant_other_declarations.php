<?php
//****************************************************************************************
//Generated by Cobalt, a rapid application development framework. http://cobalt.jvroig.com
//Cobalt developed by JV Roig (jvroig@jvroig.com)
//****************************************************************************************
require 'path.php';
init_cobalt('Edit applicant other declarations');



if(isset($_GET['applicant_other_declaration_id']))
{
    $applicant_other_declaration_id = rawurldecode($_GET['applicant_other_declaration_id']);
    require 'form_data_applicant_other_declarations.php';
}
require_once 'components/session_per_applicant_child_table.php';
$anchor_tag = '';

if(xsrf_guard())
{
    init_var($_POST['btn_cancel']);
    init_var($_POST['btn_submit']);
    init_var($_POST['btn_mf_other_declarations_name_smgroup']);
    init_var($_POST['btn_mf_other_declarations_name_smic_employ']);
    // require 'components/query_string_standard.php';
    require 'subclasses/applicant_other_declarations.php';
    $dbh_applicant_other_declarations = new applicant_other_declarations;

    $object_name = 'dbh_applicant_other_declarations';
    require 'components/create_form_data.php';

    extract($arr_form_data);

    if($_POST['btn_cancel'])
    {
        log_action('Pressed cancel button');
        redirect("../../main.php");
    }

    if($_POST['btn_mf_other_declarations_name_smgroup'])
    {
        $anchor_tag = "Other_Declarations_Name_Smgroup";
    }

    if($_POST['btn_mf_other_declarations_name_smic_employ'])
    {
        $anchor_tag = "Other_Declarations_Name_Smic_Employ";
    }

        init_var($other_declarations_name_smgroup, 0);
        init_var($other_declarations_name_smic_employ_count, 0);
    if($_POST['btn_mf_other_declarations_name_smic_employ'] === "+")
    {

        $other_declarations_name_smic_employ_count += 1;

    }


    if($_POST['btn_mf_other_declarations_name_smgroup'] === "+")
    {

        $other_declarations_name_smgroup_count += 1;

    }

    if($_POST['btn_submit'])
    {
        log_action('Pressed submit button');

        $delete_details = FALSE;
        if($crime_convict == 'No')
        {
            $arr_form_data['details'] = "1";
            $delete_details = TRUE;
        }

        //reset array values of multifields
        if(isset($arr_form_data['cf_other_declarations_name_smgroup_name']))
        {
            $arr_form_data['cf_other_declarations_name_smgroup_name'] = array_values($arr_form_data['cf_other_declarations_name_smgroup_name']);
        }

        if(isset($arr_form_data['cf_other_declarations_name_smgroup_relationship']))
        {
            $arr_form_data['cf_other_declarations_name_smgroup_relationship']  = array_values($arr_form_data['cf_other_declarations_name_smgroup_relationship']);
        }

        if(isset($arr_form_data['cf_other_declarations_name_smgroup_position']))
        {
            $arr_form_data['cf_other_declarations_name_smgroup_position'] = array_values($arr_form_data['cf_other_declarations_name_smgroup_position']);

        }

        if(isset($arr_form_data['cf_other_declarations_name_smgroup_company']))
        {

            $arr_form_data['cf_other_declarations_name_smgroup_company'] = array_values($arr_form_data['cf_other_declarations_name_smgroup_company']);
        }

        if(isset($arr_form_data['cf_other_declarations_name_smic_employ_name']))
        {
            $arr_form_data['cf_other_declarations_name_smic_employ_name'] = array_values($arr_form_data['cf_other_declarations_name_smic_employ_name']);

        }

        if(isset($arr_form_data['cf_other_declarations_name_smic_employ_relationship']))
        {
            $arr_form_data['cf_other_declarations_name_smic_employ_relationship'] = array_values($arr_form_data['cf_other_declarations_name_smic_employ_relationship']);

        }

        if(isset($arr_form_data['cf_other_declarations_name_smic_employ_position']))
        {
            $arr_form_data['cf_other_declarations_name_smic_employ_position'] = array_values($arr_form_data['cf_other_declarations_name_smic_employ_position']);

        }

        if(isset($arr_form_data['cf_other_declarations_name_smic_employ_department_company']))
        {

            $arr_form_data['cf_other_declarations_name_smic_employ_department_company'] = array_values($arr_form_data['cf_other_declarations_name_smic_employ_department_company']);
        }

        // $arr_form_data['cf_other_declarations_name_smic_employ_department_company'] = array_values($arr_form_data['cf_other_declarations_name_smic_employ_department_company']);
        // debug($arr_form_data);
        // die();
        $message .= $dbh_applicant_other_declarations->sanitize($arr_form_data)->lst_error;
        extract($arr_form_data);



        if($delete_details)
        {
            $arr_form_data['details'] = "";
        }
        if($dbh_applicant_other_declarations->check_uniqueness_for_editing($arr_form_data)->is_unique)
        {
            //Good, no duplicate in database
        }
        else
        {
            $message = "Record already exists with the same primary identifiers!";
        }

        if($message=="")
        {
            cobalt_load_class('other_declarations_name_smgroup')->delete_many($arr_form_data);            $dbh_other_declarations_name_smgroup = new other_declarations_name_smgroup;

            $param = array();
            for($a=0; $a<$other_declarations_name_smgroup_count;$a++)
            {
                $param['applicant_other_declaration_id'] = $applicant_other_declaration_id;
                $param['name'] = $cf_other_declarations_name_smgroup_name[$a];
                $param['relationship'] = $cf_other_declarations_name_smgroup_relationship[$a];
                $param['position'] = $cf_other_declarations_name_smgroup_position[$a];
                $param['company'] = $cf_other_declarations_name_smgroup_company[$a];
                $dbh_other_declarations_name_smgroup->add($param);
            }

            cobalt_load_class('other_declarations_name_smic_employ')->delete_many($arr_form_data);            $dbh_other_declarations_name_smic_employ = new other_declarations_name_smic_employ;

            $param = array();
            for($a=0; $a<$other_declarations_name_smic_employ_count;$a++)
            {
                $param['applicant_other_declaration_id'] = $applicant_other_declaration_id;
                $param['name'] = $cf_other_declarations_name_smic_employ_name[$a];
                $param['relationship'] = $cf_other_declarations_name_smic_employ_relationship[$a];
                $param['position'] = $cf_other_declarations_name_smic_employ_position[$a];
                $param['department_company'] = $cf_other_declarations_name_smic_employ_department_company[$a];
                $dbh_other_declarations_name_smic_employ->add($param);
            }


            $dbh_applicant_other_declarations->edit($arr_form_data);

            // redirect("listview_applicant_other_declarations.php?$query_string");
            redirect("../../main.php");

        }
    }
}
require 'subclasses/applicant_other_declarations_html.php';
$html = new applicant_other_declarations_html;
$html->draw_header('Other Declarations', $message, $message_type);
// $html->draw_listview_referrer_info($filter_field_used, $filter_used, $page_from, $filter_sort_asc, $filter_sort_desc);
$html->draw_hidden('applicant_other_declaration_id');
require_once "components/html_applicant_child_table.php";


$html->fields['crime_convict']['extra'] = 'onchange="this.form.submit();"';
$html->draw_controls_add_line('edit');
// $html->draw_controls('edit');

$html->draw_footer();
?>

<script>
function deleteRow(count,table,hidden_field) {
    var i = count.parentNode.parentNode.rowIndex;
    document.getElementById(table).deleteRow(i);
    var current_count = document.getElementById(hidden_field).value;
    var new_count = current_count - 1;
    document.getElementById(hidden_field).value = new_count;
}
</script>
