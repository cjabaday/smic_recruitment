<?php
require_once "path.php";
init_cobalt();
//check if applicant already have a record
$dbh = cobalt_load_class('applicant_other_declarations');
$dbh->set_where('applicant_id = ?');
$dbh->stmt_bind_param($_SESSION['applicant_id']);
$dbh->stmt_prepare()->stmt_fetch('single');
$applicant_other_declaration_id = $dbh->dump['applicant_other_declaration_id'];

if($dbh->num_rows > 0)
{
    // brpt();
    redirect("edit_applicant_other_declarations.php?applicant_other_declaration_id=$applicant_other_declaration_id");
}
else
{
    redirect("add_applicant_other_declarations.php");

}
