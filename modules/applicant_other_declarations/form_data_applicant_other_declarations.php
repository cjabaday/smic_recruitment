<?php
require 'components/get_listview_referrer.php';

require 'subclasses/applicant_other_declarations.php';
$dbh_applicant_other_declarations = new applicant_other_declarations;
$dbh_applicant_other_declarations->set_where("applicant_other_declaration_id = ?");
$dbh_applicant_other_declarations->stmt_bind_param($applicant_other_declaration_id);

$data = $dbh_applicant_other_declarations->stmt_prepare()->stmt_fetch('single')->dump;

// debug($applicant_other_declaration_id);
if(is_array($data))
{
    extract($data);
}

require_once 'subclasses/other_declarations_name_smgroup.php';
$dbh_other_declarations_name_smgroup = new other_declarations_name_smgroup;
$dbh_other_declarations_name_smgroup->set_fields('name, relationship, position, company');
$dbh_other_declarations_name_smgroup->set_where("applicant_other_declaration_id = ?");
$dbh_other_declarations_name_smgroup->stmt_bind_param($applicant_other_declaration_id);

$data = $dbh_other_declarations_name_smgroup->stmt_prepare()->stmt_fetch('rowdump')->dump;
$num_other_declarations_name_smgroup = count($data);
$a = 0;
foreach($data as $column)
{
        $cf_other_declarations_name_smgroup_name[$a] = $column['name'];
        $cf_other_declarations_name_smgroup_company[$a] = $column['company'];
        $cf_other_declarations_name_smgroup_relationship[$a] = $column['relationship'];
        $cf_other_declarations_name_smgroup_position[$a] = $column['position'];
++$a;
}

require_once 'subclasses/other_declarations_name_smic_employ.php';
$dbh_other_declarations_name_smic_employ = new other_declarations_name_smic_employ;
$dbh_other_declarations_name_smic_employ->set_fields('name, relationship, position, department_company');
$dbh_other_declarations_name_smic_employ->set_where("applicant_other_declaration_id = ?");
$dbh_other_declarations_name_smic_employ->stmt_bind_param($applicant_other_declaration_id);

$data = $dbh_other_declarations_name_smic_employ->stmt_prepare()->stmt_fetch('rowdump')->dump;
$num_other_declarations_name_smic_employ = count($data);
$a = 0;
foreach($data as $column)
{
        $cf_other_declarations_name_smic_employ_name[$a] = $column['name'];
        $cf_other_declarations_name_smic_employ_department_company[$a] = $column['department_company'];
        $cf_other_declarations_name_smic_employ_relationship[$a] = $column['relationship'];
        $cf_other_declarations_name_smic_employ_position[$a] = $column['position'];
++$a;
}
