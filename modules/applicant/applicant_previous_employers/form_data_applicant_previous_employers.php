<?php
require 'components/get_listview_referrer.php';

require 'subclasses/applicant_previous_employers.php';
$dbh_applicant_previous_employers = new applicant_previous_employers;
$dbh_applicant_previous_employers->set_where("applicant_previous_employer_id = ?");
$dbh_applicant_previous_employers->stmt_bind_param($applicant_previous_employer_id);

$data = $dbh_applicant_previous_employers->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);

    $temp = explode('-',$previous_employer_date_from);
    if(count($temp) == 3)
    {
        $previous_employer_date_from_year = $temp[0];
        $previous_employer_date_from_month = $temp[1];
        $previous_employer_date_from_day = $temp[2];
    }

    $temp = explode('-',$previous_employer_date_to);
    if(count($temp) == 3)
    {
        $previous_employer_date_to_year = $temp[0];
        $previous_employer_date_to_month = $temp[1];
        $previous_employer_date_to_day = $temp[2];
    }
}
