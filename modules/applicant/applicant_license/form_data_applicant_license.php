<?php
require 'components/get_listview_referrer.php';

require 'subclasses/applicant_license.php';
$dbh_applicant_license = new applicant_license;
$dbh_applicant_license->set_where("applicant_license_id = ?");
$dbh_applicant_license->stmt_bind_param($applicant_license_id);

$data = $dbh_applicant_license->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);

    $temp = explode('-',$license_expiry);
    if(count($temp) == 3)
    {
        $license_expiry_year = $temp[0];
        $license_expiry_month = $temp[1];
        $license_expiry_day = $temp[2];
    }
}
