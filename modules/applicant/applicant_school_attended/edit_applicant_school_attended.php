<?php
//****************************************************************************************
//Generated by Cobalt, a rapid application development framework. http://cobalt.jvroig.com
//Cobalt developed by JV Roig (jvroig@jvroig.com)
//****************************************************************************************
require 'path.php';
init_cobalt('Edit applicant school attended');

if(isset($_GET['applicant_school_attended_id']))
{
    $applicant_school_attended_id = rawurldecode($_GET['applicant_school_attended_id']);
    require 'form_data_applicant_school_attended.php';

}
require_once 'components/session_per_applicant_child_table.php';
$anchor_tag = '';

if(xsrf_guard())
{
    init_var($_POST['btn_cancel']);
    init_var($_POST['btn_submit']);
    require 'components/query_string_standard.php';
    require 'subclasses/applicant_school_attended.php';
    $dbh_applicant_school_attended = new applicant_school_attended;

    $object_name = 'dbh_applicant_school_attended';
    require 'components/create_form_data.php';

    extract($arr_form_data);

    if($_POST['btn_cancel'])
    {
        log_action('Pressed cancel button');
        redirect("listview_applicant_school_attended.php?$query_string");
    }

    if($_POST['btn_submit'])
    {
        log_action('Pressed submit button');

        //college, masteral and doctorate
        if($_POST['educational_attainment'] == 4 OR $_POST['educational_attainment'] == 5 OR $_POST['educational_attainment'] == 6)
        {
            // do nothing..
            $dbh_applicant_school_attended->fields['course']['required'] = TRUE;
        }
        
        $message .= $dbh_applicant_school_attended->sanitize($arr_form_data)->lst_error;
        extract($arr_form_data);

        if($dbh_applicant_school_attended->check_uniqueness_for_editing($arr_form_data)->is_unique)
        {
            //Good, no duplicate in database
        }
        else
        {
            $message = "Record already exists with the same primary identifiers!";
        }

        $date_from_value = $date_from;
        $date_to_value = $date_to;
        require_once 'components/date_from_to_checker.php';
        if($message=="")
        {

            $dbh_applicant_school_attended->edit($arr_form_data);

            redirect("listview_applicant_school_attended.php?$query_string");
        }
    }
}
require 'subclasses/applicant_school_attended_html.php';
$html = new applicant_school_attended_html;
$html->draw_header('Edit %%', $message, $message_type);
$html->draw_listview_referrer_info($filter_field_used, $filter_used, $page_from, $filter_sort_asc, $filter_sort_desc);
$html->draw_hidden('applicant_school_attended_id');

require_once "components/html_applicant_child_table.php";
$html->draw_controls('edit');

$html->draw_footer();
