<?php
require 'components/get_listview_referrer.php';

require 'subclasses/applicant_family_members.php';
$dbh_applicant_family_members = new applicant_family_members;
$dbh_applicant_family_members->set_where("applicant_family_member_id = ?");
$dbh_applicant_family_members->stmt_bind_param($applicant_family_member_id);

$data = $dbh_applicant_family_members->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);

    $temp = explode('-',$birthday);
    if(count($temp) == 3)
    {
        $birthday_year = $temp[0];
        $birthday_month = $temp[1];
        $birthday_day = $temp[2];
    }
}
