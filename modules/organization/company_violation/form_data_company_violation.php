<?php
require 'components/get_listview_referrer.php';

require 'subclasses/company_violation.php';
$dbh_company_violation = new company_violation;
$dbh_company_violation->set_where("company_violation_id = ?");
$dbh_company_violation->stmt_bind_param($company_violation_id);

$data = $dbh_company_violation->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);

    $data = explode('-',$violation_date);
    if(count($data) == 3)
    {
        $violation_date_year = $data[0];
        $violation_date_month = $data[1];
        $violation_date_day = $data[2];
    }
}

