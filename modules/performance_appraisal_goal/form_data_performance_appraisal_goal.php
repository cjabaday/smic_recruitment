<?php
require 'components/get_listview_referrer.php';

require 'subclasses/performance_appraisal_goal.php';
$dbh_performance_appraisal_goal = new performance_appraisal_goal;
$dbh_performance_appraisal_goal->set_where("performance_appraisal_goal_id = ?");
$dbh_performance_appraisal_goal->stmt_bind_param($performance_appraisal_goal_id);

$data = $dbh_performance_appraisal_goal->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}

