<?php

require_once "thirdparty/PHPMailer/PHPMailerAutoload.php";
require_once "thirdparty/PHPMailer/class.phpmailer.php";



$dbh_system_settings = cobalt_load_class('system_settings');
$emailer_email = $dbh_system_settings->get('Emailer Username', FALSE)->dump['value'];
$emailer_password = $dbh_system_settings->get('Emailer Password', FALSE)->dump['value'];
$emailer_host = $dbh_system_settings->get('Emailer Host', FALSE)->dump['value'];
$emailer_encryption = $dbh_system_settings->get('Emailer Encryption', FALSE)->dump['value'];
$emailer_from_name = $dbh_system_settings->get('Emailer From Name', FALSE)->dump['value'];
$emailer_port = $dbh_system_settings->get('Emailer TCP Port', FALSE)->dump['value'];

//
// debug($emailer_port);
// die();
//PHPMailer Object
$mail = new PHPMailer;
// $mail->SMTPDebug = 3;
//Set PHPMailer to use SMTP.
$mail->isSMTP();
$mail->Host = $emailer_host; // add this to system settings
$mail->SMTPAuth = true;
$mail->SMTPOptions = array(
                            'ssl' => array(
                            'verify_peer' => false,
                            'verify_peer_name' => false,
                            'allow_self_signed' => true
    )
);

$mail->Username = $emailer_email; // add this to system settings
$mail->Password = $emailer_password; // add this to system settings
//If SMTP requires TLS encryption then set it
$mail->SMTPSecure = $emailer_encryption; // add this to system settings
//Set TCP port to connect to
$mail->Port = $emailer_port; // add this to system settings
//From email address and name

// for local testing
// $mail->From = $emailer_email; // add this to system settings

//for smic prod
$mail->From = $emailer_from_name; // add this to system settings

$mail->FromName = $emailer_from_name; // add this to system settings
$full_name = $recepient_name;
if($email == "")
{
 // do nothing..    
}
else
{
    $mail->addAddress($email, $full_name);
}

init_var($recruiter_emails);
if($recruiter_emails == "")
{
    //do nothing..
}
else
{
    if(strpos($recruiter_emails,','))
    {
        $email_explode = explode(",",$recruiter_emails);

        for($a = 0; $a < count($email_explode); ++$a)
        {
            $mail->addAddress($email_explode[$a]);

        }
        //multiple C&B email

    }
    else
    {
        $mail->addAddress($recruiter_emails);
    }

}

// die();
//CC and BCC
// $mail->addCC("cc@example.com");
// $mail->addBCC("bcc@example.com");

//Send HTML or Plain Text email
$mail->isHTML(true);


$mail->Subject = $email_subject;
$mail->Body = $email_body;

// recepient, subject, date, record if failed or not
if(!$mail->send())
{
    $reason = "Recepient:$email,$recruiter_emails\nSubject:$email_subject\nStatus: Failed to send email.";
    $reason .= "\nSMTP ERROR LOG:\n$mail->ErrorInfo";
    log_action($reason);
    // echo "<script> alert('Mailer Error: " . $mail->ErrorInfo."');</script>";
}
else
{
    $reason = "Recepient:$email,$recruiter_emails \nSubject:$email_subject\nStatus: Email Successfully sent.";
    log_action($reason);
}
