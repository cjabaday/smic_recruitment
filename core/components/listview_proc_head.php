<?php
$filter               = '';
$filter_field         = '';
$filter_sort_asc      = '';
$filter_sort_desc     = '';
$enc_filter           = '';
$enc_filter_field     = '';
$enc_filter_sort_asc  = '';
$enc_filter_sort_desc = '';
$result_pager         = '';
$current_page         = '';
$page_from            = '';

if(isset($_GET['filter']) || isset($_GET['filter_sort_asc']) || isset($_GET['filter_sort_desc']))
{
    if(isset($_GET['filter']))
    {
        $filter = rawurldecode($_GET['filter']);
    }
    if(isset($_GET['filter_field']))
    {
        $filter_field = rawurldecode($_GET['filter_field']);
    }
    if(isset($_GET['filter_sort_asc']))
    {
        $filter_sort_asc = rawurldecode($_GET['filter_sort_asc']);
    }
    if(isset($_GET['filter_sort_desc']))
    {
        $filter_sort_desc = rawurldecode($_GET['filter_sort_desc']);
    }
    if(isset($_GET['current_page']))
    {
        $current_page = rawurldecode($_GET['current_page']);
    }
    if(isset($_GET['page_from']))
    {
        $page_from = rawurldecode($_GET['page_from']);
    }

    if($current_page < 1)
    {
        $current_page = $page_from;
    }
}

if(xsrf_guard())
{
    init_var($_POST['btn_back']);

    if(isset($_POST['filter']))
    {
        $filter = $_POST['filter'];
    }
    if(isset($_POST['filter_field']))
    {
        $filter_field = $_POST['filter_field'];
    }
    if(isset($_POST['filter_sort_asc']))
    {
        $filter_sort_asc = $_POST['filter_sort_asc'];
    }
    if(isset($_POST['filter_sort_desc']))
    {
        $filter_sort_desc = $_POST['filter_sort_desc'];
    }
    if(isset($_POST['current_page']))
    {
        $current_page = $_POST['current_page'];
    }
    if(isset($_POST['result_pager']))
    {
        $result_pager = $_POST['result_pager'];
    }

    if($_POST['btn_back'])
    {
        log_action('Pressed cancel button');

        // debug($_GET);
        init_var($iprf_get_id,0);
        if($iprf_get_id)
        {
            redirect("/".BASE_DIRECTORY . "/career_page.php");
        }
        else
        {

            redirect(HOME_PAGE);
        }
    }
}

if(trim($filter)!='')
{
    $enc_filter = rawurlencode($filter);
}
if(trim($filter_field)!='')
{
   $enc_filter_field = rawurlencode($filter_field);
}
if(trim($filter_sort_asc)!='')
{
   $enc_filter_sort_asc = rawurlencode($filter_sort_asc);
}
if(trim($filter_sort_desc)!='')
{
   $enc_filter_sort_desc = rawurlencode($filter_sort_desc);
}
