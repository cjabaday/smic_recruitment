<?php


function setup_table_drop_down($html_object, $field_name, $table, $auto_id_name, $label)
{
    $d = cobalt_load_class($table);
    $d->set_fields("$auto_id_name, $label");
    $d->select();


    $html_object->fields[$field_name]['list_type'] = 'predefined';
    $html_object->fields[$field_name]['list_settings']['per_line'] = TRUE;
    $html_object->fields[$field_name]['list_settings']['items'] = $d->dump[$label];
    $html_object->fields[$field_name]['list_settings']['values'] = $d->dump[$auto_id_name];


    return $html_object;
}
