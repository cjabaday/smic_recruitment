<?php
// this component notifies the recruiters that an applicant applies for a job position


$dbh = cobalt_load_class('applicant');
$dbh->set_fields('last_name, middle_name, first_name');
$dbh->set_where('applicant_id = ?');
$dbh->stmt_bind_param($_SESSION['applicant_id']);
$dbh->stmt_prepare();
$dbh->stmt_fetch('single');

$applicant_name = $dbh->dump['first_name'] . ' ' . $dbh->dump['middle_name'] . ' ' . $dbh->dump['last_name'];

// get iprf_details
$dbh = cobalt_load_class('iprf_staffrequest');
$dbh->set_table('iprf_staffrequest a LEFT JOIN position b ON a.position_id = b.position_id');
$dbh->set_fields('title, PRF_No');
$dbh->set_where('iprf_staffrequest_id = ?');
$dbh->stmt_bind_param($iprf_staffrequest_id);
$dbh->stmt_prepare();
$dbh->stmt_fetch('single');

extract($dbh->dump);

// get email address of recruiters from system settings
// $recepient_name = "";
// $email          = 'lauro.martinez@sminvestments.com';

$dbh_system_settings = cobalt_load_class('system_settings');
$recruiter_emails = $dbh_system_settings->get('Recruiter Emails', FALSE)->dump['value'];
$email_subject  = "Application for $title";
$email_body     = "You have received an application for $title from $applicant_name.
                   <br><br>
                   Login to <i><a href='" . DEFAULT_DB_HOST . "/smic_recruitment/applicant_portal.php'> SMIC Careers Portal</a></i> to review the applicant information.";

// debug($email_subject);
// debug($email_body);
require_once 'components/emailer.php';
