
<style>
fieldset.middle {
    height:60%;
    overflow-y:scroll;
}
</style>
<?php
require_once 'html_class.php';
$modal = new html;

echo "<div id='locking_overlay' class='throbber_screen_locker' style='min-width:50%'>";
    echo "<div class = 'container' style='width:50%;margin:auto'>";
        echo "<fieldset style='min-width:500px;display: inline-block;border:0;background-color:RGBA(255,255,255,.1);text-align:justify;line-height:2;'>";

            $modal->draw_fieldset_header('Consent Form for Online Applicants');
            $modal->draw_fieldset_body_start();
                //body content starts here
?>
    <p>By clicking <strong>AGREE</strong>, I hereby grant my express, unconditional, voluntary and informed consent to, and herby authorize SM Investment Corporation and other internal parties involved in processing the information so disclosed, to collect, store, access, use, verify, process and/or dispose my information necessary for the purpose of and in the course of my application, or termination or cessation of such application, hereinafter collectively referred to as "Purposes," including but not limited to carrying out data analytics, management, profiling, manual or automated decision-making, and any activity in the furtherance of the Purposes.</p>
    <br>
    <p>I knowingly acknowledge and confirm that I have been informed of my rights under the law with respect to my information, and that I am solely responsible for the consequences of such disclosure, transfer and sharing of such, including all requirements, records and documents, which shall be submitted to SM Investment Corporation for similar Purposes and of the same nature and which are generally made known or disclosed by me or any other third party through social media and other non-private means. Thus, I hereby hold the Company free and harmless from suits in connection with or arising from privacy breach of my personal data.</p>
    <br>
    <p>Finally, I hereby voluntarily confirm the due execution, validity and effectivity of this consent clause, which I have executed out of my own volition and free will, for as long as necessary within the period allowed by the applicable laws and regulations.</p>


<?php

                //body content ends here
            $modal->draw_fieldset_body_end();
            $modal->draw_fieldset_footer_start();
            $modal->draw_button($type='special', $button_class='btn_submit', $button_name='btn_accept', $button_label='AGREE', $draw_table_tags=FALSE, $colspan="2", $extra='');
            $modal->draw_button($type='special', $button_class='btn_back', $button_name='btn_cancel', $button_label='CANCEL', $draw_table_tags=FALSE, $colspan="2", $extra='');
        echo "</fieldset>";
    echo "</div>";
echo "</div>";


?>
