<?php

if(check_link('Recruiter access'))
{
    // viewing of resume
    echo "<a href='/".BASE_DIRECTORY."/modules/applicant/applicant/printable_applicant.php?$pkey_string' target='_blank'>[View Resume]&nbsp</a>";

    //sending of email
    $temp_data = explode(' - ',$updates);

    if($temp_data[0] == 'Invitation Sent')
    {
        $action_type = 'Resend Interview Invitation';
        $additional_url_param = '&resend=yes';
    }
    else
    {
        $action_type = 'Send Interview Invitation';
        $additional_url_param = '';

    }
    echo "<a href='/".BASE_DIRECTORY."/applicant_portal_modules/send_email_to_applicant.php?$pkey_string$additional_url_param'>[$action_type]</a>";
    // echo "<a href='/".BASE_DIRECTORY."/modules/applicant/applicant/send_email_invitation.php?$pkey_string'>[Send Interview Invitation]</a>";
    echo "&nbsp;<a href='/".BASE_DIRECTORY."/applicant_portal_modules/export_to_ihris.php?$pkey_string'>[Export to iHRIS]</a>";

}
