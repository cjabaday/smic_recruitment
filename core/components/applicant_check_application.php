<?php

class applicant_check_application
{
    var $staffrequest_id = 0;
    var $session_applicant_id = 0;


    function __construct()
    {

    }

    function inherit($param)
    {
        extract($param);
        $this->staffrequest_id = $staffrequest_id;
        $this->session_applicant_id = $session_applicant_id;

        return $this;
    }

    function go()
    {
        //fetch applicants of staffrequest
        $d = cobalt_load_class('iprf_staffrequest_applicants');
        $count = $d->check_application_in_staffrequest($this->session_applicant_id, $this->staffrequest_id)->num_rows;

        if($count > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}
