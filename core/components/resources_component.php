<?php
$directory = '../../../resources/citizenship.txt';
$file = fopen($directory,'r');
$value = fread($file,filesize($directory));
fclose($file);
$arr_citizenship = explode(",",$value);

$directory_religion = '../../../resources/religion.txt';
$file_religion = fopen($directory_religion,'r');
$value = fread($file_religion,filesize($directory_religion));
fclose($file_religion);

$arr_first_explode = explode("\r\n",$value);

$arr_religion = array();

// debug($arr_first_explode);
$a = 0;
foreach($arr_first_explode as $val)
{
    if(strchr($val,","))
    {
        $data_temp = explode(", ",$val);

        foreach($data_temp as $record)
        {
            $arr_religion[] = trim($record,' ');
        }
    }
    else
    {
        $arr_religion[] = trim($val,' ');
    }

}



$arr_religion = array_unique($arr_religion);
array_pop($arr_religion);
$arr_religion = array_values($arr_religion);

// debug($arr_religion);
$html->fields['citizenship']['control_type'] = "drop-down list";
$html->fields['citizenship']['list_type'] = "predefined";
$html->fields['citizenship']['list_settings']['per_line'] = TRUE;
$html->fields['citizenship']['list_settings']['items'] = $arr_citizenship;
$html->fields['citizenship']['list_settings']['values'] = $arr_citizenship;


$html->fields['religion']['control_type'] = "drop-down list";
$html->fields['religion']['list_type'] = "predefined";
$html->fields['religion']['list_settings']['per_line'] = TRUE;
$html->fields['religion']['list_settings']['items'] = $arr_religion;
$html->fields['religion']['list_settings']['values'] = $arr_religion;
