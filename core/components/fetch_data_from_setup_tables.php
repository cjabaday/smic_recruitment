<?php
// expects array that contains the field as key and value as the table name

foreach($arr_set_up_table as $key => $value)
{
    $field_generic_name = substr($value,1);
    // debug($field_generic_name);
    $str_to_lower = strtolower($value);
    $d = cobalt_load_class($str_to_lower);
    $d->set_where("{$field_generic_name}ID = ?");
    $d->stmt_bind_param($$key);
    $d->stmt_prepare();
    $$key = $d->stmt_fetch('single')->dump["{$field_generic_name}Desc"];

}
