<?php
// expects field name, table_name and field value

function fetch_desc_title($table_name, $value)
{
    $field_generic_name = substr($table_name,1);
    // debug($field_generic_name);
    $d = cobalt_load_class($table_name);
    $d->set_where("{$field_generic_name}ID = ?");
    $d->stmt_bind_param($value);
    $d->stmt_prepare();
    return $d->stmt_fetch('single')->dump["{$field_generic_name}Desc"];
}


?>
