<?php

function format_date($year,$month = "",$day = "",$format='F d, Y')
{
    $temp_year = explode("-",$year);
    if(count($temp_year) == 3)
    {
        $year = $temp_year[0];
        $month = $temp_year[1];
        $day = $temp_year[2];
    }

    $combine_date = "$year-$month-$day";

    $formatted_date = date($format,strtotime($combine_date));

    return $formatted_date;
}

function format_time($time,$format = 'h:i A')
{
    $formatted_time = date($format,strtotime($time));

    return $formatted_time;
}
