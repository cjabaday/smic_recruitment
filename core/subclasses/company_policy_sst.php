<?php
require_once 'sst_class.php';
require_once 'company_policy_dd.php';
class company_policy_sst extends sst
{
    function __construct()
    {
        $this->fields        = company_policy_dd::load_dictionary();
        $this->relations     = company_policy_dd::load_relationships();
        $this->subclasses    = company_policy_dd::load_subclass_info();
        $this->table_name    = company_policy_dd::$table_name;
        $this->readable_name = company_policy_dd::$readable_name;
        parent::__construct();
    }
}
