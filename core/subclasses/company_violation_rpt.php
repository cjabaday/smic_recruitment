<?php
require_once 'company_violation_dd.php';
class company_violation_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'COMPANY_VIOLATION_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'company_violation_html';
    var $data_subclass = 'company_violation';
    var $result_page = 'reporter_result_company_violation.php';
    var $cancel_page = 'listview_company_violation.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_company_violation.php';

    function __construct()
    {
        $this->fields        = company_violation_dd::load_dictionary();
        $this->relations     = company_violation_dd::load_relationships();
        $this->subclasses    = company_violation_dd::load_subclass_info();
        $this->table_name    = company_violation_dd::$table_name;
        $this->tables        = company_violation_dd::$table_name;
        $this->readable_name = company_violation_dd::$readable_name;
        $this->get_report_fields();
    }
}
