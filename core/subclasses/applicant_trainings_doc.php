<?php
require_once 'documentation_class.php';
require_once 'applicant_trainings_dd.php';
class applicant_trainings_doc extends documentation
{
    function __construct()
    {
        $this->fields        = applicant_trainings_dd::load_dictionary();
        $this->relations     = applicant_trainings_dd::load_relationships();
        $this->subclasses    = applicant_trainings_dd::load_subclass_info();
        $this->table_name    = applicant_trainings_dd::$table_name;
        $this->readable_name = applicant_trainings_dd::$readable_name;
        parent::__construct();
    }
}
