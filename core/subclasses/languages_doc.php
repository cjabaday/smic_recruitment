<?php
require_once 'documentation_class.php';
require_once 'languages_dd.php';
class languages_doc extends documentation
{
    function __construct()
    {
        $this->fields        = languages_dd::load_dictionary();
        $this->relations     = languages_dd::load_relationships();
        $this->subclasses    = languages_dd::load_subclass_info();
        $this->table_name    = languages_dd::$table_name;
        $this->readable_name = languages_dd::$readable_name;
        parent::__construct();
    }
}
