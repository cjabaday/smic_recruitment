<?php
require_once 'applicant_skills_dd.php';
class applicant_skills extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = applicant_skills_dd::load_dictionary();
        $this->relations  = applicant_skills_dd::load_relationships();
        $this->subclasses = applicant_skills_dd::load_subclass_info();
        $this->table_name = applicant_skills_dd::$table_name;
        $this->tables     = applicant_skills_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('applicant_id, skill, proficiency, years_of_experience');
            $this->set_values("?,?,?,?");

            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['skill']);
            $this->stmt_bind_param($param['proficiency']);
            $this->stmt_bind_param($param['years_of_experience']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("applicant_id = ?, skill = ?, proficiency = ?, years_of_experience = ?");
            $this->set_where("applicant_skill_id = ?");

            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['skill']);
            $this->stmt_bind_param($param['proficiency']);
            $this->stmt_bind_param($param['years_of_experience']);
            $this->stmt_bind_param($param['applicant_skill_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("applicant_skill_id = ?");

            $this->stmt_bind_param($param['applicant_skill_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");


            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("applicant_skill_id = ?");

        $this->stmt_bind_param($param['applicant_skill_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("applicant_skill_id = ? AND (applicant_skill_id != ?)");

        $this->stmt_bind_param($param['applicant_skill_id']);
        $this->stmt_bind_param($param['applicant_skill_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function select_record($applicant_id)
    {
        $this->set_where('applicant_id = ?');
        $this->stmt_bind_param($applicant_id);
        $this->stmt_fetch('rowdump');

        return $this;
    }
}
