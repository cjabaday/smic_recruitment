<?php
require_once 'company_dd.php';
class company extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = company_dd::load_dictionary();
        $this->relations  = company_dd::load_relationships();
        $this->subclasses = company_dd::load_subclass_info();
        $this->table_name = company_dd::$table_name;
        $this->tables     = company_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('official_name, short_name, head, assistant');
            $this->set_values("?,?,?,?");

            $this->stmt_bind_param($param['official_name']);
            $this->stmt_bind_param($param['short_name']);
            $this->stmt_bind_param($param['head']);
            $this->stmt_bind_param($param['assistant']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("official_name = ?, short_name = ?, head = ?, assistant = ?");
            $this->set_where("company_id = ?");

            $this->stmt_bind_param($param['official_name']);
            $this->stmt_bind_param($param['short_name']);
            $this->stmt_bind_param($param['head']);
            $this->stmt_bind_param($param['assistant']);
            $this->stmt_bind_param($param['company_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("company_id = ?");

            $this->stmt_bind_param($param['company_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");


            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("company_id = ?");

        $this->stmt_bind_param($param['company_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("company_id = ? AND (company_id != ?)");

        $this->stmt_bind_param($param['company_id']);
        $this->stmt_bind_param($param['company_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_label($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("official_name = ?");

        $this->stmt_bind_param($param['official_name']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function add_company(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('company_id, official_name');
            $this->set_values("?,?");

            $this->stmt_bind_param($param['company_id']);
            $this->stmt_bind_param($param['official_name']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }
}
