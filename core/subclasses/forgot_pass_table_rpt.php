<?php
require_once 'forgot_pass_table_dd.php';
class forgot_pass_table_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'FORGOT_PASS_TABLE_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'forgot_pass_table_html';
    var $data_subclass = 'forgot_pass_table';
    var $result_page = 'reporter_result_forgot_pass_table.php';
    var $cancel_page = 'listview_forgot_pass_table.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_forgot_pass_table.php';

    function __construct()
    {
        $this->fields        = forgot_pass_table_dd::load_dictionary();
        $this->relations     = forgot_pass_table_dd::load_relationships();
        $this->subclasses    = forgot_pass_table_dd::load_subclass_info();
        $this->table_name    = forgot_pass_table_dd::$table_name;
        $this->tables        = forgot_pass_table_dd::$table_name;
        $this->readable_name = forgot_pass_table_dd::$readable_name;
        $this->get_report_fields();
    }
}
