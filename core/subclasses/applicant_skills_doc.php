<?php
require_once 'documentation_class.php';
require_once 'applicant_skills_dd.php';
class applicant_skills_doc extends documentation
{
    function __construct()
    {
        $this->fields        = applicant_skills_dd::load_dictionary();
        $this->relations     = applicant_skills_dd::load_relationships();
        $this->subclasses    = applicant_skills_dd::load_subclass_info();
        $this->table_name    = applicant_skills_dd::$table_name;
        $this->readable_name = applicant_skills_dd::$readable_name;
        parent::__construct();
    }
}
