<?php
require_once 'system_log_dd.php';
class system_log extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = system_log_dd::load_dictionary();
        $this->relations  = system_log_dd::load_relationships();
        $this->subclasses = system_log_dd::load_subclass_info();
        $this->table_name = system_log_dd::$table_name;
        $this->tables     = system_log_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('ip_address, user, datetime, action, module');
            $this->set_values("?,?,?,?,?");
            
            $this->stmt_bind_param($param['ip_address']);
            $this->stmt_bind_param($param['user']);
            $this->stmt_bind_param($param['datetime']);
            $this->stmt_bind_param($param['action']);
            $this->stmt_bind_param($param['module']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("ip_address = ?, user = ?, datetime = ?, action = ?, module = ?");
            $this->set_where("entry_id = ?");
            
            $this->stmt_bind_param($param['ip_address']);
            $this->stmt_bind_param($param['user']);
            $this->stmt_bind_param($param['datetime']);
            $this->stmt_bind_param($param['action']);
            $this->stmt_bind_param($param['module']);
            $this->stmt_bind_param($param['entry_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("entry_id = ?");
            
            $this->stmt_bind_param($param['entry_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("entry_id = ?");
        
        $this->stmt_bind_param($param['entry_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("entry_id = ? AND (entry_id != ?)");
        
        $this->stmt_bind_param($param['entry_id']);
        $this->stmt_bind_param($param['entry_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
