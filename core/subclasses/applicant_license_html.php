<?php
require_once 'applicant_license_dd.php';
class applicant_license_html extends html
{
    function __construct()
    {
        $this->fields        = applicant_license_dd::load_dictionary();
        $this->relations     = applicant_license_dd::load_relationships();
        $this->subclasses    = applicant_license_dd::load_subclass_info();
        $this->table_name    = applicant_license_dd::$table_name;
        $this->readable_name = applicant_license_dd::$readable_name;
    }
}
