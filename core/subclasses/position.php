<?php
require_once 'position_dd.php';
class position extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = position_dd::load_dictionary();
        $this->relations  = position_dd::load_relationships();
        $this->subclasses = position_dd::load_subclass_info();
        $this->table_name = position_dd::$table_name;
        $this->tables     = position_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('title, description');
            $this->set_values("?,?");

            $this->stmt_bind_param($param['title']);
            $this->stmt_bind_param($param['description']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("title = ?, description = ?");
            $this->set_where("position_id = ?");

            $this->stmt_bind_param($param['title']);
            $this->stmt_bind_param($param['description']);
            $this->stmt_bind_param($param['position_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("position_id = ?");

            $this->stmt_bind_param($param['position_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");


            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("position_id = ?");

        $this->stmt_bind_param($param['position_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("position_id = ? AND (position_id != ?)");

        $this->stmt_bind_param($param['position_id']);
        $this->stmt_bind_param($param['position_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_label($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("title = ?");

        $this->stmt_bind_param($param['title']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function add_position(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('position_id, title');
            $this->set_values("?,?");
            $this->stmt_bind_param($param['position_id']);
            $this->stmt_bind_param($param['title']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }
}
