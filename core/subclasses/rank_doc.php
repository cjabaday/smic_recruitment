<?php
require_once 'documentation_class.php';
require_once 'rank_dd.php';
class rank_doc extends documentation
{
    function __construct()
    {
        $this->fields        = rank_dd::load_dictionary();
        $this->relations     = rank_dd::load_relationships();
        $this->subclasses    = rank_dd::load_subclass_info();
        $this->table_name    = rank_dd::$table_name;
        $this->readable_name = rank_dd::$readable_name;
        parent::__construct();
    }
}
