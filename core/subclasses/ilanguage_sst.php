<?php
require_once 'sst_class.php';
require_once 'ilanguage_dd.php';
class ilanguage_sst extends sst
{
    function __construct()
    {
        $this->fields        = ilanguage_dd::load_dictionary();
        $this->relations     = ilanguage_dd::load_relationships();
        $this->subclasses    = ilanguage_dd::load_subclass_info();
        $this->table_name    = ilanguage_dd::$table_name;
        $this->readable_name = ilanguage_dd::$readable_name;
        parent::__construct();
    }
}
