<?php
require_once 'icourse_dd.php';
class icourse_html extends html
{
    function __construct()
    {
        $this->fields        = icourse_dd::load_dictionary();
        $this->relations     = icourse_dd::load_relationships();
        $this->subclasses    = icourse_dd::load_subclass_info();
        $this->table_name    = icourse_dd::$table_name;
        $this->readable_name = icourse_dd::$readable_name;
    }
}
