<?php
require_once 'documentation_class.php';
require_once 'icivilstatus_dd.php';
class icivilstatus_doc extends documentation
{
    function __construct()
    {
        $this->fields        = icivilstatus_dd::load_dictionary();
        $this->relations     = icivilstatus_dd::load_relationships();
        $this->subclasses    = icivilstatus_dd::load_subclass_info();
        $this->table_name    = icivilstatus_dd::$table_name;
        $this->readable_name = icivilstatus_dd::$readable_name;
        parent::__construct();
    }
}
