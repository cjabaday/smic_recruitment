<?php
require_once 'school_dd.php';
class school extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = school_dd::load_dictionary();
        $this->relations  = school_dd::load_relationships();
        $this->subclasses = school_dd::load_subclass_info();
        $this->table_name = school_dd::$table_name;
        $this->tables     = school_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('name, description');
            $this->set_values("?,?");
            
            $this->stmt_bind_param($param['name']);
            $this->stmt_bind_param($param['description']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("name = ?, description = ?");
            $this->set_where("school_id = ?");
            
            $this->stmt_bind_param($param['name']);
            $this->stmt_bind_param($param['description']);
            $this->stmt_bind_param($param['school_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("school_id = ?");
            
            $this->stmt_bind_param($param['school_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("school_id = ?");
        
        $this->stmt_bind_param($param['school_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("school_id = ? AND (school_id != ?)");
        
        $this->stmt_bind_param($param['school_id']);
        $this->stmt_bind_param($param['school_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
