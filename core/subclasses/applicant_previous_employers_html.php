<?php
require_once 'applicant_previous_employers_dd.php';
class applicant_previous_employers_html extends html
{
    function __construct()
    {
        $this->fields        = applicant_previous_employers_dd::load_dictionary();
        $this->relations     = applicant_previous_employers_dd::load_relationships();
        $this->subclasses    = applicant_previous_employers_dd::load_subclass_info();
        $this->table_name    = applicant_previous_employers_dd::$table_name;
        $this->readable_name = applicant_previous_employers_dd::$readable_name;
    }
}
