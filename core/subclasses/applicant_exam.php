<?php
require_once 'applicant_exam_dd.php';
class applicant_exam extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = applicant_exam_dd::load_dictionary();
        $this->relations  = applicant_exam_dd::load_relationships();
        $this->subclasses = applicant_exam_dd::load_subclass_info();
        $this->table_name = applicant_exam_dd::$table_name;
        $this->tables     = applicant_exam_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('applicant_id, name, date, score, remarks');
            $this->set_values("?,?,?,?,?");
            
            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['name']);
            $this->stmt_bind_param($param['date']);
            $this->stmt_bind_param($param['score']);
            $this->stmt_bind_param($param['remarks']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("applicant_id = ?, name = ?, date = ?, score = ?, remarks = ?");
            $this->set_where("applicant_exam_id = ?");
            
            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['name']);
            $this->stmt_bind_param($param['date']);
            $this->stmt_bind_param($param['score']);
            $this->stmt_bind_param($param['remarks']);
            $this->stmt_bind_param($param['applicant_exam_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("applicant_exam_id = ?");
            
            $this->stmt_bind_param($param['applicant_exam_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("applicant_exam_id = ?");
        
        $this->stmt_bind_param($param['applicant_exam_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("applicant_exam_id = ? AND (applicant_exam_id != ?)");
        
        $this->stmt_bind_param($param['applicant_exam_id']);
        $this->stmt_bind_param($param['applicant_exam_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
