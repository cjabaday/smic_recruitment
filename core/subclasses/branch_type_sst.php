<?php
require_once 'sst_class.php';
require_once 'branch_type_dd.php';
class branch_type_sst extends sst
{
    function __construct()
    {
        $this->fields        = branch_type_dd::load_dictionary();
        $this->relations     = branch_type_dd::load_relationships();
        $this->subclasses    = branch_type_dd::load_subclass_info();
        $this->table_name    = branch_type_dd::$table_name;
        $this->readable_name = branch_type_dd::$readable_name;
        parent::__construct();
    }
}
