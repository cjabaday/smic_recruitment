<?php
require_once 'applicant_previous_employers_dd.php';
class applicant_previous_employers extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = applicant_previous_employers_dd::load_dictionary();
        $this->relations  = applicant_previous_employers_dd::load_relationships();
        $this->subclasses = applicant_previous_employers_dd::load_subclass_info();
        $this->table_name = applicant_previous_employers_dd::$table_name;
        $this->tables     = applicant_previous_employers_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('applicant_id, previous_employer_name, address, job_description, basic_salary, reason_for_leaving, previous_employer_position, previous_employer_date_from, previous_employer_date_to');
            $this->set_values("?,?,?,?,?,?,?,?,?");

            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['previous_employer_name']);
            $this->stmt_bind_param($param['address']);
            $this->stmt_bind_param($param['job_description']);
            $this->stmt_bind_param($param['basic_salary']);
            $this->stmt_bind_param($param['reason_for_leaving']);
            $this->stmt_bind_param($param['previous_employer_position']);
            $this->stmt_bind_param($param['previous_employer_date_from']);
            $this->stmt_bind_param($param['previous_employer_date_to']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("applicant_id = ?, previous_employer_name = ?, address = ?, job_description = ?, basic_salary = ?, reason_for_leaving = ?, previous_employer_position = ?, previous_employer_date_from = ?, previous_employer_date_to = ?");
            $this->set_where("applicant_previous_employer_id = ?");

            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['previous_employer_name']);
            $this->stmt_bind_param($param['address']);
            $this->stmt_bind_param($param['job_description']);
            $this->stmt_bind_param($param['basic_salary']);
            $this->stmt_bind_param($param['reason_for_leaving']);
            $this->stmt_bind_param($param['previous_employer_position']);
            $this->stmt_bind_param($param['previous_employer_date_from']);
            $this->stmt_bind_param($param['previous_employer_date_to']);
            $this->stmt_bind_param($param['applicant_previous_employer_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("applicant_previous_employer_id = ?");

            $this->stmt_bind_param($param['applicant_previous_employer_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("applicant_id = ?");

            $this->stmt_bind_param($param['applicant_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("applicant_previous_employer_id = ?");

        $this->stmt_bind_param($param['applicant_previous_employer_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("applicant_previous_employer_id = ? AND (applicant_previous_employer_id != ?)");

        $this->stmt_bind_param($param['applicant_previous_employer_id']);
        $this->stmt_bind_param($param['applicant_previous_employer_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
