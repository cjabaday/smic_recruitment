<?php
require_once 'other_declarations_name_smgroup_dd.php';
class other_declarations_name_smgroup_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'OTHER_DECLARATIONS_NAME_SMGROUP_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'other_declarations_name_smgroup_html';
    var $data_subclass = 'other_declarations_name_smgroup';
    var $result_page = 'reporter_result_other_declarations_name_smgroup.php';
    var $cancel_page = 'listview_other_declarations_name_smgroup.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_other_declarations_name_smgroup.php';

    function __construct()
    {
        $this->fields        = other_declarations_name_smgroup_dd::load_dictionary();
        $this->relations     = other_declarations_name_smgroup_dd::load_relationships();
        $this->subclasses    = other_declarations_name_smgroup_dd::load_subclass_info();
        $this->table_name    = other_declarations_name_smgroup_dd::$table_name;
        $this->tables        = other_declarations_name_smgroup_dd::$table_name;
        $this->readable_name = other_declarations_name_smgroup_dd::$readable_name;
        $this->get_report_fields();
    }
}
