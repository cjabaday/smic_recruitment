<?php
require_once 'applicant_attachments_dd.php';
class applicant_attachments_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'APPLICANT_ATTACHMENTS_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'applicant_attachments_html';
    var $data_subclass = 'applicant_attachments';
    var $result_page = 'reporter_result_applicant_attachments.php';
    var $cancel_page = 'listview_applicant_attachments.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_applicant_attachments.php';

    function __construct()
    {
        $this->fields        = applicant_attachments_dd::load_dictionary();
        $this->relations     = applicant_attachments_dd::load_relationships();
        $this->subclasses    = applicant_attachments_dd::load_subclass_info();
        $this->table_name    = applicant_attachments_dd::$table_name;
        $this->tables        = applicant_attachments_dd::$table_name;
        $this->readable_name = applicant_attachments_dd::$readable_name;
        $this->get_report_fields();
    }
}
