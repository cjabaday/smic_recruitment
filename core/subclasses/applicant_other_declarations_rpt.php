<?php
require_once 'applicant_other_declarations_dd.php';
class applicant_other_declarations_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'APPLICANT_OTHER_DECLARATIONS_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'applicant_other_declarations_html';
    var $data_subclass = 'applicant_other_declarations';
    var $result_page = 'reporter_result_applicant_other_declarations.php';
    var $cancel_page = 'listview_applicant_other_declarations.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_applicant_other_declarations.php';

    function __construct()
    {
        $this->fields        = applicant_other_declarations_dd::load_dictionary();
        $this->relations     = applicant_other_declarations_dd::load_relationships();
        $this->subclasses    = applicant_other_declarations_dd::load_subclass_info();
        $this->table_name    = applicant_other_declarations_dd::$table_name;
        $this->tables        = applicant_other_declarations_dd::$table_name;
        $this->readable_name = applicant_other_declarations_dd::$readable_name;
        $this->get_report_fields();
    }
}
