<?php
require_once 'applicant_family_members_dd.php';
class applicant_family_members_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'APPLICANT_FAMILY_MEMBERS_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'applicant_family_members_html';
    var $data_subclass = 'applicant_family_members';
    var $result_page = 'reporter_result_applicant_family_members.php';
    var $cancel_page = 'listview_applicant_family_members.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_applicant_family_members.php';

    function __construct()
    {
        $this->fields        = applicant_family_members_dd::load_dictionary();
        $this->relations     = applicant_family_members_dd::load_relationships();
        $this->subclasses    = applicant_family_members_dd::load_subclass_info();
        $this->table_name    = applicant_family_members_dd::$table_name;
        $this->tables        = applicant_family_members_dd::$table_name;
        $this->readable_name = applicant_family_members_dd::$readable_name;
        $this->get_report_fields();
    }
}
