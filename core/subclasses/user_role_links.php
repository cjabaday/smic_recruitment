<?php
require_once 'user_role_links_dd.php';
class user_role_links extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = user_role_links_dd::load_dictionary();
        $this->relations  = user_role_links_dd::load_relationships();
        $this->subclasses = user_role_links_dd::load_subclass_info();
        $this->table_name = user_role_links_dd::$table_name;
        $this->tables     = user_role_links_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('role_id, link_id');
            $this->set_values("?,?");

            $this->stmt_bind_param($param['role_id']);
            $this->stmt_bind_param($param['link_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("role_id = ?, link_id = ?");
            $this->set_where("role_id = ? AND link_id = ?");

            $this->stmt_bind_param($param['role_id']);
            $this->stmt_bind_param($param['link_id']);
            $this->stmt_bind_param($param['orig_role_id']);
            $this->stmt_bind_param($param['orig_link_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("role_id = ? AND link_id = ?");

            $this->stmt_bind_param($param['role_id']);
            $this->stmt_bind_param($param['link_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("role_id = ? AND link_id = ?");

            $this->stmt_bind_param($param['role_id']);
            $this->stmt_bind_param($param['link_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("role_id = ? AND link_id = ?");

        $this->stmt_bind_param($param['role_id']);
        $this->stmt_bind_param($param['link_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("role_id = ? AND link_id = ? AND (role_id != ? OR link_id != ?)");

        $this->stmt_bind_param($param['role_id']);
        $this->stmt_bind_param($param['link_id']);
        $this->stmt_bind_param($param['orig_role_id']);
        $this->stmt_bind_param($param['orig_link_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function get_user_role_links($role_id)
    {
        $this->set_table("$this->tables a, user_links b");
        $this->set_fields('a.link_id, b.descriptive_title');
        $this->set_where("a.role_id = ? AND a.link_id=b.link_id");
        $this->set_order("b.descriptive_title");
        $this->stmt_bind_param($role_id);
        $this->stmt_prepare();
        $this->stmt_fetch();

        return $this;
    }
}
