<?php
require_once 'applicant_attachments_dd.php';
class applicant_attachments extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = applicant_attachments_dd::load_dictionary();
        $this->relations  = applicant_attachments_dd::load_relationships();
        $this->subclasses = applicant_attachments_dd::load_subclass_info();
        $this->table_name = applicant_attachments_dd::$table_name;
        $this->tables     = applicant_attachments_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('applicant_id, attachments, date_uploaded');
            $this->set_values("?,?,?");
            
            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['attachments']);
            $this->stmt_bind_param($param['date_uploaded']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("applicant_id = ?, attachments = ?, date_uploaded = ?");
            $this->set_where("applicant_attachment_id = ?");
            
            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['attachments']);
            $this->stmt_bind_param($param['date_uploaded']);
            $this->stmt_bind_param($param['applicant_attachment_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("applicant_attachment_id = ?");
            
            $this->stmt_bind_param($param['applicant_attachment_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("applicant_id = ?");
            
            $this->stmt_bind_param($param['applicant_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("applicant_attachment_id = ?");
        
        $this->stmt_bind_param($param['applicant_attachment_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("applicant_attachment_id = ? AND (applicant_attachment_id != ?)");
        
        $this->stmt_bind_param($param['applicant_attachment_id']);
        $this->stmt_bind_param($param['applicant_attachment_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
