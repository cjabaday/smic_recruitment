<?php
require_once 'clearance_dd.php';
class clearance_html extends html
{
    function __construct()
    {
        $this->fields        = clearance_dd::load_dictionary();
        $this->relations     = clearance_dd::load_relationships();
        $this->subclasses    = clearance_dd::load_subclass_info();
        $this->table_name    = clearance_dd::$table_name;
        $this->readable_name = clearance_dd::$readable_name;
    }
}
