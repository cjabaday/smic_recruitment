<?php
require_once 'rank_dd.php';
class rank_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'RANK_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'rank_html';
    var $data_subclass = 'rank';
    var $result_page = 'reporter_result_rank.php';
    var $cancel_page = 'listview_rank.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_rank.php';

    function __construct()
    {
        $this->fields        = rank_dd::load_dictionary();
        $this->relations     = rank_dd::load_relationships();
        $this->subclasses    = rank_dd::load_subclass_info();
        $this->table_name    = rank_dd::$table_name;
        $this->tables        = rank_dd::$table_name;
        $this->readable_name = rank_dd::$readable_name;
        $this->get_report_fields();
    }
}
