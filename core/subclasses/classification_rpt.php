<?php
require_once 'classification_dd.php';
class classification_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'CLASSIFICATION_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'classification_html';
    var $data_subclass = 'classification';
    var $result_page = 'reporter_result_classification.php';
    var $cancel_page = 'listview_classification.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_classification.php';

    function __construct()
    {
        $this->fields        = classification_dd::load_dictionary();
        $this->relations     = classification_dd::load_relationships();
        $this->subclasses    = classification_dd::load_subclass_info();
        $this->table_name    = classification_dd::$table_name;
        $this->tables        = classification_dd::$table_name;
        $this->readable_name = classification_dd::$readable_name;
        $this->get_report_fields();
    }
}
