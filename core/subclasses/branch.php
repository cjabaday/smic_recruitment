<?php
require_once 'branch_dd.php';
class branch extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = branch_dd::load_dictionary();
        $this->relations  = branch_dd::load_relationships();
        $this->subclasses = branch_dd::load_subclass_info();
        $this->table_name = branch_dd::$table_name;
        $this->tables     = branch_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('company_id, building_id, branch_type_id, name, description, head, assistant');
            $this->set_values("?,?,?,?,?,?,?");
            
            $this->stmt_bind_param($param['company_id']);
            $this->stmt_bind_param($param['building_id']);
            $this->stmt_bind_param($param['branch_type_id']);
            $this->stmt_bind_param($param['name']);
            $this->stmt_bind_param($param['description']);
            $this->stmt_bind_param($param['head']);
            $this->stmt_bind_param($param['assistant']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("company_id = ?, building_id = ?, branch_type_id = ?, name = ?, description = ?, head = ?, assistant = ?");
            $this->set_where("branch_id = ?");
            
            $this->stmt_bind_param($param['company_id']);
            $this->stmt_bind_param($param['building_id']);
            $this->stmt_bind_param($param['branch_type_id']);
            $this->stmt_bind_param($param['name']);
            $this->stmt_bind_param($param['description']);
            $this->stmt_bind_param($param['head']);
            $this->stmt_bind_param($param['assistant']);
            $this->stmt_bind_param($param['branch_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("branch_id = ?");
            
            $this->stmt_bind_param($param['branch_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("branch_id = ?");
        
        $this->stmt_bind_param($param['branch_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("branch_id = ? AND (branch_id != ?)");
        
        $this->stmt_bind_param($param['branch_id']);
        $this->stmt_bind_param($param['branch_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
