<?php
require_once 'upload_material_dd.php';
class upload_material_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'UPLOAD_MATERIAL_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'upload_material_html';
    var $data_subclass = 'upload_material';
    var $result_page = 'reporter_result_upload_material.php';
    var $cancel_page = 'listview_upload_material.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_upload_material.php';

    function __construct()
    {
        $this->fields        = upload_material_dd::load_dictionary();
        $this->relations     = upload_material_dd::load_relationships();
        $this->subclasses    = upload_material_dd::load_subclass_info();
        $this->table_name    = upload_material_dd::$table_name;
        $this->tables        = upload_material_dd::$table_name;
        $this->readable_name = upload_material_dd::$readable_name;
        $this->get_report_fields();
    }
}
