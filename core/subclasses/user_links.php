<?php
require_once 'user_links_dd.php';
class user_links extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = user_links_dd::load_dictionary();
        $this->relations  = user_links_dd::load_relationships();
        $this->subclasses = user_links_dd::load_subclass_info();
        $this->table_name = user_links_dd::$table_name;
        $this->tables     = user_links_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('name, target, descriptive_title, description, passport_group_id, show_in_tasklist, status, icon, priority');
            $this->set_values("?,?,?,?,?,?,?,?,?");
            
            $this->stmt_bind_param($param['name']);
            $this->stmt_bind_param($param['target']);
            $this->stmt_bind_param($param['descriptive_title']);
            $this->stmt_bind_param($param['description']);
            $this->stmt_bind_param($param['passport_group_id']);
            $this->stmt_bind_param($param['show_in_tasklist']);
            $this->stmt_bind_param($param['status']);
            $this->stmt_bind_param($param['icon']);
            $this->stmt_bind_param($param['priority']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ?");
            $this->set_where("link_id = ?");
            
            $this->stmt_bind_param($param['name']);
            $this->stmt_bind_param($param['target']);
            $this->stmt_bind_param($param['descriptive_title']);
            $this->stmt_bind_param($param['description']);
            $this->stmt_bind_param($param['passport_group_id']);
            $this->stmt_bind_param($param['show_in_tasklist']);
            $this->stmt_bind_param($param['status']);
            $this->stmt_bind_param($param['icon']);
            $this->stmt_bind_param($param['priority']);
            $this->stmt_bind_param($param['link_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("link_id = ?");
            
            $this->stmt_bind_param($param['link_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("link_id = ?");
        
        $this->stmt_bind_param($param['link_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("link_id = ? AND (link_id != ?)");
        
        $this->stmt_bind_param($param['link_id']);
        $this->stmt_bind_param($param['link_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
