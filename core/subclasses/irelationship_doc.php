<?php
require_once 'documentation_class.php';
require_once 'irelationship_dd.php';
class irelationship_doc extends documentation
{
    function __construct()
    {
        $this->fields        = irelationship_dd::load_dictionary();
        $this->relations     = irelationship_dd::load_relationships();
        $this->subclasses    = irelationship_dd::load_subclass_info();
        $this->table_name    = irelationship_dd::$table_name;
        $this->readable_name = irelationship_dd::$readable_name;
        parent::__construct();
    }
}
