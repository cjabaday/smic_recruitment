<?php
require_once 'igender_dd.php';
class igender extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = igender_dd::load_dictionary();
        $this->relations  = igender_dd::load_relationships();
        $this->subclasses = igender_dd::load_subclass_info();
        $this->table_name = igender_dd::$table_name;
        $this->tables     = igender_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('GenderID, GenderCode, GenderDesc, EncodeBy, EncodeDate');
            $this->set_values("?,?,?,?,?");
            
            $this->stmt_bind_param($param['GenderID']);
            $this->stmt_bind_param($param['GenderCode']);
            $this->stmt_bind_param($param['GenderDesc']);
            $this->stmt_bind_param($param['EncodeBy']);
            $this->stmt_bind_param($param['EncodeDate']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("GenderID = ?, GenderCode = ?, GenderDesc = ?, EncodeBy = ?, EncodeDate = ?");
            $this->set_where("GenderID = ?");
            
            $this->stmt_bind_param($param['GenderID']);
            $this->stmt_bind_param($param['GenderCode']);
            $this->stmt_bind_param($param['GenderDesc']);
            $this->stmt_bind_param($param['EncodeBy']);
            $this->stmt_bind_param($param['EncodeDate']);
            $this->stmt_bind_param($param['orig_GenderID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("GenderID = ?");
            
            $this->stmt_bind_param($param['GenderID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("GenderID = ?");
            
            $this->stmt_bind_param($param['GenderID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("GenderID = ?");
        
        $this->stmt_bind_param($param['GenderID']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("GenderID = ? AND (GenderID != ?)");
        
        $this->stmt_bind_param($param['GenderID']);
        $this->stmt_bind_param($param['orig_GenderID']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
