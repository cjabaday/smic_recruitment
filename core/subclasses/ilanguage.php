<?php
require_once 'ilanguage_dd.php';
class ilanguage extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = ilanguage_dd::load_dictionary();
        $this->relations  = ilanguage_dd::load_relationships();
        $this->subclasses = ilanguage_dd::load_subclass_info();
        $this->table_name = ilanguage_dd::$table_name;
        $this->tables     = ilanguage_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('LanguageID, LanguageCode, LanguageDesc, EncodeBy, EncodeDate');
            $this->set_values("?,?,?,?,?");
            
            $this->stmt_bind_param($param['LanguageID']);
            $this->stmt_bind_param($param['LanguageCode']);
            $this->stmt_bind_param($param['LanguageDesc']);
            $this->stmt_bind_param($param['EncodeBy']);
            $this->stmt_bind_param($param['EncodeDate']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("LanguageID = ?, LanguageCode = ?, LanguageDesc = ?, EncodeBy = ?, EncodeDate = ?");
            $this->set_where("LanguageID = ?");
            
            $this->stmt_bind_param($param['LanguageID']);
            $this->stmt_bind_param($param['LanguageCode']);
            $this->stmt_bind_param($param['LanguageDesc']);
            $this->stmt_bind_param($param['EncodeBy']);
            $this->stmt_bind_param($param['EncodeDate']);
            $this->stmt_bind_param($param['orig_LanguageID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("LanguageID = ?");
            
            $this->stmt_bind_param($param['LanguageID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("LanguageID = ?");
            
            $this->stmt_bind_param($param['LanguageID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("LanguageID = ?");
        
        $this->stmt_bind_param($param['LanguageID']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("LanguageID = ? AND (LanguageID != ?)");
        
        $this->stmt_bind_param($param['LanguageID']);
        $this->stmt_bind_param($param['orig_LanguageID']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
