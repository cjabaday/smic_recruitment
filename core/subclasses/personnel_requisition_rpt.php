<?php
require_once 'personnel_requisition_dd.php';
class personnel_requisition_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'PERSONNEL_REQUISITION_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'personnel_requisition_html';
    var $data_subclass = 'personnel_requisition';
    var $result_page = 'reporter_result_personnel_requisition.php';
    var $cancel_page = 'listview_personnel_requisition.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_personnel_requisition.php';

    function __construct()
    {
        $this->fields        = personnel_requisition_dd::load_dictionary();
        $this->relations     = personnel_requisition_dd::load_relationships();
        $this->subclasses    = personnel_requisition_dd::load_subclass_info();
        $this->table_name    = personnel_requisition_dd::$table_name;
        $this->tables        = personnel_requisition_dd::$table_name;
        $this->readable_name = personnel_requisition_dd::$readable_name;
        $this->get_report_fields();
    }
}
