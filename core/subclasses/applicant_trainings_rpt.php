<?php
require_once 'applicant_trainings_dd.php';
class applicant_trainings_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'APPLICANT_TRAININGS_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'applicant_trainings_html';
    var $data_subclass = 'applicant_trainings';
    var $result_page = 'reporter_result_applicant_trainings.php';
    var $cancel_page = 'listview_applicant_trainings.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_applicant_trainings.php';

    function __construct()
    {
        $this->fields        = applicant_trainings_dd::load_dictionary();
        $this->relations     = applicant_trainings_dd::load_relationships();
        $this->subclasses    = applicant_trainings_dd::load_subclass_info();
        $this->table_name    = applicant_trainings_dd::$table_name;
        $this->tables        = applicant_trainings_dd::$table_name;
        $this->readable_name = applicant_trainings_dd::$readable_name;
        $this->get_report_fields();
    }
}
