<?php
require_once 'calendar_event_type_dd.php';
class calendar_event_type_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'CALENDAR_EVENT_TYPE_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'calendar_event_type_html';
    var $data_subclass = 'calendar_event_type';
    var $result_page = 'reporter_result_calendar_event_type.php';
    var $cancel_page = 'listview_calendar_event_type.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_calendar_event_type.php';

    function __construct()
    {
        $this->fields        = calendar_event_type_dd::load_dictionary();
        $this->relations     = calendar_event_type_dd::load_relationships();
        $this->subclasses    = calendar_event_type_dd::load_subclass_info();
        $this->table_name    = calendar_event_type_dd::$table_name;
        $this->tables        = calendar_event_type_dd::$table_name;
        $this->readable_name = calendar_event_type_dd::$readable_name;
        $this->get_report_fields();
    }
}
