<?php
require_once 'branch_type_dd.php';
class branch_type extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = branch_type_dd::load_dictionary();
        $this->relations  = branch_type_dd::load_relationships();
        $this->subclasses = branch_type_dd::load_subclass_info();
        $this->table_name = branch_type_dd::$table_name;
        $this->tables     = branch_type_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('title, description');
            $this->set_values("?,?");
            
            $this->stmt_bind_param($param['title']);
            $this->stmt_bind_param($param['description']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("title = ?, description = ?");
            $this->set_where("branch_type_id = ?");
            
            $this->stmt_bind_param($param['title']);
            $this->stmt_bind_param($param['description']);
            $this->stmt_bind_param($param['branch_type_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("branch_type_id = ?");
            
            $this->stmt_bind_param($param['branch_type_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("branch_type_id = ?");
        
        $this->stmt_bind_param($param['branch_type_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("branch_type_id = ? AND (branch_type_id != ?)");
        
        $this->stmt_bind_param($param['branch_type_id']);
        $this->stmt_bind_param($param['branch_type_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
