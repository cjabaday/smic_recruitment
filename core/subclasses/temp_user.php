<?php
require_once 'temp_user_dd.php';
class temp_user extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = temp_user_dd::load_dictionary();
        $this->relations  = temp_user_dd::load_relationships();
        $this->subclasses = temp_user_dd::load_subclass_info();
        $this->table_name = temp_user_dd::$table_name;
        $this->tables     = temp_user_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('personal_email, password, salt, iteration, method, token, first_name, middle_name, last_name, gender');
            $this->set_values("?,?,?,?,?,?,?,?,?,?");


            $this->stmt_bind_param($param['personal_email']);
            $this->stmt_bind_param($param['password']);
            $this->stmt_bind_param($param['salt']);
            $this->stmt_bind_param($param['iteration']);
            $this->stmt_bind_param($param['method']);
            $this->stmt_bind_param($param['token']);
            $this->stmt_bind_param($param['first_name']);
            $this->stmt_bind_param($param['middle_name']);
            $this->stmt_bind_param($param['last_name']);
            $this->stmt_bind_param($param['gender']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("username = ?, personal_email = ?, password = ?, salt = ?, iteration = ?, method = ?, token = ?, first_name = ?, middle_name = ?, last_name = ?, gender = ?");
            $this->set_where("username = ?");

            $this->stmt_bind_param($param['username']);
            $this->stmt_bind_param($param['personal_email']);
            $this->stmt_bind_param($param['password']);
            $this->stmt_bind_param($param['salt']);
            $this->stmt_bind_param($param['iteration']);
            $this->stmt_bind_param($param['method']);
            $this->stmt_bind_param($param['token']);
            $this->stmt_bind_param($param['first_name']);
            $this->stmt_bind_param($param['middle_name']);
            $this->stmt_bind_param($param['last_name']);
            $this->stmt_bind_param($param['gender']);
            $this->stmt_bind_param($param['orig_username']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete($id)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("temp_user_id = ?");

            $this->stmt_bind_param($id);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("username = ?");

            $this->stmt_bind_param($param['username']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("username = ?");

        $this->stmt_bind_param($param['username']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("username = ? AND (username != ?)");

        $this->stmt_bind_param($param['username']);
        $this->stmt_bind_param($param['orig_username']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
