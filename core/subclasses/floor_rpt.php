<?php
require_once 'floor_dd.php';
class floor_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'FLOOR_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'floor_html';
    var $data_subclass = 'floor';
    var $result_page = 'reporter_result_floor.php';
    var $cancel_page = 'listview_floor.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_floor.php';

    function __construct()
    {
        $this->fields        = floor_dd::load_dictionary();
        $this->relations     = floor_dd::load_relationships();
        $this->subclasses    = floor_dd::load_subclass_info();
        $this->table_name    = floor_dd::$table_name;
        $this->tables        = floor_dd::$table_name;
        $this->readable_name = floor_dd::$readable_name;
        $this->get_report_fields();
    }
}
