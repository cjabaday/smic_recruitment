<?php
require_once 'iprf_staffrequest_dd.php';
class iprf_staffrequest_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'IPRF_STAFFREQUEST_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'iprf_staffrequest_html';
    var $data_subclass = 'iprf_staffrequest';
    var $result_page = 'reporter_result_iprf_staffrequest.php';
    var $cancel_page = 'listview_iprf_staffrequest.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_iprf_staffrequest.php';

    function __construct()
    {
        $this->fields        = iprf_staffrequest_dd::load_dictionary();
        $this->relations     = iprf_staffrequest_dd::load_relationships();
        $this->subclasses    = iprf_staffrequest_dd::load_subclass_info();
        $this->table_name    = iprf_staffrequest_dd::$table_name;
        $this->tables        = iprf_staffrequest_dd::$table_name;
        $this->readable_name = iprf_staffrequest_dd::$readable_name;
        $this->get_report_fields();
    }
}
