<?php
require_once 'hr_program_attendees_dd.php';
class hr_program_attendees_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'HR_PROGRAM_ATTENDEES_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'hr_program_attendees_html';
    var $data_subclass = 'hr_program_attendees';
    var $result_page = 'reporter_result_hr_program_attendees.php';
    var $cancel_page = 'listview_hr_program_attendees.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_hr_program_attendees.php';

    function __construct()
    {
        $this->fields        = hr_program_attendees_dd::load_dictionary();
        $this->relations     = hr_program_attendees_dd::load_relationships();
        $this->subclasses    = hr_program_attendees_dd::load_subclass_info();
        $this->table_name    = hr_program_attendees_dd::$table_name;
        $this->tables        = hr_program_attendees_dd::$table_name;
        $this->readable_name = hr_program_attendees_dd::$readable_name;
        $this->get_report_fields();
    }
}
