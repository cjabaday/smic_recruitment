<?php
require_once 'languages_dd.php';
class languages extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = languages_dd::load_dictionary();
        $this->relations  = languages_dd::load_relationships();
        $this->subclasses = languages_dd::load_subclass_info();
        $this->table_name = languages_dd::$table_name;
        $this->tables     = languages_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('language');
            $this->set_values("?");

            $this->stmt_bind_param($param['language']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("language = ?");
            $this->set_where("language_id = ?");

            $this->stmt_bind_param($param['language']);
            $this->stmt_bind_param($param['language_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("language_id = ?");

            $this->stmt_bind_param($param['language_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");


            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("language_id = ?");

        $this->stmt_bind_param($param['language_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("language_id = ? AND (language_id != ?)");

        $this->stmt_bind_param($param['language_id']);
        $this->stmt_bind_param($param['language_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function get_language($id)
    {
        $this->set_where('language_id = ?');
        $this->stmt_bind_param($id);
        $this->stmt_prepare();
        $this->fetch('single');

        return $this;
    }
}
