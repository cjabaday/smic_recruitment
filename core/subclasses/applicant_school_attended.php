<?php
require_once 'applicant_school_attended_dd.php';
class applicant_school_attended extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = applicant_school_attended_dd::load_dictionary();
        $this->relations  = applicant_school_attended_dd::load_relationships();
        $this->subclasses = applicant_school_attended_dd::load_subclass_info();
        $this->table_name = applicant_school_attended_dd::$table_name;
        $this->tables     = applicant_school_attended_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('applicant_id, school_id, educational_attainment, course, awards, address, date_from, date_to');
            $this->set_values("?,?,?,?,?,?,?,?");

            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['school_id']);
            $this->stmt_bind_param($param['educational_attainment']);
            $this->stmt_bind_param($param['course']);
            $this->stmt_bind_param($param['awards']);
            $this->stmt_bind_param($param['address']);
            $this->stmt_bind_param($param['date_from']);
            $this->stmt_bind_param($param['date_to']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("applicant_id = ?, school_id = ?, educational_attainment = ?, course = ?, awards = ?, address = ?, date_from = ?, date_to = ?");
            $this->set_where("applicant_school_attended_id = ?");

            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['school_id']);
            $this->stmt_bind_param($param['educational_attainment']);
            $this->stmt_bind_param($param['course']);
            $this->stmt_bind_param($param['awards']);
            $this->stmt_bind_param($param['address']);
            $this->stmt_bind_param($param['date_from']);
            $this->stmt_bind_param($param['date_to']);
            $this->stmt_bind_param($param['applicant_school_attended_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("applicant_school_attended_id = ?");

            $this->stmt_bind_param($param['applicant_school_attended_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("applicant_id = ?");

            $this->stmt_bind_param($param['applicant_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("applicant_school_attended_id = ?");

        $this->stmt_bind_param($param['applicant_school_attended_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("applicant_school_attended_id = ? AND (applicant_school_attended_id != ?)");

        $this->stmt_bind_param($param['applicant_school_attended_id']);
        $this->stmt_bind_param($param['applicant_school_attended_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function select_record($applicant_id)
    {
        $this->set_table('applicant_school_attended a LEFT JOIN school b ON a.school_id = b.school_id');
        $this->set_fields('*, b.name as `school_name`');
        $this->set_where('applicant_id = ?');
        $this->stmt_bind_param($applicant_id);
        $this->stmt_fetch('rowdump');

        return $this;
    }
}
