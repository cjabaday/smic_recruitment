<?php
require_once 'plantilla_dd.php';
class plantilla_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'PLANTILLA_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'plantilla_html';
    var $data_subclass = 'plantilla';
    var $result_page = 'reporter_result_plantilla.php';
    var $cancel_page = 'listview_plantilla.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_plantilla.php';

    function __construct()
    {
        $this->fields        = plantilla_dd::load_dictionary();
        $this->relations     = plantilla_dd::load_relationships();
        $this->subclasses    = plantilla_dd::load_subclass_info();
        $this->table_name    = plantilla_dd::$table_name;
        $this->tables        = plantilla_dd::$table_name;
        $this->readable_name = plantilla_dd::$readable_name;
        $this->get_report_fields();
    }
}
