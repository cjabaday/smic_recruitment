<?php
require_once 'calendar_event_type_dd.php';
class calendar_event_type extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = calendar_event_type_dd::load_dictionary();
        $this->relations  = calendar_event_type_dd::load_relationships();
        $this->subclasses = calendar_event_type_dd::load_subclass_info();
        $this->table_name = calendar_event_type_dd::$table_name;
        $this->tables     = calendar_event_type_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('event_type, event_color');
            $this->set_values("?,?");
            
            $this->stmt_bind_param($param['event_type']);
            $this->stmt_bind_param($param['event_color']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("event_type = ?, event_color = ?");
            $this->set_where("calendar_event_type_id = ?");
            
            $this->stmt_bind_param($param['event_type']);
            $this->stmt_bind_param($param['event_color']);
            $this->stmt_bind_param($param['calendar_event_type_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("calendar_event_type_id = ?");
            
            $this->stmt_bind_param($param['calendar_event_type_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("calendar_event_type_id = ?");
        
        $this->stmt_bind_param($param['calendar_event_type_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("calendar_event_type_id = ? AND (calendar_event_type_id != ?)");
        
        $this->stmt_bind_param($param['calendar_event_type_id']);
        $this->stmt_bind_param($param['calendar_event_type_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
