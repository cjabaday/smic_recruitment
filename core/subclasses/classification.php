<?php
require_once 'classification_dd.php';
class classification extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = classification_dd::load_dictionary();
        $this->relations  = classification_dd::load_relationships();
        $this->subclasses = classification_dd::load_subclass_info();
        $this->table_name = classification_dd::$table_name;
        $this->tables     = classification_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('name, description');
            $this->set_values("?,?");

            $this->stmt_bind_param($param['name']);
            $this->stmt_bind_param($param['description']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("name = ?, description = ?");
            $this->set_where("classification_id = ?");

            $this->stmt_bind_param($param['name']);
            $this->stmt_bind_param($param['description']);
            $this->stmt_bind_param($param['classification_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("classification_id = ?");

            $this->stmt_bind_param($param['classification_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");


            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("classification_id = ?");

        $this->stmt_bind_param($param['classification_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("classification_id = ? AND (classification_id != ?)");

        $this->stmt_bind_param($param['classification_id']);
        $this->stmt_bind_param($param['classification_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_label($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("name = ?");

        $this->stmt_bind_param($param['name']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function add_classification(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('classification_id, name');
            $this->set_values("?,?");

            $this->stmt_bind_param($param['classification_id']);
            $this->stmt_bind_param($param['name']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }
}
