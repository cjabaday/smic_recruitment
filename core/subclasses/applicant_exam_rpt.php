<?php
require_once 'applicant_exam_dd.php';
class applicant_exam_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'APPLICANT_EXAM_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'applicant_exam_html';
    var $data_subclass = 'applicant_exam';
    var $result_page = 'reporter_result_applicant_exam.php';
    var $cancel_page = 'listview_applicant_exam.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_applicant_exam.php';

    function __construct()
    {
        $this->fields        = applicant_exam_dd::load_dictionary();
        $this->relations     = applicant_exam_dd::load_relationships();
        $this->subclasses    = applicant_exam_dd::load_subclass_info();
        $this->table_name    = applicant_exam_dd::$table_name;
        $this->tables        = applicant_exam_dd::$table_name;
        $this->readable_name = applicant_exam_dd::$readable_name;
        $this->get_report_fields();
    }
}
