<?php
require_once 'iprf_staffrequest_dd.php';
class iprf_staffrequest extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = iprf_staffrequest_dd::load_dictionary();
        $this->relations  = iprf_staffrequest_dd::load_relationships();
        $this->subclasses = iprf_staffrequest_dd::load_subclass_info();
        $this->table_name = iprf_staffrequest_dd::$table_name;
        $this->tables     = iprf_staffrequest_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, location_id, reporting_to_id, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, reason_for_request, replacement_of, date_needed, file_status, date_filed, requested_by, secondary_unit');
            $this->set_values("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?");

            $this->stmt_bind_param($param['iprf_staffrequest_id']);
            $this->stmt_bind_param($param['PRF_No']);
            $this->stmt_bind_param($param['company_id']);
            $this->stmt_bind_param($param['department_id']);
            $this->stmt_bind_param($param['position_id']);
            $this->stmt_bind_param($param['rank_id']);
            $this->stmt_bind_param($param['classification_id']);
            $this->stmt_bind_param($param['status']);
            $this->stmt_bind_param($param['duration']);
            $this->stmt_bind_param($param['responsibility']);
            $this->stmt_bind_param($param['location_id']);
            $this->stmt_bind_param($param['reporting_to_id']);
            $this->stmt_bind_param($param['general_duties']);
            $this->stmt_bind_param($param['detailed_duties']);
            $this->stmt_bind_param($param['education']);
            $this->stmt_bind_param($param['professional_eligibility_skills']);
            $this->stmt_bind_param($param['experience']);
            $this->stmt_bind_param($param['skills']);
            $this->stmt_bind_param($param['reason_for_request']);
            $this->stmt_bind_param($param['replacement_of']);
            $this->stmt_bind_param($param['date_needed']);
            $this->stmt_bind_param($param['file_status']);
            $this->stmt_bind_param($param['date_filed']);
            $this->stmt_bind_param($param['requested_by']);
            $this->stmt_bind_param($param['secondary_unit']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("PRF_No = ?, company_id = ?, department_id = ?, position_id = ?, rank_id = ?, classification_id = ?, status = ?, duration = ?, responsibility = ?, location_id = ?, reporting_to_id = ?, general_duties = ?, detailed_duties = ?, education = ?, professional_eligibility_skills = ?, experience = ?, skills = ?, reason_for_request = ?, replacement_of = ?, date_needed = ?, file_status = ?, date_filed = ?, requested_by = ?, secondary_unit = ?");
            $this->set_where("iprf_staffrequest_id = ?");

            $this->stmt_bind_param($param['PRF_No']);
            $this->stmt_bind_param($param['company_id']);
            $this->stmt_bind_param($param['department_id']);
            $this->stmt_bind_param($param['position_id']);
            $this->stmt_bind_param($param['rank_id']);
            $this->stmt_bind_param($param['classification_id']);
            $this->stmt_bind_param($param['status']);
            $this->stmt_bind_param($param['duration']);
            $this->stmt_bind_param($param['responsibility']);
            $this->stmt_bind_param($param['location_id']);
            $this->stmt_bind_param($param['reporting_to_id']);
            $this->stmt_bind_param($param['general_duties']);
            $this->stmt_bind_param($param['detailed_duties']);
            $this->stmt_bind_param($param['education']);
            $this->stmt_bind_param($param['professional_eligibility_skills']);
            $this->stmt_bind_param($param['experience']);
            $this->stmt_bind_param($param['skills']);
            $this->stmt_bind_param($param['reason_for_request']);
            $this->stmt_bind_param($param['replacement_of']);
            $this->stmt_bind_param($param['date_needed']);
            $this->stmt_bind_param($param['file_status']);
            $this->stmt_bind_param($param['date_filed']);
            $this->stmt_bind_param($param['requested_by']);
            $this->stmt_bind_param($param['secondary_unit']);
            $this->stmt_bind_param($param['iprf_staffrequest_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("iprf_staffrequest_id = ?");

            $this->stmt_bind_param($param['iprf_staffrequest_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");


            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("iprf_staffrequest_id = ?");

        $this->stmt_bind_param($param['iprf_staffrequest_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("iprf_staffrequest_id = ? AND (iprf_staffrequest_id != ?)");

        $this->stmt_bind_param($param['iprf_staffrequest_id']);
        $this->stmt_bind_param($param['iprf_staffrequest_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function add_iprf(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed');
            $this->set_values("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?");

            $this->stmt_bind_param($param['iprf_staffrequest_id']);
            $this->stmt_bind_param($param['PRF_No']);
            $this->stmt_bind_param($param['company_id']);
            $this->stmt_bind_param($param['department_id']);
            $this->stmt_bind_param($param['position_id']);
            $this->stmt_bind_param($param['rank_id']);
            $this->stmt_bind_param($param['classification_id']);
            $this->stmt_bind_param($param['status']);
            $this->stmt_bind_param($param['duration']);
            $this->stmt_bind_param($param['responsibility']);
            $this->stmt_bind_param($param['general_duties']);
            $this->stmt_bind_param($param['detailed_duties']);
            $this->stmt_bind_param($param['education']);
            $this->stmt_bind_param($param['professional_eligibility_skills']);
            $this->stmt_bind_param($param['experience']);
            $this->stmt_bind_param($param['skills']);
            $this->stmt_bind_param($param['date_needed']);
            $this->stmt_bind_param($param['date_filed']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }


}
