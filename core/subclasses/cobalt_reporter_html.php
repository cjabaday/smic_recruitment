<?php
require_once 'cobalt_reporter_dd.php';
class cobalt_reporter_html extends html
{
    function __construct()
    {
        $this->fields        = cobalt_reporter_dd::load_dictionary();
        $this->relations     = cobalt_reporter_dd::load_relationships();
        $this->subclasses    = cobalt_reporter_dd::load_subclass_info();
        $this->table_name    = cobalt_reporter_dd::$table_name;
        $this->readable_name = cobalt_reporter_dd::$readable_name;
    }
}
