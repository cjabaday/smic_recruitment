<?php
require_once 'action_notice_dd.php';
class action_notice_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'ACTION_NOTICE_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'action_notice_html';
    var $data_subclass = 'action_notice';
    var $result_page = 'reporter_result_action_notice.php';
    var $cancel_page = 'listview_action_notice.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_action_notice.php';

    function __construct()
    {
        $this->fields        = action_notice_dd::load_dictionary();
        $this->relations     = action_notice_dd::load_relationships();
        $this->subclasses    = action_notice_dd::load_subclass_info();
        $this->table_name    = action_notice_dd::$table_name;
        $this->tables        = action_notice_dd::$table_name;
        $this->readable_name = action_notice_dd::$readable_name;
        $this->get_report_fields();
    }
}
