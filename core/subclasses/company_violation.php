<?php
require_once 'company_violation_dd.php';
class company_violation extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = company_violation_dd::load_dictionary();
        $this->relations  = company_violation_dd::load_relationships();
        $this->subclasses = company_violation_dd::load_subclass_info();
        $this->table_name = company_violation_dd::$table_name;
        $this->tables     = company_violation_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('employee_id, company_policy_id, violation_date, details, status');
            $this->set_values("?,?,?,?,?");
            
            $this->stmt_bind_param($param['employee_id']);
            $this->stmt_bind_param($param['company_policy_id']);
            $this->stmt_bind_param($param['violation_date']);
            $this->stmt_bind_param($param['details']);
            $this->stmt_bind_param($param['status']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("employee_id = ?, company_policy_id = ?, violation_date = ?, details = ?, status = ?");
            $this->set_where("company_violation_id = ?");
            
            $this->stmt_bind_param($param['employee_id']);
            $this->stmt_bind_param($param['company_policy_id']);
            $this->stmt_bind_param($param['violation_date']);
            $this->stmt_bind_param($param['details']);
            $this->stmt_bind_param($param['status']);
            $this->stmt_bind_param($param['company_violation_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("company_violation_id = ?");
            
            $this->stmt_bind_param($param['company_violation_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("company_violation_id = ?");
        
        $this->stmt_bind_param($param['company_violation_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("company_violation_id = ? AND (company_violation_id != ?)");
        
        $this->stmt_bind_param($param['company_violation_id']);
        $this->stmt_bind_param($param['company_violation_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
