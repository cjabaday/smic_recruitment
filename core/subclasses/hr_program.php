<?php
require_once 'hr_program_dd.php';
class hr_program extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = hr_program_dd::load_dictionary();
        $this->relations  = hr_program_dd::load_relationships();
        $this->subclasses = hr_program_dd::load_subclass_info();
        $this->table_name = hr_program_dd::$table_name;
        $this->tables     = hr_program_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('program_name, start_date, start_time, end_date, end_time');
            $this->set_values("?,?,?,?,?");
            
            $this->stmt_bind_param($param['program_name']);
            $this->stmt_bind_param($param['start_date']);
            $this->stmt_bind_param($param['start_time']);
            $this->stmt_bind_param($param['end_date']);
            $this->stmt_bind_param($param['end_time']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("program_name = ?, start_date = ?, start_time = ?, end_date = ?, end_time = ?");
            $this->set_where("hr_program_id = ?");
            
            $this->stmt_bind_param($param['program_name']);
            $this->stmt_bind_param($param['start_date']);
            $this->stmt_bind_param($param['start_time']);
            $this->stmt_bind_param($param['end_date']);
            $this->stmt_bind_param($param['end_time']);
            $this->stmt_bind_param($param['hr_program_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("hr_program_id = ?");
            
            $this->stmt_bind_param($param['hr_program_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("hr_program_id = ?");
        
        $this->stmt_bind_param($param['hr_program_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("hr_program_id = ? AND (hr_program_id != ?)");
        
        $this->stmt_bind_param($param['hr_program_id']);
        $this->stmt_bind_param($param['hr_program_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
