<?php
require_once 'sst_class.php';
require_once 'iemployeestatus_dd.php';
class iemployeestatus_sst extends sst
{
    function __construct()
    {
        $this->fields        = iemployeestatus_dd::load_dictionary();
        $this->relations     = iemployeestatus_dd::load_relationships();
        $this->subclasses    = iemployeestatus_dd::load_subclass_info();
        $this->table_name    = iemployeestatus_dd::$table_name;
        $this->readable_name = iemployeestatus_dd::$readable_name;
        parent::__construct();
    }
}
