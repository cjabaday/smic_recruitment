<?php
require_once 'sst_class.php';
require_once 'applicant_preferred_positions_dd.php';
class applicant_preferred_positions_sst extends sst
{
    function __construct()
    {
        $this->fields        = applicant_preferred_positions_dd::load_dictionary();
        $this->relations     = applicant_preferred_positions_dd::load_relationships();
        $this->subclasses    = applicant_preferred_positions_dd::load_subclass_info();
        $this->table_name    = applicant_preferred_positions_dd::$table_name;
        $this->readable_name = applicant_preferred_positions_dd::$readable_name;
        parent::__construct();
    }
}
