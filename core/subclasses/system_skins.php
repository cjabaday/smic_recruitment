<?php
require_once 'system_skins_dd.php';
class system_skins extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = system_skins_dd::load_dictionary();
        $this->relations  = system_skins_dd::load_relationships();
        $this->subclasses = system_skins_dd::load_subclass_info();
        $this->table_name = system_skins_dd::$table_name;
        $this->tables     = system_skins_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('skin_name, header, footer, master_css, colors_css, fonts_css, override_css, icon_set');
            $this->set_values("?,?,?,?,?,?,?,?");

            $this->stmt_bind_param($param['skin_name']);
            $this->stmt_bind_param($param['header']);
            $this->stmt_bind_param($param['footer']);
            $this->stmt_bind_param($param['master_css']);
            $this->stmt_bind_param($param['colors_css']);
            $this->stmt_bind_param($param['fonts_css']);
            $this->stmt_bind_param($param['override_css']);
            $this->stmt_bind_param($param['icon_set']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("skin_name = ?, header = ?, footer = ?, master_css = ?, colors_css = ?, fonts_css = ?, override_css = ?, icon_set = ?");
            $this->set_where("skin_id = ?");

            $this->stmt_bind_param($param['skin_name']);
            $this->stmt_bind_param($param['header']);
            $this->stmt_bind_param($param['footer']);
            $this->stmt_bind_param($param['master_css']);
            $this->stmt_bind_param($param['colors_css']);
            $this->stmt_bind_param($param['fonts_css']);
            $this->stmt_bind_param($param['override_css']);
            $this->stmt_bind_param($param['icon_set']);
            $this->stmt_bind_param($param['skin_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("skin_id = ?");

            $this->stmt_bind_param($param['skin_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");


            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("skin_id = ?");

        $this->stmt_bind_param($param['skin_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("skin_id = ? AND (skin_id != ?)");

        $this->stmt_bind_param($param['skin_id']);
        $this->stmt_bind_param($param['skin_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
