<?php
require_once 'iproficiency_dd.php';
class iproficiency extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = iproficiency_dd::load_dictionary();
        $this->relations  = iproficiency_dd::load_relationships();
        $this->subclasses = iproficiency_dd::load_subclass_info();
        $this->table_name = iproficiency_dd::$table_name;
        $this->tables     = iproficiency_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('ProficiencyID, ProficiencyCode, ProficiencyDesc, EncodeBy, EncodeDate');
            $this->set_values("?,?,?,?,?");
            
            $this->stmt_bind_param($param['ProficiencyID']);
            $this->stmt_bind_param($param['ProficiencyCode']);
            $this->stmt_bind_param($param['ProficiencyDesc']);
            $this->stmt_bind_param($param['EncodeBy']);
            $this->stmt_bind_param($param['EncodeDate']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("ProficiencyID = ?, ProficiencyCode = ?, ProficiencyDesc = ?, EncodeBy = ?, EncodeDate = ?");
            $this->set_where("ProficiencyID = ?");
            
            $this->stmt_bind_param($param['ProficiencyID']);
            $this->stmt_bind_param($param['ProficiencyCode']);
            $this->stmt_bind_param($param['ProficiencyDesc']);
            $this->stmt_bind_param($param['EncodeBy']);
            $this->stmt_bind_param($param['EncodeDate']);
            $this->stmt_bind_param($param['orig_ProficiencyID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("ProficiencyID = ?");
            
            $this->stmt_bind_param($param['ProficiencyID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("ProficiencyID = ?");
            
            $this->stmt_bind_param($param['ProficiencyID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("ProficiencyID = ?");
        
        $this->stmt_bind_param($param['ProficiencyID']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("ProficiencyID = ? AND (ProficiencyID != ?)");
        
        $this->stmt_bind_param($param['ProficiencyID']);
        $this->stmt_bind_param($param['orig_ProficiencyID']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
