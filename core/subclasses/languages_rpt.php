<?php
require_once 'languages_dd.php';
class languages_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'LANGUAGES_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'languages_html';
    var $data_subclass = 'languages';
    var $result_page = 'reporter_result_languages.php';
    var $cancel_page = 'listview_languages.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_languages.php';

    function __construct()
    {
        $this->fields        = languages_dd::load_dictionary();
        $this->relations     = languages_dd::load_relationships();
        $this->subclasses    = languages_dd::load_subclass_info();
        $this->table_name    = languages_dd::$table_name;
        $this->tables        = languages_dd::$table_name;
        $this->readable_name = languages_dd::$readable_name;
        $this->get_report_fields();
    }
}
