<?php
require_once 'igender_dd.php';
class igender_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'igender_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'igender_html';
    var $data_subclass = 'igender';
    var $result_page = 'reporter_result_igender.php';
    var $cancel_page = 'listview_igender.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_igender.php';

    function __construct()
    {
        $this->fields        = igender_dd::load_dictionary();
        $this->relations     = igender_dd::load_relationships();
        $this->subclasses    = igender_dd::load_subclass_info();
        $this->table_name    = igender_dd::$table_name;
        $this->tables        = igender_dd::$table_name;
        $this->readable_name = igender_dd::$readable_name;
        $this->get_report_fields();
    }
}
