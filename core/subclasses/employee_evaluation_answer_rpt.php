<?php
require_once 'employee_evaluation_answer_dd.php';
class employee_evaluation_answer_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'EMPLOYEE_EVALUATION_ANSWER_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'employee_evaluation_answer_html';
    var $data_subclass = 'employee_evaluation_answer';
    var $result_page = 'reporter_result_employee_evaluation_answer.php';
    var $cancel_page = 'listview_employee_evaluation_answer.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_employee_evaluation_answer.php';

    function __construct()
    {
        $this->fields        = employee_evaluation_answer_dd::load_dictionary();
        $this->relations     = employee_evaluation_answer_dd::load_relationships();
        $this->subclasses    = employee_evaluation_answer_dd::load_subclass_info();
        $this->table_name    = employee_evaluation_answer_dd::$table_name;
        $this->tables        = employee_evaluation_answer_dd::$table_name;
        $this->readable_name = employee_evaluation_answer_dd::$readable_name;
        $this->get_report_fields();
    }
}
