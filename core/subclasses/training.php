<?php
require_once 'training_dd.php';
class training extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = training_dd::load_dictionary();
        $this->relations  = training_dd::load_relationships();
        $this->subclasses = training_dd::load_subclass_info();
        $this->table_name = training_dd::$table_name;
        $this->tables     = training_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('training_name, training_date, training_details');
            $this->set_values("?,?,?");
            
            $this->stmt_bind_param($param['training_name']);
            $this->stmt_bind_param($param['training_date']);
            $this->stmt_bind_param($param['training_details']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("training_name = ?, training_date = ?, training_details = ?");
            $this->set_where("training_id = ?");
            
            $this->stmt_bind_param($param['training_name']);
            $this->stmt_bind_param($param['training_date']);
            $this->stmt_bind_param($param['training_details']);
            $this->stmt_bind_param($param['training_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("training_id = ?");
            
            $this->stmt_bind_param($param['training_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("training_id = ?");
        
        $this->stmt_bind_param($param['training_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("training_id = ? AND (training_id != ?)");
        
        $this->stmt_bind_param($param['training_id']);
        $this->stmt_bind_param($param['training_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
