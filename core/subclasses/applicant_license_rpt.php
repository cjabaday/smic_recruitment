<?php
require_once 'applicant_license_dd.php';
class applicant_license_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'APPLICANT_LICENSE_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'applicant_license_html';
    var $data_subclass = 'applicant_license';
    var $result_page = 'reporter_result_applicant_license.php';
    var $cancel_page = 'listview_applicant_license.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_applicant_license.php';

    function __construct()
    {
        $this->fields        = applicant_license_dd::load_dictionary();
        $this->relations     = applicant_license_dd::load_relationships();
        $this->subclasses    = applicant_license_dd::load_subclass_info();
        $this->table_name    = applicant_license_dd::$table_name;
        $this->tables        = applicant_license_dd::$table_name;
        $this->readable_name = applicant_license_dd::$readable_name;
        $this->get_report_fields();
    }
}
