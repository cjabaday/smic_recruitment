<?php
require_once 'other_declarations_name_smic_employ_dd.php';
class other_declarations_name_smic_employ_html extends html
{
    function __construct()
    {
        $this->fields        = other_declarations_name_smic_employ_dd::load_dictionary();
        $this->relations     = other_declarations_name_smic_employ_dd::load_relationships();
        $this->subclasses    = other_declarations_name_smic_employ_dd::load_subclass_info();
        $this->table_name    = other_declarations_name_smic_employ_dd::$table_name;
        $this->readable_name = other_declarations_name_smic_employ_dd::$readable_name;
    }
}
