<?php
require_once 'city_dd.php';
class city extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = city_dd::load_dictionary();
        $this->relations  = city_dd::load_relationships();
        $this->subclasses = city_dd::load_subclass_info();
        $this->table_name = city_dd::$table_name;
        $this->tables     = city_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('region_id, province_id, city_name, city_code');
            $this->set_values("?,?,?,?");

            $this->stmt_bind_param($param['region_id']);
            $this->stmt_bind_param($param['province_id']);
            $this->stmt_bind_param($param['city_name']);
            $this->stmt_bind_param($param['city_code']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("region_id = ?, province_id = ?, city_name = ?, city_code = ?");
            $this->set_where("city_id = ?");

            $this->stmt_bind_param($param['region_id']);
            $this->stmt_bind_param($param['province_id']);
            $this->stmt_bind_param($param['city_name']);
            $this->stmt_bind_param($param['city_code']);
            $this->stmt_bind_param($param['city_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("city_id = ?");

            $this->stmt_bind_param($param['city_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");


            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("city_id = ?");

        $this->stmt_bind_param($param['city_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("city_id = ? AND (city_id != ?)");

        $this->stmt_bind_param($param['city_id']);
        $this->stmt_bind_param($param['city_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function get_city($id)
    {
        $this->set_where('city_id = ?');
        $this->stmt_bind_param($id);
        $this->stmt_prepare();

        return $this;
    }
}
