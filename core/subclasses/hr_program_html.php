<?php
require_once 'hr_program_dd.php';
class hr_program_html extends html
{
    function __construct()
    {
        $this->fields        = hr_program_dd::load_dictionary();
        $this->relations     = hr_program_dd::load_relationships();
        $this->subclasses    = hr_program_dd::load_subclass_info();
        $this->table_name    = hr_program_dd::$table_name;
        $this->readable_name = hr_program_dd::$readable_name;
    }
}
