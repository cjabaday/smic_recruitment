<?php
require_once 'site_dd.php';
class site_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'SITE_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'site_html';
    var $data_subclass = 'site';
    var $result_page = 'reporter_result_site.php';
    var $cancel_page = 'listview_site.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_site.php';

    function __construct()
    {
        $this->fields        = site_dd::load_dictionary();
        $this->relations     = site_dd::load_relationships();
        $this->subclasses    = site_dd::load_subclass_info();
        $this->table_name    = site_dd::$table_name;
        $this->tables        = site_dd::$table_name;
        $this->readable_name = site_dd::$readable_name;
        $this->get_report_fields();
    }
}
