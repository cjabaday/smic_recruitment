<?php
require_once 'documentation_class.php';
require_once 'igender_dd.php';
class igender_doc extends documentation
{
    function __construct()
    {
        $this->fields        = igender_dd::load_dictionary();
        $this->relations     = igender_dd::load_relationships();
        $this->subclasses    = igender_dd::load_subclass_info();
        $this->table_name    = igender_dd::$table_name;
        $this->readable_name = igender_dd::$readable_name;
        parent::__construct();
    }
}
