<?php
require_once 'cobalt_sst_dd.php';
class cobalt_sst extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = cobalt_sst_dd::load_dictionary();
        $this->relations  = cobalt_sst_dd::load_relationships();
        $this->subclasses = cobalt_sst_dd::load_subclass_info();
        $this->table_name = cobalt_sst_dd::$table_name;
        $this->tables     = cobalt_sst_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('title, description, config_file');
            $this->set_values("?,?,?");

            $this->stmt_bind_param($param['title']);
            $this->stmt_bind_param($param['description']);
            $this->stmt_bind_param($param['config_file']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("title = ?, description = ?, config_file = ?");
            $this->set_where("auto_id = ?");

            $this->stmt_bind_param($param['title']);
            $this->stmt_bind_param($param['description']);
            $this->stmt_bind_param($param['config_file']);
            $this->stmt_bind_param($param['auto_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("auto_id = ?");

            $this->stmt_bind_param($param['auto_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");


            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("auto_id = ?");

        $this->stmt_bind_param($param['auto_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("auto_id = ? AND (auto_id != ?)");

        $this->stmt_bind_param($param['auto_id']);
        $this->stmt_bind_param($param['auto_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
