<?php
require_once 'sst_class.php';
require_once 'site_dd.php';
class site_sst extends sst
{
    function __construct()
    {
        $this->fields        = site_dd::load_dictionary();
        $this->relations     = site_dd::load_relationships();
        $this->subclasses    = site_dd::load_subclass_info();
        $this->table_name    = site_dd::$table_name;
        $this->readable_name = site_dd::$readable_name;
        parent::__construct();
    }
}
