<?php
require_once 'resignation_dd.php';
class resignation_html extends html
{
    function __construct()
    {
        $this->fields        = resignation_dd::load_dictionary();
        $this->relations     = resignation_dd::load_relationships();
        $this->subclasses    = resignation_dd::load_subclass_info();
        $this->table_name    = resignation_dd::$table_name;
        $this->readable_name = resignation_dd::$readable_name;
    }
}
