<?php
require_once 'branch_type_dd.php';
class branch_type_rpt extends reporter
{
    var $tables='';
    var $session_array_name = 'BRANCH_TYPE_REPORT_CUSTOM';
    var $report_title = '%%: Custom Reporting Tool';
    var $html_subclass = 'branch_type_html';
    var $data_subclass = 'branch_type';
    var $result_page = 'reporter_result_branch_type.php';
    var $cancel_page = 'listview_branch_type.php';
    var $pdf_reporter_filename = 'reporter_pdfresult_branch_type.php';

    function __construct()
    {
        $this->fields        = branch_type_dd::load_dictionary();
        $this->relations     = branch_type_dd::load_relationships();
        $this->subclasses    = branch_type_dd::load_subclass_info();
        $this->table_name    = branch_type_dd::$table_name;
        $this->tables        = branch_type_dd::$table_name;
        $this->readable_name = branch_type_dd::$readable_name;
        $this->get_report_fields();
    }
}
