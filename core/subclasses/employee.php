<?php
require_once 'employee_dd.php';
class employee extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = employee_dd::load_dictionary();
        $this->relations  = employee_dd::load_relationships();
        $this->subclasses = employee_dd::load_subclass_info();
        $this->table_name = employee_dd::$table_name;
        $this->tables     = employee_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('branch_id, department_id, company_id, approver1_employee_id, approver2_employee_id, plantilla_id, employee_first_name, employee_middle_name, employee_last_name, employee_nickname, present_address, present_contact_no, provincial_address, provincial_contact_no, gender, civil_status, religion, citizenship, birthplace, birthdate, age, application_source, start_date, sss_no, hdmf_no, philhealth_no, tin, employment_status');
            $this->set_values("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?");
            
            $this->stmt_bind_param($param['branch_id']);
            $this->stmt_bind_param($param['department_id']);
            $this->stmt_bind_param($param['company_id']);
            $this->stmt_bind_param($param['approver1_employee_id']);
            $this->stmt_bind_param($param['approver2_employee_id']);
            $this->stmt_bind_param($param['plantilla_id']);
            $this->stmt_bind_param($param['employee_first_name']);
            $this->stmt_bind_param($param['employee_middle_name']);
            $this->stmt_bind_param($param['employee_last_name']);
            $this->stmt_bind_param($param['employee_nickname']);
            $this->stmt_bind_param($param['present_address']);
            $this->stmt_bind_param($param['present_contact_no']);
            $this->stmt_bind_param($param['provincial_address']);
            $this->stmt_bind_param($param['provincial_contact_no']);
            $this->stmt_bind_param($param['gender']);
            $this->stmt_bind_param($param['civil_status']);
            $this->stmt_bind_param($param['religion']);
            $this->stmt_bind_param($param['citizenship']);
            $this->stmt_bind_param($param['birthplace']);
            $this->stmt_bind_param($param['birthdate']);
            $this->stmt_bind_param($param['age']);
            $this->stmt_bind_param($param['application_source']);
            $this->stmt_bind_param($param['start_date']);
            $this->stmt_bind_param($param['sss_no']);
            $this->stmt_bind_param($param['hdmf_no']);
            $this->stmt_bind_param($param['philhealth_no']);
            $this->stmt_bind_param($param['tin']);
            $this->stmt_bind_param($param['employment_status']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("branch_id = ?, department_id = ?, company_id = ?, approver1_employee_id = ?, approver2_employee_id = ?, plantilla_id = ?, employee_first_name = ?, employee_middle_name = ?, employee_last_name = ?, employee_nickname = ?, present_address = ?, present_contact_no = ?, provincial_address = ?, provincial_contact_no = ?, gender = ?, civil_status = ?, religion = ?, citizenship = ?, birthplace = ?, birthdate = ?, age = ?, application_source = ?, start_date = ?, sss_no = ?, hdmf_no = ?, philhealth_no = ?, tin = ?, employment_status = ?");
            $this->set_where("employee_id = ?");
            
            $this->stmt_bind_param($param['branch_id']);
            $this->stmt_bind_param($param['department_id']);
            $this->stmt_bind_param($param['company_id']);
            $this->stmt_bind_param($param['approver1_employee_id']);
            $this->stmt_bind_param($param['approver2_employee_id']);
            $this->stmt_bind_param($param['plantilla_id']);
            $this->stmt_bind_param($param['employee_first_name']);
            $this->stmt_bind_param($param['employee_middle_name']);
            $this->stmt_bind_param($param['employee_last_name']);
            $this->stmt_bind_param($param['employee_nickname']);
            $this->stmt_bind_param($param['present_address']);
            $this->stmt_bind_param($param['present_contact_no']);
            $this->stmt_bind_param($param['provincial_address']);
            $this->stmt_bind_param($param['provincial_contact_no']);
            $this->stmt_bind_param($param['gender']);
            $this->stmt_bind_param($param['civil_status']);
            $this->stmt_bind_param($param['religion']);
            $this->stmt_bind_param($param['citizenship']);
            $this->stmt_bind_param($param['birthplace']);
            $this->stmt_bind_param($param['birthdate']);
            $this->stmt_bind_param($param['age']);
            $this->stmt_bind_param($param['application_source']);
            $this->stmt_bind_param($param['start_date']);
            $this->stmt_bind_param($param['sss_no']);
            $this->stmt_bind_param($param['hdmf_no']);
            $this->stmt_bind_param($param['philhealth_no']);
            $this->stmt_bind_param($param['tin']);
            $this->stmt_bind_param($param['employment_status']);
            $this->stmt_bind_param($param['employee_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("employee_id = ?");
            
            $this->stmt_bind_param($param['employee_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("");
            

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("employee_id = ?");
        
        $this->stmt_bind_param($param['employee_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("employee_id = ? AND (employee_id != ?)");
        
        $this->stmt_bind_param($param['employee_id']);
        $this->stmt_bind_param($param['employee_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
