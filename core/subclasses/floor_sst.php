<?php
require_once 'sst_class.php';
require_once 'floor_dd.php';
class floor_sst extends sst
{
    function __construct()
    {
        $this->fields        = floor_dd::load_dictionary();
        $this->relations     = floor_dd::load_relationships();
        $this->subclasses    = floor_dd::load_subclass_info();
        $this->table_name    = floor_dd::$table_name;
        $this->readable_name = floor_dd::$readable_name;
        parent::__construct();
    }
}
