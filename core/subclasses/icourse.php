<?php
require_once 'icourse_dd.php';
class icourse extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = icourse_dd::load_dictionary();
        $this->relations  = icourse_dd::load_relationships();
        $this->subclasses = icourse_dd::load_subclass_info();
        $this->table_name = icourse_dd::$table_name;
        $this->tables     = icourse_dd::$table_name;
    }

    function add(&$param = null)
    {   
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('CourseID, CourseCode, CourseDesc, EncodeBy, EncodeDate');
            $this->set_values("?,?,?,?,?");
            
            $this->stmt_bind_param($param['CourseID']);
            $this->stmt_bind_param($param['CourseCode']);
            $this->stmt_bind_param($param['CourseDesc']);
            $this->stmt_bind_param($param['EncodeBy']);
            $this->stmt_bind_param($param['EncodeDate']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("CourseID = ?, CourseCode = ?, CourseDesc = ?, EncodeBy = ?, EncodeDate = ?");
            $this->set_where("CourseID = ?");
            
            $this->stmt_bind_param($param['CourseID']);
            $this->stmt_bind_param($param['CourseCode']);
            $this->stmt_bind_param($param['CourseDesc']);
            $this->stmt_bind_param($param['EncodeBy']);
            $this->stmt_bind_param($param['EncodeDate']);
            $this->stmt_bind_param($param['orig_CourseID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("CourseID = ?");
            
            $this->stmt_bind_param($param['CourseID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("CourseID = ?");
            
            $this->stmt_bind_param($param['CourseID']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("CourseID = ?");
        
        $this->stmt_bind_param($param['CourseID']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("CourseID = ? AND (CourseID != ?)");
        
        $this->stmt_bind_param($param['CourseID']);
        $this->stmt_bind_param($param['orig_CourseID']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }
}
