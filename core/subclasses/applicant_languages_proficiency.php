<?php
require_once 'applicant_languages_proficiency_dd.php';
class applicant_languages_proficiency extends data_abstraction
{
    var $fields = array();


    function __construct()
    {
        $this->fields     = applicant_languages_proficiency_dd::load_dictionary();
        $this->relations  = applicant_languages_proficiency_dd::load_relationships();
        $this->subclasses = applicant_languages_proficiency_dd::load_subclass_info();
        $this->table_name = applicant_languages_proficiency_dd::$table_name;
        $this->tables     = applicant_languages_proficiency_dd::$table_name;
    }

    function add(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('INSERT');
            $this->set_fields('applicant_id, language, speaking_proficiency, writing_proficiency');
            $this->set_values("?,?,?,?");

            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['language']);
            $this->stmt_bind_param($param['speaking_proficiency']);
            $this->stmt_bind_param($param['writing_proficiency']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function edit(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('UPDATE');
            $this->set_update("applicant_id = ?, language = ?, speaking_proficiency = ?, writing_proficiency = ?");
            $this->set_where("applicant_language_proficiency_id = ?");

            $this->stmt_bind_param($param['applicant_id']);
            $this->stmt_bind_param($param['language']);
            $this->stmt_bind_param($param['speaking_proficiency']);
            $this->stmt_bind_param($param['writing_proficiency']);
            $this->stmt_bind_param($param['applicant_language_proficiency_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("applicant_language_proficiency_id = ?");

            $this->stmt_bind_param($param['applicant_language_proficiency_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function delete_many(&$param = null)
    {
        if($this->stmt_template == '')
        {
            $this->set_query_type('DELETE');
            $this->set_where("applicant_id = ?");

            $this->stmt_bind_param($param['applicant_id']);

            $this->stmt_prepare();
        }
        $this->stmt_execute();
        return $this;
    }

    function select()
    {
        $this->set_query_type('SELECT');
        $this->exec_fetch('array');
        return $this;
    }

    function check_uniqueness($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("applicant_language_proficiency_id = ?");

        $this->stmt_bind_param($param['applicant_language_proficiency_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function check_uniqueness_for_editing($param)
    {
        $this->set_query_type('SELECT');
        $this->set_where("applicant_language_proficiency_id = ? AND (applicant_language_proficiency_id != ?)");

        $this->stmt_bind_param($param['applicant_language_proficiency_id']);
        $this->stmt_bind_param($param['applicant_language_proficiency_id']);

        $this->stmt_prepare();
        $this->stmt_execute();
        $this->stmt_close();

        if($this->num_rows > 0) $this->is_unique = FALSE;
        else $this->is_unique = TRUE;

        return $this;
    }

    function select_record($applicant_id)
    {
        $this->set_where('applicant_id = ?');
        $this->stmt_bind_param($applicant_id);
        $this->stmt_fetch('rowdump');

        return $this;
    }
}
