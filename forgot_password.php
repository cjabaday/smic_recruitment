<?php
require_once "path.php";
init_cobalt();

init_var($_POST['btn_submit']);
if(isset($_SESSION['logged']) && $_SESSION['logged'] == 'Logged')
{
    redirect('start.php');
}

if(isset($_GET['reason']))
{
    if($_GET['reason'] == 'ipchange')
    {
        $error_message = 'You have been logged out because your IP address has changed. Please log in again.';
    }
}


if($_POST['btn_submit'])
{
    extract($_POST);
    //check if email_address is existing user

    $dbh = cobalt_load_class('user');
    $dbh->set_where('personal_email = ?');
    $dbh->stmt_bind_param($username);
    $dbh->stmt_prepare();
    $dbh->stmt_fetch('single');
    $applicant_id = $dbh->dump['applicant_id'];
    $email = $dbh->dump['personal_email'];
    // debug($email_address);
    if($dbh->num_rows > 0)
    {
        //get applicant info
        $dbh = cobalt_load_class('applicant');
        $dbh->set_where('applicant_id = ?');
        $dbh->stmt_bind_param($applicant_id);
        $dbh->stmt_prepare();
        $dbh->stmt_fetch('single');

        $first_name = $dbh->dump['first_name'];
        $middle_name = $dbh->dump['middle_name'];
        $last_name = $dbh->dump['last_name'];

        //generate token
        $token = generate_token(16,'fs');
        //process reset pass
        $dbh = cobalt_load_class('forgot_pass_table');
        $param = array();
        $param['email_address'] = $username;
        $param['token'] = $token;
        $param['date_applied'] = date('Y-m-d');

        // debug($param);
        $dbh->add($param);

        $link = DEFAULT_DB_HOST .'/' . BASE_DIRECTORY . '/reset_password.php?token='.$token;
        $recepient_name = "$first_name $middle_name $last_name";
        $email_subject = "Password Reset";
        $email_body =  "<p>
        Dear <strong>$recepient_name</strong>,<br><br>

        Click this <a href='$link'>link to proceed to password reset</a>.
        </p>

        <p>
        If the link above is not working, copy the following link and paste it on the address bar:<br>
        $link

        <br><br>
        Sincerely,<br>
        Admin Team
        </p>";
        require_once 'components/emailer.php';
        redirect('reset_password.php?from_forgot=Yes');
    }
    else
    {
        $error_message = "User not found.";
    }

}

$html = new html;
?>
<html>

<head>
    <title>SMIC Applicant Portal</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css_applicant_portal/style.css">
</head>

<body onload="document.getElementById('username').focus();">

    <?php
    //HEADER
    require_once 'applicant_portal_header.php';


    echo '<form method="POST" action="' . basename($_SERVER['SCRIPT_NAME']) . '">';
    $form_key = generate_token();
    $form_identifier = $_SERVER['SCRIPT_NAME'];
    $_SESSION['cobalt_form_keys'][$form_identifier] = $form_key;
    echo '<input type="hidden" name="form_key" value="' . $form_key .'">';
    ?>

    <div class="bg_home">
        <div class = "space">
            <?php
            init_var($error_message);
            $_SESSION['icon_set'] = 'cobalt';
            $html->display_error($error_message);
            ?>
        </div>
        <div class = "container">
            <strong>Account Recovery</strong>
            <table border="0" width="50%" cellspacing="1" class="login_text" style="margin-left:auto;margin-right:auto">
            <tr>
                <td align="center">
                <?php $html->draw_text_field('','username',FALSE,'text',FALSE, 'id="username" size="37" autocomplete="off" placeholder="Email Address"'); ?>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <input type=submit value="SUBMIT" name="btn_submit">
                </td>
            </tr>
            </table>
            <hr>
                <p class="link">Forgot your password? Enter your email address and we will send an instruction to you to recover your account.</a></p>


        </div>
    </div>

</body>
</form>
</html>
