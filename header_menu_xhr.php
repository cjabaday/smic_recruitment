<!-- <div class="HeaderBanner">
<?php echo GLOBAL_PROJECT_NAME;?><span>&nbsp;</span>
</div> -->
<style>

li a {
    text-decoration:none;
    color:white;
}

li a:visited {
    color:white;
}

td a {
    text-decoration:none;
}

td a:visited {
}

li a {
    text-decoration:none;
}

li a:visited {
    color:white;
}
.header_menu li{
    display: inline;
    padding: 0 5%;
}

.header_menu li:hover{
    text-decoration: underline;
}
</style>
<div class="class.header" style="background-color:white; width100%; height:7%; padding:1%;">
    <img style="display: inline-block; width:12%; vertical-align: middle;" src="/<?php echo BASE_DIRECTORY?>/images_applicant_portal/smic-logo.jpg">

        <div class="headerdetails" style="display: inline-block; text-align:right; float:right;">
            <div class="headertext" style="display: inline-block;">
            <h3 style="color:#3ca2d1;"><i>Welcome to SMIC Applicant Portal</i></h3>
            <p style="font-size:70%;"><span id ="day"></span> | <span id ="date_today"> </span> | <span id ="time"></span> | </p>
            <!-- <img style="width:7%; vertical-align:middle;" src="images_applicant_portal/cloud.png"> -->
            </div>
            <img src="/<?php echo BASE_DIRECTORY?>/images_applicant_portal/anniv.png" style="padding-left:3%; width:10%; display:inline-block; float:right;">
        </div>
</div>
<!-- <div class='HeaderMenu'>
    <table width="100%">
    <tr>
        <td class="menu" width="100"  onclick="burger_me()"> <a class="menu" href="#"><div id="burger_line" style="
            display: inline-block;
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            -o-transform: rotate(90deg);
            filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
            ">|||</div> MENU  </a> </td>
        <td class="menu" width="100" align="left"> <a <?php echo $target;?> class="menu" href="/<?php echo BASE_DIRECTORY;?>/main.php">  HOME  </a> </td>
        <td class="menu" width="100"> <a <?php echo $target;?> class="menu" href="/<?php echo BASE_DIRECTORY;?>/change_password.php">  PASSWORD  </a> </td>
        <td class="menu" width="100"> <a <?php echo $target;?> class="menu" href="/<?php echo BASE_DIRECTORY;?>/change_skin.php">  SKIN  </a> </td>
        <td class="menu" width="100"> <a <?php echo $target;?> class="menu" href="/<?php echo BASE_DIRECTORY;?>/about.php">  ABOUT  </a> </td>
        <td class="menu" width="100"> <a <?php echo $target;?> class="menu" href="/<?php echo BASE_DIRECTORY;?>/help/contents.php">  HELP  </a> </td>
        <td align="right"><span class="text-normal">You are logged in as</span> <span class="text-info"><?php echo htmlentities($_SESSION['user'], ENT_COMPAT, MULTI_BYTE_ENCODING);?></span>&nbsp;</td>
        <td class="menu" width="75"> <a target="_parent" onClick="return confirm('Are you sure you wish to logout?')" href='/<?php echo BASE_DIRECTORY;?>/end.php' class="menu">  [LOGOUT]  </a> </td>
    </tr>
    </table>
</div> -->

<div class="header_menu" style="width: 100%; height:2%; border-bottom: 2px solid white; background-color:#073784; color:white; padding: 1% 0; text-align:center; vertical-align:middle;">
    <ul>
    <?php

    if(isset($_SESSION['logged']))
    {
    ?>
    <li onclick="burger_me()"> <a href="#"><div id="burger_line" style="
        display: inline-block;
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
        ">|||</div> Menu  </a> </li>
    <?php
    }
    ?>
    <li><a href="/<?php echo BASE_DIRECTORY;

    if(isset($_SESSION['logged']))
    {
        if(check_link('Recruiter access'))
        {
            // brpt();
            echo "/career_page.php";
        }
        else
        {
            echo "/main.php";
        }
    }
    else
    {
        echo "/applicant_home.php";
    }
    ?>">Home</a></li>
    <li><a href="/<?php echo BASE_DIRECTORY;?>/career_page.php">Job Postings</a></li>
    <li><a href="#">Contact Us</a></li>

    <?php

    if(isset($_SESSION['logged']))
    {
    ?>
        <li> <a target="_parent" onClick="return confirm('Are you sure you wish to logout?')" href='/<?php echo BASE_DIRECTORY;?>/end.php'>  Logout  </a> </li>
    <?php
    }
    else
    {
    ?>
        <li><a href="/<?php echo BASE_DIRECTORY;?>/applicant_portal.php">Login</a></li>
    <?php
    }
    ?>
    </ul>
</div>

<script>
var menu_state = "OFF"
var menu_loaded = false;
function burger_me()
{
    var xhr_menu = document.getElementById("xhr_menu");
    var burger = document.getElementById("burger_line");

    if (menu_state == "OFF")
    {
        xhr_menu.style.visibility = 'visible';
        xhr_menu.style.opacity = '1';

        burger.innerHTML = '&nbsp;X&nbsp;';
        burger.style['webkitTransform'] = 'rotate(0deg)';

        menu_state = 'ON';

        if(menu_loaded == false)
        {
            const xhr = new XMLHttpRequest();
            const url = '/<?php echo BASE_DIRECTORY;?>/menus.php';
            xhr.responseType = 'text';
            xhr.onreadystatechange = function()
            {
                if (xhr.readyState === XMLHttpRequest.DONE)
                {
                    console.log(xhr.response);
                    xhr_menu.innerHTML = xhr.response;
                    xhr_menu.style.transition = 'visibility 0.4s, opacity 0.4s';
                }
            };
            xhr.open('GET', url);
            xhr.send();
            menu_loaded = true;
        }
    }
    else
    {
        xhr_menu.style.visibility = 'hidden';
        xhr_menu.style.opacity = '0';
        burger.innerHTML = '|||';
        burger.style['webkitTransform'] = 'rotate(90deg)';
        menu_state = 'OFF';
    }
}

// Get Day, Date, and Time
var date = new Date();
var day = date.getDay();
var weekday = new Array(7);
var months = new Array(12);
weekday = ["Sunday", "Monday","Tuesday", "Wednesday", "Thursday","Friday","Saturday"];
months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var month_string = months[date.getMonth()];
var date_today = month_string + " " + date.getDate() + " " + date.getFullYear();
var t = date.toLocaleTimeString();
var time = setInterval(timeAMPM, 1000);

function timeAMPM() {
    var date = new Date();
    var t = date.toLocaleTimeString();
    document.getElementById("time").innerHTML = t;
}

document.getElementById("day").innerHTML = weekday[day];
document.getElementById("date_today").innerHTML = date_today;

document.getElementById("time").innerHTML = t;

</script>
<style>
@keyframes example {
    0%   {background-color: red;}
    25%  {background-color: yellow;}
    50%  {background-color: blue;}
    100% {background-color: green;}
}

div#xhr_menu {

}
</style>
<div id="xhr_menu" style="position: absolute;
                          visibility: hidden;
                          opacity: 0;
                          transition: visibility 0.8s, opacity 0.8s;
                          background-color: gray;
                          border: 1px solid gray;
                          padding: 5px;
                          top:120px;
                          left:0px;
                          min-width: 200px;
                          height:auto;
                          max-height:400px;
                          border-radius:10px;
                          overflow-y: auto" >
</div>
