<?php
require 'path.php';
init_cobalt('ALLOW_ALL',FALSE);

function menuGroupWindowHeader($group)
{
    echo '<table width="180" class="listView">
          <tr class="listRowHead"><td>' . $group . '</td></tr>';
}

function menuGroupWindowFooter()
{
    echo "</table>";
}

require_once $_SESSION['header'];

$d = new data_abstraction;
$d->set_fields('a.link_id, a.descriptive_title, a.target, a.description, c.passport_group')
         ->set_table('user_links a, user_passport b, user_passport_groups c')
         ->set_where("a.link_id=b.link_id AND b.username = ? AND a.passport_group_id=c.passport_group_id AND a.show_in_tasklist='Yes' AND a.status='On'")
         ->set_order('c.priority DESC, c.passport_group, a.priority DESC, a.descriptive_title');
$d->stmt_bind_param($_SESSION['user']);
$d->stmt_prepare();
$data = $d->stmt_fetch('rowdump')->dump;
$num_rows = $d->num_rows;
$d->close_db();

$menu_links = array();
for($a = 0; $a < $num_rows; ++$a)
{
    extract($data[$a]);
    $menu_links[$passport_group]['link_id'][]     = $link_id;
    $menu_links[$passport_group]['title'][]       = $descriptive_title;
    $menu_links[$passport_group]['target'][]      = $target;
    $menu_links[$passport_group]['description'][] = $description;
    unset($data[$a]);
}

$current_group='';
foreach($menu_links as $group => $link_info)
{
    if($current_group=='')
    {
        $current_group = $group;
        menuGroupWindowHeader($group);
    }
    for($a=0; $a<count($link_info['title']); ++$a)
    {
        if($current_group!= $group)
        {
            menuGroupWindowFooter();
            menuGroupWindowHeader($group);
            $current_group = $group;
        }

        if($a%2==0) $class='listRowOdd';
        else $class='listRowEven';
        echo "<tr class=\"$class\"><td><a href='/" . BASE_DIRECTORY . "/{$link_info['target'][$a]}' target='content_frame' class='sidebar'> {$link_info['title'][$a]} </a></td></tr>";
    }
}
menuGroupWindowFooter();
?>
</body>
</html>
