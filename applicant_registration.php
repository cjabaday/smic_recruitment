<?php
require 'path.php';

init_cobalt('');

init_var($_POST['btn_submit']);
init_var($_POST['btn_accept']);
init_var($_POST['btn_back']);
init_var($_POST['btn_cancel']);
init_var($_GET['show_consent']);
init_var($_GET['show_verification']);
init_var($_GET['show_verified']);


if($_GET['show_verified'] == 'Yes')
{
    $id = $_GET['id'];
    $token = $_GET['token'];

    //check if link tmep user is valid
    $dbh = cobalt_load_class('temp_user');
    $dbh->set_where('temp_user_id = ? AND token = ?');
    $dbh->stmt_bind_param($id);
    $dbh->stmt_bind_param($token);
    $dbh->stmt_fetch('single');

    if($dbh->num_rows > 0)
    {
        // fetch details in tempuser
        extract($dbh->dump);
        require_once 'components/generate_formatted_number.php';
        $param['applicant_number'] = $applicant_number;
        $param['first_name'] = $first_name;
        $param['last_name'] = $last_name;
        $param['middle_name'] = $middle_name;
        $param['gender'] = $gender;

        //add basic details in applicant
        $d = cobalt_load_class('applicant');
        $d->applicant_registration($param);
        $applicant_id = $d->auto_id;
        // debug($applicant_id);
        // die();
        // add in user table
        $param['username'] = $personal_email;
        $param['personal_email'] = $personal_email;
        $param['password'] = $password;
        $param['salt'] = $salt;
        $param['iteration'] = $iteration;
        $param['method'] = $method;
        $param['role_id'] = 3;
        $param['skin_id'] = 1;
        $param['user_level'] = 5;
        $param['applicant_id'] = $applicant_id;

        $dbh_user = cobalt_load_class('user');
        $dbh_user->add($param);
        //Permissions from role, if role was chosen
        extract($param);
        if($role_id != '')
        {
            $d = cobalt_load_class('user_passport');
            $d->stmt_template = "INSERT INTO user_passport
                                    SELECT ?, link_id FROM user_role_links WHERE role_id = ?";
            $d->stmt_bind_param($username);
            $d->stmt_bind_param($role_id);
            $d->stmt_prepare();
            $d->stmt_execute();
        }
        //delete record in temp_user
        $dbh_temp = cobalt_load_class('temp_user');
        $dbh_temp->delete($id);
        // die();
    }
    else
    {

        redirect(LOGIN_PAGE);
    }

}
if($_POST['btn_back'] || $_POST['btn_cancel'])
{
    redirect(LOGIN_PAGE);
}

if($_POST['btn_accept'])
{
    redirect('applicant_registration.php');
}


if($_POST['btn_submit'])
{

    extract($_POST);

    $message = "";
    $message_type = "error";
    extract($_POST);

    if(empty($_POST['first_name']))
    {
     $message = "Please fill out all fields<br>";
    }
    elseif(empty($_POST['middle_name']))
    {
     $message = "Please fill out all fields<br>";
    }
    elseif(empty($_POST['last_name']))
    {
     $message = "Please fill out all fields<br>";
    }
    elseif(empty($_POST['gender']))
    {
     $message = "Please fill out all fields<br>";
    }
    elseif(empty($_POST['email']))
    {
     $message = "Please fill out all fields<br>";
    }
    elseif(empty($_POST['password']))
    {
     $message = "Please fill out all fields<br>";
    }



    $dbh = cobalt_load_class('user');
    $dbh->set_where('username = ? AND personal_email = ?');
    $dbh->stmt_bind_param($email);
    $dbh->stmt_bind_param($email);
    $dbh->stmt_prepare();
    $dbh->stmt_fetch('single');

    if($dbh->num_rows > 0)
    {
        $message .= 'Email Address already used.<br>';
    }

    if($_POST['password'] == $_POST['confirm_password'])
    {
        //do nothing..
        // brpt();
    }
    else
    {
        // brpt();

        $message .= "Password does not match<br>";
    }



    if($message=="")
    {
        $token = generate_token(16,'fs');
        // require_once 'components/generate_formatted_number.php';
        // $param['applicant_number'] = $applicant_number;
        $param['first_name'] = $first_name;
        $param['last_name'] = $last_name;
        $param['middle_name'] = $middle_name;
        $param['gender'] = $gender;

        // $d = cobalt_load_class('applicant');
        // $d->applicant_registration($param);
        // $applicant_id = $d->auto_id;

        require 'password_crypto.php';
        //Hash the password using default Cobalt password hashing technique
        $hashed_password = cobalt_password_hash('NEW',$password, $email, $new_salt, $new_iteration, $new_method);

        $param['username'] = $email;
        $param['personal_email'] = $email;
        $param['password'] = $hashed_password;
        $param['salt'] = $new_salt;
        $param['iteration'] = $new_iteration;
        $param['method'] = $new_method;
        // $param['role_id'] = 3;
        // $param['skin_id'] = 1;
        // $param['user_level'] = 5;
        // $param['applicant_id'] = $applicant_id;
        // $param['is_verified'] = 'No';
        $param['token'] = $token;

        $dbh_user = cobalt_load_class('temp_user');
        $dbh_user->add($param);
        $temp_user_id = $dbh_user->auto_id;
        //Permissions from role, if role was chosen
        extract($param);
        // if($role_id != '')
        // {
        //     $d = cobalt_load_class('user_passport');
        //     $d->stmt_template = "INSERT INTO user_passport
        //                             SELECT ?, link_id FROM user_role_links WHERE role_id = ?";
        //     $d->stmt_bind_param($username);
        //     $d->stmt_bind_param($role_id);
        //     $d->stmt_prepare();
        //     $d->stmt_execute();
        // }

        //define paramaters to pass in email here
        $link = HOST_NAME .'/' . BASE_DIRECTORY . '/applicant_registration.php?show_verified=Yes&id='.$temp_user_id.'&token='.$token;
        $recepient_name = "$first_name $middle_name $last_name";
        $email_subject = "Email Verification";
        $email_body =  "<p>
        Dear <strong>$recepient_name</strong>,<br><br>

        This email address is being used to register an account to SMIC Application. If you initiated the registration process, click this <a href='$link'>link to activate your account</a>.
        <br>
        If the link above is not working, copy the following link and paste it on the address bar:<br>
        $link
        <br><br>
        If you did not initiate a registration with us, it is possible that someone else is trying to use your email account. Kindly disregard this.
        <br><br>
        <br>
        <i>This is a system generated message, do not reply.</i>
        </p>";
        require_once 'components/emailer.php';
        redirect("applicant_registration.php?show_verification=Yes");
    }
    // debug($message);
}

?>

<?php


$_SESSION['header'] = 'skins/default_header.php';
$_SESSION['colors_css'] = 'cobalt_colors.css';
$_SESSION['fonts_css'] = 'cobalt_fonts.css';
$_SESSION['master_css'] = 'cobalt_master.css';
$_SESSION['override_css'] = 'cobalt_override.css';

?>
<style>
.header_menu li{
    display: inline;
    padding: 0 5%;
}

.header_menu li:hover{
    text-decoration: underline;
}

li a {
    text-decoration:none;
}

li a:visited {
    color:white;
}

td a {
    text-decoration:none;
}

td a:visited {
    color:blue;
}

.link a {
    text-decoration:none;
}

.link a:visited {
    color:blue;
}
</style>
<?php
$html = cobalt_load_class('person_html');
echo '<div class="bg_home">';
$html->draw_header('',$message,$message_type);
?>

<style>
    /* body {
        background-image: url("images_applicant_portal/bgpic.png");
        background-repeat: no-repeat;
        background-size: 100%;
    } */

    .bg_home{
        max-width: 100%;
        height: 100%;
        background-image: url("images_applicant_portal/bgpic.png");
        background-repeat: no-repeat;
        background-position: top;
        background-size:cover;
        /* background-color:#073784; */
        background-color: white;

    }

    fieldset.container_invisible {
        background-color: rgba(0,0,0,0);
    }
</style>
<?php


$html->draw_container_div_start();

$html->draw_fieldset_header('Applicant Registration');
$html->draw_fieldset_body_start();

// $html_person = cobalt_load_class('person_html');
echo '<table>';
$html->draw_field('first_name');
$html->draw_field('middle_name');
$html->draw_field('last_name');
$html->draw_field('gender');
$html->draw_text_field('Personal Email', 'email', FALSE, 'text', TRUE, 'size="60"');

echo '<tr><td align="right">Password:</td><td><input type="password" name="password" id="password" size="60" tabindex = "'.++$html->tabindex.'"></td></tr>';
echo '<tr><td align="right">Confirm Password:</td><td><input type="password" name="confirm_password" id="confirm_password" size="60" tabindex = "'.++$html->tabindex.'"></td></tr>';

echo '</table>';
$html->draw_fieldset_body_end();
$html->draw_fieldset_footer_start();
$html->draw_submit_cancel();
$html->draw_fieldset_footer_end();

if($_GET['show_consent'] == 'Yes')
{
    require_once 'components/show_consent_modal.php';
}

if($_GET['show_verification'] == 'Yes')
{
    require_once 'components/show_verification.php';
}

if($_GET['show_verified'] == 'Yes')
{
    require_once 'components/show_verified.php';
}

$html->draw_container_div_end();
echo '</div>';
?>
