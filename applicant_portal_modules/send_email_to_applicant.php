<?php
require_once 'path.php';

init_cobalt('');

if(isset($_GET['iprf_staffrequest_applicant_id']))
{
    $iprf_staffrequest_applicant_id = $_GET['iprf_staffrequest_applicant_id'];
    $iprf_get_id = $_GET['iprf_get_id'];
    $d = cobalt_load_class('iprf_staffrequest_applicants');
    $d->fetch_iprf_applicants_details($iprf_staffrequest_applicant_id);
    extract($d->dump);
    // debug($d->dump);
    $temp_data = explode(' - ',$updates);
    if(count($temp_data) > 1)
    {
        $old_date = $temp_data[1];
        $old_time = $temp_data[2];
    }
    else
    {



    }
    // debug($iprf_staffrequest_id);
    // debug($iprf_staffrequest_applicant_id);
    // debug($applicant_id);
}

if(xsrf_guard())
{
    init_var($_POST['btn_cancel']);
    extract($_POST);

    $d = cobalt_load_class('iprf_staffrequest_applicants');
    $d->fetch_iprf_applicants_details($iprf_staffrequest_applicant_id);
    extract($d->dump);
    // debug($d->dump);

    if($_POST['btn_cancel'])
    {
        redirect('../modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php?iprf_staffrequest_id='.$iprf_get_id);
    }

    if($_POST['btn_submit'])
    {
        $message = "";

        require_once 'components/format_date.php';

        extract($_POST);

        if($date == "")
        {
            $message .= "No date selected<br>";
        }
        else
        {
            if($date > date('Y-m-d'))
            {
                // do nothing..
            }
            else
            {
                $message = "Invalid Schedule Date: must be ahead of current date<br>";
            }
        }

        if($time == "")
        {
            $message .= "No time selected";
        }


        if($message == "")
        {
            $formatted_time = format_time($time);
            $formatted_date = format_date($date);


            $d = cobalt_load_class('user');
            $dbh = cobalt_load_class('user');
            $dbh->set_where('applicant_id = ?');
            $dbh->stmt_bind_param($applicant_id);
            $dbh->stmt_prepare();
            $personal_email = $dbh->stmt_fetch('single')->dump['username'];

            //send email

            $dbh = cobalt_load_class('iprf_staffrequest_applicants');
            $param = [
                      'iprf_staffrequest_applicant_id' => $iprf_staffrequest_applicant_id,
                      'updates'                        => "Invitation Sent - $date - $time"
                    ];
                    // debug($param);
            $dbh->edit_updates($param);

            $recepient_name = "$first_name $middle_name $last_name";
            $email          = $personal_email;
            $email_subject  = "Interview Invitation from SM Investments Corporation";
            $email_body     = "<strong>Good Day Mr. / Ms. $recepient_name</strong><br><br>
                               Thank you for showing your interest in applying to SM Investments Corporation.<br>
                               <br>
                               We have received your application for the <strong>$title</strong> and based on your profile, you seem to be a
                               good fit for the role. In this regard, we would like to invite you for the HR assessment on the following schedule:<br>
                               <br>
                               Date: $formatted_date<br>
                               Time: $formatted_time<br>
                               <br>
                               <strong>Our head office is located at 10th Floor One Ecom Center, Palm Coast Avenue, Mall of Asia Complex, Pasay City
                               (make sure to use of the entrance beside BDO).<br>
                               <br>
                               <u>Reminders:</u></strong><br>
                               <ul>
                               <li>Come in your proper business / corporate attire.</li>
                               <li>Please be at the head office 15 minutes prior to your scheduled HR assessment.</li>
                                <li>Bring 1 valid identification card.</li>
                               </ul>
                               <br>
                               Sincerely,<br>
                               SMIC Recruitment";

            if($old_date == "")
            {
                // do nothing.
            }
            else
            {
                $email_subject = "Reschedule of Interview Invitation";
                $email_body    = "<strong>Good Day Mr. / Ms. $recepient_name</strong><br><br>
                                   We would like to inform you that there has been a changed in the schedule of the interview for the position $title .<br>
                                   <br>
                                   Please see the following details for the new schedule of your interview:<br>
                                   <br>
                                   Date: $formatted_date<br>
                                   Time: $formatted_time<br>
                                   <br>
                                   <strong>Our head office is located at 10th Floor One Ecom Center, Palm Coast Avenue, Mall of Asia Complex, Pasay City
                                   (make sure to use of the entrance beside BDO).<br>
                                   <br>
                                   <u>Reminders:</u></strong><br>
                                   <ul>
                                   <li>Come in your proper business / corporate attire.</li>
                                   <li>Please be at the head office 15 minutes prior to your scheduled HR assessment.</li>
                                    <li>Bring 1 valid identification card.</li>
                                   </ul>
                                   <br>
                                   Sincerely,<br>
                                   SMIC Recruitment";
            }

            require_once 'components/emailer.php';

            redirect('../modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php?iprf_staffrequest_id='.$iprf_get_id.'&invitation=yes');

        }

    }
}
$html = new html;
?>
<style>
.table_sched td{
    padding:5px;
}
</style>
<?php
$html->draw_header('Send Interview Invitation',$message);

$html->draw_hidden('iprf_staffrequest_applicant_id');
// debug($first_name);
$html->draw_hidden('iprf_get_id');
$html->draw_hidden('applicant_id');
$html->draw_hidden('old_date');
$html->draw_hidden('old_time');
$html->draw_container_div_start();
$html->draw_fieldset_header('Select Schedule');
$html->draw_fieldset_body_start();
echo "<table class='table_sched'>";
    echo "<tr><td align='right'>Interviewee: </td><td>$last_name, $first_name $middle_name</td></tr>";
    echo "<tr><td align='right'>Position: </td><td>$title</td></tr>";
    echo "<tr><td align='right'>Date: </td><td><input type='date' name='date'></td></tr>";
    echo "<tr><td align='right'>Time: </td><td><input type='time' name='time'></td></tr>";
echo "</table>";
$html->draw_fieldset_body_end();
$html->draw_fieldset_footer_start();
$html->draw_submit_cancel();
$html->draw_fieldset_footer_end();
$html->draw_container_div_end();
