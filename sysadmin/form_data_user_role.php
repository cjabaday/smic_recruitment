<?php
require 'components/get_listview_referrer.php';

require 'subclasses/user_role.php';
$dbh_user_role = new user_role;
$dbh_user_role->set_where("role_id= ?");
$dbh_user_role->stmt_bind_param($role_id);

$data = $dbh_user_role->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}
