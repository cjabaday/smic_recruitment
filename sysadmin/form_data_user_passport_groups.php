<?php
require 'components/get_listview_referrer.php';

require 'subclasses/user_passport_groups.php';
$dbh_user_passport_groups = new user_passport_groups;
$dbh_user_passport_groups->set_where("passport_group_id= ?");
$dbh_user_passport_groups->stmt_bind_param($passport_group_id);

$data = $dbh_user_passport_groups->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}
