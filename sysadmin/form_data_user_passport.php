<?php
require 'components/get_listview_referrer.php';

require 'subclasses/user_passport.php';
$dbh_user_passport = new user_passport;
$dbh_user_passport->set_where("username= ? AND link_id= ?");
$dbh_user_passport->stmt_bind_param($username);
$dbh_user_passport->stmt_bind_param($link_id);

$data = $dbh_user_passport->stmt_prepare()->stmt_fetch('single')->dump;
if(is_array($data))
{
    extract($data);
}
