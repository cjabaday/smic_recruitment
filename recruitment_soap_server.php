<?php
require 'path.php';
init_cobalt();

$server = new SoapServer('http://csorepo.apc.edu.ph/smic_recruitment/recruitment_soap.wsdl',
                         ['uri'=>'urn://smic_recruitment',
                          'cache_wsdl'=>WSDL_CACHE_NONE]);
$server->addFunction('send_department');
$server->addFunction('send_rank');
$server->addFunction('send_classification');
$server->addFunction('send_company');
$server->addFunction('send_position');
$server->addFunction('send_prf');
$server->handle();


function send_department($value, $id)
{
    $d = cobalt_load_class('department');
    $param = ['department_id' => $id , 'name' => $value];
    if($d->check_uniqueness_label($param)->is_unique)
    {
        // insert record
        $d->add_department($param);
        $result = "Department Added<br>";
    }
    else
    {
        $result = "Department Already Exists<br>";
    }

    return $result;

}

function send_rank($value, $id)
{
    $d = cobalt_load_class('rank');
    $param = ['rank_id' => $id , 'name' => $value];
    if($d->check_uniqueness_label($param)->is_unique)
    {
        // insert record
        $d->add_rank($param);
        $result = "Rank Added<br>";
    }
    else
    {
        $result = "Rank Already Exists<br>";
    }
    return $result;

}

function send_classification($value, $id)
{
    $d = cobalt_load_class('classification');
    $param = ['classification_id' => $id , 'name' => $value];
    if($d->check_uniqueness_label($param)->is_unique)
    {
        // insert record
        $d->add_classification($param);
        $result = "Classification Added<br>";
    }
    else
    {

        $result = "Classification Already Exists<br>";
    }
    return $result;

}

function send_company($value, $id)
{
    $d = cobalt_load_class('company');
    $param = ['company_id' => $id , 'official_name' => $value];
    if($d->check_uniqueness_label($param)->is_unique)
    {
        // insert record
        $d->add_company($param);
        $result = "Company Added<br>";
    }
    else
    {
        $result = "Company Already Exists<br>";
    }
    return $result;

}

function send_position($value, $id)
{
    $d = cobalt_load_class('position');
    $param = ['position_id' => $id , 'title' => $value];
    if($d->check_uniqueness_label($param)->is_unique)
    {
        // insert record
        $d->add_position($param);
        $result = "Position Added <br>";
    }
    else
    {
        $result = "Position Already Exists <br>";
    }
    return $result;

}

function send_prf($iprf_staffrequest_id, $PRF_No, $company_id, $department_id, $position_id, $rank_id, $classification_id, $status, $duration, $responsibility, $general_duties, $detailed_duties, $education, $professional_eligibility_skills, $experience, $skills, $date_needed, $date_filed)
{
    //$arr_prf consists of multiple arrays
    // this method expects array for company, department, position, rank, and classification for set up tables
    // this also expects array for the prf_details
    require_once 'components/format_date.php';
    $temp_date_needed = explode('/',$date_needed);
    $date_needed = format_date($temp_date_needed[2],$temp_date_needed[0],$temp_date_needed[1],'Y-m-d');
    $temp_date_filed = explode('/',$date_filed);
    $date_filed = format_date($temp_date_filed[2],$temp_date_filed[0],$temp_date_filed[1],'Y-m-d');
    $param = [
        'iprf_staffrequest_id' => $iprf_staffrequest_id,
        'PRF_No' => $PRF_No,
        'company_id' => $company_id,
        'department_id' => $department_id,
        'position_id' => $position_id,
        'rank_id' => $rank_id,
        'classification_id' => $classification_id,
        'status' => $status,
        'duration' => $duration,
        'responsibility' => $responsibility,
        'general_duties' => $general_duties,
        'detailed_duties' => $detailed_duties,
        'education' => $education,
        'professional_eligibility_skills' => $professional_eligibility_skills,
        'experience' => $experience,
        'skills' => $skills,
        'date_needed' => $date_needed,
        'date_filed' => $date_filed,
];
    //check if set up table records are existing
    $d = cobalt_load_class('iprf_staffrequest');
    if($d->check_uniqueness($param)->is_unique)
    {
        $d->add_iprf($param);

        return "/".DEFAULT_DB_HOST."/".BASE_DIRECTORY."/career_page.php";
        // return var_dump($param);
    }
    else
    {

        return 'duplicate';
    }

}

//
// // //comment out, only for testing
// $company_id = 1;
// $company = 'SM Investments Corporation';
//
// $department_id = 1;
// $department = 'Human Resources';
//
// $position_id = 2;
// $position = 'Compensation and Benefits Staff';
//
// $rank_id = 1;
// $rank = "Rank and File";
//
// $classification_id = 1;
// $classification = "Rank and File";
//
// $iprf_staffrequest_id            = 5; //change these if iprf_staffrequest_id error appears
// $PRF_No                          = 'PRF-2018-00002';
// $status                          = 'In progress';
// $duration                        = 'Sample Duration Data';
// $responsibility                  = 'Collects and process compensation and benefits of employees';
// $general_duties                  = 'Collects and process compensation and benefits of employees in general';
// $detailed_duties                 = 'Collects and process compensation and benefits of employees in detailed';
// $education                       = "Must have be college graduate";
// $professional_eligibility_skills = "Must clerical and attentive";
// $experience                      = "Nothing in Particular.";
// $skills                          = 'N/A';
// $date_needed                     = '2018-11-01';
// $date_filed                      = '2018-10-20';
//
// echo send_department($department, $department_id);
// echo send_rank($rank, $rank_id);
// echo send_classification($classification, $classification_id);
// echo send_company($company, $company_id);
// echo send_position($position, $position_id);
//
// send_prf($iprf_staffrequest_id, $PRF_No, $company_id, $department_id, $position_id, $rank_id, $classification_id, $status, $duration, $responsibility, $general_duties, $detailed_duties, $education, $professional_eligibility_skills, $experience, $skills, $date_needed, $date_filed);
