-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2018 at 02:45 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smic_recruitment`
--

-- --------------------------------------------------------

--
-- Table structure for table `action_notice`
--

CREATE TABLE `action_notice` (
  `action_notice_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `action_notice_date` date DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `applicant`
--

CREATE TABLE `applicant` (
  `applicant_id` int(11) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `current_application_status` varchar(255) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `civil_status` int(11) DEFAULT NULL,
  `citizenship` int(11) DEFAULT NULL,
  `date_of_birth` varchar(255) DEFAULT NULL,
  `height` varchar(255) DEFAULT NULL,
  `weight` varchar(255) DEFAULT NULL,
  `blood_type` int(11) DEFAULT NULL,
  `birth_place` varchar(255) DEFAULT NULL,
  `present_address_line_1` varchar(255) DEFAULT NULL,
  `present_address_line_2` varchar(255) DEFAULT NULL,
  `present_address_barangay` varchar(255) DEFAULT NULL,
  `present_address_city_municipality_id` int(11) DEFAULT NULL,
  `present_address_province_id` int(11) DEFAULT NULL,
  `provincial_address_line_1` varchar(255) DEFAULT NULL,
  `provincial_address_line_2` varchar(255) DEFAULT NULL,
  `provincial_address_barangay` varchar(255) DEFAULT NULL,
  `provincial_address_city_municipality_id` int(11) DEFAULT NULL,
  `provincial_address_province_id` int(11) DEFAULT NULL,
  `provincial_address_contact_number` varchar(255) NOT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `sss_number` varchar(255) DEFAULT NULL,
  `tin` varchar(255) DEFAULT NULL,
  `philhealth_id_number` varchar(255) DEFAULT NULL,
  `hdmf_number` varchar(255) DEFAULT NULL,
  `umid_number` varchar(255) DEFAULT NULL,
  `religion` int(11) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_relationship` varchar(255) DEFAULT NULL,
  `contact_address` varchar(255) DEFAULT NULL,
  `contact_contact_number` varchar(255) DEFAULT NULL,
  `applicant_number` varchar(255) DEFAULT NULL,
  `employee_id` int(11) NOT NULL,
  `personnel_requisition_id` int(11) NOT NULL,
  `application_date` date DEFAULT NULL,
  `application_status` varchar(255) DEFAULT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `applicant`
--

INSERT INTO `applicant` (`applicant_id`, `last_name`, `first_name`, `middle_name`, `nickname`, `current_application_status`, `company_id`, `source`, `email_address`, `gender`, `civil_status`, `citizenship`, `date_of_birth`, `height`, `weight`, `blood_type`, `birth_place`, `present_address_line_1`, `present_address_line_2`, `present_address_barangay`, `present_address_city_municipality_id`, `present_address_province_id`, `provincial_address_line_1`, `provincial_address_line_2`, `provincial_address_barangay`, `provincial_address_city_municipality_id`, `provincial_address_province_id`, `provincial_address_contact_number`, `contact_number`, `sss_number`, `tin`, `philhealth_id_number`, `hdmf_number`, `umid_number`, `religion`, `contact_name`, `contact_relationship`, `contact_address`, `contact_contact_number`, `applicant_number`, `employee_id`, `personnel_requisition_id`, `application_date`, `application_status`, `image`) VALUES
(18, 'Fernandez', 'Mark', 'Danico', 'Nico', '', 0, '', '', 0, 4, 5, '2000-09-27', '175', '62', 1, 'Sta. Cruz, Manila', 'Block 41 Kenneth Road', 'Eusebio Avenue', 'Pinagbuhatan', 2, 7, 'Block 41 Kenneth Road', 'Eusebio Avenue', 'Pinagbuhatan', 2, 7, '952-62-35', '952-62-35', '123452345612', '123445764567', '234553411', '1234563456', '', 0, 'Amelia Fernandez', '2', 'Same', '952-62-35', 'APL-1809-00018', 0, 0, '0000-00-00', '', '42431d51342b28d2bebb0673abe846200cdeb9b015134615_1778595462469230_9126298284826496227_n.jpg'),
(19, 'Fernan', 'Nico', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'APL-1810-00002', 0, 0, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `applicant_attachments`
--

CREATE TABLE `applicant_attachments` (
  `applicant_attachment_id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `attachments` varchar(255) NOT NULL,
  `date_uploaded` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `applicant_attachments`
--

INSERT INTO `applicant_attachments` (`applicant_attachment_id`, `applicant_id`, `attachments`, `date_uploaded`) VALUES
(1, 18, 'cf1a511fe40014b5d771c1db3b01a6640c4fdbb916512865-Rank-Icons-Stock-Vector-icon.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `applicant_exam`
--

CREATE TABLE `applicant_exam` (
  `applicant_exam_id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `score` varchar(255) DEFAULT NULL,
  `remarks` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `applicant_family_members`
--

CREATE TABLE `applicant_family_members` (
  `applicant_family_member_id` int(11) NOT NULL,
  `applicant_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `relationship` int(11) DEFAULT NULL,
  `is_dependent` char(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `applicant_interview`
--

CREATE TABLE `applicant_interview` (
  `applicant_interview_id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `interviewer_employee_id` int(11) NOT NULL,
  `interview_date` date DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `applicant_languages_proficiency`
--

CREATE TABLE `applicant_languages_proficiency` (
  `applicant_language_proficiency_id` int(11) NOT NULL,
  `applicant_id` int(11) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `speaking_proficiency` varchar(255) DEFAULT NULL,
  `writing_proficiency` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `applicant_license`
--

CREATE TABLE `applicant_license` (
  `applicant_license_id` int(11) NOT NULL,
  `applicant_id` int(11) DEFAULT NULL,
  `license` varchar(255) DEFAULT NULL,
  `license_number` varchar(255) DEFAULT NULL,
  `license_expiry` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `applicant_other_declarations`
--

CREATE TABLE `applicant_other_declarations` (
  `applicant_other_declaration_id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `crime_convict` char(3) NOT NULL,
  `details` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `applicant_preferred_positions`
--

CREATE TABLE `applicant_preferred_positions` (
  `applicant_preferred_position_id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `priority` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `applicant_previous_employers`
--

CREATE TABLE `applicant_previous_employers` (
  `applicant_previous_employer_id` int(11) NOT NULL,
  `applicant_id` int(11) DEFAULT NULL,
  `previous_employer_name` varchar(255) DEFAULT NULL,
  `previous_employer_position` varchar(255) DEFAULT NULL,
  `previous_employer_date_from` varchar(255) DEFAULT NULL,
  `previous_employer_date_to` varchar(255) DEFAULT NULL,
  `address` text,
  `job_description` text,
  `basic_salary` decimal(12,2) NOT NULL,
  `reason_for_leaving` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `applicant_reference`
--

CREATE TABLE `applicant_reference` (
  `applicant_reference_id` int(11) NOT NULL,
  `applicant_id` int(11) DEFAULT NULL,
  `reference_name` varchar(255) DEFAULT NULL,
  `reference_occupation` varchar(255) DEFAULT NULL,
  `reference_relationship` varchar(255) DEFAULT NULL,
  `reference_additional_information` varchar(255) DEFAULT NULL,
  `reference_address` text NOT NULL,
  `reference_contact_number` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `applicant_school_attended`
--

CREATE TABLE `applicant_school_attended` (
  `applicant_school_attended_id` int(11) NOT NULL,
  `applicant_id` int(11) DEFAULT NULL,
  `school_id` int(11) DEFAULT NULL,
  `educational_attainment` int(11) NOT NULL,
  `course` int(11) DEFAULT NULL,
  `awards` text NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `date_from` varchar(255) DEFAULT NULL,
  `date_to` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `applicant_skills`
--

CREATE TABLE `applicant_skills` (
  `applicant_skill_id` int(11) NOT NULL,
  `applicant_id` int(11) DEFAULT NULL,
  `skill` varchar(255) DEFAULT NULL,
  `proficiency` int(11) DEFAULT NULL,
  `years_of_experience` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `applicant_trainings`
--

CREATE TABLE `applicant_trainings` (
  `applicant_training_id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `training_title` varchar(255) NOT NULL,
  `training_provider` varchar(255) NOT NULL,
  `training_date` varchar(255) NOT NULL,
  `venue` varchar(255) NOT NULL,
  `no_of_hours` int(11) NOT NULL,
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `branch_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `building_id` int(11) NOT NULL,
  `branch_type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `head` int(11) NOT NULL,
  `assistant` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `branch_type`
--

CREATE TABLE `branch_type` (
  `branch_type_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `building`
--

CREATE TABLE `building` (
  `building_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `calendar_event`
--

CREATE TABLE `calendar_event` (
  `calendar_event_id` int(11) NOT NULL,
  `event_name` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `start_time` varchar(255) DEFAULT NULL,
  `end_time` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `calendar_event_type_id` int(11) NOT NULL,
  `identifier_id` int(11) DEFAULT NULL,
  `identifier_table` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `calendar_event_type`
--

CREATE TABLE `calendar_event_type` (
  `calendar_event_type_id` int(11) NOT NULL,
  `event_type` varchar(255) DEFAULT NULL,
  `event_color` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `region_id`, `province_id`, `city_name`, `city_code`) VALUES
(2, 2, 7, 'City Of Pasig', '137403000'),
(3, 2, 6, 'City Of Makati ', '137602000'),
(4, 2, 6, 'City Of Paranaque ', '137604000'),
(6, 2, 5, 'Quiapo, Manila', '133903000'),
(7, 2, 5, 'Tondo I / II ', '133901000'),
(8, 2, 5, 'Binondo', '133902000'),
(10, 2, 5, 'San Nicolas ', '133904000'),
(11, 2, 5, 'Santa Cruz ', '133905000'),
(12, 2, 5, 'Sampaloc ', '133906000'),
(13, 2, 5, 'San Miguel ', '133907000'),
(14, 2, 5, 'Ermita ', '133908000'),
(15, 2, 5, 'Intramuros ', '133909000'),
(16, 2, 5, 'Malate ', '133910000'),
(17, 2, 5, 'Paco ', '133911000'),
(18, 2, 5, 'Pandacan', '133912000'),
(19, 2, 5, 'Port Area ', '133913000'),
(20, 2, 5, 'Santa Ana ', '133914000'),
(21, 2, 6, 'City Of Las Pinas ', '137601000'),
(23, 2, 6, 'City Of Muntinlupa ', '137603000'),
(25, 2, 6, 'Pasay City ', '137605000'),
(26, 2, 6, 'Pateros ', '137606000'),
(27, 2, 6, 'Taguig City ', '137607000'),
(28, 2, 7, 'City Of Mandaluyong ', '137401000'),
(29, 2, 7, 'City Of Marikina ', '137402000'),
(31, 2, 7, 'Quezon City ', '137404000'),
(32, 2, 7, 'City Of San Juan ', '137405000'),
(33, 2, 8, 'Caloocan City ', '137501000'),
(34, 2, 8, 'City Of Malabon ', '137502000'),
(35, 2, 8, 'City Of Navotas ', '137503000'),
(36, 2, 8, 'City Of Valenzuela ', '137504000'),
(37, 5, 9, 'BACOLOD CITY (Capital)', '184501000'),
(38, 5, 9, 'BAGO CITY', '184502000'),
(39, 5, 9, 'BINALBAGAN', '184503000'),
(40, 5, 9, 'CADIZ CITY', '184504000'),
(41, 5, 9, 'CALATRAVA', '184505000'),
(42, 5, 9, 'CANDONI', '184506000'),
(43, 5, 9, 'CAUAYAN', '184507000'),
(44, 5, 9, 'ENRIQUE B. MAGALONA (SARAVIA)', '184508000'),
(45, 5, 9, 'CITY OF ESCALANTE', '184509000'),
(46, 5, 9, 'CITY OF HIMAMAYLAN', '184510000'),
(47, 5, 9, 'HINIGARAN', '184511000'),
(48, 5, 9, 'HINOBA-AN (ASIA)', '184512000'),
(49, 5, 9, 'ILOG', '184513000'),
(50, 5, 9, 'ISABELA', '184514000'),
(51, 5, 9, 'CITY OF KABANKALAN', '184515000'),
(52, 5, 9, 'LA CARLOTA CITY', '184516000'),
(53, 5, 9, 'LA CASTELLANA', '184517000'),
(54, 5, 9, 'MANAPLA', '184518000'),
(55, 5, 9, 'MOISES PADILLA (MAGALLON)', '184519000'),
(56, 5, 9, 'MURCIA', '184520000'),
(57, 5, 9, 'PONTEVEDRA', '184521000'),
(58, 5, 9, 'PULUPANDAN', '184522000'),
(59, 5, 9, 'SAGAY CITY', '184523000'),
(60, 5, 9, 'SAN CARLOS CITY', '184524000'),
(61, 5, 9, 'SAN ENRIQUE', '184525000'),
(62, 5, 9, 'SILAY CITY', '184526000'),
(63, 5, 9, 'CITY OF SIPALAY', '184527000'),
(64, 5, 9, 'CITY OF TALISAY', '184528000'),
(65, 5, 9, 'TOBOSO', '184529000'),
(66, 5, 9, 'VALLADOLID', '184530000'),
(67, 5, 9, 'CITY OF VICTORIAS', '184531000'),
(68, 5, 9, 'SALVADOR BENEDICTO', '184532000'),
(69, 5, 10, 'AMLAN (AYUQUITAN)', '184601000'),
(70, 5, 10, 'AYUNGON', '184602000'),
(71, 5, 10, 'BACONG', '184603000'),
(72, 5, 10, 'BAIS CITY', '184604000'),
(73, 5, 10, 'BASAY', '184605000'),
(74, 5, 10, 'CITY OF BAYAWAN (TULONG)', '184606000'),
(75, 5, 10, 'BINDOY (PAYABON)', '184607000'),
(76, 5, 10, 'CANLAON CITY', '184608000'),
(77, 5, 10, 'DAUIN', '184609000'),
(78, 5, 10, 'DUMAGUETE CITY (Capital)', '184610000'),
(79, 5, 10, 'CITY OF GUIHULNGAN', '184611000'),
(80, 5, 10, 'JIMALALUD', '184612000'),
(81, 5, 10, 'LA LIBERTAD', '184613000'),
(82, 5, 10, 'MABINAY', '184614000'),
(83, 5, 10, 'MANJUYOD', '184615000'),
(84, 5, 10, 'PAMPLONA', '184616000'),
(85, 5, 10, 'SAN JOSE', '184617000'),
(86, 5, 10, 'SANTA CATALINA', '184618000'),
(87, 5, 10, 'SIATON', '184619000'),
(88, 5, 10, 'SIBULAN', '184620000'),
(89, 5, 10, 'CITY OF TANJAY', '184621000'),
(90, 5, 10, 'TAYASAN', '184622000'),
(91, 5, 10, 'VALENCIA (LUZURRIAGA)', '184623000'),
(92, 5, 10, 'VALLEHERMOSO', '184624000'),
(93, 5, 10, 'ZAMBOANGUITA', '184625000'),
(94, 6, 11, 'BANGUED (Capital)', '140101000'),
(95, 6, 11, 'BOLINEY', '140102000'),
(96, 6, 11, 'BUCAY', '140103000'),
(97, 6, 11, 'BUCLOC', '140104000'),
(98, 6, 11, 'DAGUIOMAN', '140105000'),
(99, 6, 11, 'DANGLAS', '140106000'),
(100, 6, 11, 'DOLORES', '140107000'),
(101, 6, 11, 'LA PAZ', '140108000'),
(102, 6, 11, 'LACUB', '140109000'),
(103, 6, 11, 'LAGANGILANG', '140110000'),
(104, 6, 11, 'LAGAYAN', '140111000'),
(105, 6, 11, 'LANGIDEN', '140112000'),
(106, 6, 11, 'LICUAN-BAAY (LICUAN)', '140113000'),
(107, 6, 11, 'LUBA', '140114000'),
(108, 6, 11, 'MALIBCONG', '140115000'),
(109, 6, 11, 'MANABO', '140116000'),
(110, 6, 11, 'PENARRUBIA', '140117000'),
(111, 6, 11, 'PIDIGAN', '140118000'),
(112, 6, 11, 'PILAR', '140119000'),
(113, 6, 11, 'SALLAPADAN', '140120000'),
(114, 6, 11, 'SAN ISIDRO', '140121000'),
(115, 6, 11, 'SAN JUAN', '140122000'),
(116, 6, 11, 'SAN QUINTIN', '140123000'),
(117, 6, 11, 'TAYUM', '140124000'),
(118, 6, 11, 'TINEG', '140125000'),
(119, 6, 11, 'TUBO', '140126000'),
(120, 6, 11, 'VILLAVICIOSA', '140127000'),
(121, 6, 12, 'CALANASAN (BAYAG)', '148101000'),
(122, 6, 12, 'CONNER', '148102000'),
(123, 6, 12, 'FLORA', '148103000'),
(124, 6, 12, 'KABUGAO (Capital)', '148104000'),
(125, 6, 12, 'LUNA', '148105000'),
(126, 6, 12, 'PUDTOL', '148106000'),
(127, 6, 12, 'SANTA MARCELA', '148107000'),
(128, 6, 13, 'ATOK', '141101000'),
(129, 6, 13, 'BAGUIO CITY', '141102000'),
(130, 6, 13, 'BAKUN', '141103000'),
(131, 6, 13, 'BOKOD', '141104000'),
(132, 6, 13, 'BUGUIAS', '141105000'),
(133, 6, 13, 'ITOGON', '141106000'),
(134, 6, 13, 'KABAYAN', '141107000'),
(135, 6, 13, 'KAPANGAN', '141108000'),
(136, 6, 13, 'KIBUNGAN', '141109000'),
(137, 6, 13, 'LA TRINIDAD (Capital)', '141110000'),
(138, 6, 13, 'MANKAYAN', '141111000'),
(139, 6, 13, 'SABLAN', '141112000'),
(140, 6, 13, 'TUBA', '141113000'),
(141, 6, 13, 'TUBLAY', '141114000'),
(143, 6, 14, 'HUNGDUAN', '142702000'),
(144, 6, 14, 'KIANGAN', '142703000'),
(145, 6, 14, 'LAGAWE (Capital)', '142704000'),
(146, 6, 14, 'LAMUT', '142705000'),
(147, 6, 14, 'MAYOYAO', '142706000'),
(148, 6, 14, 'ALFONSO LISTA (POTIA)', '142707000'),
(149, 6, 14, 'AGUINALDO', '142708000'),
(150, 6, 14, 'HINGYON', '142709000'),
(151, 6, 14, 'TINOC', '142710000'),
(152, 6, 14, 'ASIPULO', '142711000'),
(153, 6, 15, 'BALBALAN', '143201000'),
(154, 6, 15, 'LUBUAGAN', '143206000'),
(155, 6, 15, 'PASIL', '143208000'),
(156, 6, 15, 'PINUKPUK', '143209000'),
(157, 6, 15, 'RIZAL (LIWAN)', '143211000'),
(158, 6, 15, 'CITY OF TABUK (Capital)', '143213000'),
(159, 6, 15, 'TANUDAN', '143214000'),
(160, 6, 15, 'TINGLAYAN', '143215000'),
(161, 6, 16, 'BARLIG', '144401000'),
(162, 6, 16, 'BAUKO', '144402000'),
(163, 6, 16, 'BESAO', '144403000'),
(164, 6, 16, 'BONTOC (Capital)', '144404000'),
(165, 6, 16, 'NATONIN', '144405000'),
(166, 6, 16, 'PARACELIS', '144406000'),
(167, 6, 16, 'SABANGAN', '144407000'),
(168, 6, 16, 'SADANGA', '144408000'),
(169, 6, 16, 'SAGADA', '144409000'),
(170, 6, 16, 'TADIAN', '144410000'),
(171, 7, 17, 'ADAMS', '12801000'),
(172, 7, 17, 'BACARRA', '12802000'),
(173, 7, 17, 'BADOC', '12803000'),
(174, 7, 17, 'BANGUI', '12804000'),
(175, 7, 17, 'CITY OF BATAC', '12805000'),
(176, 7, 17, 'BURGOS', '12806000'),
(177, 7, 17, 'CARASI', '12807000'),
(178, 7, 17, 'CURRIMAO', '12808000'),
(179, 7, 17, 'DINGRAS', '12809000'),
(180, 7, 17, 'DUMALNEG', '12810000'),
(181, 7, 17, 'BANNA (ESPIRITU)', '12811000'),
(182, 7, 17, 'LAOAG CITY (Capital)', '12812000'),
(183, 7, 17, 'MARCOS', '12813000'),
(184, 7, 17, 'NUEVA ERA', '12814000'),
(185, 7, 17, 'PAGUDPUD', '12815000'),
(186, 7, 17, 'PAOAY', '12816000'),
(187, 7, 17, 'PASUQUIN', '12817000'),
(188, 7, 17, 'PIDDIG', '12818000'),
(189, 7, 17, 'PINILI', '12819000'),
(190, 7, 17, 'SAN NICOLAS', '12820000'),
(191, 7, 17, 'SARRAT', '12821000'),
(192, 7, 17, 'SOLSONA', '12822000'),
(193, 7, 17, 'VINTAR', '12823000'),
(195, 7, 18, 'ALILEM', '12901000'),
(196, 7, 18, 'BANAYOYO', '12902000'),
(197, 7, 18, 'BANTAY', '12903000'),
(198, 7, 18, 'BURGOS', '12904000'),
(199, 7, 18, 'CABUGAO', '12905000'),
(200, 7, 18, 'CITY OF CANDON', '12906000'),
(201, 7, 18, 'CAOAYAN', '12907000'),
(202, 7, 18, 'CERVANTES', '12908000'),
(203, 7, 18, 'GALIMUYOD', '12909000'),
(204, 7, 18, 'GREGORIO DEL PILAR (CONCEPCION)', '12910000'),
(205, 7, 18, 'LIDLIDDA', '12911000'),
(206, 7, 18, 'MAGSINGAL', '12912000'),
(207, 7, 18, 'NAGBUKEL', '12913000'),
(208, 7, 18, 'NARVACAN', '12914000'),
(209, 7, 18, 'QUIRINO (ANGKAKI)', '12915000'),
(210, 7, 18, 'SALCEDO (BAUGEN)', '12916000'),
(211, 7, 18, 'SAN EMILIO', '12917000'),
(212, 7, 18, 'SAN ESTEBAN', '12918000'),
(213, 7, 18, 'SAN ILDEFONSO', '12919000'),
(214, 7, 18, 'SAN JUAN (LAPOG)', '12920000'),
(215, 7, 18, 'SAN VICENTE', '12921000'),
(216, 7, 18, 'SANTA', '12922000'),
(217, 7, 18, 'SANTA CATALINA', '12923000'),
(218, 7, 18, 'SANTA CRUZ', '12924000'),
(219, 7, 18, 'SANTA LUCIA', '12925000'),
(220, 7, 18, 'SANTA MARIA', '12926000'),
(221, 7, 18, 'SANTIAGO', '12927000'),
(222, 7, 18, 'SANTO DOMINGO', '12928000'),
(223, 7, 18, 'SIGAY', '12929000'),
(224, 7, 18, 'SINAIT', '12930000'),
(225, 7, 18, 'SUGPON', '12931000'),
(226, 7, 18, 'SUYO', '12932000'),
(227, 7, 18, 'TAGUDIN', '12933000'),
(228, 7, 18, 'CITY OF VIGAN (Capital)', '12934000'),
(229, 7, 19, 'AGOO', '13301000'),
(230, 7, 19, 'ARINGAY', '13302000'),
(231, 7, 19, 'BACNOTAN', '13303000'),
(232, 7, 19, 'BAGULIN', '13304000'),
(233, 7, 19, 'BALAOAN', '13305000'),
(234, 7, 19, 'BANGAR', '13306000'),
(235, 7, 19, 'BAUANG', '13307000'),
(236, 7, 19, 'BURGOS', '13308000'),
(237, 7, 19, 'CABA', '13309000'),
(238, 7, 19, 'LUNA', '13310000'),
(239, 7, 19, 'NAGUILIAN', '13311000'),
(240, 7, 19, 'PUGO', '13312000'),
(241, 7, 19, 'ROSARIO', '13313000'),
(242, 7, 19, 'CITY OF SAN FERNANDO (Capital)', '13314000'),
(243, 7, 19, 'SAN GABRIEL', '13315000'),
(244, 7, 19, 'SAN JUAN', '13316000'),
(245, 7, 19, 'SANTO TOMAS', '13317000'),
(246, 7, 19, 'SANTOL', '13318000'),
(247, 7, 19, 'SUDIPEN', '13319000'),
(248, 7, 19, 'TUBAO', '13320000'),
(249, 7, 20, 'AGNO', '15501000'),
(250, 7, 20, 'AGUILAR', '15502000'),
(251, 7, 20, 'CITY OF ALAMINOS', '15503000'),
(252, 7, 20, 'ALCALA', '15504000'),
(253, 7, 20, 'ANDA', '15505000'),
(254, 7, 20, 'ASINGAN', '15506000'),
(255, 7, 20, 'BALUNGAO', '15507000'),
(256, 7, 20, 'BANI', '15508000'),
(257, 7, 20, 'BASISTA', '15509000'),
(258, 7, 20, 'BAUTISTA', '15510000'),
(259, 7, 20, 'BAYAMBANG', '15511000'),
(260, 7, 20, 'BINALONAN', '15512000'),
(261, 7, 20, 'BINMALEY', '15513000'),
(262, 7, 20, 'BOLINAO', '15514000'),
(263, 7, 20, 'BUGALLON', '15515000'),
(264, 7, 20, 'BURGOS', '15516000'),
(265, 7, 20, 'CALASIAO', '15517000'),
(266, 7, 20, 'DAGUPAN CITY', '15518000'),
(267, 7, 20, 'DASOL', '15519000'),
(268, 7, 20, 'INFANTA', '15520000'),
(269, 7, 20, 'LABRADOR', '15521000'),
(270, 7, 20, 'LINGAYEN (Capital)', '15522000'),
(271, 7, 20, 'MABINI', '15523000'),
(272, 7, 20, 'MALASIQUI', '15524000'),
(273, 7, 20, 'MANAOAG', '15525000'),
(274, 7, 20, 'MANGALDAN', '15526000'),
(275, 7, 20, 'MANGATAREM', '15527000'),
(276, 7, 20, 'MAPANDAN', '15528000'),
(277, 7, 20, 'NATIVIDAD', '15529000'),
(278, 7, 20, 'POZORRUBIO', '15530000'),
(279, 7, 20, 'ROSALES', '15531000'),
(280, 7, 20, 'SAN CARLOS CITY', '15532000'),
(281, 7, 20, 'SAN FABIAN', '15533000'),
(282, 7, 20, 'SAN JACINTO', '15534000'),
(283, 7, 20, 'SAN MANUEL', '15535000'),
(284, 7, 20, 'SAN NICOLAS', '15536000'),
(285, 7, 20, 'SAN QUINTIN', '15537000'),
(286, 7, 20, 'SANTA BARBARA', '15538000'),
(287, 7, 20, 'SANTA MARIA', '15539000'),
(288, 7, 20, 'SANTO TOMAS', '15540000'),
(289, 7, 20, 'SISON', '15541000'),
(290, 7, 20, 'SUAL', '15542000'),
(291, 7, 20, 'TAYUG', '15543000'),
(292, 7, 20, 'UMINGAN', '15544000'),
(293, 7, 20, 'URBIZTONDO', '15545000'),
(294, 7, 20, 'CITY OF URDANETA', '15546000'),
(295, 7, 20, 'VILLASIS', '15547000'),
(296, 7, 20, 'LAOAC', '15548000'),
(297, 8, 21, 'BASCO (Capital)', '20901000'),
(298, 8, 21, 'ITBAYAT', '20902000'),
(299, 8, 21, 'IVANA', '20903000'),
(300, 8, 21, 'MAHATAO', '20904000'),
(301, 8, 21, 'SABTANG', '20905000'),
(302, 8, 21, 'UYUGAN', '20906000'),
(303, 8, 22, 'ABULUG', '21501000'),
(304, 8, 22, 'ALCALA', '21502000'),
(305, 8, 22, 'ALLACAPAN', '21503000'),
(306, 8, 22, 'AMULUNG', '21504000'),
(307, 8, 22, 'APARRI', '21505000'),
(308, 8, 22, 'BAGGAO', '21506000'),
(309, 8, 22, 'BALLESTEROS', '21507000'),
(310, 8, 22, 'BUGUEY', '21508000'),
(311, 8, 22, 'CALAYAN', '21509000'),
(312, 8, 22, 'CAMALANIUGAN', '21510000'),
(313, 8, 22, 'CLAVERIA', '21511000'),
(314, 8, 22, 'ENRILE', '21512000'),
(315, 8, 22, 'GATTARAN', '21513000'),
(316, 8, 22, 'GONZAGA', '21514000'),
(317, 8, 22, 'IGUIG', '21515000'),
(318, 8, 22, 'LAL-LO', '21516000'),
(319, 8, 22, 'LASAM', '21517000'),
(320, 8, 22, 'PAMPLONA', '21518000'),
(321, 8, 22, 'PENABLANCA', '21519000'),
(322, 8, 22, 'PIAT', '21520000'),
(323, 8, 22, 'RIZAL', '21521000'),
(324, 8, 22, 'SANCHEZ-MIRA', '21522000'),
(325, 8, 22, 'SANTA ANA', '21523000'),
(326, 8, 22, 'SANTA PRAXEDES', '21524000'),
(327, 8, 22, 'SANTA TERESITA', '21525000'),
(328, 8, 22, 'SANTO NINO (FAIRE)', '21526000'),
(329, 8, 22, 'SOLANA', '21527000'),
(330, 8, 22, 'TUAO', '21528000'),
(331, 8, 22, 'TUGUEGARAO CITY (Capital)', '21529000'),
(332, 8, 23, 'ALICIA', '23101000'),
(333, 8, 23, 'ANGADANAN', '23102000'),
(334, 8, 23, 'AURORA', '23103000'),
(335, 8, 23, 'BENITO SOLIVEN', '23104000'),
(336, 8, 23, 'BURGOS', '23105000'),
(337, 8, 23, 'CABAGAN', '23106000'),
(338, 8, 23, 'CABATUAN', '23107000'),
(339, 8, 23, 'CITY OF CAUAYAN', '23108000'),
(340, 8, 23, 'CORDON', '23109000'),
(341, 8, 23, 'DINAPIGUE', '23110000'),
(342, 8, 23, 'DIVILACAN', '23111000'),
(343, 8, 23, 'ECHAGUE', '23112000'),
(344, 8, 23, 'GAMU', '23113000'),
(345, 8, 23, 'ILAGAN CITY (Capital)', '23114000'),
(346, 8, 23, 'JONES', '23115000'),
(347, 8, 23, 'LUNA', '23116000'),
(348, 8, 23, 'MACONACON', '23117000'),
(349, 8, 23, 'DELFIN ALBANO (MAGSAYSAY)', '23118000'),
(350, 8, 23, 'MALLIG', '23119000'),
(351, 8, 23, 'NAGUILIAN', '23120000'),
(352, 8, 23, 'PALANAN', '23121000'),
(353, 8, 23, 'QUEZON', '23122000'),
(354, 8, 23, 'QUIRINO', '23123000'),
(355, 8, 23, 'RAMON', '23124000'),
(356, 8, 23, 'REINA MERCEDES', '23125000'),
(357, 8, 23, 'ROXAS', '23126000'),
(358, 8, 23, 'SAN AGUSTIN', '23127000'),
(359, 8, 23, 'SAN GUILLERMO', '23128000'),
(360, 8, 23, 'SAN ISIDRO', '23129000'),
(361, 8, 23, 'SAN MANUEL', '23130000'),
(362, 8, 23, 'SAN MARIANO', '23131000'),
(363, 8, 23, 'SAN MATEO', '23132000'),
(364, 8, 23, 'SAN PABLO', '23133000'),
(365, 8, 23, 'SANTA MARIA', '23134000'),
(366, 8, 23, 'CITY OF SANTIAGO', '23135000'),
(367, 8, 23, 'SANTO TOMAS', '23136000'),
(368, 8, 23, 'TUMAUINI', '23137000'),
(369, 8, 24, 'AMBAGUIO', '25001000'),
(370, 8, 24, 'ARITAO', '25002000'),
(371, 8, 24, 'BAGABAG', '25003000'),
(372, 8, 24, 'BAMBANG', '25004000'),
(373, 8, 24, 'BAYOMBONG (Capital)', '25005000'),
(374, 8, 24, 'DIADI', '25006000'),
(375, 8, 24, 'DUPAX DEL NORTE', '25007000'),
(376, 8, 24, 'DUPAX DEL SUR', '25008000'),
(377, 8, 24, 'KASIBU', '25009000'),
(378, 8, 24, 'KAYAPA', '25010000'),
(379, 8, 24, 'QUEZON', '25011000'),
(380, 8, 24, 'SANTA FE', '25012000'),
(381, 8, 24, 'SOLANO', '25013000'),
(382, 8, 24, 'VILLAVERDE', '25014000'),
(383, 8, 24, 'ALFONSO CASTANEDA', '25015000'),
(384, 8, 25, 'AGLIPAY', '25701000'),
(385, 8, 25, 'CABARROGUIS (Capital)', '25702000'),
(386, 8, 25, 'DIFFUN', '25703000'),
(387, 8, 25, 'MADDELA', '25704000'),
(388, 8, 25, 'SAGUDAY', '25705000'),
(389, 8, 25, 'NAGTIPUNAN', '25706000'),
(390, 9, 26, 'BALER (Capital)', '37701000'),
(391, 9, 26, 'CASIGURAN', '37702000'),
(392, 9, 26, 'DILASAG', '37703000'),
(393, 9, 26, 'DINALUNGAN', '37704000'),
(394, 9, 26, 'DINGALAN', '37705000'),
(395, 9, 26, 'DIPACULAO', '37706000'),
(396, 9, 26, 'MARIA AURORA', '37707000'),
(397, 9, 26, 'SAN LUIS', '37708000'),
(398, 9, 27, 'ABUCAY', '30801000'),
(399, 9, 27, 'BAGAC', '30802000'),
(400, 9, 27, 'CITY OF BALANGA (Capital)', '30803000'),
(401, 9, 27, 'DINALUPIHAN', '30804000'),
(402, 9, 27, 'HERMOSA', '30805000'),
(403, 9, 27, 'LIMAY', '30806000'),
(404, 9, 27, 'MARIVELES', '30807000'),
(405, 9, 27, 'MORONG', '30808000'),
(406, 9, 27, 'ORANI', '30809000'),
(407, 9, 27, 'ORION', '30810000'),
(408, 9, 27, 'PILAR', '30811000'),
(409, 9, 27, 'SAMAL', '30812000'),
(411, 9, 28, 'ANGAT', '31401000'),
(412, 9, 28, 'BALAGTAS (BIGAA)', '31402000'),
(413, 9, 28, 'BALIUAG', '31403000'),
(414, 9, 28, 'BOCAUE', '31404000'),
(415, 9, 28, 'BULACAN', '31405000'),
(416, 9, 28, 'BUSTOS', '31406000'),
(417, 9, 28, 'CALUMPIT', '31407000'),
(418, 9, 28, 'GUIGUINTO', '31408000'),
(419, 9, 28, 'HAGONOY', '31409000'),
(420, 9, 28, 'CITY OF MALOLOS (Capital)', '31410000'),
(421, 9, 28, 'MARILAO', '31411000'),
(422, 9, 28, 'CITY OF MEYCAUAYAN', '31412000'),
(423, 9, 28, 'NORZAGARAY', '31413000'),
(424, 9, 28, 'OBANDO', '31414000'),
(425, 9, 28, 'PANDI', '31415000'),
(426, 9, 28, 'PAOMBONG', '31416000'),
(427, 9, 28, 'PLARIDEL', '31417000'),
(428, 9, 28, 'PULILAN', '31418000'),
(429, 9, 28, 'SAN ILDEFONSO', '31419000'),
(430, 9, 28, 'CITY OF SAN JOSE DEL MONTE', '31420000'),
(431, 9, 28, 'SAN MIGUEL', '31421000'),
(432, 9, 28, 'SAN RAFAEL', '31422000'),
(433, 9, 28, 'SANTA MARIA', '31423000'),
(434, 9, 28, 'DONA REMEDIOS TRINIDAD', '31424000'),
(435, 9, 29, 'ALIAGA', '34901000'),
(436, 9, 29, 'BONGABON', '34902000'),
(437, 9, 29, 'CABANATUAN CITY', '34903000'),
(438, 9, 29, 'CABIAO', '34904000'),
(439, 9, 29, 'CARRANGLAN', '34905000'),
(440, 9, 29, 'CUYAPO', '34906000'),
(441, 9, 29, 'GABALDON (BITULOK & SABANI)', '34907000'),
(442, 9, 29, 'CITY OF GAPAN', '34908000'),
(443, 9, 29, 'GENERAL MAMERTO NATIVIDAD', '34909000'),
(444, 9, 29, 'GENERAL TINIO (PAPAYA)', '34910000'),
(445, 9, 29, 'GUIMBA', '34911000'),
(446, 9, 29, 'JAEN', '34912000'),
(447, 9, 29, 'LAUR', '34913000'),
(448, 9, 29, 'LICAB', '34914000'),
(449, 9, 29, 'LLANERA', '34915000'),
(450, 9, 29, 'LUPAO', '34916000'),
(451, 9, 29, 'SCIENCE CITY OF MU?OZ', '34917000'),
(452, 9, 29, 'NAMPICUAN', '34918000'),
(453, 9, 29, 'PALAYAN CITY (Capital)', '34919000'),
(454, 9, 29, 'PANTABANGAN', '34920000'),
(455, 9, 29, 'PE?ARANDA', '34921000'),
(456, 9, 29, 'QUEZON', '34922000'),
(457, 9, 29, 'RIZAL', '34923000'),
(458, 9, 29, 'SAN ANTONIO', '34924000'),
(459, 9, 29, 'SAN ISIDRO', '34925000'),
(460, 9, 29, 'SAN JOSE CITY', '34926000'),
(461, 9, 29, 'SAN LEONARDO', '34927000'),
(462, 9, 29, 'SANTA ROSA', '34928000'),
(463, 9, 29, 'SANTO DOMINGO', '34929000'),
(464, 9, 29, 'TALAVERA', '34930000'),
(465, 9, 29, 'TALUGTUG', '34931000'),
(466, 9, 29, 'ZARAGOZA', '34932000'),
(467, 9, 30, 'ANGELES CITY', '35401000'),
(468, 9, 30, 'APALIT', '35402000'),
(469, 9, 30, 'ARAYAT', '35403000'),
(470, 9, 30, 'BACOLOR', '35404000'),
(471, 9, 30, 'CANDABA', '35405000'),
(472, 9, 30, 'FLORIDABLANCA', '35406000'),
(473, 9, 30, 'GUAGUA', '35407000'),
(474, 9, 30, 'LUBAO', '35408000'),
(475, 9, 30, 'MABALACAT CITY', '35409000'),
(476, 9, 30, 'MACABEBE', '35410000'),
(477, 9, 30, 'MAGALANG', '35411000'),
(478, 9, 30, 'MASANTOL', '35412000'),
(479, 9, 30, 'MEXICO', '35413000'),
(480, 9, 30, 'MINALIN', '35414000'),
(481, 9, 30, 'PORAC', '35415000'),
(482, 9, 30, 'CITY OF SAN FERNANDO (Capital)', '35416000'),
(483, 9, 30, 'SAN LUIS', '35417000'),
(484, 9, 30, 'SAN SIMON', '35418000'),
(485, 9, 30, 'SANTA ANA', '35419000'),
(486, 9, 30, 'SANTA RITA', '35420000'),
(487, 9, 30, 'SANTO TOMAS', '35421000'),
(488, 9, 30, 'SASMUAN (Sexmoan)', '35422000'),
(489, 9, 31, 'ANAO', '36901000'),
(490, 9, 31, 'BAMBAN', '36902000'),
(491, 9, 31, 'CAMILING', '36903000'),
(492, 9, 31, 'CAPAS', '36904000'),
(493, 9, 31, 'CONCEPCION', '36905000'),
(494, 9, 31, 'GERONA', '36906000'),
(495, 9, 31, 'LA PAZ', '36907000'),
(496, 9, 31, 'MAYANTOC', '36908000'),
(497, 9, 31, 'MONCADA', '36909000'),
(498, 9, 31, 'PANIQUI', '36910000'),
(499, 9, 31, 'PURA', '36911000'),
(500, 9, 31, 'RAMOS', '36912000'),
(501, 9, 31, 'SAN CLEMENTE', '36913000'),
(502, 9, 31, 'SAN MANUEL', '36914000'),
(503, 9, 31, 'SANTA IGNACIA', '36915000'),
(504, 9, 31, 'CITY OF TARLAC (Capital)', '36916000'),
(505, 9, 31, 'VICTORIA', '36917000'),
(506, 9, 31, 'SAN JOSE', '36918000'),
(507, 9, 32, 'BOTOLAN', '37101000'),
(508, 9, 32, 'CABANGAN', '37102000'),
(509, 9, 32, 'CANDELARIA', '37103000'),
(510, 9, 32, 'CASTILLEJOS', '37104000'),
(511, 9, 32, 'IBA (Capital)', '37105000'),
(512, 9, 32, 'MASINLOC', '37106000'),
(513, 9, 32, 'OLONGAPO CITY', '37107000'),
(514, 9, 32, 'PALAUIG', '37108000'),
(515, 9, 32, 'SAN ANTONIO', '37109000'),
(516, 9, 32, 'SAN FELIPE', '37110000'),
(517, 9, 32, 'SAN MARCELINO', '37111000'),
(518, 9, 32, 'SAN NARCISO', '37112000'),
(519, 9, 32, 'SANTA CRUZ', '37113000'),
(520, 9, 32, 'SUBIC', '37114000'),
(522, 10, 33, 'AGONCILLO', '41001000'),
(523, 10, 33, 'ALITAGTAG', '41002000'),
(524, 10, 33, 'BALAYAN', '41003000'),
(525, 10, 33, 'BALETE', '41004000'),
(526, 10, 33, 'BATANGAS CITY (Capital)', '41005000'),
(527, 10, 33, 'BAUAN', '41006000'),
(528, 10, 33, 'CALACA', '41007000'),
(529, 10, 33, 'CALATAGAN', '41008000'),
(530, 10, 33, 'CUENCA', '41009000'),
(531, 10, 33, 'IBAAN', '41010000'),
(532, 10, 33, 'LAUREL', '41011000'),
(533, 10, 33, 'LEMERY', '41012000'),
(534, 10, 33, 'LIAN', '41013000'),
(535, 10, 33, 'LIPA CITY', '41014000'),
(536, 10, 33, 'LOBO', '41015000'),
(537, 10, 33, 'MABINI', '41016000'),
(538, 10, 33, 'MALVAR', '41017000'),
(539, 10, 33, 'MATAASNAKAHOY', '41018000'),
(540, 10, 33, 'NASUGBU', '41019000'),
(541, 10, 33, 'PADRE GARCIA', '41020000'),
(542, 10, 33, 'ROSARIO', '41021000'),
(543, 10, 33, 'SAN JOSE', '41022000'),
(544, 10, 33, 'SAN JUAN', '41023000'),
(545, 10, 33, 'SAN LUIS', '41024000'),
(546, 10, 33, 'SAN NICOLAS', '41025000'),
(547, 10, 33, 'SAN PASCUAL', '41026000'),
(548, 10, 33, 'SANTA TERESITA', '41027000'),
(549, 10, 33, 'SANTO TOMAS', '41028000'),
(550, 10, 33, 'TAAL', '41029000'),
(551, 10, 33, 'TALISAY', '41030000'),
(552, 10, 33, 'CITY OF TANAUAN', '41031000'),
(553, 10, 33, 'TAYSAN', '41032000'),
(554, 10, 33, 'TINGLOY', '41033000'),
(555, 10, 33, 'TUY', '41034000'),
(557, 10, 34, 'ALFONSO', '42101000'),
(558, 10, 34, 'AMADEO', '42102000'),
(559, 10, 34, 'BACOOR CITY', '42103000'),
(560, 10, 34, 'CARMONA', '42104000'),
(561, 10, 34, 'CAVITE CITY', '42105000'),
(562, 10, 34, 'CITY OF DASMARINAS', '42106000'),
(563, 10, 34, 'GENERAL EMILIO AGUINALDO', '42107000'),
(564, 10, 34, 'CITY OF GENERAL TRIAS', '42108000'),
(565, 10, 34, 'IMUS CITY', '42109000'),
(566, 10, 34, 'INDANG', '42110000'),
(567, 10, 34, 'KAWIT', '42111000'),
(568, 10, 34, 'MAGALLANES', '42112000'),
(569, 10, 34, 'MARAGONDON', '42113000'),
(570, 10, 34, 'MENDEZ (MENDEZ-NUNEZ)', '42114000'),
(571, 10, 34, 'NAIC', '42115000'),
(572, 10, 34, 'NOVELETA', '42116000'),
(573, 10, 34, 'ROSARIO', '42117000'),
(574, 10, 34, 'SILANG', '42118000'),
(575, 10, 34, 'TAGAYTAY CITY', '42119000'),
(576, 10, 34, 'TANZA', '42120000'),
(577, 10, 34, 'TERNATE', '42121000'),
(578, 10, 34, 'TRECE MARTIRES CITY (Capital)', '42122000'),
(579, 10, 34, 'GEN. MARIANO ALVAREZ', '42123000'),
(580, 10, 35, 'ALAMINOS', '43401000'),
(581, 10, 35, 'BAY', '43402000'),
(582, 10, 35, 'CITY OF BINAN', '43403000'),
(583, 10, 35, 'CABUYAO CITY', '43404000'),
(584, 10, 35, 'CITY OF CALAMBA', '43405000'),
(585, 10, 35, 'CALAUAN', '43406000'),
(586, 10, 35, 'CAVINTI', '43407000'),
(587, 10, 35, 'FAMY', '43408000'),
(588, 10, 35, 'KALAYAAN', '43409000'),
(589, 10, 35, 'LILIW', '43410000'),
(590, 10, 35, 'LOS BANOS', '43411000'),
(591, 10, 35, 'LUISIANA', '43412000'),
(592, 10, 35, 'LUMBAN', '43413000'),
(593, 10, 35, 'MABITAC', '43414000'),
(594, 10, 35, 'MAGDALENA', '43415000'),
(595, 10, 35, 'MAJAYJAY', '43416000'),
(596, 10, 35, 'NAGCARLAN', '43417000'),
(597, 10, 35, 'PAETE', '43418000'),
(598, 10, 35, 'PAGSANJAN', '43419000'),
(599, 10, 35, 'PAKIL', '43420000'),
(600, 10, 35, 'PANGIL', '43421000'),
(601, 10, 35, 'PILA', '43422000'),
(602, 10, 35, 'RIZAL', '43423000'),
(603, 10, 35, 'SAN PABLO CITY', '43424000'),
(604, 10, 35, 'CITY OF SAN PEDRO', '43425000'),
(605, 10, 35, 'SANTA CRUZ (Capital)', '43426000'),
(606, 10, 35, 'SANTA MARIA', '43427000'),
(607, 10, 35, 'CITY OF SANTA ROSA', '43428000'),
(608, 10, 35, 'SINILOAN', '43429000'),
(609, 10, 35, 'VICTORIA', '43430000'),
(610, 10, 36, 'AGDANGAN', '45601000'),
(611, 10, 36, 'ALABAT', '45602000'),
(612, 10, 36, 'ATIMONAN', '45603000'),
(613, 10, 36, 'BUENAVISTA', '45605000'),
(614, 10, 36, 'BURDEOS', '45606000'),
(615, 10, 36, 'CALAUAG', '45607000'),
(616, 10, 36, 'CANDELARIA', '45608000'),
(617, 10, 36, 'CATANAUAN', '45610000'),
(618, 10, 36, 'DOLORES', '45615000'),
(619, 10, 36, 'GENERAL LUNA', '45616000'),
(620, 10, 36, 'GENERAL NAKAR', '45617000'),
(621, 10, 36, 'GUINAYANGAN', '45618000'),
(622, 10, 36, 'GUMACA', '45619000'),
(623, 10, 36, 'INFANTA', '45620000'),
(624, 10, 36, 'JOMALIG', '45621000'),
(625, 10, 36, 'LOPEZ', '45622000'),
(626, 10, 36, 'LUCBAN', '45623000'),
(627, 10, 36, 'LUCENA CITY (Capital)', '45624000'),
(628, 10, 36, 'MACALELON', '45625000'),
(629, 10, 36, 'MAUBAN', '45627000'),
(630, 10, 36, 'MULANAY', '45628000'),
(631, 10, 36, 'PADRE BURGOS', '45629000'),
(632, 10, 36, 'PAGBILAO', '45630000'),
(633, 10, 36, 'PANUKULAN', '45631000'),
(634, 10, 36, 'PATNANUNGAN', '45632000'),
(635, 10, 36, 'PEREZ', '45633000'),
(636, 10, 36, 'PITOGO', '45634000'),
(637, 10, 36, 'PLARIDEL', '45635000'),
(638, 10, 36, 'POLILLO', '45636000'),
(639, 10, 36, 'QUEZON', '45637000'),
(640, 10, 36, 'REAL', '45638000'),
(641, 10, 36, 'SAMPALOC', '45639000'),
(642, 10, 36, 'SAN ANDRES', '45640000'),
(643, 10, 36, 'SAN ANTONIO', '45641000'),
(644, 10, 36, 'SAN FRANCISCO (AURORA)', '45642000'),
(645, 10, 36, 'SAN NARCISO', '45644000'),
(646, 10, 36, 'SARIAYA', '45645000'),
(647, 10, 36, 'TAGKAWAYAN', '45646000'),
(648, 10, 36, 'CITY OF TAYABAS', '45647000'),
(649, 10, 36, 'TIAONG', '45648000'),
(650, 10, 36, 'UNISAN', '45649000'),
(651, 10, 37, 'ANGONO', '45801000'),
(652, 10, 37, 'CITY OF ANTIPOLO', '45802000'),
(653, 10, 37, 'BARAS', '45803000'),
(654, 10, 37, 'BINANGONAN', '45804000'),
(655, 10, 37, 'CAINTA', '45805000'),
(656, 10, 37, 'CARDONA', '45806000'),
(657, 10, 37, 'JALA-JALA', '45807000'),
(658, 10, 37, 'RODRIGUEZ (MONTALBAN)', '45808000'),
(659, 10, 37, 'MORONG', '45809000'),
(660, 10, 37, 'PILILLA', '45810000'),
(661, 10, 37, 'SAN MATEO', '45811000'),
(662, 10, 37, 'TANAY', '45812000'),
(663, 10, 37, 'TAYTAY', '45813000'),
(664, 10, 37, 'TERESA', '45814000'),
(665, 11, 38, 'BOAC (Capital)', '174001000'),
(666, 11, 38, 'BUENAVISTA', '174002000'),
(667, 11, 38, 'GASAN', '174003000'),
(668, 11, 38, 'MOGPOG', '174004000'),
(669, 11, 38, 'SANTA CRUZ', '174005000'),
(670, 11, 38, 'TORRIJOS', '174006000'),
(671, 11, 39, 'ABRA DE ILOG', '175101000'),
(672, 11, 39, 'CALINTAAN', '175102000'),
(673, 11, 39, 'LOOC', '175103000'),
(674, 11, 39, 'LUBANG', '175104000'),
(675, 11, 39, 'MAGSAYSAY', '175105000'),
(676, 11, 39, 'MAMBURAO (Capital)', '175106000'),
(677, 11, 39, 'PALUAN', '175107000'),
(678, 11, 39, 'RIZAL', '175108000'),
(679, 11, 39, 'SABLAYAN', '175109000'),
(680, 11, 39, 'SAN JOSE', '175110000'),
(681, 11, 39, 'SANTA CRUZ', '175111000'),
(682, 11, 40, 'BACO', '175201000'),
(683, 11, 40, 'BANSUD', '175202000'),
(684, 11, 40, 'BONGABONG', '175203000'),
(685, 11, 40, 'BULALACAO (SAN PEDRO)', '175204000'),
(686, 11, 40, 'CITY OF CALAPAN (Capital)', '175205000'),
(687, 11, 40, 'GLORIA', '175206000'),
(688, 11, 40, 'MANSALAY', '175207000'),
(689, 11, 40, 'NAUJAN', '175208000'),
(690, 11, 40, 'PINAMALAYAN', '175209000'),
(691, 11, 40, 'POLA', '175210000'),
(692, 11, 40, 'PUERTO GALERA', '175211000'),
(693, 11, 40, 'ROXAS', '175212000'),
(694, 11, 40, 'SAN TEODORO', '175213000'),
(695, 11, 40, 'SOCORRO', '175214000'),
(696, 11, 40, 'VICTORIA', '175215000'),
(697, 11, 41, 'ABORLAN', '175301000'),
(698, 11, 41, 'AGUTAYA', '175302000'),
(699, 11, 41, 'ARACELI', '175303000'),
(700, 11, 41, 'BALABAC', '175304000'),
(701, 11, 41, 'BATARAZA', '175305000'),
(702, 11, 41, 'BROOKE''S POINT', '175306000'),
(703, 11, 41, 'BUSUANGA', '175307000'),
(704, 11, 41, 'CAGAYANCILLO', '175308000'),
(705, 11, 41, 'CORON', '175309000'),
(706, 11, 41, 'CUYO', '175310000'),
(707, 11, 41, 'DUMARAN', '175311000'),
(708, 11, 41, 'EL NIDO (BACUIT)', '175312000'),
(709, 11, 41, 'LINAPACAN', '175313000'),
(710, 11, 41, 'MAGSAYSAY', '175314000'),
(711, 11, 41, 'NARRA', '175315000'),
(712, 11, 41, 'PUERTO PRINCESA CITY (Capital)', '175316000'),
(713, 11, 41, 'QUEZON', '175317000'),
(714, 11, 41, 'ROXAS', '175318000'),
(715, 11, 41, 'SAN VICENTE', '175319000'),
(716, 11, 41, 'TAYTAY', '175320000'),
(717, 11, 41, 'KALAYAAN', '175321000'),
(718, 11, 41, 'CULION', '175322000'),
(719, 11, 41, 'RIZAL (MARCOS)', '175323000'),
(720, 11, 41, 'SOFRONIO ESPANOLA', '175324000'),
(721, 11, 42, 'ALCANTARA', '175901000'),
(722, 11, 42, 'BANTON', '175902000'),
(723, 11, 42, 'CAJIDIOCAN', '175903000'),
(724, 11, 42, 'CALATRAVA', '175904000'),
(725, 11, 42, 'CONCEPCION', '175905000'),
(726, 11, 42, 'CORCUERA', '175906000'),
(727, 11, 42, 'LOOC', '175907000'),
(728, 11, 42, 'MAGDIWANG', '175908000'),
(729, 11, 42, 'ODIONGAN', '175909000'),
(730, 11, 42, 'ROMBLON (Capital)', '175910000'),
(731, 11, 42, 'SAN AGUSTIN', '175911000'),
(732, 11, 42, 'SAN ANDRES', '175912000'),
(733, 11, 42, 'SAN FERNANDO', '175913000'),
(734, 11, 42, 'SAN JOSE', '175914000'),
(735, 11, 42, 'SANTA FE', '175915000'),
(736, 11, 42, 'FERROL', '175916000'),
(737, 11, 42, 'SANTA MARIA (IMELDA)', ''),
(738, 12, 43, 'BACACAY', '50501000'),
(739, 12, 43, 'CAMALIG', '50502000'),
(740, 12, 43, 'DARAGA (LOCSIN)', '50503000'),
(741, 12, 43, 'GUINOBATAN', '50504000'),
(742, 12, 43, 'JOVELLAR', '50505000'),
(743, 12, 43, 'LEGAZPI CITY (Capital)', '50506000'),
(744, 12, 43, 'LIBON', '50507000'),
(745, 12, 43, 'CITY OF LIGAO', '50508000'),
(746, 12, 43, 'MALILIPOT', '50509000'),
(747, 12, 43, 'MALINAO', '50510000'),
(748, 12, 43, 'MANITO', '50511000'),
(749, 12, 43, 'OAS', '50512000'),
(750, 12, 43, 'PIO DURAN', '50513000'),
(751, 12, 43, 'POLANGUI', '50514000'),
(752, 12, 43, 'RAPU-RAPU', '50515000'),
(753, 12, 43, 'SANTO DOMINGO (LIBOG)', '50516000'),
(754, 12, 43, 'CITY OF TABACO', '50517000'),
(755, 12, 43, 'TIWI', '50518000'),
(756, 12, 44, 'BASUD', '51601000'),
(757, 12, 44, 'CAPALONGA', '51602000'),
(758, 12, 44, 'DAET (Capital)', '51603000'),
(759, 12, 44, 'SAN LORENZO RUIZ (IMELDA)', '51604000'),
(760, 12, 44, 'JOSE PANGANIBAN', '51605000'),
(761, 12, 44, 'LABO', '51606000'),
(762, 12, 44, 'MERCEDES', '51607000'),
(763, 12, 44, 'PARACALE', '51608000'),
(764, 12, 44, 'SAN VICENTE', '51609000'),
(765, 12, 44, 'SANTA ELENA', '51610000'),
(766, 12, 44, 'TALISAY', '51611000'),
(767, 12, 44, 'VINZONS', '51612000'),
(768, 12, 45, 'BAAO', '51701000'),
(769, 12, 45, 'BALATAN', '51702000'),
(770, 12, 45, 'BATO', '51703000'),
(771, 12, 45, 'BOMBON', '51704000'),
(772, 12, 45, 'BUHI', '51705000'),
(773, 12, 45, 'BULA', '51706000'),
(774, 12, 45, 'CABUSAO', '51707000'),
(775, 12, 45, 'CALABANGA', '51708000'),
(776, 12, 45, 'CAMALIGAN', '51709000'),
(777, 12, 45, 'CANAMAN', '51710000'),
(778, 12, 45, 'CARAMOAN', '51711000'),
(779, 12, 45, 'DEL GALLEGO', '51712000'),
(780, 12, 45, 'GAINZA', '51713000'),
(781, 12, 45, 'GARCHITORENA', '51714000'),
(782, 12, 45, 'GOA', '51715000'),
(783, 12, 45, 'IRIGA CITY', '51716000'),
(784, 12, 45, 'LAGONOY', '51717000'),
(785, 12, 45, 'LIBMANAN', '51718000'),
(786, 12, 45, 'LUPI', '51719000'),
(787, 12, 45, 'MAGARAO', '51720000'),
(788, 12, 45, 'MILAOR', '51721000'),
(789, 12, 45, 'MINALABAC', '51722000'),
(790, 12, 45, 'NABUA', '51723000'),
(791, 12, 45, 'NAGA CITY', '51724000'),
(792, 12, 45, 'OCAMPO', '51725000'),
(793, 12, 45, 'PAMPLONA', '51726000'),
(794, 12, 45, 'PASACAO', '51727000'),
(795, 12, 45, 'PILI (Capital)', '51728000'),
(796, 12, 45, 'PRESENTACION (PARUBCAN)', '51729000'),
(797, 12, 45, 'RAGAY', '51730000'),
(798, 12, 45, 'SAG?AY', '51731000'),
(799, 12, 45, 'SAN FERNANDO', '51732000'),
(800, 12, 45, 'SAN JOSE', '51733000'),
(801, 12, 45, 'SIPOCOT', '51734000'),
(802, 12, 45, 'SIRUMA', '51735000'),
(803, 12, 45, 'TIGAON', '51736000'),
(804, 12, 45, 'TINAMBAC', '51737000'),
(805, 12, 46, 'BAGAMANOC', '52001000'),
(806, 12, 46, 'BARAS', '52002000'),
(807, 12, 46, 'BATO', '52003000'),
(808, 12, 46, 'CARAMORAN', '52004000'),
(809, 12, 46, 'GIGMOTO', '52005000'),
(810, 12, 46, 'PANDAN', '52006000'),
(811, 12, 46, 'PANGANIBAN (PAYO)', '52007000'),
(812, 12, 46, 'SAN ANDRES (CALOLBON)', '52008000'),
(813, 12, 46, 'SAN MIGUEL', '52009000'),
(814, 12, 46, 'VIGA', '52010000'),
(815, 12, 46, 'VIRAC (Capital)', '52011000'),
(816, 12, 47, 'AROROY', '54101000'),
(817, 12, 47, 'BALENO', '54102000'),
(818, 12, 47, 'BALUD', '54103000'),
(819, 12, 47, 'BATUAN', '54104000'),
(820, 12, 47, 'CATAINGAN', '54105000'),
(821, 12, 47, 'CAWAYAN', '54106000'),
(822, 12, 47, 'CLAVERIA', '54107000'),
(823, 12, 47, 'DIMASALANG', '54108000'),
(824, 12, 47, 'ESPERANZA', '54109000'),
(825, 12, 47, 'MANDAON', '54110000'),
(826, 12, 47, 'CITY OF MASBATE (Capital)', '54111000'),
(827, 12, 47, 'MILAGROS', '54112000'),
(828, 12, 47, 'MOBO', '54113000'),
(829, 12, 47, 'MONREAL', '54114000'),
(830, 12, 47, 'PALANAS', '54115000'),
(831, 12, 47, 'PIO V. CORPUZ (LIMBUHAN)', '54116000'),
(832, 12, 47, 'PLACER', '54117000'),
(833, 12, 47, 'SAN FERNANDO', '54118000'),
(834, 12, 47, 'SAN JACINTO', '54119000'),
(835, 12, 47, 'SAN PASCUAL', '54120000'),
(836, 12, 47, 'USON', '54121000'),
(837, 12, 48, 'BARCELONA', '56202000'),
(838, 12, 48, 'BULAN', '56203000'),
(839, 12, 48, 'BULUSAN', '56204000'),
(840, 12, 48, 'CASIGURAN', '56205000'),
(841, 12, 48, 'CASTILLA', '56206000'),
(842, 12, 48, 'DONSOL', '56207000'),
(843, 12, 48, 'GUBAT', '56208000'),
(844, 12, 48, 'IROSIN', '56209000'),
(845, 12, 48, 'JUBAN', '56210000'),
(846, 12, 48, 'MAGALLANES', '56211000'),
(847, 12, 48, 'MATNOG', '56212000'),
(848, 12, 48, 'PILAR', '56213000'),
(849, 12, 48, 'PRIETO DIAZ', '56214000'),
(850, 12, 48, 'SANTA MAGDALENA', '56215000'),
(851, 12, 48, 'CITY OF SORSOGON (Capital)', '56216000'),
(852, 13, 49, 'ALTAVAS', '60401000'),
(853, 13, 49, 'BALETE', '60402000'),
(854, 13, 49, 'BANGA', '60403000'),
(855, 13, 49, 'BATAN', '60404000'),
(856, 13, 49, 'BURUANGA', '60405000'),
(857, 13, 49, 'IBAJAY', '60406000'),
(858, 13, 49, 'KALIBO (Capital)', '60407000'),
(859, 13, 49, 'LEZO', '60408000'),
(860, 13, 49, 'LIBACAO', '60409000'),
(861, 13, 49, 'MADALAG', '60410000'),
(862, 13, 49, 'MAKATO', '60411000'),
(863, 13, 49, 'MALAY', '60412000'),
(864, 13, 49, 'MALINAO', '60413000'),
(865, 13, 49, 'NABAS', '60414000'),
(866, 13, 49, 'NEW WASHINGTON', '60415000'),
(867, 13, 49, 'NUMANCIA', '60416000'),
(868, 13, 49, 'TANGALAN', '60417000'),
(869, 13, 50, 'ANINI-Y', '60601000'),
(870, 13, 50, 'BARBAZA', '60602000'),
(871, 13, 50, 'BELISON', '60603000'),
(872, 13, 50, 'BUGASONG', '60604000'),
(873, 13, 50, 'CALUYA', '60605000'),
(874, 13, 50, 'CULASI', '60606000'),
(875, 13, 50, 'TOBIAS FORNIER (DAO)', '60607000'),
(876, 13, 50, 'HAMTIC', '60608000'),
(877, 13, 50, 'LAUA-AN', '60609000'),
(878, 13, 50, 'LIBERTAD', '60610000'),
(879, 13, 50, 'PANDAN', '60611000'),
(880, 13, 50, 'PATNONGON', '60612000'),
(881, 13, 50, 'SAN JOSE (Capital)', '60613000'),
(882, 13, 50, 'SAN REMIGIO', '60614000'),
(883, 13, 50, 'SEBASTE', '60615000'),
(884, 13, 50, 'SIBALOM', '60616000'),
(885, 13, 50, 'TIBIAO', '60617000'),
(886, 13, 50, 'VALDERRAMA', '60618000'),
(887, 13, 51, 'CUARTERO', '61901000'),
(888, 13, 51, 'DAO', '61902000'),
(889, 13, 51, 'DUMALAG', '61903000'),
(890, 13, 51, 'DUMARAO', '61904000'),
(891, 13, 51, 'IVISAN', '61905000'),
(892, 13, 51, 'JAMINDAN', '61906000'),
(893, 13, 51, 'MA-AYON', '61907000'),
(894, 13, 51, 'MAMBUSAO', '61908000'),
(895, 13, 51, 'PANAY', '61909000'),
(896, 13, 51, 'PANITAN', '61910000'),
(897, 13, 51, 'PILAR', '61911000'),
(898, 13, 51, 'PONTEVEDRA', '61912000'),
(899, 13, 51, 'PRESIDENT ROXAS', '61913000'),
(900, 13, 51, 'ROXAS CITY (Capital)', '61914000'),
(901, 13, 51, 'SAPI-AN', '61915000'),
(902, 13, 51, 'SIGMA', '61916000'),
(903, 13, 51, 'TAPAZ', '61917000'),
(904, 13, 52, 'BUENAVISTA', '67901000'),
(905, 13, 52, 'JORDAN (Capital)', '67902000'),
(906, 13, 52, 'NUEVA VALENCIA', '67903000'),
(907, 13, 52, 'SAN LORENZO', '67904000'),
(908, 13, 52, 'SIBUNAG', '67905000'),
(909, 13, 53, 'AJUY', '63001000'),
(910, 13, 53, 'ALIMODIAN', '63002000'),
(911, 13, 53, 'ANILAO', '63003000'),
(912, 13, 53, 'BADIANGAN', '63004000'),
(913, 13, 53, 'BALASAN', '63005000'),
(914, 13, 53, 'BANATE', '63006000'),
(915, 13, 53, 'BAROTAC NUEVO', '63007000'),
(916, 13, 53, 'BAROTAC VIEJO', '63008000'),
(917, 13, 53, 'BATAD', '63009000'),
(918, 13, 53, 'BINGAWAN', '63010000'),
(919, 13, 53, 'CABATUAN', '63012000'),
(920, 13, 53, 'CALINOG', '63013000'),
(921, 13, 53, 'CARLES', '63014000'),
(922, 13, 53, 'CONCEPCION', '63015000'),
(923, 13, 53, 'DINGLE', '63016000'),
(924, 13, 53, 'DUE?AS', '63017000'),
(925, 13, 53, 'DUMANGAS', '63018000'),
(926, 13, 53, 'ESTANCIA', '63019000'),
(927, 13, 53, 'GUIMBAL', '63020000'),
(928, 13, 53, 'IGBARAS', '63021000'),
(929, 13, 53, 'ILOILO CITY (Capital)', '63022000'),
(930, 13, 53, 'JANIUAY', '63023000'),
(931, 13, 53, 'LAMBUNAO', '63025000'),
(932, 13, 53, 'LEGANES', '63026000'),
(933, 13, 53, 'LEMERY', '63027000'),
(934, 13, 53, 'LEON', '63028000'),
(935, 13, 53, 'MAASIN', '63029000'),
(936, 13, 53, 'MIAGAO', '63030000'),
(937, 13, 53, 'MINA', '63031000'),
(938, 13, 53, 'NEW LUCENA', '63032000'),
(939, 13, 53, 'OTON', '63034000'),
(940, 13, 53, 'CITY OF PASSI', '63035000'),
(941, 13, 53, 'PAVIA', '63036000'),
(942, 13, 53, 'POTOTAN', '63037000'),
(943, 13, 53, 'SAN DIONISIO', '63038000'),
(944, 13, 53, 'SAN ENRIQUE', '63039000'),
(945, 13, 53, 'SAN JOAQUIN', '63040000'),
(946, 13, 53, 'SAN MIGUEL', '63041000'),
(947, 13, 53, 'SAN RAFAEL', '63042000'),
(948, 13, 53, 'SANTA BARBARA', '63043000'),
(949, 13, 53, 'SARA', '63044000'),
(950, 13, 53, 'TIGBAUAN', '63045000'),
(951, 13, 53, 'TUBUNGAN', '63046000'),
(952, 13, 53, 'ZARRAGA', '63047000'),
(2688, 14, 54, 'ALBURQUERQUE', '71201000'),
(2689, 14, 54, 'ALICIA', '71202000'),
(2690, 14, 54, '?ANDA', '71203000'),
(2691, 14, 54, 'ANTEQUERA', '71204000'),
(2692, 14, 54, 'BACLAYON', '71205000'),
(2693, 14, 54, 'BALILIHAN', '71206000'),
(2694, 14, 54, 'BATUAN', '71207000'),
(2695, 14, 54, 'BILAR', '71208000'),
(2696, 14, 54, 'BUENAVISTA', '71209000'),
(2697, 14, 54, 'CALAPE', '71210000'),
(2698, 14, 54, 'CANDIJAY', '71211000'),
(2699, 14, 54, 'CARMEN', '71212000'),
(2700, 14, 54, 'CATIGBIAN', '71213000'),
(2701, 14, 54, 'CLARIN', '71214000'),
(2702, 14, 54, 'CORELLA', '71215000'),
(2703, 14, 54, 'CORTES', '71216000'),
(2704, 14, 54, 'DAGOHOY', '71217000'),
(2705, 14, 54, 'DANAO', '71218000'),
(2706, 14, 54, 'DAUIS', '71219000'),
(2707, 14, 54, 'DIMIAO', '71220000'),
(2708, 14, 54, 'DUERO', '71221000'),
(2709, 14, 54, 'GARCIA HERNANDEZ', '71222000'),
(2710, 14, 54, 'GUINDULMAN', '71223000'),
(2711, 14, 54, 'INABANGA', '71224000'),
(2712, 14, 54, 'JAGNA', '71225000'),
(2713, 14, 54, 'GETAFE', '71226000'),
(2714, 14, 54, 'LILA', '71227000'),
(2715, 14, 54, 'LOAY', '71228000'),
(2716, 14, 54, 'LOBOC', '71229000'),
(2717, 14, 54, 'LOON', '71230000'),
(2718, 14, 54, 'MABINI', '71231000'),
(2719, 14, 54, 'MARIBOJOC', '71232000'),
(2720, 14, 54, 'PANGLAO', '71233000'),
(2721, 14, 54, 'PILAR', '71234000'),
(2722, 14, 54, 'PRES. CARLOS P. GARCIA (PITOGO)', '71235000'),
(2723, 14, 54, 'SAGBAYAN (BORJA)', '71236000'),
(2724, 14, 54, 'SAN ISIDRO', '71237000'),
(2725, 14, 54, 'SAN MIGUEL', '71238000'),
(2726, 14, 54, 'SEVILLA', '71239000'),
(2727, 14, 54, 'SIERRA BULLONES', '71240000'),
(2728, 14, 54, 'SIKATUNA', '71241000'),
(2729, 14, 54, 'TAGBILARAN CITY (Capital)', '71242000'),
(2730, 14, 54, 'TALIBON', '71243000'),
(2731, 14, 54, 'TRINIDAD', '71244000'),
(2732, 14, 54, 'TUBIGON', '71245000'),
(2733, 14, 54, 'UBAY', '71246000'),
(2734, 14, 54, 'VALENCIA', '71247000'),
(2735, 14, 54, 'BIEN UNIDO', '71248000'),
(2736, 14, 55, 'ALCANTARA', '72201000'),
(2737, 14, 55, 'ALCOY', '72202000'),
(2738, 14, 55, 'ALEGRIA', '72203000'),
(2739, 14, 55, 'ALOGUINSAN', '72204000'),
(2740, 14, 55, 'ARGAO', '72205000'),
(2741, 14, 55, 'ASTURIAS', '72206000'),
(2742, 14, 55, 'BADIAN', '72207000'),
(2743, 14, 55, 'BALAMBAN', '72208000'),
(2744, 14, 55, 'BANTAYAN', '72209000'),
(2745, 14, 55, 'BARILI', '72210000'),
(2746, 14, 55, 'CITY OF BOGO', '72211000'),
(2747, 14, 55, 'BOLJOON', '72212000'),
(2748, 14, 55, 'BORBON', '72213000'),
(2749, 14, 55, 'CITY OF CARCAR', '72214000'),
(2750, 14, 55, 'CARMEN', '72215000'),
(2751, 14, 55, 'CATMON', '72216000'),
(2752, 14, 55, 'CEBU CITY (Capital)', '72217000'),
(2753, 14, 55, 'COMPOSTELA', '72218000'),
(2754, 14, 55, 'CONSOLACION', '72219000'),
(2755, 14, 55, 'CORDOVA', '72220000'),
(2756, 14, 55, 'DAANBANTAYAN', '72221000'),
(2757, 14, 55, 'DALAGUETE', '72222000'),
(2758, 14, 55, 'DANAO CITY', '72223000'),
(2759, 14, 55, 'DUMANJUG', '72224000'),
(2760, 14, 55, 'GINATILAN', '72225000'),
(2761, 14, 55, 'LAPU-LAPU CITY (OPON)', '72226000'),
(2762, 14, 55, 'LILOAN', '72227000'),
(2763, 14, 55, 'MADRIDEJOS', '72228000'),
(2764, 14, 55, 'MALABUYOC', '72229000'),
(2765, 14, 55, 'MANDAUE CITY', '72230000'),
(2766, 14, 55, 'MEDELLIN', '72231000'),
(2767, 14, 55, 'MINGLANILLA', '72232000'),
(2768, 14, 55, 'MOALBOAL', '72233000'),
(2769, 14, 55, 'CITY OF NAGA', '72234000'),
(2770, 14, 55, 'OSLOB', '72235000'),
(2771, 14, 55, 'PILAR', '72236000'),
(2772, 14, 55, 'PINAMUNGAHAN', '72237000'),
(2773, 14, 55, 'PORO', '72238000'),
(2774, 14, 55, 'RONDA', '72239000'),
(2775, 14, 55, 'SAMBOAN', '72240000'),
(2776, 14, 55, 'SAN FERNANDO', '72241000'),
(2777, 14, 55, 'SAN FRANCISCO', '72242000'),
(2778, 14, 55, 'SAN REMIGIO', '72243000'),
(2779, 14, 55, 'SANTA FE', '72244000'),
(2780, 14, 55, 'SANTANDER', '72245000'),
(2781, 14, 55, 'SIBONGA', '72246000'),
(2782, 14, 55, 'SOGOD', '72247000'),
(2783, 14, 55, 'TABOGON', '72248000'),
(2784, 14, 55, 'TABUELAN', '72249000'),
(2785, 14, 55, 'CITY OF TALISAY', '72250000'),
(2786, 14, 55, 'TOLEDO CITY', '72251000'),
(2787, 14, 55, 'TUBURAN', '72252000'),
(2788, 14, 55, 'TUDELA', '72253000'),
(2789, 14, 56, 'ENRIQUE VILLANUEVA', '76101000'),
(2790, 14, 56, 'LARENA', '76102000'),
(2791, 14, 56, 'LAZI', '76103000'),
(2792, 14, 56, 'MARIA', '76104000'),
(2793, 14, 56, 'SAN JUAN', '76105000'),
(2794, 14, 56, 'SIQUIJOR (Capital)', '76106000'),
(2795, 15, 57, 'ALMERIA', '87801000'),
(2796, 15, 57, 'BILIRAN', '87802000'),
(2797, 15, 57, 'CABUCGAYAN', '87803000'),
(2798, 15, 57, 'CAIBIRAN', '87804000'),
(2799, 15, 57, 'CULABA', '87805000'),
(2800, 15, 57, 'KAWAYAN', '87806000'),
(2801, 15, 57, 'MARIPIPI', '87807000'),
(2802, 15, 57, 'NAVAL (Capital)', '87808000'),
(2803, 15, 58, '?ARTECHE', '82601000'),
(2804, 15, 58, 'BALANGIGA', '82602000'),
(2805, 15, 58, 'BALANGKAYAN', '82603000'),
(2806, 15, 58, 'CITY OF BORONGAN (Capital)', '82604000'),
(2807, 15, 58, 'CAN-AVID', '82605000'),
(2808, 15, 58, 'DOLORES', '82606000'),
(2809, 15, 58, 'GENERAL MACARTHUR', '82607000'),
(2810, 15, 58, 'GIPORLOS', '82608000'),
(2811, 15, 58, 'GUIUAN', '82609000'),
(2812, 15, 58, 'HERNANI', '82610000'),
(2813, 15, 58, 'JIPAPAD', '82611000'),
(2814, 15, 58, 'LAWAAN', '82612000'),
(2815, 15, 58, 'LLORENTE', '82613000'),
(2816, 15, 58, 'MASLOG', '82614000'),
(2817, 15, 58, 'MAYDOLONG', '82615000'),
(2818, 15, 58, 'MERCEDES', '82616000'),
(2819, 15, 58, 'ORAS', '82617000'),
(2820, 15, 58, 'QUINAPONDAN', '82618000'),
(2821, 15, 58, 'SALCEDO', '82619000'),
(2822, 15, 58, 'SAN JULIAN', '82620000'),
(2823, 15, 58, 'SAN POLICARPO', '82621000'),
(2824, 15, 58, 'SULAT', '82622000'),
(2825, 15, 58, 'TAFT', '82623000'),
(2826, 15, 59, 'ABUYOG', '83701000'),
(2827, 15, 59, 'ALANGALANG', '83702000'),
(2828, 15, 59, 'ALBUERA', '83703000'),
(2829, 15, 59, 'BABATNGON', '83705000'),
(2830, 15, 59, 'BARUGO', '83706000'),
(2831, 15, 59, 'BATO', '83707000'),
(2832, 15, 59, 'CITY OF BAYBAY', '83708000'),
(2833, 15, 59, 'BURAUEN', '83710000'),
(2834, 15, 59, 'CALUBIAN', '83713000'),
(2835, 15, 59, 'CAPOOCAN', '83714000'),
(2836, 15, 59, 'CARIGARA', '83715000'),
(2837, 15, 59, 'DAGAMI', '83717000'),
(2838, 15, 59, 'DULAG', '83718000'),
(2839, 15, 59, 'HILONGOS', '83719000'),
(2840, 15, 59, 'HINDANG', '83720000'),
(2841, 15, 59, 'INOPACAN', '83721000'),
(2842, 15, 59, 'ISABEL', '83722000'),
(2843, 15, 59, 'JARO', '83723000'),
(2844, 15, 59, 'JAVIER (BUGHO)', '83724000'),
(2845, 15, 59, 'JULITA', '83725000'),
(2846, 15, 59, 'KANANGA', '83726000'),
(2847, 15, 59, 'LA PAZ', '83728000'),
(2848, 15, 59, 'LEYTE', '83729000'),
(2849, 15, 59, 'MACARTHUR', '83730000'),
(2850, 15, 59, 'MAHAPLAG', '83731000'),
(2851, 15, 59, 'MATAG-OB', '83733000'),
(2852, 15, 59, 'MATALOM', '83734000'),
(2853, 15, 59, 'MAYORGA', '83735000'),
(2854, 15, 59, 'MERIDA', '83736000'),
(2855, 15, 59, 'ORMOC CITY', '83738000'),
(2856, 15, 59, 'PALO', '83739000'),
(2857, 15, 59, 'PALOMPON', '83740000'),
(2858, 15, 59, 'PASTRANA', '83741000'),
(2859, 15, 59, 'SAN ISIDRO', '83742000'),
(2860, 15, 59, 'SAN MIGUEL', '83743000'),
(2861, 15, 59, 'SANTA FE', '83744000'),
(2862, 15, 59, 'TABANGO', '83745000'),
(2863, 15, 59, 'TABONTABON', '83746000'),
(2864, 15, 59, 'TACLOBAN CITY (Capital)', '83747000'),
(2865, 15, 59, 'TANAUAN', '83748000'),
(2866, 15, 59, 'TOLOSA', '83749000'),
(2867, 15, 59, 'TUNGA', '83750000'),
(2868, 15, 59, 'VILLABA', '83751000'),
(2869, 15, 60, 'ALLEN', '84801000'),
(2870, 15, 60, 'BIRI', '84802000'),
(2871, 15, 60, 'BOBON', '84803000'),
(2872, 15, 60, 'CAPUL', '84804000'),
(2873, 15, 60, 'CATARMAN (Capital)', '84805000'),
(2874, 15, 60, 'CATUBIG', '84806000'),
(2875, 15, 60, 'GAMAY', '84807000'),
(2876, 15, 60, 'LAOANG', '84808000'),
(2877, 15, 60, 'LAPINIG', '84809000'),
(2878, 15, 60, 'LAS NAVAS', '84810000'),
(2879, 15, 60, 'LAVEZARES', '84811000'),
(2880, 15, 60, 'MAPANAS', '84812000'),
(2881, 15, 60, 'MONDRAGON', '84813000'),
(2882, 15, 60, 'PALAPAG', '84814000'),
(2883, 15, 60, 'PAMBUJAN', '84815000'),
(2884, 15, 60, 'ROSARIO', '84816000'),
(2885, 15, 60, 'SAN ANTONIO', '84817000'),
(2886, 15, 60, 'SAN ISIDRO', '84818000'),
(2887, 15, 60, 'SAN JOSE', '84819000'),
(2888, 15, 60, 'SAN ROQUE', '84820000'),
(2889, 15, 60, 'SAN VICENTE', '84821000'),
(2890, 15, 60, 'SILVINO LOBOS', '84822000'),
(2891, 15, 60, 'VICTORIA', '84823000'),
(2892, 15, 60, 'LOPE DE VEGA', '84824000'),
(2893, 15, 61, 'ALMAGRO', '86001000'),
(2894, 15, 61, 'BASEY', '86002000'),
(2895, 15, 61, 'CALBAYOG CITY', '86003000'),
(2896, 15, 61, 'CALBIGA', '86004000'),
(2897, 15, 61, 'CITY OF CATBALOGAN (Capital)', '86005000'),
(2898, 15, 61, 'DARAM', '86006000'),
(2899, 15, 61, 'GANDARA', '86007000'),
(2900, 15, 61, 'HINABANGAN', '86008000'),
(2901, 15, 61, 'JIABONG', '86009000'),
(2902, 15, 61, 'MARABUT', '86010000'),
(2903, 15, 61, 'MATUGUINAO', '86011000'),
(2904, 15, 61, 'MOTIONG', '86012000'),
(2905, 15, 61, 'PINABACDAO', '86013000'),
(2906, 15, 61, 'SAN JOSE DE BUAN', '86014000'),
(2907, 15, 61, 'SAN SEBASTIAN', '86015000'),
(2908, 15, 61, 'SANTA MARGARITA', '86016000'),
(2909, 15, 61, 'SANTA RITA', '86017000'),
(2910, 15, 61, 'SANTO NI?O', '86018000'),
(2911, 15, 61, 'TALALORA', '86019000'),
(2912, 15, 61, 'TARANGNAN', '86020000'),
(2913, 15, 61, 'VILLAREAL', '86021000'),
(2914, 15, 61, 'PARANAS (WRIGHT)', '86022000'),
(2915, 15, 61, 'ZUMARRAGA', '86023000'),
(2916, 15, 61, 'TAGAPUL-AN', '86024000'),
(2917, 15, 61, 'SAN JORGE', '86025000'),
(2918, 15, 61, 'PAGSANGHAN', '86026000'),
(2919, 15, 62, 'ANAHAWAN', '86401000'),
(2920, 15, 62, 'BONTOC', '86402000'),
(2921, 15, 62, 'HINUNANGAN', '86403000'),
(2922, 15, 62, 'HINUNDAYAN', '86404000'),
(2923, 15, 62, 'LIBAGON', '86405000'),
(2924, 15, 62, 'LILOAN', '86406000'),
(2925, 15, 62, 'CITY OF MAASIN (Capital)', '86407000'),
(2926, 15, 62, 'MACROHON', '86408000'),
(2927, 15, 62, 'MALITBOG', '86409000'),
(2928, 15, 62, 'PADRE BURGOS', '86410000'),
(2929, 15, 62, 'PINTUYAN', '86411000'),
(2930, 15, 62, 'SAINT BERNARD', '86412000'),
(2931, 15, 62, 'SAN FRANCISCO', '86413000'),
(2932, 15, 62, 'SAN JUAN (CABALIAN)', '86414000'),
(2933, 15, 62, 'SAN RICARDO', '86415000'),
(2934, 15, 62, 'SILAGO', '86416000'),
(2935, 15, 62, 'SOGOD', '86417000'),
(2936, 15, 62, 'TOMAS OPPUS', '86418000'),
(2937, 15, 62, 'LIMASAWA', '86419000'),
(2938, 16, 63, 'CITY OF ISABELA (Capital)', '99701000'),
(2939, 16, 64, 'DAPITAN CITY', '97201000'),
(2940, 16, 64, 'DIPOLOG CITY (Capital)', '97202000'),
(2941, 16, 64, 'KATIPUNAN', '97203000'),
(2942, 16, 64, 'LA LIBERTAD', '97204000'),
(2943, 16, 64, 'LABASON', '97205000'),
(2944, 16, 64, 'LILOY', '97206000'),
(2945, 16, 64, 'MANUKAN', '97207000'),
(2946, 16, 64, 'MUTIA', '97208000'),
(2947, 16, 64, 'PI?AN (NEW PI?AN)', '97209000'),
(2948, 16, 64, 'POLANCO', '97210000'),
(2949, 16, 64, 'PRES. MANUEL A. ROXAS', '97211000'),
(2950, 16, 64, 'RIZAL', '97212000'),
(2951, 16, 64, 'SALUG', '97213000'),
(2952, 16, 64, 'SERGIO OSME?A SR.', '97214000'),
(2953, 16, 64, 'SIAYAN', '97215000'),
(2954, 16, 64, 'SIBUCO', '97216000'),
(2955, 16, 64, 'SIBUTAD', '97217000'),
(2956, 16, 64, 'SINDANGAN', '97218000'),
(2957, 16, 64, 'SIOCON', '97219000'),
(2958, 16, 64, 'SIRAWAI', '97220000'),
(2959, 16, 64, 'TAMPILISAN', '97221000'),
(2960, 16, 64, 'JOSE DALMAN (PONOT)', '97222000'),
(2961, 16, 64, 'GUTALAC', '97223000'),
(2962, 16, 64, 'BALIGUIAN', '97224000'),
(2963, 16, 64, 'GODOD', '97225000'),
(2964, 16, 64, 'BACUNGAN (Leon T. Postigo)', '97226000'),
(2965, 16, 64, 'KALAWIT', '97227000'),
(2966, 16, 65, 'AURORA', '97302000'),
(2967, 16, 65, 'BAYOG', '97303000'),
(2968, 16, 65, 'DIMATALING', '97305000'),
(2969, 16, 65, 'DINAS', '97306000'),
(2970, 16, 65, 'DUMALINAO', '97307000'),
(2971, 16, 65, 'DUMINGAG', '97308000'),
(2972, 16, 65, 'KUMALARANG', '97311000'),
(2973, 16, 65, 'LABANGAN', '97312000'),
(2974, 16, 65, 'LAPUYAN', '97313000'),
(2975, 16, 65, 'MAHAYAG', '97315000'),
(2976, 16, 65, 'MARGOSATUBIG', '97317000'),
(2977, 16, 65, 'MIDSALIP', '97318000'),
(2978, 16, 65, 'MOLAVE', '97319000'),
(2979, 16, 65, 'PAGADIAN CITY (Capital)', '97322000'),
(2980, 16, 65, 'RAMON MAGSAYSAY (LIARGO)', '97323000'),
(2981, 16, 65, 'SAN MIGUEL', '97324000'),
(2982, 16, 65, 'SAN PABLO', '97325000'),
(2983, 16, 65, 'TABINA', '97327000'),
(2984, 16, 65, 'TAMBULIG', '97328000'),
(2985, 16, 65, 'TUKURAN', '97330000'),
(2986, 16, 65, 'ZAMBOANGA CITY', '97332000'),
(2987, 16, 65, 'LAKEWOOD', '97333000'),
(2988, 16, 65, 'JOSEFINA', '97337000'),
(2989, 16, 65, 'PITOGO', '97338000'),
(2990, 16, 65, 'SOMINOT (DON MARIANO MARCOS)', '97340000'),
(2991, 16, 65, 'VINCENZO A. SAGUN', '97341000'),
(2992, 16, 65, 'GUIPOS', '97343000'),
(2993, 16, 65, 'TIGBAO', '97344000'),
(2994, 16, 66, 'ALICIA', '98301000'),
(2995, 16, 66, 'BUUG', '98302000'),
(2996, 16, 66, 'DIPLAHAN', '98303000'),
(2997, 16, 66, 'IMELDA', '98304000'),
(2998, 16, 66, 'IPIL (Capital)', '98305000'),
(2999, 16, 66, 'KABASALAN', '98306000'),
(3000, 16, 66, 'MABUHAY', '98307000'),
(3001, 16, 66, 'MALANGAS', '98308000'),
(3002, 16, 66, 'NAGA', '98309000'),
(3003, 16, 66, 'OLUTANGA', '98310000'),
(3004, 16, 66, 'PAYAO', '98311000'),
(3005, 16, 66, 'ROSELLER LIM', '98312000'),
(3006, 16, 66, 'SIAY', '98313000'),
(3007, 16, 66, 'TALUSAN', '98314000'),
(3008, 16, 66, 'TITAY', '98315000'),
(3009, 16, 66, 'TUNGAWAN', '98316000'),
(3010, 17, 67, 'BAUNGON', '101301000'),
(3011, 17, 67, 'DAMULOG', '101302000'),
(3012, 17, 67, 'DANGCAGAN', '101303000'),
(3013, 17, 67, 'DON CARLOS', '101304000'),
(3014, 17, 67, 'IMPASUG-ONG', '101305000'),
(3015, 17, 67, 'KADINGILAN', '101306000'),
(3016, 17, 67, 'KALILANGAN', '101307000'),
(3017, 17, 67, 'KIBAWE', '101308000'),
(3018, 17, 67, 'KITAOTAO', '101309000'),
(3019, 17, 67, 'LANTAPAN', '101310000'),
(3020, 17, 67, 'LIBONA', '101311000'),
(3021, 17, 67, 'CITY OF MALAYBALAY (Capital)', '101312000'),
(3022, 17, 67, 'MALITBOG', '101313000'),
(3023, 17, 67, 'MANOLO FORTICH', '101314000'),
(3024, 17, 67, 'MARAMAG', '101315000'),
(3025, 17, 67, 'PANGANTUCAN', '101316000'),
(3026, 17, 67, 'QUEZON', '101317000'),
(3027, 17, 67, 'SAN FERNANDO', '101318000'),
(3028, 17, 67, 'SUMILAO', '101319000'),
(3029, 17, 67, 'TALAKAG', '101320000'),
(3030, 17, 67, 'CITY OF VALENCIA', '101321000'),
(3031, 17, 67, 'CABANGLASAN', '101322000'),
(3032, 17, 68, 'CATARMAN', '101801000'),
(3033, 17, 68, 'GUINSILIBAN', '101802000'),
(3034, 17, 68, 'MAHINOG', '101803000'),
(3035, 17, 68, 'MAMBAJAO (Capital)', '101804000'),
(3036, 17, 68, 'SAGAY', '101805000'),
(3037, 17, 69, 'BACOLOD', '103501000'),
(3038, 17, 69, 'BALOI', '103502000'),
(3039, 17, 69, 'BAROY', '103503000'),
(3040, 17, 69, 'ILIGAN CITY', '103504000'),
(3041, 17, 69, 'KAPATAGAN', '103505000'),
(3042, 17, 69, 'SULTAN NAGA DIMAPORO (KAROMATAN)', '103506000'),
(3043, 17, 69, 'KAUSWAGAN', '103507000'),
(3044, 17, 69, 'KOLAMBUGAN', '103508000'),
(3045, 17, 69, 'LALA', '103509000'),
(3046, 17, 69, 'LINAMON', '103510000'),
(3047, 17, 69, 'MAGSAYSAY', '103511000'),
(3048, 17, 69, 'MAIGO', '103512000'),
(3049, 17, 69, 'MATUNGAO', '103513000'),
(3050, 17, 69, 'MUNAI', '103514000'),
(3051, 17, 69, 'NUNUNGAN', '103515000');
INSERT INTO `city` (`city_id`, `region_id`, `province_id`, `city_name`, `city_code`) VALUES
(3052, 17, 69, 'PANTAO RAGAT', '103516000'),
(3053, 17, 69, 'POONA PIAGAPO', '103517000'),
(3054, 17, 69, 'SALVADOR', '103518000'),
(3055, 17, 69, 'SAPAD', '103519000'),
(3056, 17, 69, 'TAGOLOAN', '103520000'),
(3057, 17, 69, 'TANGCAL', '103521000'),
(3058, 17, 69, 'TUBOD (Capital)', '103522000'),
(3059, 17, 69, 'PANTAR', '103523000'),
(3060, 17, 70, 'ALORAN', '104201000'),
(3061, 17, 70, 'BALIANGAO', '104202000'),
(3062, 17, 70, 'BONIFACIO', '104203000'),
(3063, 17, 70, 'CALAMBA', '104204000'),
(3064, 17, 70, 'CLARIN', '104205000'),
(3065, 17, 70, 'CONCEPCION', '104206000'),
(3066, 17, 70, 'JIMENEZ', '104207000'),
(3067, 17, 70, 'LOPEZ JAENA', '104208000'),
(3068, 17, 70, 'OROQUIETA CITY (Capital)', '104209000'),
(3069, 17, 70, 'OZAMIZ CITY', '104210000'),
(3070, 17, 70, 'PANAON', '104211000'),
(3071, 17, 70, 'PLARIDEL', '104212000'),
(3072, 17, 70, 'SAPANG DALAGA', '104213000'),
(3073, 17, 70, 'SINACABAN', '104214000'),
(3074, 17, 70, 'TANGUB CITY', '104215000'),
(3075, 17, 70, 'TUDELA', '104216000'),
(3076, 17, 70, 'DON VICTORIANO CHIONGBIAN (DON MARIANO MARCOS)', '104217000'),
(3077, 17, 71, 'ALUBIJID', '104301000'),
(3078, 17, 71, 'BALINGASAG', '104302000'),
(3079, 17, 71, 'BALINGOAN', '104303000'),
(3080, 17, 71, 'BINUANGAN', '104304000'),
(3081, 17, 71, 'CAGAYAN DE ORO CITY (Capital)', '104305000'),
(3082, 17, 71, 'CLAVERIA', '104306000'),
(3083, 17, 71, 'CITY OF EL SALVADOR', '104307000'),
(3084, 17, 71, 'GINGOOG CITY', '104308000'),
(3085, 17, 71, 'GITAGUM', '104309000'),
(3086, 17, 71, 'INITAO', '104310000'),
(3087, 17, 71, 'JASAAN', '104311000'),
(3088, 17, 71, 'KINOGUITAN', '104312000'),
(3089, 17, 71, 'LAGONGLONG', '104313000'),
(3090, 17, 71, 'LAGUINDINGAN', '104314000'),
(3091, 17, 71, 'LIBERTAD', '104315000'),
(3092, 17, 71, 'LUGAIT', '104316000'),
(3093, 17, 71, 'MAGSAYSAY (LINUGOS)', '104317000'),
(3094, 17, 71, 'MANTICAO', '104318000'),
(3095, 17, 71, 'MEDINA', '104319000'),
(3096, 17, 71, 'NAAWAN', '104320000'),
(3097, 17, 71, 'OPOL', '104321000'),
(3098, 17, 71, 'SALAY', '104322000'),
(3099, 17, 71, 'SUGBONGCOGON', '104323000'),
(3100, 17, 71, 'TAGOLOAN', '104324000'),
(3101, 17, 71, 'TALISAYAN', '104325000'),
(3102, 17, 71, 'VILLANUEVA', '104326000'),
(3103, 18, 72, 'COMPOSTELA', '118201000'),
(3104, 18, 72, 'LAAK (SAN VICENTE)', '118202000'),
(3105, 18, 72, 'MABINI (DO?A ALICIA)', '118203000'),
(3106, 18, 72, 'MACO', '118204000'),
(3107, 18, 72, 'MARAGUSAN (SAN MARIANO)', '118205000'),
(3108, 18, 72, 'MAWAB', '118206000'),
(3109, 18, 72, 'MONKAYO', '118207000'),
(3110, 18, 72, 'MONTEVISTA', '118208000'),
(3111, 18, 72, 'NABUNTURAN (Capital)', '118209000'),
(3112, 18, 72, 'NEW BATAAN', '118210000'),
(3113, 18, 72, 'PANTUKAN', '118211000'),
(3114, 18, 73, 'ASUNCION (SAUG)', '112301000'),
(3115, 18, 73, 'CARMEN', '112303000'),
(3116, 18, 73, 'KAPALONG', '112305000'),
(3117, 18, 73, 'NEW CORELLA', '112314000'),
(3118, 18, 73, 'CITY OF PANABO', '112315000'),
(3119, 18, 73, 'ISLAND GARDEN CITY OF SAMAL', '112317000'),
(3120, 18, 73, 'SANTO TOMAS', '112318000'),
(3121, 18, 73, 'CITY OF TAGUM (Capital)', '112319000'),
(3122, 18, 73, 'TALAINGOD', '112322000'),
(3123, 18, 73, 'BRAULIO E. DUJALI', '112323000'),
(3124, 18, 73, 'SAN ISIDRO', '112324000'),
(3125, 18, 74, 'BANSALAN', '112401000'),
(3126, 18, 74, 'DAVAO CITY', '112402000'),
(3127, 18, 74, 'CITY OF DIGOS (Capital)', '112403000'),
(3128, 18, 74, 'HAGONOY', '112404000'),
(3129, 18, 74, 'KIBLAWAN', '112406000'),
(3130, 18, 74, 'MAGSAYSAY', '112407000'),
(3131, 18, 74, 'MALALAG', '112408000'),
(3132, 18, 74, 'MATANAO', '112410000'),
(3133, 18, 74, 'PADADA', '112411000'),
(3134, 18, 74, 'SANTA CRUZ', '112412000'),
(3135, 18, 74, 'SULOP', '112414000'),
(3136, 18, 75, 'DON MARCELINO', '118601000'),
(3137, 18, 75, 'JOSE ABAD SANTOS (TRINIDAD)', '118602000'),
(3138, 18, 75, 'MALITA', '118603000'),
(3139, 18, 75, 'SANTA MARIA', '118604000'),
(3140, 18, 75, 'SARANGANI', '118605000'),
(3141, 18, 76, 'BAGANGA', '112501000'),
(3142, 18, 76, 'BANAYBANAY', '112502000'),
(3143, 18, 76, 'BOSTON', '112503000'),
(3144, 18, 76, 'CARAGA', '112504000'),
(3145, 18, 76, 'CATEEL', '112505000'),
(3146, 18, 76, 'GOVERNOR GENEROSO', '112506000'),
(3147, 18, 76, 'LUPON', '112507000'),
(3148, 18, 76, 'MANAY', '112508000'),
(3149, 18, 76, 'CITY OF MATI (Capital)', '112509000'),
(3150, 18, 76, 'SAN ISIDRO', '112510000'),
(3151, 18, 76, 'TARRAGONA', '112511000'),
(3152, 19, 77, 'ALAMADA', '124701000'),
(3153, 19, 77, 'CARMEN', '124702000'),
(3154, 19, 77, 'KABACAN', '124703000'),
(3155, 19, 77, 'CITY OF KIDAPAWAN (Capital)', '124704000'),
(3156, 19, 77, 'LIBUNGAN', '124705000'),
(3157, 19, 77, 'MAGPET', '124706000'),
(3158, 19, 77, 'MAKILALA', '124707000'),
(3159, 19, 77, 'MATALAM', '124708000'),
(3160, 19, 77, 'MIDSAYAP', '124709000'),
(3161, 19, 77, 'M''LANG', '124710000'),
(3162, 19, 77, 'PIGKAWAYAN', '124711000'),
(3163, 19, 77, 'PIKIT', '124712000'),
(3164, 19, 77, 'PRESIDENT ROXAS', '124713000'),
(3165, 19, 77, 'TULUNAN', '124714000'),
(3166, 19, 77, 'ANTIPAS', '124715000'),
(3167, 19, 77, 'BANISILAN', '124716000'),
(3168, 19, 77, 'ALEOSAN', '124717000'),
(3169, 19, 77, 'ARAKAN', '124718000'),
(3170, 19, 78, '?COTABATO CITY', '129804000'),
(3171, 19, 79, 'ALABEL (Capital)', '128001000'),
(3172, 19, 79, 'GLAN', '128002000'),
(3173, 19, 79, 'KIAMBA', '128003000'),
(3174, 19, 79, 'MAASIM', '128004000'),
(3175, 19, 79, 'MAITUM', '128005000'),
(3176, 19, 79, 'MALAPATAN', '128006000'),
(3177, 19, 79, 'MALUNGON', '128007000'),
(3178, 19, 80, 'BANGA', '126302000'),
(3179, 19, 80, 'GENERAL SANTOS CITY (DADIANGAS)', '126303000'),
(3180, 19, 80, 'CITY OF KORONADAL (Capital)', '126306000'),
(3181, 19, 80, 'NORALA', '126311000'),
(3182, 19, 80, 'POLOMOLOK', '126312000'),
(3183, 19, 80, 'SURALLAH', '126313000'),
(3184, 19, 80, 'TAMPAKAN', '126314000'),
(3185, 19, 80, 'TANTANGAN', '126315000'),
(3186, 19, 80, 'T''BOLI', '126316000'),
(3187, 19, 80, 'TUPI', '126317000'),
(3188, 19, 80, 'SANTO NI?O', '126318000'),
(3189, 19, 80, 'LAKE SEBU', '126319000'),
(3190, 19, 81, 'BAGUMBAYAN', '126501000'),
(3191, 19, 81, 'COLUMBIO', '126502000'),
(3192, 19, 81, 'ESPERANZA', '126503000'),
(3193, 19, 81, 'ISULAN (Capital)', '126504000'),
(3194, 19, 81, 'KALAMANSIG', '126505000'),
(3195, 19, 81, 'LEBAK', '126506000'),
(3196, 19, 81, 'LUTAYAN', '126507000'),
(3197, 19, 81, 'LAMBAYONG (MARIANO MARCOS)', '126508000'),
(3198, 19, 81, 'PALIMBANG', '126509000'),
(3199, 19, 81, 'PRESIDENT QUIRINO', '126510000'),
(3200, 19, 81, 'CITY OF TACURONG', '126511000'),
(3201, 19, 81, 'SEN. NINOY AQUINO', '126512000'),
(3202, 20, 82, 'BUENAVISTA', '160201000'),
(3203, 20, 82, 'BUTUAN CITY (Capital)', '160202000'),
(3204, 20, 82, 'CITY OF CABADBARAN', '160203000'),
(3205, 20, 82, 'CARMEN', '160204000'),
(3206, 20, 82, 'JABONGA', '160205000'),
(3207, 20, 82, 'KITCHARAO', '160206000'),
(3208, 20, 82, 'LAS NIEVES', '160207000'),
(3209, 20, 82, 'MAGALLANES', '160208000'),
(3210, 20, 82, 'NASIPIT', '160209000'),
(3211, 20, 82, 'SANTIAGO', '160210000'),
(3212, 20, 82, 'TUBAY', '160211000'),
(3213, 20, 82, 'REMEDIOS T. ROMUALDEZ', '160212000'),
(3214, 20, 83, 'CITY OF BAYUGAN', '160301000'),
(3215, 20, 83, 'BUNAWAN', '160302000'),
(3216, 20, 83, 'ESPERANZA', '160303000'),
(3217, 20, 83, 'LA PAZ', '160304000'),
(3218, 20, 83, 'LORETO', '160305000'),
(3219, 20, 83, 'PROSPERIDAD (Capital)', '160306000'),
(3220, 20, 83, 'ROSARIO', '160307000'),
(3221, 20, 83, 'SAN FRANCISCO', '160308000'),
(3222, 20, 83, 'SAN LUIS', '160309000'),
(3223, 20, 83, 'SANTA JOSEFA', '160310000'),
(3224, 20, 83, 'TALACOGON', '160311000'),
(3225, 20, 83, 'TRENTO', '160312000'),
(3226, 20, 83, 'VERUELA', '160313000'),
(3227, 20, 83, 'SIBAGAT', '160314000'),
(3228, 20, 84, 'BASILISA (RIZAL)', '168501000'),
(3229, 20, 84, 'CAGDIANAO', '168502000'),
(3230, 20, 84, 'DINAGAT', '168503000'),
(3231, 20, 84, 'LIBJO (ALBOR)', '168504000'),
(3232, 20, 84, 'LORETO', '168505000'),
(3233, 20, 84, 'SAN JOSE (Capital)', '168506000'),
(3234, 20, 84, 'TUBAJON', '168507000'),
(3235, 20, 85, 'ALEGRIA', '166701000'),
(3236, 20, 85, 'BACUAG', '166702000'),
(3237, 20, 85, 'BURGOS', '166704000'),
(3238, 20, 85, 'CLAVER', '166706000'),
(3239, 20, 85, 'DAPA', '166707000'),
(3240, 20, 85, 'DEL CARMEN', '166708000'),
(3241, 20, 85, 'GENERAL LUNA', '166710000'),
(3242, 20, 85, 'GIGAQUIT', '166711000'),
(3243, 20, 85, 'MAINIT', '166714000'),
(3244, 20, 85, 'MALIMONO', '166715000'),
(3245, 20, 85, 'PILAR', '166716000'),
(3246, 20, 85, 'PLACER', '166717000'),
(3247, 20, 85, 'SAN BENITO', '166718000'),
(3248, 20, 85, 'SAN FRANCISCO (ANAO-AON)', '166719000'),
(3249, 20, 85, 'SAN ISIDRO', '166720000'),
(3250, 20, 85, 'SANTA MONICA (SAPAO)', '166721000'),
(3251, 20, 85, 'SISON', '166722000'),
(3252, 20, 85, 'SOCORRO', '166723000'),
(3253, 20, 85, 'SURIGAO CITY (Capital)', '166724000'),
(3254, 20, 85, 'TAGANA-AN', '166725000'),
(3255, 20, 85, 'TUBOD', '166727000'),
(3256, 20, 86, 'BAROBO', '166801000'),
(3257, 20, 86, 'BAYABAS', '166802000'),
(3258, 20, 86, 'CITY OF BISLIG', '166803000'),
(3259, 20, 86, 'CAGWAIT', '166804000'),
(3260, 20, 86, 'CANTILAN', '166805000'),
(3261, 20, 86, 'CARMEN', '166806000'),
(3262, 20, 86, 'CARRASCAL', '166807000'),
(3263, 20, 86, 'CORTES', '166808000'),
(3264, 20, 86, 'HINATUAN', '166809000'),
(3265, 20, 86, 'LANUZA', '166810000'),
(3266, 20, 86, 'LIANGA', '166811000'),
(3267, 20, 86, 'LINGIG', '166812000'),
(3268, 20, 86, 'MADRID', '166813000'),
(3269, 20, 86, 'MARIHATAG', '166814000'),
(3270, 20, 86, 'SAN AGUSTIN', '166815000'),
(3271, 20, 86, 'SAN MIGUEL', '166816000'),
(3272, 20, 86, 'TAGBINA', '166817000'),
(3273, 20, 86, 'TAGO', '166818000'),
(3274, 20, 86, 'CITY OF TANDAG (Capital)', '166819000'),
(3275, 21, 87, 'CITY OF LAMITAN', '150702000'),
(3276, 21, 87, 'LANTAWAN', '150703000'),
(3277, 21, 87, 'MALUSO', '150704000'),
(3278, 21, 87, 'SUMISIP', '150705000'),
(3279, 21, 87, 'TIPO-TIPO', '150706000'),
(3280, 21, 87, 'TUBURAN', '150707000'),
(3281, 21, 87, 'AKBAR', '150708000'),
(3282, 21, 87, 'AL-BARKA', '150709000'),
(3283, 21, 87, 'HADJI MOHAMMAD AJUL', '150710000'),
(3284, 21, 87, 'UNGKAYA PUKAN', '150711000'),
(3285, 21, 87, 'HADJI MUHTAMAD', '150712000'),
(3286, 21, 87, 'TABUAN-LASA', '150713000'),
(3287, 21, 88, 'BACOLOD-KALAWI (BACOLOD GRANDE)', '153601000'),
(3288, 21, 88, 'BALABAGAN', '153602000'),
(3289, 21, 88, 'BALINDONG (WATU)', '153603000'),
(3290, 21, 88, 'BAYANG', '153604000'),
(3291, 21, 88, 'BINIDAYAN', '153605000'),
(3292, 21, 88, 'BUBONG', '153606000'),
(3293, 21, 88, 'BUTIG', '153607000'),
(3294, 21, 88, 'GANASSI', '153609000'),
(3295, 21, 88, 'KAPAI', '153610000'),
(3296, 21, 88, 'LUMBA-BAYABAO (MAGUING)', '153611000'),
(3297, 21, 88, 'LUMBATAN', '153612000'),
(3298, 21, 88, 'MADALUM', '153613000'),
(3299, 21, 88, 'MADAMBA', '153614000'),
(3300, 21, 88, 'MALABANG', '153615000'),
(3301, 21, 88, 'MARANTAO', '153616000'),
(3302, 21, 88, 'MARAWI CITY (Capital)', '153617000'),
(3303, 21, 88, 'MASIU', '153618000'),
(3304, 21, 88, 'MULONDO', '153619000'),
(3305, 21, 88, 'PAGAYAWAN (TATARIKAN)', '153620000'),
(3306, 21, 88, 'PIAGAPO', '153621000'),
(3307, 21, 88, 'POONA BAYABAO (GATA)', '153622000'),
(3308, 21, 88, 'PUALAS', '153623000'),
(3309, 21, 88, 'DITSAAN-RAMAIN', '153624000'),
(3310, 21, 88, 'SAGUIARAN', '153625000'),
(3311, 21, 88, 'TAMPARAN', '153626000'),
(3312, 21, 88, 'TARAKA', '153627000'),
(3313, 21, 88, 'TUBARAN', '153628000'),
(3314, 21, 88, 'TUGAYA', '153629000'),
(3315, 21, 88, 'WAO', '153630000'),
(3316, 21, 88, 'MAROGONG', '153631000'),
(3317, 21, 88, 'CALANOGAS', '153632000'),
(3318, 21, 88, 'BUADIPOSO-BUNTONG', '153633000'),
(3319, 21, 88, 'MAGUING', '153634000'),
(3320, 21, 88, 'PICONG (SULTAN GUMANDER)', '153635000'),
(3321, 21, 88, 'LUMBAYANAGUE', '153636000'),
(3322, 21, 88, 'BUMBARAN', '153637000'),
(3323, 21, 88, 'TAGOLOAN II', '153638000'),
(3324, 21, 88, 'KAPATAGAN', '153639000'),
(3325, 21, 88, 'SULTAN DUMALONDONG', '153640000'),
(3326, 21, 88, 'LUMBACA-UNAYAN', '153641000'),
(3327, 21, 89, 'AMPATUAN', '153801000'),
(3328, 21, 89, 'BULDON', '153802000'),
(3329, 21, 89, 'BULUAN', '153803000'),
(3330, 21, 89, 'DATU PAGLAS', '153805000'),
(3331, 21, 89, 'DATU PIANG', '153806000'),
(3332, 21, 89, 'DATU ODIN SINSUAT (DINAIG)', '153807000'),
(3333, 21, 89, 'SHARIFF AGUAK (MAGANOY) (Capital)', '153808000'),
(3334, 21, 89, 'MATANOG', '153809000'),
(3335, 21, 89, 'PAGALUNGAN', '153810000'),
(3336, 21, 89, 'PARANG', '153811000'),
(3337, 21, 89, 'SULTAN KUDARAT (NULING)', '153812000'),
(3338, 21, 89, 'SULTAN SA BARONGIS (LAMBAYONG)', '153813000'),
(3339, 21, 89, 'KABUNTALAN (TUMBAO)', '153814000'),
(3340, 21, 89, 'UPI', '153815000'),
(3341, 21, 89, 'TALAYAN', '153816000'),
(3342, 21, 89, 'SOUTH UPI', '153817000'),
(3343, 21, 89, 'BARIRA', '153818000'),
(3344, 21, 89, 'GEN. S.K. PENDATUN', '153819000'),
(3345, 21, 89, 'MAMASAPANO', '153820000'),
(3346, 21, 89, 'TALITAY', '153821000'),
(3347, 21, 89, 'PAGAGAWAN', '153822000'),
(3348, 21, 89, 'PAGLAT', '153823000'),
(3349, 21, 89, 'SULTAN MASTURA', '153824000'),
(3350, 21, 89, 'GUINDULUNGAN', '153825000'),
(3351, 21, 89, 'DATU SAUDI-AMPATUAN', '153826000'),
(3352, 21, 89, 'DATU UNSAY', '153827000'),
(3353, 21, 89, 'DATU ABDULLAH SANGKI', '153828000'),
(3354, 21, 89, 'RAJAH BUAYAN', '153829000'),
(3355, 21, 89, 'DATU BLAH T. SINSUAT', '153830000'),
(3356, 21, 89, 'DATU ANGGAL MIDTIMBANG', '153831000'),
(3357, 21, 89, 'MANGUDADATU', '153832000'),
(3358, 21, 89, 'PANDAG', '153833000'),
(3359, 21, 89, 'NORTHERN KABUNTALAN', '153834000'),
(3360, 21, 89, 'DATU HOFFER AMPATUAN', '153835000'),
(3361, 21, 89, 'DATU SALIBO', '153836000'),
(3362, 21, 89, 'SHARIFF SAYDONA MUSTAPHA', '153837000'),
(3363, 21, 90, 'INDANAN', '156601000'),
(3364, 21, 90, 'JOLO (Capital)', '156602000'),
(3365, 21, 90, 'KALINGALAN CALUANG', '156603000'),
(3366, 21, 90, 'LUUK', '156604000'),
(3367, 21, 90, 'MAIMBUNG', '156605000'),
(3368, 21, 90, 'HADJI PANGLIMA TAHIL (MARUNGGAS)', '156606000'),
(3369, 21, 90, 'OLD PANAMAO', '156607000'),
(3370, 21, 90, 'PANGUTARAN', '156608000'),
(3371, 21, 90, 'PARANG', '156609000'),
(3372, 21, 90, 'PATA', '156610000'),
(3373, 21, 90, 'PATIKUL', '156611000'),
(3374, 21, 90, 'SIASI', '156612000'),
(3375, 21, 90, 'TALIPAO', '156613000'),
(3376, 21, 90, 'TAPUL', '156614000'),
(3377, 21, 90, 'TONGKIL', '156615000'),
(3378, 21, 90, 'PANGLIMA ESTINO (NEW PANAMAO)', '156616000'),
(3379, 21, 90, 'LUGUS', '156617000'),
(3380, 21, 90, 'PANDAMI', '156618000'),
(3381, 21, 90, 'OMAR', '156619000'),
(3382, 21, 91, 'PANGLIMA SUGALA (BALIMBING)', '157001000'),
(3383, 21, 91, 'BONGAO (Capital)', '157002000'),
(3384, 21, 91, 'MAPUN (CAGAYAN DE TAWI-TAWI)', '157003000'),
(3385, 21, 91, 'SIMUNUL', '157004000'),
(3386, 21, 91, 'SITANGKAI', '157005000'),
(3387, 21, 91, 'SOUTH UBIAN', '157006000'),
(3388, 21, 91, 'TANDUBAS', '157007000'),
(3389, 21, 91, 'TURTLE ISLANDS', '157008000'),
(3390, 21, 91, 'LANGUYAN', '157009000'),
(3391, 21, 91, 'SAPA-SAPA', '157010000'),
(3392, 21, 91, 'SIBUTU', '157011000'),
(3393, 6, 14, 'BANAUE', '142701000');

-- --------------------------------------------------------

--
-- Table structure for table `classification`
--

CREATE TABLE `classification` (
  `classification_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `classification`
--

INSERT INTO `classification` (`classification_id`, `name`, `description`) VALUES
(1, 'Rank and File', '');

-- --------------------------------------------------------

--
-- Table structure for table `clearance`
--

CREATE TABLE `clearance` (
  `clearance_id` int(11) NOT NULL,
  `resignation_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `clearance_approval`
--

CREATE TABLE `clearance_approval` (
  `clearance_approval_id` int(11) NOT NULL,
  `clearance_id` int(11) NOT NULL,
  `clearance_approver_id` int(11) NOT NULL,
  `clearance_category` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `clearance_approver`
--

CREATE TABLE `clearance_approver` (
  `clearance_approver_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `clearance_category` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cobalt_reporter`
--

CREATE TABLE `cobalt_reporter` (
  `module_name` varchar(255) NOT NULL,
  `report_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `show_field` blob NOT NULL,
  `hidden_field` blob NOT NULL,
  `operator` blob NOT NULL,
  `text_field` blob NOT NULL,
  `sum_field` blob NOT NULL,
  `count_field` blob NOT NULL,
  `group_field1` blob NOT NULL,
  `group_field2` blob NOT NULL,
  `group_field3` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cobalt_sst`
--

CREATE TABLE `cobalt_sst` (
  `auto_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `config_file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `official_name` varchar(255) NOT NULL,
  `short_name` varchar(255) NOT NULL,
  `head` int(11) NOT NULL,
  `assistant` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `official_name`, `short_name`, `head`, `assistant`) VALUES
(1, 'SM Investments Corporation', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `company_policy`
--

CREATE TABLE `company_policy` (
  `company_policy_id` int(11) NOT NULL,
  `parent_company_policy_id` int(11) DEFAULT NULL,
  `policy_code` varchar(255) DEFAULT NULL,
  `policy_name` varchar(255) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `company_violation`
--

CREATE TABLE `company_violation` (
  `company_violation_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `company_policy_id` int(11) NOT NULL,
  `violation_date` date DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `department_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `floor_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `head` int(11) DEFAULT NULL,
  `assistant` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`department_id`, `branch_id`, `floor_id`, `name`, `code`, `description`, `head`, `assistant`) VALUES
(1, 0, 0, 'Human Resources', 'HR', 'sampe', 0, 0),
(3, 0, 0, 'Information Technology', 'IT', 'Sample', 0, 0),
(5, 0, 0, 'Accounting', 'Acctg', 'ss', 0, 0),
(6, 0, 0, 'Payroll', 'PRoll', 'a', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `employee_id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `approver1_employee_id` int(11) DEFAULT NULL,
  `approver2_employee_id` int(11) DEFAULT NULL,
  `plantilla_id` int(11) NOT NULL,
  `employee_first_name` varchar(255) DEFAULT NULL,
  `employee_middle_name` varchar(255) DEFAULT NULL,
  `employee_last_name` varchar(255) DEFAULT NULL,
  `employee_nickname` varchar(255) DEFAULT NULL,
  `present_address` varchar(255) DEFAULT NULL,
  `present_contact_no` varchar(255) DEFAULT NULL,
  `provincial_address` varchar(255) DEFAULT NULL,
  `provincial_contact_no` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `civil_status` varchar(255) DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `citizenship` varchar(255) DEFAULT NULL,
  `birthplace` varchar(255) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `application_source` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `sss_no` varchar(255) DEFAULT NULL,
  `hdmf_no` varchar(255) DEFAULT NULL,
  `philhealth_no` varchar(255) DEFAULT NULL,
  `tin` varchar(255) DEFAULT NULL,
  `employment_status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employee_education`
--

CREATE TABLE `employee_education` (
  `employee_education_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `level` varchar(255) DEFAULT NULL,
  `school` varchar(255) DEFAULT NULL,
  `period` varchar(255) DEFAULT NULL,
  `awards` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employee_evaluation`
--

CREATE TABLE `employee_evaluation` (
  `employee_evaluation_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `employee_evaluation_template_id` int(11) NOT NULL,
  `evaluation_date` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employee_evaluation_answer`
--

CREATE TABLE `employee_evaluation_answer` (
  `employee_evaluation_answer_id` int(11) NOT NULL,
  `employee_evaluation_id` int(11) NOT NULL,
  `criteria_name` varchar(255) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `rating` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employee_evaluation_template`
--

CREATE TABLE `employee_evaluation_template` (
  `employee_evaluation_template_id` int(11) NOT NULL,
  `template_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employee_evaluation_template_criteria`
--

CREATE TABLE `employee_evaluation_template_criteria` (
  `employee_evaluation_template_criteria_id` int(11) NOT NULL,
  `employee_evaluation_template_id` int(11) NOT NULL,
  `criteria_name` varchar(255) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `floor`
--

CREATE TABLE `floor` (
  `floor_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `building_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `forgot_pass_table`
--

CREATE TABLE `forgot_pass_table` (
  `forgot_pass_id` int(11) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `date_applied` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `forgot_pass_table`
--

INSERT INTO `forgot_pass_table` (`forgot_pass_id`, `email_address`, `token`, `date_applied`) VALUES
(1, 'markdanico.fernandez1@gmail.com', 'c2cf527c9431e29ef78fcd566380ded31925b250', '2018-07-23 00:00:00'),
(2, 'markdanico.fernandez1@gmail.com', '60647b5da54d213777af7cd59be4e2ac091dd11e', '2018-07-23 00:00:00'),
(3, 'markdanico.fernandez@gmail.com', '565e90bc47db4e4f5906526857fc5190e05d6202', '2018-07-23 00:00:00'),
(4, 'markdanico.fernandez@gmail.com', '566621a826329295af87ee821cfd7d27eb99ecb1', '2018-07-23 00:00:00'),
(5, 'markdanico.fernandez@gmail.com', 'e00c991228962350610fdb0d5706e42b1dd1acf4', '2018-07-23 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hr_program`
--

CREATE TABLE `hr_program` (
  `hr_program_id` int(11) NOT NULL,
  `program_name` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `start_time` varchar(255) DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `end_time` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hr_program_attendees`
--

CREATE TABLE `hr_program_attendees` (
  `hr_program_attendees_id` int(11) NOT NULL,
  `hr_program_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ibloodtype`
--

CREATE TABLE `ibloodtype` (
  `BloodTypeID` int(10) NOT NULL,
  `BloodTypeCode` varchar(50) DEFAULT NULL,
  `BloodTypeDesc` varchar(200) DEFAULT NULL,
  `EncodeBy` bigint(19) DEFAULT NULL,
  `EncodeDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ibloodtype`
--

INSERT INTO `ibloodtype` (`BloodTypeID`, `BloodTypeCode`, `BloodTypeDesc`, `EncodeBy`, `EncodeDate`) VALUES
(1, 'A+', 'A+', 370, '2013-07-29'),
(2, 'A-', 'A-', 370, '2013-07-29'),
(3, 'B+', 'B+', 370, '2013-07-29'),
(4, 'B-', 'B-', 370, '2013-07-29'),
(5, 'AB+', 'AB+', 370, '2013-07-29'),
(6, 'AB-', 'AB-', 370, '2013-07-29'),
(7, 'O+', 'O+', 370, '2013-07-29'),
(8, 'O-', 'O-', 370, '2013-07-29');

-- --------------------------------------------------------

--
-- Table structure for table `icitizenship`
--

CREATE TABLE `icitizenship` (
  `CitizenshipID` int(10) NOT NULL,
  `CitizenshipCode` varchar(50) DEFAULT NULL,
  `CitizenshipDesc` varchar(200) DEFAULT NULL,
  `EncodeBy` bigint(19) DEFAULT NULL,
  `EncodeDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `icitizenship`
--

INSERT INTO `icitizenship` (`CitizenshipID`, `CitizenshipCode`, `CitizenshipDesc`, `EncodeBy`, `EncodeDate`) VALUES
(1, '001', 'American', NULL, NULL),
(2, '002', 'Australian', NULL, NULL),
(3, '003', 'Brazilian', NULL, NULL),
(4, '004', 'Briton', NULL, NULL),
(5, '005', 'Cambodian', NULL, NULL),
(6, '006', 'Canadian', NULL, NULL),
(7, '007', 'Chinese', NULL, NULL),
(8, '008', 'English', NULL, NULL),
(9, '009', 'Filipino', NULL, NULL),
(10, '010', 'Hong Konger', NULL, NULL),
(11, '011', 'Indian', NULL, NULL),
(12, '012', 'Indonesian', NULL, NULL),
(13, '013', 'Italian', NULL, NULL),
(14, '014', 'Japanese', NULL, NULL),
(15, '015', 'Korean', NULL, NULL),
(16, '016', 'Lao', NULL, NULL),
(17, '017', 'Malaysian', NULL, NULL),
(18, '018', 'Mexican', NULL, NULL),
(19, '019', 'New Zealanders', NULL, NULL),
(20, '020', 'Russian', NULL, NULL),
(21, '021', 'Singaporean', NULL, NULL),
(22, '022', 'Taiwanese', NULL, NULL),
(23, '023', 'Thai', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `icivilstatus`
--

CREATE TABLE `icivilstatus` (
  `CivilStatusID` int(10) NOT NULL,
  `CivilStatusCode` varchar(50) DEFAULT NULL,
  `CivilStatusDesc` varchar(200) DEFAULT NULL,
  `EncodeBy` bigint(19) DEFAULT NULL,
  `EncodeDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `icivilstatus`
--

INSERT INTO `icivilstatus` (`CivilStatusID`, `CivilStatusCode`, `CivilStatusDesc`, `EncodeBy`, `EncodeDate`) VALUES
(1, 'Single', 'Single', NULL, NULL),
(2, 'Married', 'Married', NULL, NULL),
(3, 'Widowed', 'Widowed', NULL, NULL),
(4, 'Legally Separated', 'Legally Separated', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `icourse`
--

CREATE TABLE `icourse` (
  `CourseID` bigint(19) NOT NULL,
  `CourseCode` varchar(50) DEFAULT NULL,
  `CourseDesc` varchar(200) DEFAULT NULL,
  `EncodeBy` bigint(19) DEFAULT NULL,
  `EncodeDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `icourse`
--

INSERT INTO `icourse` (`CourseID`, `CourseCode`, `CourseDesc`, `EncodeBy`, `EncodeDate`) VALUES
(1, 'C001', 'Advertising/Media', NULL, NULL),
(2, 'C002', 'Agriculture/Aquaculture/Forestry', NULL, NULL),
(3, 'C003', 'Airline Operation/Airport Management', NULL, NULL),
(4, 'C004', 'Architecture', NULL, NULL),
(5, 'C005', 'Art/Design/Creative Multimedia', NULL, NULL),
(6, 'C006', 'Biology', NULL, NULL),
(7, 'C007', 'BioTechnology', NULL, NULL),
(8, 'C008', 'Business Studies/Administration/Management', NULL, NULL),
(9, 'C009', 'Chemistry', NULL, NULL),
(10, 'C010', 'Commerce', NULL, NULL),
(11, 'C011', 'Computer Science/Information Technology', NULL, NULL),
(12, 'C012', 'Dentistry', NULL, NULL),
(13, 'C013', 'Economics', NULL, NULL),
(14, 'C014', 'Education/Teaching/Training', NULL, NULL),
(15, 'C015', 'Engineering (Aviation/Aeronautics/Astronautics)', NULL, NULL),
(16, 'C016', 'Engineering (Bioengineering/Biomedical)', NULL, NULL),
(17, 'C017', 'Engineering (Chemical)', NULL, NULL),
(18, 'C018', 'Engineering (Civil)', NULL, NULL),
(19, 'C019', 'Engineering (Computer/Telecommunication)', NULL, NULL),
(20, 'C020', 'Engineering (Electrical/Electronic)', NULL, NULL),
(21, 'C021', 'Engineering (Environmental/Health/Safety)', NULL, NULL),
(22, 'C022', 'Engineering (Industrial)', NULL, NULL),
(23, 'C023', 'Engineering (Marine)', NULL, NULL),
(24, 'C024', 'Engineering (Material Science)', NULL, NULL),
(25, 'C025', 'Engineering (Mechanical)', NULL, NULL),
(26, 'C026', 'Engineering (Mechatronic/Electromechanical)', NULL, NULL),
(27, 'C027', 'Engineering (Metal Fabrication/Tool & Die/Welding)', NULL, NULL),
(28, 'C028', 'Engineering (Mining/Mineral)', NULL, NULL),
(29, 'C029', 'Engineering (Others)', NULL, NULL),
(30, 'C030', 'Engineering (Petroleum/Oil/Gas)', NULL, NULL),
(31, 'C031', 'Finance/Accountancy/Banking', NULL, NULL),
(32, 'C032', 'Food & Beverage Services Management', NULL, NULL),
(33, 'C033', 'Food Technology/Nutrition/Dietetics', NULL, NULL),
(34, 'C034', 'Geographical Science', NULL, NULL),
(35, 'C035', 'Geology/Geophysics', NULL, NULL),
(36, 'C036', 'History', NULL, NULL),
(37, 'C037', 'Hospitality/Tourism/Hotel Management', NULL, NULL),
(38, 'C038', 'Human Resource Management', NULL, NULL),
(39, 'C039', 'Humanities/Liberal Arts', NULL, NULL),
(40, 'C040', 'Journalism', NULL, NULL),
(41, 'C041', 'Law', NULL, NULL),
(42, 'C042', 'Library Management', NULL, NULL),
(43, 'C043', 'Linguistics/Languages', NULL, NULL),
(44, 'C044', 'Logistic/Transportation', NULL, NULL),
(45, 'C045', 'Maritime Studies', NULL, NULL),
(46, 'C046', 'Marketing', NULL, NULL),
(47, 'C047', 'Mass Communications', NULL, NULL),
(48, 'C048', 'Mathematics', NULL, NULL),
(49, 'C049', 'Medical Science', NULL, NULL),
(50, 'C050', 'Medicine', NULL, NULL),
(51, 'C051', 'Music/Performing Arts Studies', NULL, NULL),
(52, 'C052', 'Nursing', NULL, NULL),
(53, 'C053', 'Optometry', NULL, NULL),
(54, 'C054', 'Personal Services', NULL, NULL),
(55, 'C055', 'Pharmacy/Pharmacology', NULL, NULL),
(56, 'C056', 'Philosophy', NULL, NULL),
(57, 'C057', 'Physical Therapy/Physiotherapy', NULL, NULL),
(58, 'C058', 'Physics', NULL, NULL),
(59, 'C059', 'Political Science', NULL, NULL),
(60, 'C060', 'Property Development/Real Estate Management', NULL, NULL),
(61, 'C061', 'Protective Services & Management', NULL, NULL),
(62, 'C062', 'Psychology', NULL, NULL),
(63, 'C063', 'Quantity Survey', NULL, NULL),
(64, 'C064', 'Science & Technology', NULL, NULL),
(65, 'C065', 'Secretarial', NULL, NULL),
(66, 'C066', 'Social Science/Sociology', NULL, NULL),
(67, 'C067', 'Sports Science & Management', NULL, NULL),
(68, 'C068', 'Textile/Fashion Design', NULL, NULL),
(69, 'C069', 'Urban Studies/Town Planning', NULL, NULL),
(70, 'C070', 'Veterinary', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ieducationallevel`
--

CREATE TABLE `ieducationallevel` (
  `EducationalLevelID` int(10) NOT NULL,
  `EducationalLevelCode` varchar(50) DEFAULT NULL,
  `EducationalLevelDesc` varchar(200) DEFAULT NULL,
  `EncodeBy` bigint(19) DEFAULT NULL,
  `EncodeDate` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ieducationallevel`
--

INSERT INTO `ieducationallevel` (`EducationalLevelID`, `EducationalLevelCode`, `EducationalLevelDesc`, `EncodeBy`, `EncodeDate`) VALUES
(1, '0001', 'Elementary', NULL, NULL),
(2, '0002', 'High School', NULL, NULL),
(3, '0003', 'Vocational / Trade Course', NULL, NULL),
(4, '0004', 'College', NULL, NULL),
(5, '0005', 'Masteral', NULL, NULL),
(6, '0006', 'Doctorate', NULL, NULL),
(7, '0007', 'Others', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `iemployeestatus`
--

CREATE TABLE `iemployeestatus` (
  `EmployeeStatusID` int(10) NOT NULL,
  `EmployeeStatusCode` varchar(50) DEFAULT NULL,
  `EmployeeStatusDesc` varchar(200) DEFAULT NULL,
  `EncodeBy` bigint(19) DEFAULT NULL,
  `EncodeDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `iemployeestatus`
--

INSERT INTO `iemployeestatus` (`EmployeeStatusID`, `EmployeeStatusCode`, `EmployeeStatusDesc`, `EncodeBy`, `EncodeDate`) VALUES
(1, '1', 'Probationary', NULL, NULL),
(2, '2', 'Regular', NULL, NULL),
(3, '5', 'Project-Based', 825, '2013-01-21'),
(4, '3', 'Separated', NULL, NULL),
(5, '7', 'Suspended', -1, '2012-11-19'),
(6, '6', 'All Around Services', -1, '2012-11-19'),
(7, '4', 'Consultant', 825, '2013-02-04'),
(8, '8', 'Casual', 825, '2013-02-20'),
(9, '9', 'On-The-Job-Training', 1379, '2018-09-25');

-- --------------------------------------------------------

--
-- Table structure for table `igender`
--

CREATE TABLE `igender` (
  `GenderID` int(10) NOT NULL,
  `GenderCode` varchar(50) DEFAULT NULL,
  `GenderDesc` varchar(200) DEFAULT NULL,
  `EncodeBy` bigint(19) DEFAULT NULL,
  `EncodeDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `igender`
--

INSERT INTO `igender` (`GenderID`, `GenderCode`, `GenderDesc`, `EncodeBy`, `EncodeDate`) VALUES
(1, 'M', 'Male', NULL, NULL),
(2, 'F', 'Female', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ilanguage`
--

CREATE TABLE `ilanguage` (
  `LanguageID` int(10) NOT NULL,
  `LanguageCode` varchar(50) DEFAULT NULL,
  `LanguageDesc` varchar(200) DEFAULT NULL,
  `EncodeBy` bigint(19) DEFAULT NULL,
  `EncodeDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ilanguage`
--

INSERT INTO `ilanguage` (`LanguageID`, `LanguageCode`, `LanguageDesc`, `EncodeBy`, `EncodeDate`) VALUES
(1, NULL, 'Arabic\r\n', NULL, NULL),
(2, NULL, 'Bahasa Indonesia\r\n', NULL, NULL),
(3, NULL, 'Bahasa Malaysia\r\n', NULL, NULL),
(4, NULL, 'Bengali\r\n', NULL, NULL),
(5, NULL, 'Chinese\r\n', NULL, NULL),
(6, NULL, 'Dutch\r\n', NULL, NULL),
(7, NULL, 'English\r\n', NULL, NULL),
(8, NULL, 'Filipino\r\n', NULL, NULL),
(9, NULL, 'French\r\n', NULL, NULL),
(10, NULL, 'German\r\n', NULL, NULL),
(11, NULL, 'Hebrew\r\n', NULL, NULL),
(12, NULL, 'Hindi\r\n', NULL, NULL),
(13, NULL, 'Italian\r\n', NULL, NULL),
(14, NULL, 'Japanese\r\n', NULL, NULL),
(15, NULL, 'Korean\r\n', NULL, NULL),
(16, NULL, 'Portuguese\r\n', NULL, NULL),
(17, NULL, 'Russian\r\n', NULL, NULL),
(18, NULL, 'Spanish\r\n', NULL, NULL),
(19, NULL, 'Tamil\r\n', NULL, NULL),
(20, NULL, 'Thai\r\n', NULL, NULL),
(21, NULL, 'Vietnam\r\n', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `iprf_staffrequest`
--

CREATE TABLE `iprf_staffrequest` (
  `iprf_staffrequest_id` int(11) NOT NULL,
  `PRF_No` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `rank_id` int(11) NOT NULL,
  `classification_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `duration` text NOT NULL,
  `responsibility` text NOT NULL,
  `location_id` int(11) NOT NULL,
  `reporting_to_id` int(11) NOT NULL,
  `general_duties` text NOT NULL,
  `detailed_duties` text NOT NULL,
  `education` text NOT NULL,
  `professional_eligibility_skills` text NOT NULL,
  `experience` text NOT NULL,
  `skills` varchar(255) NOT NULL,
  `reason_for_request` text NOT NULL,
  `replacement_of` int(11) NOT NULL,
  `date_needed` date NOT NULL,
  `file_status` int(11) NOT NULL,
  `date_filed` date NOT NULL,
  `requested_by` int(11) NOT NULL,
  `secondary_unit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `iprf_staffrequest_applicants`
--

CREATE TABLE `iprf_staffrequest_applicants` (
  `iprf_staffrequest_applicant_id` int(11) NOT NULL,
  `iprf_staffrequest_id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `date_applied` datetime NOT NULL,
  `updates` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `iproficiency`
--

CREATE TABLE `iproficiency` (
  `ProficiencyID` int(10) NOT NULL,
  `ProficiencyCode` varchar(50) DEFAULT NULL,
  `ProficiencyDesc` varchar(200) DEFAULT NULL,
  `EncodeBy` bigint(19) DEFAULT NULL,
  `EncodeDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `iproficiency`
--

INSERT INTO `iproficiency` (`ProficiencyID`, `ProficiencyCode`, `ProficiencyDesc`, `EncodeBy`, `EncodeDate`) VALUES
(1, NULL, 'Beginner', NULL, NULL),
(2, NULL, 'Intermediate', NULL, NULL),
(3, NULL, 'Advanced', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `irelationship`
--

CREATE TABLE `irelationship` (
  `RelationshipID` int(10) NOT NULL,
  `RelationshipCode` varchar(50) DEFAULT NULL,
  `RelationshipDesc` varchar(200) DEFAULT NULL,
  `EncodeBy` bigint(19) DEFAULT NULL,
  `EncodeDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `irelationship`
--

INSERT INTO `irelationship` (`RelationshipID`, `RelationshipCode`, `RelationshipDesc`, `EncodeBy`, `EncodeDate`) VALUES
(1, 'C001', 'Father', NULL, NULL),
(2, 'C002', 'Mother', NULL, NULL),
(3, 'C003', 'Brother', NULL, NULL),
(4, 'C004', 'Sister', NULL, NULL),
(5, 'C006', 'Son', NULL, NULL),
(6, 'C007', 'Daughter', NULL, NULL),
(7, 'C008', 'Spouse', NULL, NULL),
(8, 'C009', 'Contact', 825, '2013-04-29');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `language_id` int(11) NOT NULL,
  `language` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`language_id`, `language`) VALUES
(1, 'English'),
(2, 'Abkhaz'),
(3, 'Adyghe'),
(4, 'Afrikaans'),
(5, 'Akan'),
(6, 'Albanian'),
(7, 'American Sign Language'),
(8, 'Amharic'),
(9, 'Arabic'),
(10, 'Aragonese'),
(11, 'Aramaic'),
(12, 'Armenian'),
(13, 'Assamese'),
(14, 'Aymara'),
(15, 'Balinese'),
(16, 'Basque'),
(17, 'Betawi'),
(18, 'Bosnian'),
(19, 'Breton'),
(20, 'Bulgarian'),
(21, 'Cantonese'),
(22, 'Catalan'),
(23, 'Cherokee'),
(24, 'Chickasaw'),
(25, 'Chinese'),
(26, 'Coptic'),
(27, 'Cornish'),
(28, 'Corsican'),
(29, 'Crimean Tatar'),
(30, 'Croatian'),
(31, 'Czech'),
(32, 'Danish'),
(33, 'Dutch'),
(34, 'Dawro'),
(35, 'Esperanto'),
(36, 'Estonian'),
(37, 'Ewe'),
(38, 'Fiji Hindi'),
(39, 'Filipino'),
(40, 'Finnish'),
(41, 'French'),
(42, 'Galician'),
(43, 'Georgian'),
(44, 'German'),
(45, 'Greek, Modern'),
(46, 'Ancient Greek'),
(47, 'Greenlandic'),
(48, 'Haitian Creole'),
(49, 'Hawaiian'),
(50, 'Hebrew'),
(51, 'Hindi'),
(52, 'Hungarian'),
(53, 'Icelandic'),
(54, 'Indonesian'),
(55, 'Inuktitut'),
(56, 'Interlingua'),
(57, 'Irish'),
(58, 'Italian'),
(59, 'Japanese'),
(60, 'Javanese'),
(61, 'Kabardian'),
(62, 'Kalasha'),
(63, 'Kannada'),
(64, 'Kashubian'),
(65, 'Khmer'),
(66, 'Kinyarwanda'),
(67, 'Korean'),
(68, 'Kurdish'),
(69, 'Ladin'),
(70, 'Latgalian'),
(71, 'Latin'),
(72, 'Lingala'),
(73, 'Livonian'),
(74, 'Lojban'),
(75, 'Lower Sorbian'),
(76, 'Low German'),
(77, 'Macedonian'),
(78, 'Malay'),
(79, 'Malayalam'),
(80, 'Mandarin'),
(81, 'Manx'),
(82, 'Maori'),
(83, 'Mauritian Creole'),
(84, 'Middle English'),
(85, 'Middle Low German'),
(86, 'Min Nan'),
(87, 'Mongolian'),
(88, 'Norwegian'),
(89, 'Old Armenian'),
(90, 'Old English'),
(91, 'Old French'),
(92, 'Old Javanese'),
(93, 'Old Norse'),
(94, 'Old Prussian'),
(95, 'Oriya'),
(96, 'Pangasinan'),
(97, 'Papiamentu'),
(98, 'Pashto'),
(99, 'Persian'),
(100, 'Pitjantjatjara'),
(101, 'Polish'),
(102, 'Portuguese'),
(103, 'Proto-Slavic'),
(104, 'Quenya'),
(105, 'Rajasthani'),
(106, 'Rapa Nui'),
(107, 'Romanian'),
(108, 'Russian'),
(109, 'Sanskrit'),
(110, 'Scots'),
(111, 'Scottish Gaelic'),
(112, 'Serbian'),
(113, 'Serbo-Croatian'),
(114, 'Slovak'),
(115, 'Slovene'),
(116, 'Spanish'),
(117, 'Sinhalese'),
(118, 'Swahili'),
(119, 'Swedish'),
(120, 'Tagalog'),
(121, 'Tajik'),
(122, 'Tamil'),
(123, 'Tarantino'),
(124, 'Telugu'),
(125, 'Thai'),
(126, 'Tok Pisin'),
(127, 'Turkish'),
(128, 'Twi'),
(129, 'Ukrainian'),
(130, 'Upper Sorbian'),
(131, 'Urdu'),
(132, 'Uyghur'),
(133, 'Uzbek'),
(134, 'Venetian'),
(135, 'Vietnamese'),
(136, 'Vilamovian'),
(137, 'Volap?k'),
(138, 'V?ro'),
(139, 'Welsh'),
(140, 'Xhosa'),
(141, 'Yiddish'),
(142, 'Zazaki');

-- --------------------------------------------------------

--
-- Table structure for table `other_declarations_name_smgroup`
--

CREATE TABLE `other_declarations_name_smgroup` (
  `other_declarations_name_smgroup_id` int(11) NOT NULL,
  `applicant_other_declaration_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `relationship` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `company` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `other_declarations_name_smic_employ`
--

CREATE TABLE `other_declarations_name_smic_employ` (
  `other_declarations_name_smic_employ_id` int(11) NOT NULL,
  `applicant_other_declaration_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `relationship` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `department_company` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `performance_appraisal`
--

CREATE TABLE `performance_appraisal` (
  `performance_appraisal_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `appraisal_year` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `performance_appraisal_goal`
--

CREATE TABLE `performance_appraisal_goal` (
  `performance_appraisal_goal_id` int(11) NOT NULL,
  `performance_appraisal_id` int(11) NOT NULL,
  `goal` varchar(255) DEFAULT NULL,
  `rating` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `person_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`person_id`, `first_name`, `middle_name`, `last_name`, `gender`) VALUES
(1, 'Super User', 'X', 'Root', 'Male');

-- --------------------------------------------------------

--
-- Table structure for table `personnel_requisition`
--

CREATE TABLE `personnel_requisition` (
  `personnel_requisition_id` int(11) NOT NULL,
  `plantilla_id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `requisition_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `plantilla`
--

CREATE TABLE `plantilla` (
  `plantilla_id` int(11) NOT NULL,
  `parent_plantilla_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `position_title` varchar(255) DEFAULT NULL,
  `job_description` varchar(255) DEFAULT NULL,
  `skill_requirements` varchar(255) DEFAULT NULL,
  `recruitment_notes` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `date_opened` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plantilla`
--

INSERT INTO `plantilla` (`plantilla_id`, `parent_plantilla_id`, `branch_id`, `department_id`, `company_id`, `position_title`, `job_description`, `skill_requirements`, `recruitment_notes`, `status`, `date_opened`) VALUES
(2, 0, 0, 5, 1, 'Accounting Assistant', 'Sample Sample Sample', 'Clerical', 'none', '', '2018-07-13'),
(3, 0, 0, 3, 1, 'Desktop Support', 'Tedious job', 'Knowledgable in supporting PCs', 'none', '', '2018-07-02');

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE `position` (
  `position_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`position_id`, `title`, `description`) VALUES
(1, 'Compensation and Benefits', ''),
(2, 'Software Developer', '');

-- --------------------------------------------------------

--
-- Table structure for table `province`
--

CREATE TABLE `province` (
  `province_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `province_name` varchar(255) NOT NULL,
  `province_code` varchar(255) NOT NULL,
  `province_image` varchar(255) NOT NULL,
  `info` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `province`
--

INSERT INTO `province` (`province_id`, `region_id`, `province_name`, `province_code`, `province_image`, `info`) VALUES
(5, 2, 'City of Manila (1st District)', '133900000', '747d8809463dec86f033852669ddbce2d6150c44Metro_Manila.svg', 'City = 1, City District = 14, Barangay =  897'),
(6, 2, '2nd District', '137600000', 'c5ea7a89152a2bb8417f11605a66b4d5019bd1cdMetro_Manila.svg', 'City  =  6, Municipality =  1, Barangay = 317'),
(7, 2, '3rd District', '137400000', 'c2ff293f40e316f586e3326762a8a93aaee96e9dMetro_Manila.svg', 'City = 5, Barangay = 236'),
(8, 2, '4th District', '137500000', '0c87ec7c8c39af8035d483d745eee7c2ac33de48Metro_Manila.svg', 'City = 4, Barangay = 255'),
(9, 5, 'Negros Occidental', '184500000', 'f06394c7df9c508b42efcf8925d91396830e0fddNegros_Occidental.svg', 'City = 13, Municipality = 19, Barangay = 662'),
(10, 5, 'Negros Oriental', '184600000', '21691fe710ba68d01a55eda950674f3ce9eba33eNegros_Oriental.svg', 'City = 6, Municipality = 19, Barangay = 557'),
(11, 6, 'Abra', '140100000', '6b5f2867c037f467f4c561712e219db179152efcAbra.svg', 'Municipality = 27, Barangay = 303'),
(12, 6, 'Apayao', '148100000', '19e46e4e4425cb80bc9162b72a11de462115f9c6Kalinga_Apayao.svg', 'Municipality = 7, Barangay = 133'),
(13, 6, 'Benguet', '141100000', '80e53fccb78c9353f314b09b586b3637cb912a13Benguet.svg', 'City = 1, Municipality = 13, Barangay = 269'),
(14, 6, 'Ifugao', '142700000', '5185adb62e27f804d43b47c06b212f9482d78498Ifugao.svg', 'Municipality = 11, Barangay = 175'),
(15, 6, 'Kalinga', '143200000', '604af095b1405c064c6c5b9b886f68e16fb4283cKalinga.svg', 'City = 1, Municipality = 7, Barangay = 152'),
(16, 6, 'Mountain Province', '144400000', '42f25e6ae84e88eb0dfdd2f84a98dbc73cd24649Mountain_Province.svg', 'Municipality = 10, Barangay = 144'),
(17, 7, 'Ilocos Norte', '12800000', '282639ab54989ea0945dc0cbc1b257f94009a859Ilocos_Sur.svg', 'City =  2, Municipality = 21, Barangay = 557'),
(18, 7, 'Ilocos Sur', '12900000', 'ab7e642f6882401f8554b55326ff79e6715071b9Ilocos_Sur.svg', 'City = 2, Municipality = 32, Barangay = 768'),
(19, 7, 'La Union', '13300000', '2304605a055a812c439f22f0d44054ef8d888aebLa_Union.svg', 'City = 1, Municipality = 19, Barangay = 576'),
(20, 7, 'Pangasinan', '15500000', '2e60bbbf33ce8bdfa6eb3ce4a22af8dd4f0a50e2Pangasinan.svg', 'City = 4, Municipality = 44, Barangay = 1364'),
(21, 8, 'Batanes', '20900000', '5b911818e62e1f5dde1b608d9449a18b10cfed58Batanes.svg', 'Municipality = 6, Barangay = 29'),
(22, 8, 'Cagayan', '21500000', '6952b6e3203a09ae16612caa549cb6918b75a78bCagayan.svg', 'City = 1, Municipality = 28, Barangay = 820'),
(23, 8, 'Isabela', '23100000', 'cbfa0e514e465c1b28b68013c5ec96b27df1172aIsabela.svg', 'City = 2, Municipality = 35, Barangay = 1055'),
(24, 8, 'Nueva Vizcaya', '25000000', '57eb3f6c5aa3109a7cf270e14169fd3a27d8617fNueva_Viscaya.svg', 'Municipality = 15, Barangay = 275'),
(25, 8, 'Quirino', '25700000', '469308b08ac338f539202bf474377476957601b6Quirino.svg', 'Municipality = 6, Barangay = 132'),
(26, 9, 'Aurora', '37700000', '40addbfdd1aea46baacef4a7339d13200c7ded21Aurora.svg', 'Municipality = 8, Barangay = 151'),
(27, 9, 'Bataan', '30800000', 'e1cca4b4094210559331fe119417d270be774865Bataan.svg', 'City = 1, Municipality = 11, Barangay = 237'),
(28, 9, 'Bulacan', '31400000', '63dc189aaed79ea5b914c22f41a49faad7db1bf8Bulacan.svg', 'City = 3, Municipality = 21, Barangay = 567'),
(29, 9, 'Nueva Ecija', '34900000', 'dc37961d8695de2c2608a28d4675a3b494e0544aNueva_Ecija.svg', 'City = 5, Municipality = 27, Barangay = 849'),
(30, 9, 'Pampanga', '35400000', '7dfbbe73efbce5c5afe592638fbe9ee3e6d67b2fPampanga.svg', 'City = 3, Municipality = 19, Barangay = 538'),
(31, 9, 'Tarlac', '36900000', 'd2f225b659c9b9120607e82442aef8bf8d4b5140Tarlac.svg', 'City = 1, Municipality = 17, Barangay = 511'),
(32, 9, 'Zambales', '37100000', '6547bef15f90c0d93b31c566159f9143f2066c2dZambales.svg', 'City = 1, Municipality = 13, Barangay = 247'),
(33, 10, 'Batangas', '41000000', '4603a502ded5d27324e9589a82ae69035bdd7558Batangas.svg', 'City = 3, Municipality = 31, Barangay = 1078'),
(34, 10, 'Cavite', '42100000', '09280f9b44206a5066e4b2328766c9bf3f3f7d8fCavite.svg', 'City = 6, Municipality = 17, Barangay = 829'),
(35, 10, 'Laguna', '43400000', '61c8cea1326831407dc97598e418b338f1569e96Laguna.svg', 'City = 5, Municipality = 25, Barangay = 674'),
(36, 10, 'Quezon', '45600000', '0d608f747a044dd73b7cfc61495ce32bd6ae11e5Quezon.svg', 'City = 2, Municipality = 39, Barangay = 1242'),
(37, 10, 'Rizal', '45800000', '99a81a3da374f44e953371f647067f0ef7de85b1Rizal.svg', 'City = 1, Municipality = 13, Barangay = 188'),
(38, 11, 'Marinduque', '174000000', '001816e266fade94bde999aaa575e832bf69614fMarinduque.svg', 'Municipality = 6, Barangay = 218'),
(39, 11, 'Occidental Mindoro', '175100000', '3a0c129dec5a7b7fbf0a47d3652616d9a796d005Occidental_Mindoro.svg', 'Municipality = 11, Barangay = 162'),
(40, 11, 'Oriental Mindoro', '175200000', 'efad8c6e2bed313f0080a9c8a65e2a9dab3753e7Oriental_Mindoro.svg', 'City = 1, Municipality = 14, Barangay = 426'),
(41, 11, 'Palawan', '175300000', 'df3c1353ec616b1d9f6e2bb58054d1e1e8515257Palawan.svg', 'City = 1, Municipality = 23, Barangay = 433'),
(42, 11, 'Romblon', '175900000', '2e72717953eece3846c6fec7f1fc0e44592e3997Romblon.svg', 'Muncipality = 17, Barangay = 219'),
(43, 12, 'Albay', '50500000', '2ca4314dc7df13de4b1e60faa9cffa97dc06ff9dAlbay.svg', 'City = 3, Municipality = 15, Barangay = 720'),
(44, 12, 'Camarines Norte', '51600000', 'cc92c46d0527206e81ca1f863b1379ae87fd3923Camarines_Norte.svg', 'Municipality = 12, Barangay = 282'),
(45, 12, 'Camarines Sur', '51700000', 'd915623523ddaa088b31af2f53bcb850dcada7cbCamarines_Sur.svg', 'City = 2, Municipality = 35, Barangay = 1063'),
(46, 12, 'Catanduanes', '52000000', '2994e796d526982ac0d2797e6522d4a5e8bf3f02Catanduanes.svg', 'Municipality = 11, Barangay = 315'),
(47, 12, 'Masbate', '54100000', 'c4e29c3caeabe47c8545a9ab8d4639ddf487f952Masbate.svg', 'City = 1, Municipality = 20, Barangay = 550'),
(48, 12, 'Sorsogon', '56200000', 'fee470b0f052998af3cd7df87a08347134f3532bSorsogon.svg', 'City = 1, Municipality = 14, Barangay = 541'),
(49, 13, 'Aklan', '60400000', '4a91c817ecb9e2e1404f19fa8e92c31eb65675c8Aklan.svg', 'Muncipality = 17, Barangay = 327'),
(50, 13, 'Antique', '60600000', '42251566713d6276e13ec5fbec065b9619a7903eAntique.svg', 'Municipality = 18, Barangay 590'),
(51, 13, 'Capiz', '61900000', '37a462d1d12cd45a246ba60b3f977a339d622604Capiz.svg', 'City = 1, Municipality = 196, Barangay = 473'),
(52, 13, 'Guimaras', '67900000', '3b5725184a51984c897d4b7c741ce3fc39d9474bGuimaras.svg', 'Municipality = 5, Barangay = 98'),
(53, 13, 'Iloilo', '63000000', 'f40f021129190997ec72155fa231cfd26acbfce7Iloilo.svg', 'City = 2, Municipality  = 42, Barangay = 1901'),
(54, 14, 'Bohol', '71200000', 'fc234573b314fd7a05d6a39a130dbd9584521473Bohol.svg', 'City = 1, Municipality = 47, Barangay = 1109'),
(55, 14, 'Cebu', '72200000', '3ed8f832d9b8d304caac7bdadb37f9db3ca0a939Cebu.svg', 'City = 9, Municipality = 44, Barangay = 1203'),
(56, 14, 'Siquijor', '76100000', '8b4408b074cf6130cab90c17c9a4eaf1d216d2daSiquijor.svg', 'Municipality = 6, Barangay = 134'),
(57, 15, 'Biliran', '87800000', '8cffd3be574dc9ceb5a47ade8b8d79b168d91ee3Biliran.svg', 'Municipality = 8, Barangay = 132'),
(58, 15, 'Eastern Samar', '82600000', '430bba020962835c3ba9cadf9f57eb1eb77a8f0dEastern_Samar.svg', 'City = 1, Municipality = 22, Barangay = 597'),
(59, 15, 'Leyte', '83700000', 'b0f9e515f96ccd30e674db9f73561d64a7a98c0bLeyte.svg', 'City = 3, Municipality = 40, Barangay = 1641'),
(60, 15, 'Northern Samar', '84800000', '4378e2d203492d7d5ef41ff9458128dd585c1496Northern_Samar.svg', 'Municipality = 24, Barangay = 569'),
(61, 15, 'Samar (Western Samar)', '86000000', '96e98eb57ca65d1e8785705fceb598bef9f48f30Western_Samar.svg', 'City = 2, Municipality = 24, Barangay = 951'),
(62, 15, 'Southern Leyte', '86400000', 'f6d732162df45bf3fdc18d511f6ca7f66c5ab1cfSouthern_Leyte.svg', 'City = 1, Municipality = 18, Barangay = 500'),
(64, 16, 'Zamboanga Del Norte', '97200000', '6fd6108ca559fd883fa382ee98f472db566b7af6Zamboanga_del_Norte.svg', 'City = 2, Municipality = 25, Barangay = 691'),
(65, 16, 'Zamboanga Del Sur', '97300000', '3fe8c2673b71e1b65bab67a6a6936baddfa1343cZamboanga_del_Sur.svg', 'City = 1, Municipality = 26, Barangay = 681'),
(66, 16, 'Zamboanga Sibugay', '98300000', '43a73b1d0d265483dff63d63e7582267ea6463c1Zamboang_a_sibugay.svg', 'Municipality = 16, Barangay = 389'),
(67, 17, 'Bukidnon', '101300000', '25ad07754d6340a21fb3b947b8b9f41f72e55337Bukidnon.svg', 'City = 2, Municipality = 20, Barangay = 464'),
(68, 17, 'Camiguin', '101800000', '968468663020f87e8227aeb3632220ce4472192aCamiguin.svg', 'Municipality = 5, Barangay = 58'),
(69, 17, 'Lanao Del Norte', '103500000', 'a318bbde652ebb49ca92540e8c7582480f46b937Lanao_del_Norte.svg', 'City =1 , Municipality = 22, Barangay = 506'),
(70, 17, 'Misamis Occidental', '104200000', '73db7856523b569403a4eb6f6313c7c993c54d03Misamis_Occidental.svg', 'City = 3, Municipality =14, Barangay = 490'),
(71, 17, 'Misamis Oriental', '104300000', '5cc72960743176123fa196d4bc061e4d05b2187fMisamis_Oriental.svg', 'City = 3, Municipality = 23, Barangay = 504'),
(72, 18, 'Compostela Valley', '118200000', '11cb7aad440d923db407331014903d6fb5437e07Composte_la_Valley.svg', 'Municipality = 11, Barangay = 237'),
(73, 18, 'Davao Del Norte', '112300000', '83b8fb80d2740039338af18527d16ae5440965cbDavao_del_Norte.svg', 'City = 3, Municipality = 8, Barangay = 223'),
(74, 18, 'Davao Del Sur', '112400000', '3414d384df14760cc7fecbdc5f42da642fa7ccdfDavao_del_Sur.svg', 'City = 2, Municipality = 14, Barangay = 519'),
(75, 18, 'Davao Occidental', '118600000', '6e5b0f237e001a8ddc8c2ba72fd017b344f5a0b3Davao_Occidental.svg', 'Municipality = 5, Barangay = 105'),
(76, 18, 'Davao Oriental', '112500000', 'ba017fb9a187f2c262036053d509f01ea551a1d2Davao_Oriental.svg', 'City = 1, Municipality = 10, Barangay = 183'),
(77, 19, 'Cotabato (North Cotabato)', '124700000', '9d35299a69f685119feb6d36b00512364084bbbbNorth_Cotabato.svg', 'City = 1, Municipality = 17, Barangay = 543'),
(78, 19, 'Cotabato', '129800000', '62ab5d8477104ce1ef02fa9ccd8c54298f1ef865Cotabato.svg', 'City = 1, Barangay = 37'),
(79, 19, 'Sarangani', '128000000', 'a9703b7cb0e9b506feeba5d83e968b16c8aeef3aSarangani.svg', 'Municipality = 7, Barangay = 141'),
(80, 19, 'South Cotabato', '126300000', 'bcc90b0f57d459efa4ef958378ec911221bbd8eaSouth_Cotabato.svg', 'City = 2, Municipality = 10, Barangay = 225'),
(81, 19, 'Sultan Kudarat', '126500000', 'b8d02aa7ae7e4ea5ca820f0bf3571bf49ad60361Sultan_Kudarat.svg', 'City = 1, Municipality = 11, Barangay = 249'),
(82, 20, 'Agusan Del Norte', '160200000', '9d03627da744d207f17d21e680e8c301c8c07b00Agusan_del_Norte.svg', 'City = 2, Municipality = 10, Barangay = 253'),
(83, 20, 'Agusan Del Sur', '160300000', '0d1f38835f1b493c8bb6e3f6c35af519e490850bAgusan_del_Sur.svg', 'City = 1, Municipality = 13, Barangay = 314'),
(84, 20, 'Dinagat Islands', '168500000', '56820cf509d901aab06b0a55728a846045b063ebDinagat_Island.svg', 'Municipality = 7, Barangay = 100'),
(85, 20, 'Surigao Del Norte', '166700000', '47ca2338c85a82d3ec931884518e1443f70c85d9Surigao_del_Norte.svg', 'City = 1, Municipality = 20, Barangay = 335'),
(86, 20, 'Surigao Del Sur', '166800000', '5829150868a48e212f9726346c17f69e1744ad4cSurigao_del_Sur.svg', 'City = 2, Municipality = 17, Barangay = 309'),
(87, 21, 'Basilan', '150700000', '62241473b291340c697eb79a2350659e3dec38a5Basilan.svg', 'City = 1, Municipality = 11, Barangay = 210'),
(88, 21, 'Lanao Del Sur', '153600000', '0017ce90057805eb903422e3679097f0df051064Lanao_del_Sur.svg', 'City = 1, Municipality = 39, Barangay = 1159'),
(89, 21, 'Maguindanao', '153800000', '07199ece1cd0d2462750b8287925e75e19031bbcMaguidanao.svg', 'Municipality = 36, Barangay = 506'),
(90, 21, 'Sulu', '156600000', 'f44f809e569cd87e583127e2d153a0f8eafa2e2fSulu.svg', 'Municipality =19, Barangay = 410'),
(91, 21, 'Tawi-Tawi', '157000000', 'b8920e9efd5ab793e86e0809ea3ca5e670893e71Tawi-Tawi.svg', 'Municipality = 11, Barangay = 203');

-- --------------------------------------------------------

--
-- Table structure for table `rank`
--

CREATE TABLE `rank` (
  `rank_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rank`
--

INSERT INTO `rank` (`rank_id`, `name`, `description`) VALUES
(1, 'Rank and File', '');

-- --------------------------------------------------------

--
-- Table structure for table `resignation`
--

CREATE TABLE `resignation` (
  `resignation_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `resignation_date` date DEFAULT NULL,
  `resignation_reason` varchar(255) DEFAULT NULL,
  `exit_interview_notes` varchar(255) DEFAULT NULL,
  `resignation_status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `school`
--

CREATE TABLE `school` (
  `school_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `school`
--

INSERT INTO `school` (`school_id`, `name`, `description`) VALUES
(1, 'Abada College', 'Region IVB - MIMAROPA'),
(2, 'ABE International Business College Quezon Province', 'Region IV - CALABARZON'),
(3, 'ABE International Business College, Caloocan City', 'National Capital Region (NCR)'),
(4, 'ABE International Business College, Commonwealth', 'National Capital Region (NCR)'),
(5, 'ABE International Business College, Makati', 'National Capital Region (NCR)'),
(6, 'ABE International Business College-Manila', 'National Capital Region (NCR)'),
(7, 'ABE International College of Business & Accountancy, Inc.-Malolos City', 'Region III - Central Luzon'),
(8, 'ABE International College of Business & Economics-Cabanatuan City, Inc.', 'Region III - Central Luzon'),
(9, 'ABE International College Of Business And Accountancy - Tacloban City', 'Region VIII - Eastern Visayas'),
(10, 'ABE International College of Business and Accountancy - Urdaneta', 'Region I - Ilocos Region'),
(11, 'ABE International College of Business and Accountancy, Cubao', 'National Capital Region (NCR)'),
(12, 'ABE International College of Business and Accountancy, Las Piñas', 'National Capital Region (NCR)'),
(13, 'ABE International College of Business and Economics, Bacolod City', 'Region VII - Central Visayas'),
(14, 'ABE International College of Business and Economics, Iloilo City', 'Region VI - Western Visayas'),
(15, 'ABE International College of Business and Economics-Cainta', 'Region IV - CALABARZON'),
(16, 'Abra State Institute of Science and Technology- Bangued', 'Cordillera Administrative Region (CAR)'),
(17, 'Abra State Institute of Science and Technology- Lagangilang', 'Cordillera Administrative Region (CAR)'),
(18, 'Abra Valley Colleges', 'Cordillera Administrative Region (CAR)'),
(19, 'Academia De San Lorenzo Dema-Ala, Inc.', 'Region III - Central Luzon'),
(20, 'Access Computer and Technical College, Camarin', 'National Capital Region (NCR)'),
(21, 'Access Computer and Technical College, Lagro', 'National Capital Region (NCR)'),
(22, 'Access Computer and Technical College, Quezon City', 'National Capital Region (NCR)'),
(23, 'Access Computer and Technical Colleges, Cubao', 'National Capital Region (NCR)'),
(24, 'Access Computer and Technical Colleges-Manila', 'National Capital Region (NCR)'),
(25, 'ACES Polytechnic College', 'Region XI - Davao Region'),
(26, 'ACES Tagum College', 'Region XI - Davao Region'),
(27, 'ACLC College – Tagbilaran', 'Region VII - Central Visayas'),
(28, 'ACLC College of Apalit, Inc.', 'Region III - Central Luzon'),
(29, 'ACLC College of Balanga, Inc.', 'Region III - Central Luzon'),
(30, 'ACLC College of Bukidnon, Inc.', 'Region X - Northern Mindanao'),
(31, 'ACLC College Of Butuan City, Inc.', 'CARAGA'),
(32, 'ACLC College of Calapan', 'Region IVB - MIMAROPA'),
(33, 'ACLC College of Daet', 'Region V - Bicol Region'),
(34, 'ACLC College of Gapan, Inc.', 'Region III - Central Luzon'),
(35, 'ACLC College of Iloilo', 'Region VI - Western Visayas'),
(36, 'ACLC College of Iriga City, Inc.', 'Region V - Bicol Region'),
(37, 'ACLC College of Mabalacat, Inc.', 'Region III - Central Luzon'),
(38, 'ACLC College of Malolos, Inc.', 'Region III - Central Luzon'),
(39, 'ACLC College of Mandaue', 'Region VII - Central Visayas'),
(40, 'ACLC College of Marbel', 'Region XII - SOCCSKSARGEN'),
(41, 'ACLC College of Meycauayan, Inc.', 'Region III - Central Luzon'),
(42, 'ACLC College of Poblacion, Baliuag, Inc.', 'Region III - Central Luzon'),
(43, 'ACLC College Of San Pablo', 'Region IV - CALABARZON'),
(44, 'ACLC College of Sorsogon, Inc.', 'Region V - Bicol Region'),
(45, 'ACLC College of Sta. Maria, Inc.', 'Region III - Central Luzon'),
(46, 'ACLC College of Tagum', 'Region XI - Davao Region'),
(47, 'ACLC College of Taytay', 'Region IV - CALABARZON'),
(48, 'ACLC College Tacloban City', 'Region VIII - Eastern Visayas'),
(49, 'ACLC College, Taguig', 'National Capital Region (NCR)'),
(50, 'ACLC College-Ormoc', 'Region VIII - Eastern Visayas'),
(51, 'ACQ College of Ministries', 'Region XI - Davao Region'),
(52, 'ACSI College of Iloilo, Inc.', 'Region VI - Western Visayas'),
(53, 'ACTS Computer College', 'Region IV - CALABARZON'),
(54, 'ACTS Computer College Infanta', 'Region IV - CALABARZON'),
(55, 'Adamson University', 'National Capital Region (NCR)'),
(56, 'Advance Central College', 'Region VI - Western Visayas'),
(57, 'Adventist College of Technology, Incorporated', 'Region XII - SOCCSKSARGEN'),
(58, 'Adventist Medical Center College', 'Region X - Northern Mindanao'),
(59, 'Adventist Technological Institute', 'Region X - Northern Mindanao'),
(60, 'Adventist University of the Philippines', 'Region IV - CALABARZON'),
(61, 'Aemilianum College', 'Region V - Bicol Region'),
(62, 'Aeronautical Academy of the Philippines', 'Region V - Bicol Region'),
(63, 'Ago Medical and Educational Center', 'Region V - Bicol Region'),
(64, 'Agoncillo College', 'Region IV - CALABARZON'),
(65, 'Agro-Industrial Foundation College of the Philippines-Matina', 'Region XI - Davao Region'),
(66, 'Agusan Colleges, Inc.', 'CARAGA'),
(67, 'Agusan Del Sur College, Inc.', 'CARAGA'),
(68, 'Agusan Del Sur State College Of Agriculture And Technology', 'CARAGA'),
(69, 'AIE College, Inc.', 'Region I - Ilocos Region'),
(70, 'Aim High College', 'Region IX - Zamboanga Peninsula'),
(71, 'Air Link International Aviation College', 'National Capital Region (NCR)'),
(72, 'Aklan Catholic College', 'Region VI - Western Visayas'),
(73, 'Aklan Polytechnic College', 'Region VI - Western Visayas'),
(74, 'Aklan State University, Ibajay Campus', 'Region VI - Western Visayas'),
(75, 'Aklan State University, Kalibo Campus', 'Region VI - Western Visayas'),
(76, 'Aklan State University, Main Campus', 'Region VI - Western Visayas'),
(77, 'Aklan State University, Makato Campus', 'Region VI - Western Visayas'),
(78, 'Aklan State University, New Washington Campus', 'Region VI - Western Visayas'),
(79, 'Aldersgate College', 'Region II - Cagayan Valley'),
(80, 'Alfelor Sr. Memorial College', 'Region V - Bicol Region'),
(81, 'Alhadeetha Mindanao College', 'Region IX - Zamboanga Peninsula'),
(82, 'Alitagtag College', 'Region IV - CALABARZON'),
(83, 'All Nations College', 'Region IV - CALABARZON'),
(84, 'Alliance Graduate School', 'National Capital Region (NCR)'),
(85, 'Alpha Centauri Educational System', 'Region IV - CALABARZON'),
(86, 'AMA Computer College', 'Region XI - Davao Region'),
(87, 'AMA Computer College - Cagayan De Oro City', 'Region X - Northern Mindanao'),
(88, 'AMA Computer College – Cebu', 'Region VII - Central Visayas'),
(89, 'AMA Computer College – Dagupan City', 'Region I - Ilocos Region'),
(90, 'AMA Computer College – Iloilo', 'Region VI - Western Visayas'),
(91, 'AMA Computer College – La Union', 'Region I - Ilocos Region'),
(92, 'AMA Computer College – Legazpi City', 'Region V - Bicol Region'),
(93, 'AMA Computer College – Naga', 'Region V - Bicol Region'),
(94, 'AMA Computer College Bacolod', 'Negros Island Region (NIR)'),
(95, 'AMA Computer College Batangas City', 'Region IV - CALABARZON'),
(96, 'AMA Computer College Binan', 'Region IV - CALABARZON'),
(97, 'AMA Computer College Calamba', 'Region IV - CALABARZON'),
(98, 'AMA Computer College Dasmarinas', 'Region IV - CALABARZON'),
(99, 'AMA Computer College East Rizal', 'Region IV - CALABARZON'),
(100, 'AMA Computer College Lipa', 'Region IV - CALABARZON'),
(101, 'AMA Computer College Lucena', 'Region IV - CALABARZON'),
(102, 'AMA Computer College Sta.Cruz', 'Region IV - CALABARZON'),
(103, 'AMA Computer College- Tacloban City', 'Region VIII - Eastern Visayas'),
(104, 'AMA Computer College Tuguegarao City', 'Region II - Cagayan Valley'),
(105, 'AMA Computer College, Fairview', 'National Capital Region (NCR)'),
(106, 'AMA Computer College, Makati City', 'National Capital Region (NCR)'),
(107, 'AMA Computer College, Mandaluyong', 'National Capital Region (NCR)'),
(108, 'AMA Computer College-Angeles City', 'Region III - Central Luzon'),
(109, 'AMA Computer College-Baguio', 'Cordillera Administrative Region (CAR)'),
(110, 'AMA Computer College-Cabanatuan City', 'Region III - Central Luzon'),
(111, 'AMA Computer College-Caloocan', 'National Capital Region (NCR)'),
(112, 'AMA Computer College-Dumaguete', 'Region VII - Central Visayas'),
(113, 'AMA Computer College-General Santos Campus', 'Region XII - SOCCSKSARGEN'),
(114, 'AMA Computer College-Laoag City', 'Region I - Ilocos Region'),
(115, 'AMA Computer College-Las Piñas', 'National Capital Region (NCR)'),
(116, 'AMA Computer College-Malolos City', 'Region III - Central Luzon'),
(117, 'AMA Computer College-Olongapo City', 'Region III - Central Luzon'),
(118, 'AMA Computer College-Parañaque', 'National Capital Region (NCR)'),
(119, 'AMA Computer College-Pasig City', 'National Capital Region (NCR)'),
(120, 'AMA Computer College-San Fernando City', 'Region III - Central Luzon'),
(121, 'AMA Computer College-Santiago City', 'Region II - Cagayan Valley'),
(122, 'AMA Computer College-Sta. Mesa', 'National Capital Region (NCR)'),
(123, 'AMA Computer College-Tarlac City', 'Region III - Central Luzon'),
(124, 'AMA Computer College-Zamboanga City', 'Region IX - Zamboanga Peninsula'),
(125, 'AMA Computer Learning Center College', 'Region XII - SOCCSKSARGEN'),
(126, 'AMA School of Medicine', 'National Capital Region (NCR)'),
(127, 'AMA University', 'National Capital Region (NCR)'),
(128, 'Amando Cope College', 'Region V - Bicol Region'),
(129, 'Andres Bonifacio College, Inc.', 'Region IX - Zamboanga Peninsula'),
(130, 'Andres Soriano College', 'CARAGA'),
(131, 'Angeles University Foundation', 'Region III - Central Luzon'),
(132, 'Angelicum College', 'National Capital Region (NCR)'),
(133, 'Annunciation College of Bacon Sorsogon Unit, Inc.', 'Region V - Bicol Region'),
(134, 'Antonio R. Pacheco College, Inc.', 'Region XII - SOCCSKSARGEN'),
(135, 'Apayao State College-Conner', 'Cordillera Administrative Region (CAR)'),
(136, 'Apayao State College-Luna', 'Cordillera Administrative Region (CAR)'),
(137, 'Aquinas University of Legazpi', 'Region V - Bicol Region'),
(138, 'Arellano University, Malabon', 'National Capital Region (NCR)'),
(139, 'Arellano University, Mandaluyong', 'National Capital Region (NCR)'),
(140, 'Arellano University, Manila', 'National Capital Region (NCR)'),
(141, 'Arellano University, Pasay', 'National Capital Region (NCR)'),
(142, 'Arellano University, Pasig', 'National Capital Region (NCR)'),
(143, 'Arriesgado College Foundation', 'Region XI - Davao Region'),
(144, 'Asbury College, Inc.', 'Region I - Ilocos Region'),
(145, 'Asia College Of Advance Studies In Arts Sciences And Technology, Inc.', 'Region VIII - Eastern Visayas'),
(146, 'Asia Graduate School of Theology', 'National Capital Region (NCR)'),
(147, 'Asia Harvesters College and Seminary Inc.', 'National Capital Region (NCR)'),
(148, 'Asia Pacific Christian College And Seminary', 'Region IV - CALABARZON'),
(149, 'Asia Pacific College', 'National Capital Region (NCR)'),
(150, 'Asia Pacific College of Advanced Studies', 'Region III - Central Luzon'),
(151, 'Asia School of Arts and Sciences', 'National Capital Region (NCR)'),
(152, 'Asia Tech. School of Sci. and Arts', 'Region IV - CALABARZON'),
(153, 'Asiacareer College Foundation, Inc', 'Region I - Ilocos Region'),
(154, 'Asian College Foundation', 'CARAGA'),
(155, 'Asian College of Science and Technology, Cubao', 'National Capital Region (NCR)'),
(156, 'Asian College of Science and Technology-Dumaguete', 'Region VII - Central Visayas'),
(157, 'Asian College of Technology International Education Foundation', 'Region VII - Central Visayas'),
(158, 'Asian Colleges and Technological Institute, Inc.', 'Region XII - SOCCSKSARGEN'),
(159, 'Asian Development Foundation College', 'Region VIII - Eastern Visayas'),
(160, 'Asian Institute for Distance Education', 'National Capital Region (NCR)'),
(161, 'Asian Institute of Computer Studies, Bicutan', 'National Capital Region (NCR)'),
(162, 'Asian Institute of Computer Studies, Caloocan', 'National Capital Region (NCR)'),
(163, 'Asian Institute of Computer Studies, Commonwealth', 'National Capital Region (NCR)'),
(164, 'Asian Institute of Computer Studies-Central Inc.', 'Region III - Central Luzon'),
(165, 'Asian Institute of Journalism and Communication', 'National Capital Region (NCR)'),
(166, 'Asian Institute of Management', 'National Capital Region (NCR)'),
(167, 'Asian Institute of Maritime Studies', 'National Capital Region (NCR)'),
(168, 'Asian Institute of Science and Technology', 'National Capital Region (NCR)'),
(169, 'Asian Institute of Technology and Education Tiaong Campus', 'Region IV - CALABARZON'),
(170, 'Asian International School of Aeronautics and Technology', 'Region XI - Davao Region'),
(171, 'Asian School of Aviation Professional', 'National Capital Region (NCR)'),
(172, 'Asian School of Hospitality Arts', 'Region IV - CALABARZON'),
(173, 'Asian Seminary of Christian Ministries', 'National Capital Region (NCR)'),
(174, 'Asian Social Institute', 'National Capital Region (NCR)'),
(175, 'Asian Theological Seminary', 'National Capital Region (NCR)'),
(176, 'Asia-Pacific Nazarene Theological Seminary', 'Region IV - CALABARZON'),
(177, 'Assumption College', 'National Capital Region (NCR)'),
(178, 'Assumption College of Davao', 'Region XI - Davao Region'),
(179, 'Assumption College of Nabunturan', 'Region XI - Davao Region'),
(180, 'Ateneo de Davao University', 'Region XI - Davao Region'),
(181, 'Ateneo de Manila University Graduate School of Business', 'National Capital Region (NCR)'),
(182, 'Ateneo De Manila University School of Medicine and Public Health', 'National Capital Region (NCR)'),
(183, 'Ateneo de Manila University, Quezon City', 'National Capital Region (NCR)'),
(184, 'Ateneo De Naga University', 'Region V - Bicol Region'),
(185, 'Ateneo de Zamboanga University', 'Region IX - Zamboanga Peninsula'),
(186, 'Aurora Pioneers Memorial College', 'Region IX - Zamboanga Peninsula'),
(187, 'Aurora Polytechnic College, Inc.', 'Region III - Central Luzon'),
(188, 'Aurora State College of Technology-Casiguran Campus', 'Region III - Central Luzon'),
(189, 'Aurora State College of Technology-Main Campus', 'Region III - Central Luzon'),
(190, 'Aurora State College of Technology-Maria Aurora Campus', 'Region III - Central Luzon'),
(191, 'Ave Maria College', 'Region IX - Zamboanga Peninsula'),
(192, 'B.E.S.T. College of Polomolok, Inc.', 'Region XII - SOCCSKSARGEN'),
(193, 'Baccarra Medical Center School of Midwifery', 'Region I - Ilocos Region'),
(194, 'Baguio Central University', 'Cordillera Administrative Region (CAR)'),
(195, 'Baguio Christian Mission International College, Inc.', 'Cordillera Administrative Region (CAR)'),
(196, 'Baguio Colleges of Technology', 'Cordillera Administrative Region (CAR)'),
(197, 'Balite Institute of Technology-Butuan', 'CARAGA'),
(198, 'Baliuag University', 'Region III - Central Luzon'),
(199, 'Baliwag Maritime Academy', 'Region III - Central Luzon'),
(200, 'Bantayan Southern Institute', 'Region VII - Central Visayas'),
(201, 'Baptist Bible Seminary and Institute', 'Region IV - CALABARZON'),
(202, 'Baptist Theological College', 'Region VII - Central Visayas'),
(203, 'Baptist Voice Bible College', 'Region IV - CALABARZON'),
(204, 'Basilan State College Lamitan Extension', 'Region IX - Zamboanga Peninsula'),
(205, 'Basilan State College Main Campus', 'Region IX - Zamboanga Peninsula'),
(206, 'Basilan State College Maluso Campus', 'Region IX - Zamboanga Peninsula'),
(207, 'Bataan Heroes Memorial College', 'Region III - Central Luzon'),
(208, 'Bataan Peninsula State University-Abucay Campus', 'Region III - Central Luzon'),
(209, 'Bataan Peninsula State University-Balanga Campus', 'Region III - Central Luzon'),
(210, 'Bataan Peninsula State University-Dinalupihan Campus', 'Region III - Central Luzon'),
(211, 'Bataan Peninsula State University-Main Campus', 'Region III - Central Luzon'),
(212, 'Bataan Peninsula State University-Orani Campus', 'Region III - Central Luzon'),
(213, 'Batanes State College, Basco, Batanes', 'Region II - Cagayan Valley'),
(214, 'Batangas College of Arts and Sciences', 'Region IV - CALABARZON'),
(215, 'Batangas Eastern College', 'Region IV - CALABARZON'),
(216, 'Batangas State University Apolinario R. Apacible School of Fisheries Nasugbu', 'Region IV - CALABARZON'),
(217, 'Batangas State University Balayan', 'Region IV - CALABARZON'),
(218, 'Batangas State University Jose P. Laurel Polytechnic College Malvar', 'Region IV - CALABARZON'),
(219, 'Batangas State University Lemery', 'Region IV - CALABARZON'),
(220, 'Batangas State University Lipa City', 'Region IV - CALABARZON'),
(221, 'Batangas State University Lobo', 'Region IV - CALABARZON'),
(222, 'Batangas State University Main', 'Region IV - CALABARZON'),
(223, 'Batangas State University Rosario', 'Region IV - CALABARZON'),
(224, 'Batangas State University San Juan', 'Region IV - CALABARZON'),
(225, 'Bato Institute Of Technology', 'Region VIII - Eastern Visayas'),
(226, 'Batuan Colleges', 'Region VII - Central Visayas'),
(227, 'Belen B. Francisco Foundation-Daraga', 'Region V - Bicol Region'),
(228, 'Belen B. Francisco Foundation-Sorsogon', 'Region V - Bicol Region'),
(229, 'Benedicto College', 'Region VII - Central Visayas'),
(230, 'Benguet State University-Bokod', 'Cordillera Administrative Region (CAR)'),
(231, 'Benguet State University-Buguias', 'Cordillera Administrative Region (CAR)'),
(232, 'Benguet State University-Main', 'Cordillera Administrative Region (CAR)'),
(233, 'Benguet State University-Open University', 'Cordillera Administrative Region (CAR)'),
(234, 'Benthel Asia School of Technology Inc.', 'Region VII - Central Visayas'),
(235, 'Bernardo College', 'National Capital Region (NCR)'),
(236, 'Bestlink College of the Philippines', 'National Capital Region (NCR)'),
(237, 'Bestlink College of the Philippines, Inc.', 'Region III - Central Luzon'),
(238, 'Bethel Bible College of the Assemblies of God', 'National Capital Region (NCR)'),
(239, 'Biblical Seminary of the Philippines', 'National Capital Region (NCR)'),
(240, 'Bicol Christian College of Medicine', 'Region V - Bicol Region'),
(241, 'Bicol College', 'Region V - Bicol Region'),
(242, 'Bicol State College of Applied Sciences and Technology (formerly: CSPC-Naga Campus)', 'Region V - Bicol Region'),
(243, 'Bicol University – Daraga Campus', 'Region V - Bicol Region'),
(244, 'Bicol University – Gubat Campus', 'Region V - Bicol Region'),
(245, 'Bicol University – Guinobatan Campus', 'Region V - Bicol Region'),
(246, 'Bicol University – Main', 'Region V - Bicol Region'),
(247, 'Bicol University – Polangui Campus', 'Region V - Bicol Region'),
(248, 'Bicol University – Tabaco Campus', 'Region V - Bicol Region'),
(249, 'Binalbagan Catholic College', 'Negros Island Region (NIR)'),
(250, 'Binangonan Catholic College', 'Region IV - CALABARZON'),
(251, 'BIT International College – Carmen', 'Region VII - Central Visayas'),
(252, 'BIT International College – Tagbilaran', 'Region VII - Central Visayas'),
(253, 'BIT International College- Siquijor', 'Region VII - Central Visayas'),
(254, 'BIT International College- Talibon', 'Region VII - Central Visayas'),
(255, 'BIT International College-Jagna', 'Region VII - Central Visayas'),
(256, 'Blancia College Foundation, Inc.', 'Region IX - Zamboanga Peninsula'),
(257, 'Blessed Mother College', 'Region X - Northern Mindanao'),
(258, 'Blessed Trinity College', 'Region VII - Central Visayas'),
(259, 'Bohol International Learning College', 'Region VII - Central Visayas'),
(260, 'Bohol Island State University – Balilihan', 'Region VII - Central Visayas'),
(261, 'Bohol Island State University – Bilar', 'Region VII - Central Visayas'),
(262, 'Bohol Island State University – Calape', 'Region VII - Central Visayas'),
(263, 'Bohol Island State University – Candijay', 'Region VII - Central Visayas'),
(264, 'Bohol Island State University – Clarin', 'Region VII - Central Visayas'),
(265, 'Bohol Island State University – Tagbilaran', 'Region VII - Central Visayas'),
(266, 'Bohol Northern Star College, Inc.', 'Region VII - Central Visayas'),
(267, 'Bohol Northwestern Colleges', 'Region VII - Central Visayas'),
(268, 'Bohol Wisdom School', 'Region VII - Central Visayas'),
(269, 'Brent Hospital and Colleges, Inc.', 'Region IX - Zamboanga Peninsula'),
(270, 'Brentwood College of Asia International School', 'Region V - Bicol Region'),
(271, 'Brokenshire College', 'Region XI - Davao Region'),
(272, 'Brokenshire College of Socksargen, Inc.', 'Region XII - SOCCSKSARGEN'),
(273, 'Brookfield College', 'Region IV - CALABARZON'),
(274, 'BSBT College', 'Cordillera Administrative Region (CAR)'),
(275, 'Bucas Grande Foundation College', 'CARAGA'),
(276, 'Bukidnon State University', 'Region X - Northern Mindanao'),
(277, 'Bukidnon State University – Alubijid', 'Region X - Northern Mindanao'),
(278, 'Bukidnon State University – Baungon', 'Region X - Northern Mindanao'),
(279, 'Bukidnon State University – Compostela', 'Region X - Northern Mindanao'),
(280, 'Bukidnon State University – Gingoog', 'Region X - Northern Mindanao'),
(281, 'Bukidnon State University – Kadingilan', 'Region X - Northern Mindanao'),
(282, 'Bukidnon State University – Kalilangan', 'Region X - Northern Mindanao'),
(283, 'Bukidnon State University – Kinoguitan', 'Region X - Northern Mindanao'),
(284, 'Bukidnon State University – Libertad', 'Region X - Northern Mindanao'),
(285, 'Bukidnon State University – Libona', 'Region X - Northern Mindanao'),
(286, 'Bukidnon State University – Magsaysay', 'Region X - Northern Mindanao'),
(287, 'Bukidnon State University – Malitbog', 'Region X - Northern Mindanao'),
(288, 'Bukidnon State University – Manticao', 'Region X - Northern Mindanao'),
(289, 'Bukidnon State University – Maragusan', 'Region X - Northern Mindanao'),
(290, 'Bukidnon State University – Medina', 'Region X - Northern Mindanao'),
(291, 'Bukidnon State University – MOGCHS', 'Region X - Northern Mindanao'),
(292, 'Bukidnon State University – Monkayo', 'Region X - Northern Mindanao'),
(293, 'Bukidnon State University – Montevista', 'Region X - Northern Mindanao'),
(294, 'Bukidnon State University – New Bataan', 'Region X - Northern Mindanao'),
(295, 'Bukidnon State University – Salay', 'Region X - Northern Mindanao'),
(296, 'Bukidnon State University – Sto. Tomas', 'Region X - Northern Mindanao'),
(297, 'Bukidnon State University – Sugbongcogon', 'Region X - Northern Mindanao'),
(298, 'Bukidnon State University – Talakag', 'Region X - Northern Mindanao'),
(299, 'Bukidnon State University – Talisayan', 'Region X - Northern Mindanao'),
(300, 'Bulacan Agricultural State College', 'Region III - Central Luzon'),
(301, 'Bulacan State University – Bulacan (Meneses) Campus', 'Region III - Central Luzon'),
(302, 'Bulacan State University – Bustos Campus', 'Region III - Central Luzon'),
(303, 'Bulacan State University – Hagonoy Campus', 'Region III - Central Luzon'),
(304, 'Bulacan State University – Main Campus', 'Region III - Central Luzon'),
(305, 'Bulacan State University – Sarmiento Campus', 'Region III - Central Luzon'),
(306, 'Butuan City Colleges', 'CARAGA'),
(307, 'Butuan Doctors’ College', 'CARAGA'),
(308, 'BVS Colleges', 'Cordillera Administrative Region (CAR)'),
(309, 'BWEST College', 'Region IV - CALABARZON'),
(310, 'Cabalum Western College', 'Region VI - Western Visayas'),
(311, 'Cabarrus Catholic College', 'Negros Island Region (NIR)'),
(312, 'Cagayan de Oro College', 'Region X - Northern Mindanao'),
(313, 'Cagayan State University – Tuguegarao', 'Region II - Cagayan Valley'),
(314, 'Cagayan State University-Andrews Campus, Tuguegarao City', 'Region II - Cagayan Valley'),
(315, 'Cagayan State University-Aparri', 'Region II - Cagayan Valley'),
(316, 'Cagayan State University-Carig Campus, Tuguegarao City', 'Region II - Cagayan Valley'),
(317, 'Cagayan State University-Gonzaga Campus', 'Region II - Cagayan Valley'),
(318, 'Cagayan State University-Lal-lo Campus', 'Region II - Cagayan Valley'),
(319, 'Cagayan State University-Lasam Campus', 'Region II - Cagayan Valley'),
(320, 'Cagayan State University-Piat Campus', 'Region II - Cagayan Valley'),
(321, 'Cagayan State University-Sanchez Mira Campus', 'Region II - Cagayan Valley'),
(322, 'Cagayan Valley Colleges of Quirino', 'Region II - Cagayan Valley'),
(323, 'Cagayan Valley Computer and Information Technology College', 'Region II - Cagayan Valley'),
(324, 'Cainta Catholic College', 'Region IV - CALABARZON'),
(325, 'Calamba Doctors’ College<', 'Region IV - CALABARZON'),
(326, 'Calauag Central College', 'Region IV - CALABARZON'),
(327, 'Calayan Educational Foundation', 'Region IV - CALABARZON'),
(328, 'Calbiga Western Leyte College, Inc.', 'Region VIII - Eastern Visayas'),
(329, 'Camarines Norte College', 'Region V - Bicol Region'),
(330, 'Camarines Norte College of Arts and Business', 'Region V - Bicol Region'),
(331, 'Camarines Norte School of Laws, Arts and Sciences', 'Region V - Bicol Region'),
(332, 'Camarines Norte State College-Labo Campus', 'Region V - Bicol Region'),
(333, 'Camarines Norte State College-Main', 'Region V - Bicol Region'),
(334, 'Camarines Norte State College-Mercedes Campus', 'Region V - Bicol Region'),
(335, 'Camarines Norte State College-Panganiban Campus', 'Region V - Bicol Region'),
(336, 'Camarines Sur Polytechnic College-Main', 'Region V - Bicol Region'),
(337, 'Camiguin Polytechnic State College', 'Region X - Northern Mindanao'),
(338, 'Camiling Colleges', 'Region III - Central Luzon'),
(339, 'Candelaria Institute Of Cabadbaran, Inc.', 'CARAGA'),
(340, 'Canossa College', 'Region IV - CALABARZON'),
(341, 'CAP College Foundation', 'National Capital Region (NCR)'),
(342, 'Capalonga College, Inc.', 'Region V - Bicol Region'),
(343, 'Capitol Medical Center Colleges', 'National Capital Region (NCR)'),
(344, 'Capitol University', 'Region X - Northern Mindanao'),
(345, 'Capiz State University, Burias Campus', 'Region VI - Western Visayas'),
(346, 'Capiz State University, Dayao Campus', 'Region VI - Western Visayas'),
(347, 'Capiz State University, Dumarao Campus', 'Region VI - Western Visayas'),
(348, 'Capiz State University, Main Campus', 'Region VI - Western Visayas'),
(349, 'Capiz State University, Mambusao Campus', 'Region VI - Western Visayas'),
(350, 'Capiz State University, Pilar Campus', 'Region VI - Western Visayas'),
(351, 'Capiz State University, Pontevedra Campus', 'Region VI - Western Visayas'),
(352, 'Capiz State University, Sapian Campus', 'Region VI - Western Visayas'),
(353, 'Capiz State University, Sigma Campus', 'Region VI - Western Visayas'),
(354, 'Capiz State University, Tapaz Campus', 'Region VI - Western Visayas'),
(355, 'Caraga Institute Of Technology', 'CARAGA'),
(356, 'Caraga State University – Cabadbaran Campus', 'CARAGA'),
(357, 'Caraga State University – Main', 'CARAGA'),
(358, 'CARD-MRI Development Institute', 'Region IV - CALABARZON'),
(359, 'Carlos Hilado Memorial State College, Alijis Campus', 'Negros Island Region (NIR)'),
(360, 'Carlos Hilado Memorial State College, Bacolod Campus', 'Negros Island Region (NIR)'),
(361, 'Carlos Hilado Memorial State College, Binalbagan Campus', 'Negros Island Region (NIR)'),
(362, 'Carlos Hilado Memorial State College, Fortune Towne Campus', 'Negros Island Region (NIR)'),
(363, 'Carlos Hilado Memorial State College, Main Campus', 'Negros Island Region (NIR)'),
(364, 'Carmelite College of Siquijor', 'Region VII - Central Visayas'),
(365, 'Carthel Science Educational Foundation (CSEF), Inc.', 'Region III - Central Luzon'),
(366, 'Casa Del Nino College', 'Region IV - CALABARZON'),
(367, 'Casiciaco Recoletos Seminary', 'Cordillera Administrative Region (CAR)'),
(368, 'Catanduanes Colleges', 'Region V - Bicol Region'),
(369, 'Catanduanes Institute of Technology Foundation', 'Region V - Bicol Region'),
(370, 'Catanduanes State University – Main', 'Region V - Bicol Region'),
(371, 'Catanduanes State University – Panganiban', 'Region V - Bicol Region'),
(372, 'Cavite State University Bacoor', 'Region IV - CALABARZON'),
(373, 'Cavite State University Carmona', 'Region IV - CALABARZON'),
(374, 'Cavite State University Cavite City', 'Region IV - CALABARZON'),
(375, 'Cavite State University General Trias Campus', 'Region IV - CALABARZON'),
(376, 'Cavite State University Imus', 'Region IV - CALABARZON'),
(377, 'Cavite State University Main', 'Region IV - CALABARZON'),
(378, 'Cavite State University Naic', 'Region IV - CALABARZON'),
(379, 'Cavite State University Rosario', 'Region IV - CALABARZON'),
(380, 'Cavite State University Silang Campus', 'Region IV - CALABARZON'),
(381, 'Cavite State University Tanza Campus', 'Region IV - CALABARZON'),
(382, 'Cavite State University Trece Martires City', 'Region IV - CALABARZON'),
(383, 'Cavite West Point College Cavite City', 'Region IV - CALABARZON'),
(384, 'Cavite West Point College Ternate', 'Region IV - CALABARZON'),
(385, 'CBD College', 'Region VII - Central Visayas'),
(386, 'Cebu Aeronautical Technical School', 'Region VII - Central Visayas'),
(387, 'Cebu Doctors’ University', 'Region VII - Central Visayas'),
(388, 'Cebu Doctors’ University College of Medicine', 'Region VII - Central Visayas'),
(389, 'Cebu Eastern College', 'Region VII - Central Visayas'),
(390, 'Cebu Institute of Medicine', 'Region VII - Central Visayas'),
(391, 'Cebu Institute of Technology – University', 'Region VII - Central Visayas'),
(392, 'Cebu Mary Immaculate College', 'Region VII - Central Visayas'),
(393, 'Cebu Normal University', 'Region VII - Central Visayas'),
(394, 'Cebu Roosevelt Memorial College', 'Region VII - Central Visayas'),
(395, 'Cebu Sacred Heart College', 'Region VII - Central Visayas'),
(396, 'Cebu School of Midwifery', 'Region VII - Central Visayas'),
(397, 'Cebu Technological University – Argao', 'Region VII - Central Visayas'),
(398, 'Cebu Technological University – Carmen', 'Region VII - Central Visayas'),
(399, 'Cebu Technological University – Danao', 'Region VII - Central Visayas'),
(400, 'Cebu Technological University – Main', 'Region VII - Central Visayas'),
(401, 'Cebu Technological University – Moalboal', 'Region VII - Central Visayas'),
(402, 'Cebu Technological University – San Francisco', 'Region VII - Central Visayas'),
(403, 'Cebu Technological University – Tuburan', 'Region VII - Central Visayas'),
(404, 'Ceguera Technological College', 'Region V - Bicol Region'),
(405, 'Center for Biblical Studies Institute and Seminary', 'Region IV - CALABARZON'),
(406, 'Center for Culinary Arts', 'National Capital Region (NCR)'),
(407, 'Central Bicol State University of Agriculture – Calabanga', 'Region V - Bicol Region'),
(408, 'Central Bicol State University of Agriculture – Main', 'Region V - Bicol Region'),
(409, 'Central Bicol State University of Agriculture – Pasacao', 'Region V - Bicol Region'),
(410, 'Central Bicol State University of Agriculture – Sipocot', 'Region V - Bicol Region'),
(411, 'Central College of the Philippines', 'National Capital Region (NCR)'),
(412, 'Central Luzon College of Science And Technology Olongapo City, Inc.', 'Region III - Central Luzon'),
(413, 'Central Luzon College of Science And Technology, Inc.-San Fernando City', 'Region III - Central Luzon'),
(414, 'Central Luzon Doctors’ Hospital Educational Institution, Inc.', 'Region III - Central Luzon'),
(415, 'Central Luzon State University', 'Region III - Central Luzon'),
(416, 'Central Mindanao Colleges', 'Region XII - SOCCSKSARGEN'),
(417, 'Central Mindanao Computer School Inc.', 'Region XII - SOCCSKSARGEN'),
(418, 'Central Mindanao University', 'Region X - Northern Mindanao'),
(419, 'Central Philippine Adventist College', 'Negros Island Region (NIR)'),
(420, 'Central Philippine Bible College, Inc.', 'Region VII - Central Visayas'),
(421, 'Central Philippine University', 'Region VI - Western Visayas'),
(422, 'Central Philippines State University, Candoni Campus', 'Negros Island Region (NIR)'),
(423, 'Central Philippines State University, Cauayan Campus', 'Negros Island Region (NIR)'),
(424, 'Central Philippines State University, Hinigaran Campus', 'Negros Island Region (NIR)'),
(425, 'Central Philippines State University, Hinobaan Campus', 'Negros Island Region (NIR)'),
(426, 'Central Philippines State University, Ilog Campus', 'Negros Island Region (NIR)'),
(427, 'Central Philippines State University, Main Campus', 'Negros Island Region (NIR)'),
(428, 'Central Philippines State University, Moises Padilla Campus', 'Negros Island Region (NIR)'),
(429, 'Central Philippines State University, San Carlos Campus', 'Negros Island Region (NIR)'),
(430, 'Central Philippines State University, Sipalay Campus', 'Negros Island Region (NIR)'),
(431, 'Central Philippines State University, Victorias Campus', 'Negros Island Region (NIR)'),
(432, 'Centre for International Education Global Colleges', 'Region VII - Central Visayas'),
(433, 'Centro Colegio De Tarlac, Inc.', 'Region III - Central Luzon'),
(434, 'Centro Escolar University At Malolos', 'Region III - Central Luzon'),
(435, 'Centro Escolar University, Manila', 'National Capital Region (NCR)'),
(436, 'Centro Escolar University-Makati', 'National Capital Region (NCR)'),
(437, 'Chiang Kai Shek College', 'National Capital Region (NCR)'),
(438, 'Children of Mary Immaculate College', 'National Capital Region (NCR)'),
(439, 'Chinese General Hospital Colleges, Inc.', 'National Capital Region (NCR)'),
(440, 'Christ The King College - Calbayog', 'Region VIII - Eastern Visayas'),
(441, 'Christ the King College - Gingoog', 'Region X - Northern Mindanao'),
(442, 'Christ the King College de Maranding', 'Region X - Northern Mindanao'),
(443, 'Christ the King College of Science and Technology', 'National Capital Region (NCR)'),
(444, 'Christian College of Tanauan', 'Region IV - CALABARZON'),
(445, 'Christian Colleges of Southeast Asia', 'Region XI - Davao Region'),
(446, 'Christian Polytechnic Institute of Catanduanes', 'Region V - Bicol Region'),
(447, 'CICOSAT Colleges', 'Region I - Ilocos Region'),
(448, 'CIIT College of Arts and Technology', 'National Capital Region (NCR)'),
(449, 'CIT Colleges of Paniqui Foundation, Inc.', 'Region III - Central Luzon'),
(450, 'CITI Global College Inc. Biñan', 'Region IV - CALABARZON'),
(451, 'CITI Global College Inc. Cabuyao', 'Region IV - CALABARZON'),
(452, 'Clarendon College', 'Region IVB - MIMAROPA'),
(453, 'Claret College of Isabela', 'Region IX - Zamboanga Peninsula'),
(454, 'Clark College of Science And Technology, Inc.', 'Region III - Central Luzon'),
(455, 'CLCC Institute of Computer Arts & Technology', 'Region IVB - MIMAROPA'),
(456, 'Coland Systems Technology, Inc.', 'Region XII - SOCCSKSARGEN'),
(457, 'Colegio de Amore', 'Region IV - CALABARZON'),
(458, 'Colegio De Calumpit, Inc.', 'Region III - Central Luzon'),
(459, 'Colegio de Dagupan', 'Region I - Ilocos Region'),
(460, 'Colegio De Kidapawan', 'Region XII - SOCCSKSARGEN'),
(461, 'Colegio De La Salle Fondation De Tacloban Inc.', 'Region VIII - Eastern Visayas'),
(462, 'Colegio de Los Banos', 'Region IV - CALABARZON'),
(463, 'Colegio de Porta Vaga', 'Region IV - CALABARZON'),
(464, 'Colegio de San Antonio de Padua', 'Region VII - Central Visayas'),
(465, 'Colegio De San Francisco Javier', 'Region IX - Zamboanga Peninsula'),
(466, 'Colegio De San Gabriel Arcangel, Inc.', 'Region III - Central Luzon'),
(467, 'Colegio de San Gabriel Archangel of Caloocan', 'National Capital Region (NCR)'),
(468, 'Colegio de San Jose', 'Region VI - Western Visayas'),
(469, 'Colegio De San Juan De Letran', 'Region III - Central Luzon'),
(470, 'Colegio de San Juan de Letran – Manaoag (formerly Our Lady of Manaoag College)', 'Region I - Ilocos Region'),
(471, 'Colegio De San Juan De Letran Calamba', 'Region IV - CALABARZON'),
(472, 'Colegio de San Juan de Letran, Manila', 'National Capital Region (NCR)'),
(473, 'Colegio De San Juan Samar, Inc.', 'Region VIII - Eastern Visayas'),
(474, 'Colegio de San Lorenzo', 'National Capital Region (NCR)'),
(475, 'Colegio De San Lorenzo Ruiz De Manila Of Northern Samar, Inc', 'Region VIII - Eastern Visayas'),
(476, 'Colegio De San Pascual Baylon', 'Region III - Central Luzon'),
(477, 'Colegio De San Pedro', 'Region IV - CALABARZON'),
(478, 'Colegio De San Sebastian', 'Region IVB - MIMAROPA'),
(479, 'Colegio de Santa Catalina de Alejandria', 'Region VII - Central Visayas'),
(480, 'Colegio De Sebastian-Pampanga, Inc.', 'Region III - Central Luzon'),
(481, 'Colegio De Sta Lourdes Of Leyte Foundation,Inc.', 'Region VIII - Eastern Visayas'),
(482, 'Colegio de Sta. Ana de Victorias', 'Negros Island Region (NIR)'),
(483, 'Colegio de Sta. Monica of Polangui, Inc.', 'Region V - Bicol Region'),
(484, 'Colegio de Sta. Teresa de Avila Foundation, Inc.', 'National Capital Region (NCR)'),
(485, 'Colegio de Sto. Cristo de Burgos', 'Region IV - CALABARZON'),
(486, 'Colegio De Sto. Domingo De Silos', 'Region IV - CALABARZON'),
(487, 'Colegio de Sto. Nino de Jasaan', 'Region X - Northern Mindanao'),
(488, 'Colegio del Sagrado Corazon de Jesus', 'Region VI - Western Visayas'),
(489, 'Colegio dela Purisima Concepcion', 'Region VI - Western Visayas'),
(490, 'Colegio San Agustin – Bacolod', 'Negros Island Region (NIR)'),
(491, 'Colegio San Agustin Binan', 'Region IV - CALABARZON'),
(492, 'Colegio San Jose De Alaminos', 'Region I - Ilocos Region'),
(493, 'College For Research & Technology of Cabanatuan, Inc.', 'Region III - Central Luzon'),
(494, 'College of Arts and Sciences of Asia and the Pacific - Pasig', 'National Capital Region (NCR)'),
(495, 'College of Arts and Sciences of Asia and the Pacific - Rizal', 'Region IV - CALABARZON'),
(496, 'College of Divine Wisdom', 'National Capital Region (NCR)'),
(497, 'College of Mary Immaculate of Pandi, Bulacan, Inc.', 'Region III - Central Luzon'),
(498, 'College of Our Lady of Mt. Carmel (Pampanga), Inc.', 'Region III - Central Luzon'),
(499, 'College of Saint Lawrence, Inc.', 'Region III - Central Luzon'),
(500, 'College of Saints John and Paul II Arts and Sciences Inc.', 'Region IV - CALABARZON'),
(501, 'College of San Benildo Rizal', 'Region IV - CALABARZON'),
(502, 'College of St. Catherine', 'National Capital Region (NCR)'),
(503, 'College of St. John-Roxas', 'Region VI - Western Visayas'),
(504, 'College of Subic Montessori Subic Bay, Inc.', 'Region III - Central Luzon'),
(505, 'College of Subic Montessori, Inc.', 'Region III - Central Luzon'),
(506, 'College of Technological Sciences – Cebu', 'Region VII - Central Visayas'),
(507, 'College of the Holy Spirit of Manila', 'National Capital Region (NCR)'),
(508, 'College of the Holy Spirit of Tarlac', 'Region III - Central Luzon'),
(509, 'College of the Immaculate Conception', 'Region III - Central Luzon'),
(510, 'College of the Most Holy Trinity Catholic Foundation, Inc.', 'Region III - Central Luzon'),
(511, 'College of the Our Lady of Mercy of Pulilan Foundation, Inc.', 'Region III - Central Luzon'),
(512, 'Colleges of Advance Technology And Management of the Phils. (CATMAN), Inc.', 'Region III - Central Luzon'),
(513, 'Collegium Societatis Angeli Pacis', 'Region VII - Central Visayas'),
(514, 'Columban College, Inc.-Olongapo City', 'Region III - Central Luzon'),
(515, 'Columban College-Sta. Cruz', 'Region III - Central Luzon'),
(516, 'Compostela Valley State College – Maragusan', 'Region XI - Davao Region'),
(517, 'Compostela Valley State College – Montevista', 'Region XI - Davao Region'),
(518, 'Compostela Valley State College – New Bataan', 'Region XI - Davao Region'),
(519, 'Compostela Valley State College-Main', 'Region XI - Davao Region'),
(520, 'Computer Arts and Technological College', 'Region V - Bicol Region'),
(521, 'Computer Technologies Institute of Basilan', 'Region IX - Zamboanga Peninsula'),
(522, 'Computer Technologies Institute of Zamboanga City', 'Region IX - Zamboanga Peninsula'),
(523, 'Comteq Computer & Business College', 'Region III - Central Luzon'),
(524, 'Concepcion Holy Cross College, Inc.', 'Region III - Central Luzon'),
(525, 'Concord Technical Institute', 'Region VII - Central Visayas'),
(526, 'Concordia College', 'National Capital Region (NCR)'),
(527, 'Concordia College of Benguet', 'Cordillera Administrative Region (CAR)'),
(528, 'Consolatrix College of Toledo City', 'Region VII - Central Visayas'),
(529, 'Convention Baptist Bible College', 'Negros Island Region (NIR)'),
(530, 'Cor Jesu College', 'Region XI - Davao Region'),
(531, 'Cordillera A+ Computer Technology College', 'Cordillera Administrative Region (CAR)'),
(532, 'Cordillera Career Development College', 'Cordillera Administrative Region (CAR)'),
(533, 'Core Gateway College, Inc.', 'Region III - Central Luzon'),
(534, 'Cotabato City State Polytechnic College', 'Region XII - SOCCSKSARGEN'),
(535, 'Cotabato Foundation College of Science and Technology', 'Region XII - SOCCSKSARGEN'),
(536, 'Cotabato Foundation College of Science and Technology – Antipas', 'Region XII - SOCCSKSARGEN'),
(537, 'Cotabato Foundation College of Science and Technology – Katipunan', 'Region XII - SOCCSKSARGEN'),
(538, 'Cotabato Foundation College of Science and Technology – Pikit', 'Region XII - SOCCSKSARGEN'),
(539, 'Cotabato Medical Foundation College, Inc.', 'Region XII - SOCCSKSARGEN'),
(540, 'Criminal Justice College', 'National Capital Region (NCR)'),
(541, 'Cristal e-College Panglao', 'Region VII - Central Visayas'),
(542, 'Cristal e-College Tagbilaran', 'Region VII - Central Visayas'),
(543, 'Cronasia Foundation College, Inc.', 'Region XII - SOCCSKSARGEN'),
(544, 'CSTC College of Science, Technology and Communication, Inc.', 'Region IV - CALABARZON'),
(545, 'CVE Colleges', 'Region IV - CALABARZON'),
(546, 'D’Illumina College', 'Region IV - CALABARZON'),
(547, 'Daehan College Of Business And Technology, Inc.', 'Region IV - CALABARZON'),
(548, 'Dagupan Colleges Foundation', 'Region I - Ilocos Region'),
(549, 'Daniel B. Pena Memorial College Foundation', 'Region V - Bicol Region'),
(550, 'Data Center College of the Philippines of Laoag City, Inc.', 'Region I - Ilocos Region'),
(551, 'Data Center College of the Philippines-Vigan City', 'Region I - Ilocos Region'),
(552, 'Data Center of the Philippines of Baguio City, Inc.', 'Cordillera Administrative Region (CAR)'),
(553, 'Data Center of the Philippines of Bangued, Abra', 'Cordillera Administrative Region (CAR)'),
(554, 'Datamex College of Saint Adeline, Fairview', 'National Capital Region (NCR)'),
(555, 'Datamex-College of Saint Adeline, Parañaque', 'National Capital Region (NCR)'),
(556, 'Datamex-College of Saint Adeline, Valenzuela', 'National Capital Region (NCR)'),
(557, 'Davao Central College', 'Region XI - Davao Region'),
(558, 'Davao del Norte State College', 'Region XI - Davao Region'),
(559, 'Davao Doctors College<', 'Region XI - Davao Region'),
(560, 'Davao Medical School Foundation', 'Region XI - Davao Region'),
(561, 'Davao Oriental State College of Science and Technology-Cateel Extension', 'Region XI - Davao Region'),
(562, 'Davao Oriental State College of Science and Technology-Main', 'Region XI - Davao Region'),
(563, 'Davao Oriental State College of Science and Technology-San Isidro Extension', 'Region XI - Davao Region'),
(564, 'Davao Vision Colleges', 'Region XI - Davao Region'),
(565, 'Davao Winchester Colleges', 'Region XI - Davao Region'),
(566, 'De La Salle Andres Soriano Memorial College', 'Region VII - Central Visayas'),
(567, 'De La Salle College of Saint Benilde', 'National Capital Region (NCR)'),
(568, 'De La Salle Health Sciences Institute', 'Region IV - CALABARZON'),
(569, 'De La Salle John Bosco College', 'CARAGA'),
(570, 'De La Salle Lipa', 'Region IV - CALABARZON'),
(571, 'De La Salle University', 'National Capital Region (NCR)'),
(572, 'De La Salle University Dasmarinas', 'Region IV - CALABARZON'),
(573, 'De La Salle-Araneta University', 'National Capital Region (NCR)'),
(574, 'De Ocampo Memorial College', 'National Capital Region (NCR)'),
(575, 'De Vera Institute of Technology', 'Region V - Bicol Region'),
(576, 'Deaf Evangelistic Alliance Foundation', 'Region IV - CALABARZON'),
(577, 'Diaz College', 'Region VII - Central Visayas'),
(578, 'Diliman College', 'National Capital Region (NCR)'),
(579, 'Dipolog City Institute of Technology', 'Region IX - Zamboanga Peninsula'),
(580, 'Divina Pastora College', 'Region III - Central Luzon'),
(581, 'Divine Mercy College Foundation, Inc.', 'National Capital Region (NCR)'),
(582, 'Divine Word College of Bangued', 'Cordillera Administrative Region (CAR)'),
(583, 'Divine Word College of Calapan', 'Region IVB - MIMAROPA'),
(584, 'Divine Word College of Laoag', 'Region I - Ilocos Region'),
(585, 'Divine Word College of Legazpi', 'Region V - Bicol Region'),
(586, 'Divine Word College of San Jose', 'Region IVB - MIMAROPA'),
(587, 'Divine Word College of Urdaneta', 'Region I - Ilocos Region'),
(588, 'Divine Word College of Vigan', 'Region I - Ilocos Region'),
(589, 'Divine Word Mission Seminary', 'National Capital Region (NCR)'),
(590, 'Divine Word Seminary', 'Region IV - CALABARZON'),
(591, 'DMC College Foundation, Inc.', 'Region IX - Zamboanga Peninsula'),
(592, 'DMMA College of Southern Philippines', 'Region XI - Davao Region'),
(593, 'DMMC Institute of Health Sciences', 'Region IV - CALABARZON'),
(594, 'Dominican College', 'National Capital Region (NCR)'),
(595, 'Dominican College of Iloilo', 'Region VI - Western Visayas'),
(596, 'Dominican College of Tarlac, Inc.', 'Region III - Central Luzon'),
(597, 'Dominican College Sta. Rosa', 'Region IV - CALABARZON'),
(598, 'Don Bosco College Canlubang', 'Region IV - CALABARZON'),
(599, 'Don Bosco Technical College', 'National Capital Region (NCR)'),
(600, 'Don Bosco Technology Center', 'Region VII - Central Visayas'),
(601, 'Don Carlo Cavina School, Inc', 'National Capital Region (NCR)'),
(602, 'Don Honorio Ventura Technological State University – Main', 'Region III - Central Luzon'),
(603, 'Don Jose Ecleo Memorial Foundation College Of Science And Technology', 'CARAGA'),
(604, 'Don Mariano Marcos Memorial State University – Mid La Union', 'Region I - Ilocos Region'),
(605, 'Don Mariano Marcos Memorial State University – North La Union – Main Campus', 'Region I - Ilocos Region'),
(606, 'Don Mariano Marcos Memorial State University – Open University', 'Region I - Ilocos Region'),
(607, 'Don Mariano Marcos Memorial State University – South La Union', 'Region I - Ilocos Region'),
(608, 'Dona Remedios Trinidad Romualdez Medical Foundation', 'Region VIII - Eastern Visayas'),
(609, 'Dr. Aurelio Mendoza Memorial College', 'Region IX - Zamboanga Peninsula'),
(610, 'Dr. Carlos S. Lanting College', 'National Capital Region (NCR)'),
(611, 'Dr. Carlos S. Lanting College-Dr. Ruby Lanting Casaul Educational Foundation, Inc.', 'Region V - Bicol Region'),
(612, 'Dr. Domingo B. Tamondong Memorial School, Inc.', 'Region XII - SOCCSKSARGEN'),
(613, 'Dr. Emilio B. Espinosa, Sr. Memorial State College of Agriculture and Technology', 'Region V - Bicol Region'),
(614, 'Dr. Francisco L. Calingasan Memorial Colleges Foundation', 'Region IV - CALABARZON'),
(615, 'Dr. Gloria D. Lacson Foundation Colleges of Cabanatuan City, Inc.', 'Region III - Central Luzon'),
(616, 'Dr. Gloria D. Lacson Foundation Colleges, Inc.', 'Region III - Central Luzon'),
(617, 'Dr. Jose Fabella Memorial Hospital School of Midwifery', 'National Capital Region (NCR)'),
(618, 'Dr. P. Ocampo Colleges, Inc.', 'Region XII - SOCCSKSARGEN'),
(619, 'Dr. Solomon U. Molina College, Inc.', 'Region X - Northern Mindanao'),
(620, 'Dr. Vicente Orestes Romualdez Educational Foundation', 'Region VIII - Eastern Visayas'),
(621, 'Dr. Yanga’s Colleges, Inc.', 'Region III - Central Luzon'),
(622, 'East Pacific Computer College', 'Region VIII - Eastern Visayas'),
(623, 'East Systems College', 'Region IV - CALABARZON'),
(624, 'Easter College, Inc.', 'Cordillera Administrative Region (CAR)'),
(625, 'Eastern Mindanao College of Technology', 'Region IX - Zamboanga Peninsula'),
(626, 'Eastern Mindoro College', 'Region IVB - MIMAROPA'),
(627, 'Eastern Quezon College', 'Region IV - CALABARZON'),
(628, 'Eastern Samar State University- Guiuan', 'Region VIII - Eastern Visayas'),
(629, 'Eastern Samar State University- Main', 'Region VIII - Eastern Visayas'),
(630, 'Eastern Samar State University- Salcedo', 'Region VIII - Eastern Visayas'),
(631, 'Eastern Samar State University-Can-Avid', 'Region VIII - Eastern Visayas'),
(632, 'Eastern Samar State University-Maydolong', 'Region VIII - Eastern Visayas'),
(633, 'Eastern Tayabas College', 'Region IV - CALABARZON'),
(634, 'Eastern Visayas Central Colleges, Inc.', 'Region VIII - Eastern Visayas'),
(635, 'Eastern Visayas State University- Burauen', 'Region VIII - Eastern Visayas'),
(636, 'Eastern Visayas State University Carigara', 'Region VIII - Eastern Visayas'),
(637, 'Eastern Visayas State University- Main', 'Region VIII - Eastern Visayas'),
(638, 'Eastern Visayas State University Tanauan', 'Region VIII - Eastern Visayas'),
(639, 'Eastern Visayas State University-Ormoc', 'Region VIII - Eastern Visayas'),
(640, 'Eastwoods Professional College of Science And Technology, Inc.', 'Region III - Central Luzon'),
(641, 'Ebenezer Bible College and Seminary', 'Region IX - Zamboanga Peninsula'),
(642, 'Ebenezer International Colleges', 'Region IV - CALABARZON'),
(643, 'Eclaro Academy', 'National Capital Region (NCR)'),
(644, 'Ecumenical Christian College, Inc.', 'Region III - Central Luzon'),
(645, 'Edenton Mission College, Inc', 'Region XII - SOCCSKSARGEN'),
(646, 'Educational Systems Technological Institute', 'Region IVB - MIMAROPA'),
(647, 'Electron College of Technical Education', 'National Capital Region (NCR)'),
(648, 'Elijah International World Mission Institute', 'Region IV - CALABARZON'),
(649, 'Elisa R. Ochoa Memorial Northern Mindanao School Of Midwifery', 'CARAGA'),
(650, 'EMA EMITS College Philippines', 'Region IVB - MIMAROPA'),
(651, 'Emilio Aguinaldo College', 'National Capital Region (NCR)'),
(652, 'Emilio Aguinaldo College (Yaman ng Lahi Foundation)', 'Region IV - CALABARZON'),
(653, 'Emmanuel System College of Bulacan, Inc.', 'Region III - Central Luzon'),
(654, 'Enderun College', 'National Capital Region (NCR)'),
(655, 'Entrepreneurs School of Asia', 'National Capital Region (NCR)'),
(656, 'Erhard Science and Technological Institute – Oriental Mindoro, Inc.', 'Region IVB - MIMAROPA'),
(657, 'Erhard Science College-Bulacan, Inc.', 'Region III - Central Luzon'),
(658, 'Erhard Systems Technological Institute', 'Region IVB - MIMAROPA'),
(659, 'Estenias Science Foundation School', 'Region V - Bicol Region'),
(660, 'Eulogio “Amang” Rodriguez Institute of Science and Technology', 'National Capital Region (NCR)'),
(661, 'Evangelical Mission College ,Inc.', 'Region XI - Davao Region'),
(662, 'Evangelical Theological College of the Philippines', 'Region VII - Central Visayas'),
(663, 'Evangelical Theological College of the Philippines Luzon', 'Region IV - CALABARZON'),
(664, 'Eveland Christian College', 'Region II - Cagayan Valley'),
(665, 'Evelyn E. Fabie College, Inc.', 'Region XI - Davao Region'),
(666, 'Exact Colleges of Asia, Inc.', 'Region III - Central Luzon'),
(667, 'Far East Asia Pacific Institute of Tourism Science', 'Region IV - CALABARZON'),
(668, 'Far East Institute of Arts & Sciences Foundation', 'National Capital Region (NCR)'),
(669, 'Far Eastern College Silang', 'Region IV - CALABARZON');
INSERT INTO `school` (`school_id`, `name`, `description`) VALUES
(670, 'Far Eastern Polytechnic College', 'Region IV - CALABARZON'),
(671, 'Far Eastern University', 'National Capital Region (NCR)'),
(672, 'Far Eastern University, Makati', 'National Capital Region (NCR)'),
(673, 'Father Saturnino Urios University', 'CARAGA'),
(674, 'Fatima College of Camiguin', 'Region X - Northern Mindanao'),
(675, 'Fatima School of Science and Technology', 'Region V - Bicol Region'),
(676, 'FEATI University', 'National Capital Region (NCR)'),
(677, 'Febias College of Bible', 'National Capital Region (NCR)'),
(678, 'Felipe R. Verallo Memorial Foundation-Bogo', 'Region VII - Central Visayas'),
(679, 'Fellowship Baptist College', 'Negros Island Region (NIR)'),
(680, 'Fernandez College of Arts & Technology', 'Region III - Central Luzon'),
(681, 'FEU Institute of Technology', 'National Capital Region (NCR)'),
(682, 'FEU-Dr. Nicanor Reyes Medical Foundation', 'National Capital Region (NCR)'),
(683, 'FEU-FERN College', 'National Capital Region (NCR)'),
(684, 'Filamer Christian University', 'Region VI - Western Visayas'),
(685, 'First Asia Institute of Technology and Humanities', 'Region IV - CALABARZON'),
(686, 'First City Providential College, Inc.', 'Region III - Central Luzon'),
(687, 'FL Vargas College-Tuguegarao City', 'Region II - Cagayan Valley'),
(688, 'Flight School International', 'National Capital Region (NCR)'),
(689, 'Forbes Colleges', 'Region V - Bicol Region'),
(690, 'Foundation University', 'Region VII - Central Visayas'),
(691, 'Franciscan College Of The Immaculate Conception', 'Region VIII - Eastern Visayas'),
(692, 'Fullbright College', 'Region IVB - MIMAROPA'),
(693, 'Fundamental Baptist College For Asians, Inc.', 'Region III - Central Luzon'),
(694, 'Fuzeko Polytechnic College', 'Region II - Cagayan Valley'),
(695, 'Gabriel Taborin College of Davao Foundation', 'Region XI - Davao Region'),
(696, 'Garcia College of Technology', 'Region VI - Western Visayas'),
(697, 'Gardner College Diliman Inc.', 'National Capital Region (NCR)'),
(698, 'General Baptist Bible College', 'Region XI - Davao Region'),
(699, 'General De Jesus College', 'Region III - Central Luzon'),
(700, 'General Santos Academy, Inc.', 'Region XII - SOCCSKSARGEN'),
(701, 'General Santos Doctors’ Medical School Foundation, Inc.', 'Region XII - SOCCSKSARGEN'),
(702, 'Gensan College of Technology', 'Region XII - SOCCSKSARGEN'),
(703, 'Gensantos Foundation, Inc.', 'Region XII - SOCCSKSARGEN'),
(704, 'Gerona Junior Colege, Inc.', 'Region III - Central Luzon'),
(705, 'Gingoog Christian College', 'Region X - Northern Mindanao'),
(706, 'Gingoog City Colleges', 'Region X - Northern Mindanao'),
(707, 'Global Academy of Technology and Entrepreneurship, Inc. (Formerly STI College Santiago, Inc.)', 'Region II - Cagayan Valley'),
(708, 'Global City Innovative College', 'National Capital Region (NCR)'),
(709, 'Global Reciprocal Colleges', 'National Capital Region (NCR)'),
(710, 'Global School For Technological Studies, Inc.', 'Region VIII - Eastern Visayas'),
(711, 'Golden Gate Colleges', 'Region IV - CALABARZON'),
(712, 'Golden Heritage Polytechnic College', 'Region X - Northern Mindanao'),
(713, 'Golden Link College Foundation, Inc.', 'National Capital Region (NCR)'),
(714, 'Golden Success College', 'Region VII - Central Visayas'),
(715, 'Golden Valley Colleges, Inc.', 'Region III - Central Luzon'),
(716, 'Golden West Colleges', 'Region I - Ilocos Region'),
(717, 'Goldenstate College', 'Region XII - SOCCSKSARGEN'),
(718, 'Goldenstate College of Koronadal City', 'Region XII - SOCCSKSARGEN'),
(719, 'Governor Andres Pascual College', 'National Capital Region (NCR)'),
(720, 'Grace Christian College', 'National Capital Region (NCR)'),
(721, 'Grace Mission College', 'Region IVB - MIMAROPA'),
(722, 'Granby College of Science & Technology', 'Region IV - CALABARZON'),
(723, 'Green Valley College Foundation, Inc.', 'Region XII - SOCCSKSARGEN'),
(724, 'Greenville College', 'National Capital Region (NCR)'),
(725, 'GSEF College', 'Region VI - Western Visayas'),
(726, 'Guagua National Colleges', 'Region III - Central Luzon'),
(727, 'Guang Ming College', 'National Capital Region (NCR)'),
(728, 'Guardian Bona Fide for Hope Foundation', 'Region IV - CALABARZON'),
(729, 'Guimaras State College, Baterna Campus', 'Region VI - Western Visayas'),
(730, 'Guimaras State College, Main Campus', 'Region VI - Western Visayas'),
(731, 'Guimaras State College, Mosqueda Campus', 'Region VI - Western Visayas'),
(732, 'Guinayangan College Fdtn.', 'Region IV - CALABARZON'),
(733, 'Guzman College of Science and Technology', 'National Capital Region (NCR)'),
(734, 'Harris Memorial College', 'Region IV - CALABARZON'),
(735, 'Headstart College of Cotabato', 'Region XII - SOCCSKSARGEN'),
(736, 'HERCOR College', 'Region VI - Western Visayas'),
(737, 'HG Baquiran College Tumauini, Isabela', 'Region II - Cagayan Valley'),
(738, 'HMIJ – Foundation Philippine Islamic College', 'Region IX - Zamboanga Peninsula'),
(739, 'Holy Angel University, Inc.', 'Region III - Central Luzon'),
(740, 'Holy Child College of Davao', 'Region XI - Davao Region'),
(741, 'Holy Child College of Information Technology, Inc.', 'Region XII - SOCCSKSARGEN'),
(742, 'Holy Child College of Information Technology, Inc. – Koronadal', 'Region XII - SOCCSKSARGEN'),
(743, 'Holy Child Colleges Of Butuan', 'CARAGA'),
(744, 'Holy Child Jesus College', 'Region IV - CALABARZON'),
(745, 'Holy Cross College of Calinan', 'Region XI - Davao Region'),
(746, 'Holy Cross College Of Carigara', 'Region VIII - Eastern Visayas'),
(747, 'Holy Cross College of Sasa', 'Region XI - Davao Region'),
(748, 'Holy Cross College-Nueva Ecija', 'Region III - Central Luzon'),
(749, 'Holy Cross College-Pampanga', 'Region III - Central Luzon'),
(750, 'Holy Cross of Davao College', 'Region XI - Davao Region'),
(751, 'Holy Infant College', 'Region VIII - Eastern Visayas'),
(752, 'Holy Name University', 'Region VII - Central Visayas'),
(753, 'Holy Rosary College Foundation', 'National Capital Region (NCR)'),
(754, 'Holy Rosary Colleges Foundation of Calaba', 'Region III - Central Luzon'),
(755, 'Holy Spirit College Foundation In Leyte, Inc', 'Region VIII - Eastern Visayas'),
(756, 'Holy Trinity College - Bato', 'Region V - Bicol Region'),
(757, 'Holy Trinity College - Ginatilan', 'Region VII - Central Visayas'),
(758, 'Holy Trinity College of General Santos City', 'Region XII - SOCCSKSARGEN'),
(759, 'Holy Trinity University', 'Region IVB - MIMAROPA'),
(760, 'Holy Virgin Of Salvacion Foundation College, Inc.', 'Region VIII - Eastern Visayas'),
(761, 'Hua Siong College of Iloilo', 'Region VI - Western Visayas'),
(762, 'Hyrons College Philippines', 'Region IX - Zamboanga Peninsula'),
(763, 'I Link College of Science and Technology', 'Region XII - SOCCSKSARGEN'),
(764, 'IBA College of Mindanao', 'Region X - Northern Mindanao'),
(765, 'ICCT Colleges', 'Region IV - CALABARZON'),
(766, 'ICCT Colleges Fdtn. Antipolo', 'Region IV - CALABARZON'),
(767, 'ICCT Colleges Fdtn. Binangonan', 'Region IV - CALABARZON'),
(768, 'ICCT Colleges Fdtn. San Mateo', 'Region IV - CALABARZON'),
(769, 'ICCT Colleges Fdtn. Taytay', 'Region IV - CALABARZON'),
(770, 'ICI Global University', 'National Capital Region (NCR)'),
(771, 'ICT-ED Institute of Science and Technology', 'Region IV - CALABARZON'),
(772, 'ICTI Polytecnic College', 'Region X - Northern Mindanao'),
(773, 'IETI College of Science and Technology', 'Region IV - CALABARZON'),
(774, 'IETI College of Science and Technology-Marikina', 'National Capital Region (NCR)'),
(775, 'Ifugao State University-Aguinaldo', 'Cordillera Administrative Region (CAR)'),
(776, 'Ifugao State University-Hapao', 'Cordillera Administrative Region (CAR)'),
(777, 'Ifugao State University-Lagawe', 'Cordillera Administrative Region (CAR)'),
(778, 'Ifugao State University-Main', 'Cordillera Administrative Region (CAR)'),
(779, 'Ifugao State University-Potia', 'Cordillera Administrative Region (CAR)'),
(780, 'Ifugao State University-Tinoc', 'Cordillera Administrative Region (CAR)'),
(781, 'IKON College Calamba', 'Region IV - CALABARZON'),
(782, 'Iligan Capitol College', 'Region X - Northern Mindanao'),
(783, 'Iligan Medical Center College', 'Region X - Northern Mindanao'),
(784, 'Ilocos Sur Polytechnic State College', 'Region I - Ilocos Region'),
(785, 'Ilocos Sur Polytechnic State College-Candon', 'Region I - Ilocos Region'),
(786, 'Ilocos Sur Polytechnic State College-Cervantes', 'Region I - Ilocos Region'),
(787, 'Ilocos Sur Polytechnic State College-College of Arts and Sciences-Tagudin', 'Region I - Ilocos Region'),
(788, 'Ilocos Sur Polytechnic State College-College of Fisheries and Marine Sciences – Narvacan', 'Region I - Ilocos Region'),
(789, 'Ilocos Sur Polytechnic State College-Santiago', 'Region I - Ilocos Region'),
(790, 'Iloilo Doctor’s College', 'Region VI - Western Visayas'),
(791, 'Iloilo Doctor’s College of Medicine, Inc.', 'Region VI - Western Visayas'),
(792, 'Iloilo Science And Technology University – Barotac Nuevo Campus', 'Region VI - Western Visayas'),
(793, 'Iloilo Science And Technology University – Dumangas Campus', 'Region VI - Western Visayas'),
(794, 'Iloilo Science And Technology University – Leon Campus', 'Region VI - Western Visayas'),
(795, 'Iloilo Science And Technology University – Main Campus', 'Region VI - Western Visayas'),
(796, 'Iloilo Science And Technology University – Miagao Campus', 'Region VI - Western Visayas'),
(797, 'Iloilo State College of Fisheries, Barotac Nuevo Campus', 'Region VI - Western Visayas'),
(798, 'Iloilo State College of Fisheries, Dingle Campus', 'Region VI - Western Visayas'),
(799, 'Iloilo State College of Fisheries, Dumangas Campus', 'Region VI - Western Visayas'),
(800, 'Iloilo State College of Fisheries, Main Campus', 'Region VI - Western Visayas'),
(801, 'Iloilo State College of Fisheries, San Enrique Campus', 'Region VI - Western Visayas'),
(802, 'Immaculada Concepcion College', 'National Capital Region (NCR)'),
(803, 'Immaculate Conception Archdiocesan School', 'Region IX - Zamboanga Peninsula'),
(804, 'Immaculate Conception College Balayan', 'Region IV - CALABARZON'),
(805, 'Immaculate Conception College-Albay', 'Region V - Bicol Region'),
(806, 'Immaculate Conception I-College of Arts And Technology, Inc.', 'Region III - Central Luzon'),
(807, 'Immaculate Conception Major Seminary', 'Region III - Central Luzon'),
(808, 'Immaculate Heart of Mary College, Parañaque', 'National Capital Region (NCR)'),
(809, 'Immaculate Heart of Mary Seminary', 'Region VII - Central Visayas'),
(810, 'Immanuel Bible College', 'Region VII - Central Visayas'),
(811, 'Immanuel Theological Seminary', 'Region IV - CALABARZON'),
(812, 'Imus Institute of Science and Technology Inc', 'Region IV - CALABARZON'),
(813, 'Indiana School of Aeronautics', 'Region VII - Central Visayas'),
(814, 'Infant Jesus Montessori School (College Dept), Santiago City, Isabela', 'Region II - Cagayan Valley'),
(815, 'Informatics College Caloocan, Inc.', 'National Capital Region (NCR)'),
(816, 'Informatics College Cebu', 'Region VII - Central Visayas'),
(817, 'Informatics College Eastwood, Inc.', 'National Capital Region (NCR)'),
(818, 'Informatics College Northgate, Inc.', 'National Capital Region (NCR)'),
(819, 'Informatics College-Manila', 'National Capital Region (NCR)'),
(820, 'Informatics International College Rizal', 'Region IV - CALABARZON'),
(821, 'Information and Communications Technology Academy', 'National Capital Region (NCR)'),
(822, 'Infotech College of Arts and Sciences Philippines, Inc.', 'National Capital Region (NCR)'),
(823, 'Infotech Development Systems Colleges, Inc.', 'Region V - Bicol Region'),
(824, 'Innovative College of Science and Technology', 'Region IVB - MIMAROPA'),
(825, 'Institute of Business Science & Medical Arts', 'Region IVB - MIMAROPA'),
(826, 'Institute of Community and Family Health', 'National Capital Region (NCR)'),
(827, 'Institute of Formation and Religious Studies', 'National Capital Region (NCR)'),
(828, 'Institute of International Culinary and Hospitality Entrepreneurship', 'Region XI - Davao Region'),
(829, 'Integrated Innovation and Hospitality Colleges, Inc.', 'National Capital Region (NCR)'),
(830, 'Integrated Midwives Association of the Philippines Foundation School of Midwifery, Inc.', 'Region VI - Western Visayas'),
(831, 'Intercity College of Science And Technology', 'Region XI - Davao Region'),
(832, 'Interface Computer College-Caloocan', 'National Capital Region (NCR)'),
(833, 'Interface Computer College-Davao', 'Region XI - Davao Region'),
(834, 'Interface Computer College-Iloilo', 'Region VI - Western Visayas'),
(835, 'Interface Computer College-Manila', 'National Capital Region (NCR)'),
(836, 'Inter-Global College Foundation', 'Region IV - CALABARZON'),
(837, 'International Baptist College', 'National Capital Region (NCR)'),
(838, 'International Christian College of Manila', 'Region IV - CALABARZON'),
(839, 'International Colleges for Excellence', 'Region I - Ilocos Region'),
(840, 'International Cruise Ship College, Inc.', 'Region XII - SOCCSKSARGEN'),
(841, 'International Graduate School of Leadership', 'National Capital Region (NCR)'),
(842, 'International Peace Leadership College', 'Region IV - CALABARZON'),
(843, 'International School of Asia and the Pacific', 'National Capital Region (NCR)'),
(844, 'International School of Asia and the Pacific-Tabuk', 'Cordillera Administrative Region (CAR)'),
(845, 'International School of Asia and the Pacific-Tuguegarao', 'Region II - Cagayan Valley'),
(846, 'Interworld College of Science And Technology – Tarlac City', 'Region III - Central Luzon'),
(847, 'Interworld Colleges Foundation, Inc. – Paniqui Tarlac', 'Region III - Central Luzon'),
(848, 'Isabela College of Arts and Technology', 'Region II - Cagayan Valley'),
(849, 'Isabela Colleges', 'Region II - Cagayan Valley'),
(850, 'Isabela State University, Angadanan Campus', 'Region II - Cagayan Valley'),
(851, 'Isabela State University, Cabagan Campus', 'Region II - Cagayan Valley'),
(852, 'Isabela State University, Cauayan Campus', 'Region II - Cagayan Valley'),
(853, 'Isabela State University-Ilagan Campus', 'Region II - Cagayan Valley'),
(854, 'Isabela State University-Jones Campus', 'Region II - Cagayan Valley'),
(855, 'Isabela State University-Main Campus, Echague, Isabela', 'Region II - Cagayan Valley'),
(856, 'Isabela State University-Palanan Campus', 'Region II - Cagayan Valley'),
(857, 'Isabela State University-Roxas Campus', 'Region II - Cagayan Valley'),
(858, 'Isabela State University-San Mariano Campus', 'Region II - Cagayan Valley'),
(859, 'Isabela State University-San Mateo Campus', 'Region II - Cagayan Valley'),
(860, 'ISHRM School System Bacoor', 'Region IV - CALABARZON'),
(861, 'ISHRM School System Dasmarinas', 'Region IV - CALABARZON'),
(862, 'Je Mondejar Computer College', 'Region VIII - Eastern Visayas'),
(863, 'Jesus Is Lord College Foundation, Inc.', 'Region III - Central Luzon'),
(864, 'Jesus Reigns Christian College', 'Region IV - CALABARZON'),
(865, 'Jesus Reigns Christian College Foundation, Inc.', 'National Capital Region (NCR)'),
(866, 'Jesus the Loving Shepherd Christian College', 'Region V - Bicol Region'),
(867, 'JH Cerilles State College – Bayog', 'Region IX - Zamboanga Peninsula'),
(868, 'JH Cerilles State College – Canuto MS Enerio', 'Region IX - Zamboanga Peninsula'),
(869, 'JH Cerilles State College – Dimataling', 'Region IX - Zamboanga Peninsula'),
(870, 'JH Cerilles State College – Dumingag', 'Region IX - Zamboanga Peninsula'),
(871, 'JH Cerilles State College – Guipos', 'Region IX - Zamboanga Peninsula'),
(872, 'JH Cerilles State College – Josefina', 'Region IX - Zamboanga Peninsula'),
(873, 'JH Cerilles State College – Kumalarang', 'Region IX - Zamboanga Peninsula'),
(874, 'JH Cerilles State College – Lapuyan', 'Region IX - Zamboanga Peninsula'),
(875, 'JH Cerilles State College – Mahayag', 'Region IX - Zamboanga Peninsula'),
(876, 'JH Cerilles State College – Main', 'Region IX - Zamboanga Peninsula'),
(877, 'JH Cerilles State College – Margosatubig', 'Region IX - Zamboanga Peninsula'),
(878, 'JH Cerilles State College – Midsalip', 'Region IX - Zamboanga Peninsula'),
(879, 'JH Cerilles State College – Pagadian', 'Region IX - Zamboanga Peninsula'),
(880, 'JH Cerilles State College – Ramon Magsaysay', 'Region IX - Zamboanga Peninsula'),
(881, 'JH Cerilles State College – San Pablo', 'Region IX - Zamboanga Peninsula'),
(882, 'JH Cerilles State College – Sominot', 'Region IX - Zamboanga Peninsula'),
(883, 'JH Cerilles State College – Tabina', 'Region IX - Zamboanga Peninsula'),
(884, 'JH Cerilles State College – Tambulig', 'Region IX - Zamboanga Peninsula'),
(885, 'JH Cerilles State College – Tigbao', 'Region IX - Zamboanga Peninsula'),
(886, 'JH Cerilles State College – Tukuran', 'Region IX - Zamboanga Peninsula'),
(887, 'JH Cerilles State College – Vincenzo Sagun', 'Region IX - Zamboanga Peninsula'),
(888, 'Jocson College, Inc.', 'Region III - Central Luzon'),
(889, 'John B. Lacson Colleges Foundation (Bacolod)', 'Negros Island Region (NIR)'),
(890, 'John B. Lacson Foundation Maritime University-Arevalo, Inc.', 'Region VI - Western Visayas'),
(891, 'John B. Lacson Foundation Maritime University-Molo, Inc.', 'Region VI - Western Visayas'),
(892, 'John Paul College', 'Region IVB - MIMAROPA'),
(893, 'John Paul II College of Davao', 'Region XI - Davao Region'),
(894, 'John Wesley College', 'Region II - Cagayan Valley'),
(895, 'Joji Ilagan Career Center Foundation', 'Region XI - Davao Region'),
(896, 'Joji Ilagan International Management School', 'Region XI - Davao Region'),
(897, 'Joji Ilagan International School of Hotel & Tourism Management', 'Region XII - SOCCSKSARGEN'),
(898, 'Jose C. Feliciano College Foundation, Inc.', 'Region III - Central Luzon'),
(899, 'Jose Maria College', 'Region XI - Davao Region'),
(900, 'Jose Navarro Polytechnic College', 'Region VIII - Eastern Visayas'),
(901, 'Jose Rizal Memorial State University Dipolog Campus', 'Region IX - Zamboanga Peninsula'),
(902, 'Jose Rizal Memorial State University- Katipunan Campus', 'Region IX - Zamboanga Peninsula'),
(903, 'Jose Rizal Memorial State University Main', 'Region IX - Zamboanga Peninsula'),
(904, 'Jose Rizal Memorial State University- Siocon Campus', 'Region IX - Zamboanga Peninsula'),
(905, 'Jose Rizal Memorial State University Tampilisan', 'Region IX - Zamboanga Peninsula'),
(906, 'Jose Rizal Memorial State University-Sibuco External Studies Unit', 'Region IX - Zamboanga Peninsula'),
(907, 'Jose Rizal University', 'National Capital Region (NCR)'),
(908, 'Juan S. Alano Memorial School', 'Region IX - Zamboanga Peninsula'),
(909, 'Kabankalan Catholic College', 'Negros Island Region (NIR)'),
(910, 'Kalayaan College, Inc.', 'National Capital Region (NCR)'),
(911, 'Kalinga Apayao State College-Dagupan', 'Cordillera Administrative Region (CAR)'),
(912, 'Kalinga Apayao State College-Main', 'Cordillera Administrative Region (CAR)'),
(913, 'Kalinga Apayao State College-Rizal', 'Cordillera Administrative Region (CAR)'),
(914, 'Kalinga Colleges of Science and Technology', 'Cordillera Administrative Region (CAR)'),
(915, 'Keystone College, Inc.', 'Cordillera Administrative Region (CAR)'),
(916, 'Kidapawan Doctors College, Inc.', 'Region XII - SOCCSKSARGEN'),
(917, 'King’s College of Isulan', 'Region XII - SOCCSKSARGEN'),
(918, 'King’s College of Marbel, Inc', 'Region XII - SOCCSKSARGEN'),
(919, 'King’s College of the Philippines', 'Cordillera Administrative Region (CAR)'),
(920, 'King’s College of the Philippines – Bambang', 'Region II - Cagayan Valley'),
(921, 'Kingfisher School of Business and Finance', 'Region I - Ilocos Region'),
(922, 'Korbel Foundation College, Inc.', 'Region XII - SOCCSKSARGEN'),
(923, 'Kulaman Academy, Inc.', 'Region XII - SOCCSKSARGEN'),
(924, 'Kurios Christian College Foundation', 'Region IV - CALABARZON'),
(925, 'Kutawato Darussalam College, Inc.', 'Region XII - SOCCSKSARGEN'),
(926, 'La Concepcion College', 'Region III - Central Luzon'),
(927, 'La Consolacion College – Bais', 'Region VII - Central Visayas'),
(928, 'La Consolacion College Binan', 'Region IV - CALABARZON'),
(929, 'La Consolacion College Manila', 'National Capital Region (NCR)'),
(930, 'La Consolacion College Tanauan', 'Region IV - CALABARZON'),
(931, 'La Consolacion College, Caloocan', 'National Capital Region (NCR)'),
(932, 'La Consolacion College, Isabela', 'Negros Island Region (NIR)'),
(933, 'La Consolacion College, Novaliches', 'National Capital Region (NCR)'),
(934, 'La Consolacion College, Pasig', 'National Capital Region (NCR)'),
(935, 'La Consolacion College, Valenzuela', 'National Capital Region (NCR)'),
(936, 'La Consolacion College-Bacolod', 'Negros Island Region (NIR)'),
(937, 'La Consolacion College-Daet', 'Region V - Bicol Region'),
(938, 'La Consolacion College-Iriga City', 'Region V - Bicol Region'),
(939, 'La Consolacion University Philippines (Formerly University of Regina Carmeli)', 'Region III - Central Luzon'),
(940, 'La Consolascion College – Lilo-an', 'Region VII - Central Visayas'),
(941, 'La Finn’s Scholastica', 'Region I - Ilocos Region'),
(942, 'La Fortuna College', 'Region III - Central Luzon'),
(943, 'La Salette of Roxas College Inc.', 'Region II - Cagayan Valley'),
(944, 'La Salle College Antipolo', 'Region IV - CALABARZON'),
(945, 'La Salle University', 'Region X - Northern Mindanao'),
(946, 'La Union Christian Comprehensive College', 'Region I - Ilocos Region'),
(947, 'La Union College of Science and Technology', 'Region I - Ilocos Region'),
(948, 'La Verdad Christian College', 'National Capital Region (NCR)'),
(949, 'La Verdad Christian College, Inc.', 'Region III - Central Luzon'),
(950, 'Laak Institute Foundation', 'Region XI - Davao Region'),
(951, 'Lady of Lourdes Hospital & Colleges of Caybiga, Inc.', 'National Capital Region (NCR)'),
(952, 'Laguna College', 'Region IV - CALABARZON'),
(953, 'Laguna College of Business and Arts', 'Region IV - CALABARZON'),
(954, 'Laguna Maritime, Arts & Business College', 'Region IV - CALABARZON'),
(955, 'Laguna Northwestern College', 'Region IV - CALABARZON'),
(956, 'Laguna Northwestern College SLRMC', 'Region IV - CALABARZON'),
(957, 'Laguna Science and Technology College', 'Region IV - CALABARZON'),
(958, 'Laguna State Polytechnic University Laguna College of Arts and Trades Sta. Cruz', 'Region IV - CALABARZON'),
(959, 'Laguna State Polytechnic University Los Banos College of Fisheries', 'Region IV - CALABARZON'),
(960, 'Laguna State Polytechnic University San Pablo City', 'Region IV - CALABARZON'),
(961, 'Laguna State Polytechnic University Siniloan', 'Region IV - CALABARZON'),
(962, 'Lanao School of Science and Technology Inc.', 'Region X - Northern Mindanao'),
(963, 'Larmen de Guia Memorial College', 'Region VII - Central Visayas'),
(964, 'Lebak Family Doctor’s School of Midwifery', 'Region XII - SOCCSKSARGEN'),
(965, 'Legacy College of Compostela', 'Region XI - Davao Region'),
(966, 'Lemery Colleges', 'Region IV - CALABARZON'),
(967, 'Leon Guinto Memorial College', 'Region IV - CALABARZON'),
(968, 'Leyte Colleges', 'Region VIII - Eastern Visayas'),
(969, 'Leyte Normal University', 'Region VIII - Eastern Visayas'),
(970, 'Leyte School Of Professionals', 'Region VIII - Eastern Visayas'),
(971, 'Liceo de Cagayan University', 'Region X - Northern Mindanao'),
(972, 'Liceo De Davao', 'Region XI - Davao Region'),
(973, 'Liceo De Masbate', 'Region V - Bicol Region'),
(974, 'Liceo De Pulilan Colleges, Inc.', 'Region III - Central Luzon'),
(975, 'Liceo de San Jacinto Foundation', 'Region V - Bicol Region'),
(976, 'Lipa City Colleges', 'Region IV - CALABARZON'),
(977, 'LNC Corinthian Center', 'Region IV - CALABARZON'),
(978, 'Loral Douglas Woosley Bethany Colleges', 'National Capital Region (NCR)'),
(979, 'Lorma Colleges', 'Region I - Ilocos Region'),
(980, 'Lourdes College', 'Region X - Northern Mindanao'),
(981, 'Lourdes College of Bulacan, Inc.', 'Region III - Central Luzon'),
(982, 'Lourdes College Of Pampanga, Inc.', 'Region III - Central Luzon'),
(983, 'Loyola College of Culion', 'Region IVB - MIMAROPA'),
(984, 'Lucan Central Colleges, Inc.', 'Region IX - Zamboanga Peninsula'),
(985, 'Luna Colleges', 'Region I - Ilocos Region'),
(986, 'Luna Goco Colleges', 'Region IVB - MIMAROPA'),
(987, 'Luzon College of Science and Technology (Urdaneta)', 'Region I - Ilocos Region'),
(988, 'Lyceum Northern Luzon', 'Region I - Ilocos Region'),
(989, 'Lyceum Northwestern University', 'Region I - Ilocos Region'),
(990, 'Lyceum Northwestern University-Urdaneta Campus', 'Region I - Ilocos Region'),
(991, 'Lyceum of Alabang', 'National Capital Region (NCR)'),
(992, 'Lyceum of Aparri', 'Region II - Cagayan Valley'),
(993, 'Lyceum of Cebu', 'Region VII - Central Visayas'),
(994, 'Lyceum of Iligan Foundation', 'Region X - Northern Mindanao'),
(995, 'Lyceum of Subic Bay, Inc.', 'Region III - Central Luzon'),
(996, 'Lyceum of the East-Aurora, Inc.', 'Region III - Central Luzon'),
(997, 'Lyceum of the Philippines Cavite', 'Region IV - CALABARZON'),
(998, 'Lyceum of the Philippines Laguna', 'Region IV - CALABARZON'),
(999, 'Lyceum of the Philippines St. Cabrini College of Allied Medicine', 'Region IV - CALABARZON'),
(1000, 'Lyceum of the Philippines University', 'National Capital Region (NCR)'),
(1001, 'Lyceum of the Philippines University Batangas', 'Region IV - CALABARZON'),
(1002, 'Lyceum of Tuao', 'Region II - Cagayan Valley'),
(1003, 'M. V. Gallego Foundation Colleges', 'Region III - Central Luzon'),
(1004, 'Mabini College of Batangas', 'Region IV - CALABARZON'),
(1005, 'Mabini Colleges', 'Region V - Bicol Region'),
(1006, 'Macro Colleges Inc.', 'Region I - Ilocos Region'),
(1007, 'Magsaysay Memorial College of Zambales, Inc.', 'Region III - Central Luzon'),
(1008, 'Maila Rosario Colleges, Tuguegarao City, Cagayan', 'Region II - Cagayan Valley'),
(1009, 'Makati Medical Center College, Inc.', 'National Capital Region (NCR)'),
(1010, 'Makati Science Technological Institute of the Philippines', 'National Capital Region (NCR)'),
(1011, 'Makilala Institute of Science and Technology', 'Region XII - SOCCSKSARGEN'),
(1012, 'Malasiqui Agno Valley College', 'Region I - Ilocos Region'),
(1013, 'Malayan Colleges of Laguna', 'Region IV - CALABARZON'),
(1014, 'Mallig Plains Colleges', 'Region II - Cagayan Valley'),
(1015, 'Manila Adventist College', 'National Capital Region (NCR)'),
(1016, 'Manila Business College Foundation', 'National Capital Region (NCR)'),
(1017, 'Manila Central University', 'National Capital Region (NCR)'),
(1018, 'Manila Theological College', 'National Capital Region (NCR)'),
(1019, 'Manila Tytana Colleges', 'National Capital Region (NCR)'),
(1020, 'Manuel S. Enverga Institute Foundation San Antonio', 'Region IV - CALABARZON'),
(1021, 'Manuel S. Enverga University Foundation Candelaria', 'Region IV - CALABARZON'),
(1022, 'Manuel S. Enverga University Foundation Catanauan', 'Region IV - CALABARZON'),
(1023, 'Manuel S. Enverga University Foundation Lucena', 'Region IV - CALABARZON'),
(1024, 'Mapua Institute of Technology, Makati', 'National Capital Region (NCR)'),
(1025, 'Mapua Institute of Technology, Manila', 'National Capital Region (NCR)'),
(1026, 'Marasigan Institute of Science and Technology', 'Region IV - CALABARZON'),
(1027, 'Marbel Institute of Technical College, Inc.', 'Region XII - SOCCSKSARGEN'),
(1028, 'Marbel School of Science and Technology', 'Region XII - SOCCSKSARGEN'),
(1029, 'Marcelino Fule Memorial College', 'Region IV - CALABARZON'),
(1030, 'Maria Assumpta Seminary', 'Region III - Central Luzon'),
(1031, 'Marian College', 'Region IX - Zamboanga Peninsula'),
(1032, 'Marian College of Baliuag, Inc.', 'Region III - Central Luzon'),
(1033, 'Mariano Marcos State University', 'Region I - Ilocos Region'),
(1034, 'Mariano Marcos State University – Currimao Ilocos Norte', 'Region I - Ilocos Region'),
(1035, 'Mariano Marcos State University-College of Agriculture and Forestry-Dingras', 'Region I - Ilocos Region'),
(1036, 'Mariano Marcos State University-College of Industrial Technology-Laoag City', 'Region I - Ilocos Region'),
(1037, 'Mariano Marcos State University-College of Teacher Education-Laoag City', 'Region I - Ilocos Region'),
(1038, 'Marianum College', 'National Capital Region (NCR)'),
(1039, 'Marie-Bernarde College', 'National Capital Region (NCR)'),
(1040, 'Marinduque Midwest College', 'Region IVB - MIMAROPA'),
(1041, 'Marinduque State College – Gasan', 'Region IVB - MIMAROPA'),
(1042, 'Marinduque State College – Main Campus', 'Region IVB - MIMAROPA'),
(1043, 'Marinduque State College – Sta. Cruz', 'Region IVB - MIMAROPA'),
(1044, 'Marinduque State College – Torrijos', 'Region IVB - MIMAROPA'),
(1045, 'Mariners’ Polytechnic Colleges Foundation of Canaman, Inc.', 'Region V - Bicol Region'),
(1046, 'Mariners’ Polytechnic Colleges Foundation of Legazpi City, Inc.', 'Region V - Bicol Region'),
(1047, 'Mariners’ Polytechnic Colleges-Panganiban', 'Region V - Bicol Region'),
(1048, 'Maritime Academy of Asia And the Pacific', 'Region III - Central Luzon'),
(1049, 'Martinez Memorial Colleges', 'National Capital Region (NCR)'),
(1050, 'Marvelous College of Technology', 'Region XII - SOCCSKSARGEN'),
(1051, 'Mary Chiles College', 'National Capital Region (NCR)'),
(1052, 'Mary Help of Christian College, Salesian Sisters', 'Region IV - CALABARZON'),
(1053, 'Mary Help of Christians College Seminary', 'Region I - Ilocos Region'),
(1054, 'Mary Our Help Technical Institute for Women', 'Region VII - Central Visayas'),
(1055, 'Mary the Queen College (Pampanga), Inc.', 'Region III - Central Luzon'),
(1056, 'Mary the Queen College of Quezon City Inc.', 'National Capital Region (NCR)'),
(1057, 'Mary’s Children Formation College', 'Region VII - Central Visayas'),
(1058, 'Maryhill College', 'Region IV - CALABARZON'),
(1059, 'Masbate Colleges', 'Region V - Bicol Region'),
(1060, 'Masters Technological Institute of Mindanao', 'Region X - Northern Mindanao'),
(1061, 'Mater Dei College', 'Region VII - Central Visayas'),
(1062, 'Mater Dei College of the Marian Missionaries of the Holy Cross', 'Negros Island Region (NIR)'),
(1063, 'Mater Divinae Gratiae College', 'Region VIII - Eastern Visayas'),
(1064, 'Mater Redemptoris College of San Jose City, Inc.', 'Region III - Central Luzon'),
(1065, 'Mater Redemptoris Collegium', 'Region IV - CALABARZON'),
(1066, 'Mati Doctors College', 'Region XI - Davao Region'),
(1067, 'Mati Polytechnic College', 'Region XI - Davao Region'),
(1068, 'MATS College of Technology', 'Region XI - Davao Region'),
(1069, 'Maxino College', 'Region VII - Central Visayas'),
(1070, 'Medical Colleges of Northern Philippines', 'Region II - Cagayan Valley'),
(1071, 'Medina College', 'Region X - Northern Mindanao'),
(1072, 'Medina College Ipil, Inc.', 'Region IX - Zamboanga Peninsula'),
(1073, 'Medina College, Inc.', 'Region IX - Zamboanga Peninsula'),
(1074, 'Medina Foundation College', 'Region X - Northern Mindanao'),
(1075, 'Megabyte College, Inc.-Florida Blanca', 'Region III - Central Luzon'),
(1076, 'MEIN College', 'Region IX - Zamboanga Peninsula'),
(1077, 'Meridian International College of Business and Arts', 'National Capital Region (NCR)'),
(1078, 'Messiah College Foundation', 'National Capital Region (NCR)'),
(1079, 'Metro Dumaguete College', 'Region VII - Central Visayas'),
(1080, 'Metro Subic Colleges, Inc.', 'Region III - Central Luzon'),
(1081, 'Metro-Dagupan Colleges', 'Region I - Ilocos Region'),
(1082, 'Metropolitan College of Science and Technology Santiago City, Isabela', 'Region II - Cagayan Valley'),
(1083, 'Metropolitan Institute of Arts and Sciences', 'National Capital Region (NCR)'),
(1084, 'Metropolitan Medical Center College of Arts, Science and Technology, Inc.', 'National Capital Region (NCR)'),
(1085, 'Meycauayan College', 'Region III - Central Luzon'),
(1086, 'MFI Foundation', 'National Capital Region (NCR)'),
(1087, 'MFI Polytechnic Institute, Inc', 'Region IV - CALABARZON'),
(1088, 'Micro Asia College of Science & Technology, Inc.', 'Region III - Central Luzon'),
(1089, 'Microcity Computer of Business And Technology( Formerly: Microcity Computer College Foundation, Inc.)', 'Region III - Central Luzon'),
(1090, 'Microspan Software Technology, Inc.', 'Region XII - SOCCSKSARGEN'),
(1091, 'Microsystems International Institute of Technology', 'Region VII - Central Visayas'),
(1092, 'Midway Maritime Foundation, Inc.', 'Region III - Central Luzon'),
(1093, 'Mina De Oro Institute', 'Region IVB - MIMAROPA'),
(1094, 'Mind and Integrity College', 'Region IV - CALABARZON'),
(1095, 'Mindanao Arts and Technological Institute', 'Region X - Northern Mindanao'),
(1096, 'Mindanao Capitol College, Inc.', 'Region XII - SOCCSKSARGEN'),
(1097, 'Mindanao Kokusai Daigaku', 'Region XI - Davao Region'),
(1098, 'Mindanao Medical Foundation College', 'Region XI - Davao Region'),
(1099, 'Mindanao Polytechnic College', 'Region XII - SOCCSKSARGEN'),
(1100, 'Mindanao State University Buug Campus', 'Region IX - Zamboanga Peninsula'),
(1101, 'Mindanao State University-General Santos City', 'Region XII - SOCCSKSARGEN'),
(1102, 'Mindanao State University-Iligan Institute of Technology', 'Region X - Northern Mindanao'),
(1103, 'Mindanao State University-Lanao del Norte Agricultural College', 'Region X - Northern Mindanao'),
(1104, 'Mindanao State University-Maigo School of Arts and Trades', 'Region X - Northern Mindanao'),
(1105, 'Mindanao State University-Naawan', 'Region X - Northern Mindanao'),
(1106, 'Mindanao University of Science and Technology', 'Region X - Northern Mindanao'),
(1107, 'Mindanao University of Science and Technology-Jasaan', 'Region X - Northern Mindanao'),
(1108, 'Mindanao University of Science and Technology-Oroquieta', 'Region X - Northern Mindanao'),
(1109, 'Mindanao University of Science and Technology-Panaon', 'Region X - Northern Mindanao'),
(1110, 'Mindoro State College of Agriculture & Technology – Bongabong', 'Region IVB - MIMAROPA'),
(1111, 'Mindoro State College of Agriculture & Technology – Calapan', 'Region IVB - MIMAROPA'),
(1112, 'Mindoro State College of Agriculture & Technology – Main', 'Region IVB - MIMAROPA'),
(1113, 'Miriam College', 'National Capital Region (NCR)'),
(1114, 'Misamis Institute of Technology', 'Region X - Northern Mindanao'),
(1115, 'Misamis Oriental Institute of Science and Technology', 'Region X - Northern Mindanao'),
(1116, 'Misamis Oriental State College of Agriculture and Technology', 'Region X - Northern Mindanao'),
(1117, 'Misamis University-Oroquieta', 'Region X - Northern Mindanao'),
(1118, 'Misamis University-Ozamiz', 'Region X - Northern Mindanao'),
(1119, 'MMG School of Paramedics and Allied Sciences, Inc.', 'Region XII - SOCCSKSARGEN'),
(1120, 'Mondriaan Aura College', 'Region III - Central Luzon'),
(1121, 'Montano Lamberte Go College of Learning, Inc.', 'Region VIII - Eastern Visayas'),
(1122, 'Montessori Professional College Imus', 'Region IV - CALABARZON'),
(1123, 'Mother of Good Counsel Seminary', 'Region III - Central Luzon'),
(1124, 'Mount Carmel College', 'Negros Island Region (NIR)'),
(1125, 'Mount Carmel College of Baler, Inc.', 'Region III - Central Luzon'),
(1126, 'Mount Carmel College of Casiguran, Inc.', 'Region III - Central Luzon'),
(1127, 'Mount Moriah College', 'Region VII - Central Visayas'),
(1128, 'Mountain View College', 'Region X - Northern Mindanao'),
(1129, 'Mt. Apo Science Foundation College', 'Region XI - Davao Region'),
(1130, 'Mt. Carmel College of Bocaue Bulacan, Inc.', 'Region III - Central Luzon'),
(1131, 'Mt. Carmel College Of San Francisco, Inc.', 'CARAGA'),
(1132, 'Mt. Province State Polytechnic College-Main', 'Cordillera Administrative Region (CAR)'),
(1133, 'Mt. Province State Polytechnic College-Tadian', 'Cordillera Administrative Region (CAR)'),
(1134, 'Mt. St. Aloysius Seminary', 'Region IV - CALABARZON'),
(1135, 'Mystical Rose College of Science and Technology', 'Region I - Ilocos Region'),
(1136, 'Naga College Foundation', 'Region V - Bicol Region'),
(1137, 'Naga View Adventist College', 'Region V - Bicol Region'),
(1138, 'NAMEI Polytechnic Institute', 'National Capital Region (NCR)'),
(1139, 'National Christian Life College', 'National Capital Region (NCR)'),
(1140, 'National College of Business and Arts Taytay', 'Region IV - CALABARZON'),
(1141, 'National College of Business and Arts, Cubao', 'National Capital Region (NCR)'),
(1142, 'National College of Business and Arts, Fairview', 'National Capital Region (NCR)'),
(1143, 'National College of Science and Technology', 'Region IV - CALABARZON'),
(1144, 'National Polytechnic College of Science and Technology', 'National Capital Region (NCR)'),
(1145, 'National University', 'National Capital Region (NCR)'),
(1146, 'Naval State University-Biliran', 'Region VIII - Eastern Visayas'),
(1147, 'Naval State University-Main', 'Region VIII - Eastern Visayas'),
(1148, 'Nazarenus College And Hospital Foundation, Inc.', 'Region III - Central Luzon'),
(1149, 'Nazareth Institute Of Alfonso', 'Region IV - CALABARZON'),
(1150, 'NBS College', 'National Capital Region (NCR)'),
(1151, 'Negros College', 'Region VII - Central Visayas'),
(1152, 'Negros Maritime Foundation', 'Region VII - Central Visayas'),
(1153, 'Negros Oriental State University – Bayawan', 'Negros Island Region (NIR)'),
(1154, 'Negros Oriental State University- Bais I', 'Region VII - Central Visayas'),
(1155, 'Negros Oriental State University -Bais II', 'Region VII - Central Visayas'),
(1156, 'Negros Oriental State University-Bayawan', 'Region VII - Central Visayas'),
(1157, 'Negros Oriental State University-Guihulngan', 'Region VII - Central Visayas'),
(1158, 'Negros Oriental State University-Mabinay', 'Region VII - Central Visayas'),
(1159, 'Negros Oriental State University-Main', 'Region VII - Central Visayas'),
(1160, 'Negros Oriental State University-Siaton', 'Region VII - Central Visayas'),
(1161, 'New England College', 'National Capital Region (NCR)'),
(1162, 'New Era University', 'National Capital Region (NCR)'),
(1163, 'New Era University Lipa Campus', 'Region IV - CALABARZON'),
(1164, 'New Era University-Pampanga', 'Region III - Central Luzon'),
(1165, 'New Lucena Polytechnic College', 'Region VI - Western Visayas'),
(1166, 'New Sanai School and Colleges', 'Region IV - CALABARZON'),
(1167, 'NICOSAT Colleges, Inc.', 'Region I - Ilocos Region'),
(1168, 'NJ Valdez Colleges Foundation', 'Region I - Ilocos Region'),
(1169, 'North Central Mindanao College', 'Region X - Northern Mindanao'),
(1170, 'North Davao Colleges-Panabo', 'Region XI - Davao Region'),
(1171, 'North Davao College-Tagum Foundation', 'Region XI - Davao Region'),
(1172, 'North Luzon Philippines State College', 'Region I - Ilocos Region'),
(1173, 'North Negros College', 'Negros Island Region (NIR)'),
(1174, 'North Valley College Foundation, Inc.', 'Region XII - SOCCSKSARGEN'),
(1175, 'Northeast Luzon Adventist College', 'Region II - Cagayan Valley'),
(1176, 'Northeastern Cebu Colleges', 'Region VII - Central Visayas'),
(1177, 'Northeastern College', 'Region II - Cagayan Valley'),
(1178, 'Northeastern Mindanao Colleges', 'CARAGA'),
(1179, 'Northern Cagayan Colleges Foundation', 'Region II - Cagayan Valley'),
(1180, 'Northern Cebu Colleges', 'Region VII - Central Visayas'),
(1181, 'Northern Christian College', 'Region I - Ilocos Region'),
(1182, 'Northern Iloilo Polytechnic State College, Ajuy Campus', 'Region VI - Western Visayas'),
(1183, 'Northern Iloilo Polytechnic State College, Barotac Viejo Campus', 'Region VI - Western Visayas'),
(1184, 'Northern Iloilo Polytechnic State College, Batad Campus', 'Region VI - Western Visayas'),
(1185, 'Northern Iloilo Polytechnic State College, Concepcion Campus', 'Region VI - Western Visayas'),
(1186, 'Northern Iloilo Polytechnic State College, Lemery Campus', 'Region VI - Western Visayas'),
(1187, 'Northern Iloilo Polytechnic State College, Main Campus', 'Region VI - Western Visayas'),
(1188, 'Northern Iloilo Polytechnic State College, Sara Campus', 'Region VI - Western Visayas'),
(1189, 'Northern Leyte College', 'Region VIII - Eastern Visayas'),
(1190, 'Northern Luzon Adventist College', 'Region I - Ilocos Region'),
(1191, 'Northern Mindanao Colleges, Inc.', 'CARAGA'),
(1192, 'Northern Negros State College of Science and Technology, Cadiz Campus', 'Negros Island Region (NIR)'),
(1193, 'Northern Negros State College of Science and Technology, Calatrava Campus', 'Negros Island Region (NIR)'),
(1194, 'Northern Negros State College of Science and Technology, Escalante Campus', 'Negros Island Region (NIR)'),
(1195, 'Northern Negros State College of Science and Technology, Main Campus', 'Negros Island Region (NIR)'),
(1196, 'Northern Philippines College for Maritime, Science and Technology', 'Region I - Ilocos Region'),
(1197, 'Northern Quezon College', 'Region IV - CALABARZON'),
(1198, 'Northern Samar Colleges', 'Region VIII - Eastern Visayas'),
(1199, 'Northern Zambales College, Inc.', 'Region III - Central Luzon'),
(1200, 'Northlink Technological College', 'Region XI - Davao Region'),
(1201, 'Northwest Samar State University-Main', 'Region VIII - Eastern Visayas'),
(1202, 'Northwest Samar State University-San Jorge', 'Region VIII - Eastern Visayas'),
(1203, 'Northwestern Agusan Colleges', 'CARAGA'),
(1204, 'Northwestern Mindanao Christian College', 'Region X - Northern Mindanao'),
(1205, 'Northwestern Mindanao Institute of Technology', 'Region X - Northern Mindanao'),
(1206, 'Northwestern Mindanao State College of Science and Technology', 'Region X - Northern Mindanao'),
(1207, 'Northwestern University', 'Region I - Ilocos Region'),
(1208, 'Northwestern Visayan Colleges', 'Region VI - Western Visayas'),
(1209, 'Notre Dame Center for Catechesis', 'Region XII - SOCCSKSARGEN'),
(1210, 'Notre Dame Hospital and School of Midwifery', 'Region XII - SOCCSKSARGEN'),
(1211, 'Notre Dame of Dadiangas University', 'Region XII - SOCCSKSARGEN'),
(1212, 'Notre Dame of Kidapawan College', 'Region XII - SOCCSKSARGEN'),
(1213, 'Notre Dame of Marbel University', 'Region XII - SOCCSKSARGEN'),
(1214, 'Notre Dame of Midsayap College', 'Region XII - SOCCSKSARGEN'),
(1215, 'Notre Dame of Salaman College', 'Region XII - SOCCSKSARGEN'),
(1216, 'Notre Dame of Tacurong College', 'Region XII - SOCCSKSARGEN'),
(1217, 'Notre Dame University', 'Region XII - SOCCSKSARGEN'),
(1218, 'Notre Dame-RVM College of Cotabato', 'Region XII - SOCCSKSARGEN'),
(1219, 'Notre Dame-Siena College of Polomolok', 'Region XII - SOCCSKSARGEN'),
(1220, 'Nova College', 'National Capital Region (NCR)'),
(1221, 'NTC Education Batangas', 'Region IV - CALABARZON'),
(1222, 'Nueva Ecija Doctors Colleges, Inc.', 'Region III - Central Luzon'),
(1223, 'Nueva Ecija University of Science And Technology – Gabaldon Campus', 'Region III - Central Luzon'),
(1224, 'Nueva Ecija University of Science And Technology-Atate Campus', 'Region III - Central Luzon'),
(1225, 'Nueva Ecija University of Science And Technology-Carranglan Campus', 'Region III - Central Luzon'),
(1226, 'Nueva Ecija University of Science And Technology-Fort Magsaysay Campus', 'Region III - Central Luzon'),
(1227, 'Nueva Ecija University of Science And Technology-Gen. Tinio Campus', 'Region III - Central Luzon'),
(1228, 'Nueva Ecija University of Science and Technology-San Isidro Campus', 'Region III - Central Luzon'),
(1229, 'Nueva Ecija University of Science And Technology-Sumacab Campus', 'Region III - Central Luzon'),
(1230, 'Nueva Vizcaya State University-Bambang', 'Region II - Cagayan Valley'),
(1231, 'Nueva Vizcaya State University-Main Campus, Bayombong', 'Region II - Cagayan Valley'),
(1232, 'Nuevo Zamboanga College', 'Region IX - Zamboanga Peninsula'),
(1233, 'NYK-TDG Maritime Academy', 'Region IV - CALABARZON'),
(1234, 'Oblates of Saint Joseph College of Philosophy', 'Region IV - CALABARZON'),
(1235, 'Occidental Mindoro State College – Labangan', 'Region IVB - MIMAROPA'),
(1236, 'Occidental Mindoro State College – Main', 'Region IVB - MIMAROPA'),
(1237, 'Occidental Mindoro State College – Mamburao', 'Region IVB - MIMAROPA'),
(1238, 'Occidental Mindoro State College – Murtha', 'Region IVB - MIMAROPA'),
(1239, 'Occidental Mindoro State College – Sablayan', 'Region IVB - MIMAROPA'),
(1240, 'Olivarez College', 'National Capital Region (NCR)'),
(1241, 'Olivarez College-Tagaytay', 'Region IV - CALABARZON'),
(1242, 'Oro Bible College', 'Region X - Northern Mindanao'),
(1243, 'Osias Colleges, Inc.', 'Region III - Central Luzon'),
(1244, 'Osias Educational Foundation', 'Region I - Ilocos Region'),
(1245, 'Osmena Colleges', 'Region V - Bicol Region'),
(1246, 'Our Lady of Assumption College Batangas', 'Region IV - CALABARZON'),
(1247, 'Our Lady of Assumption College Cabuyao', 'Region IV - CALABARZON'),
(1248, 'Our Lady of Assumption College of Laguna Inc. – San Pedro', 'Region IV - CALABARZON'),
(1249, 'Our Lady of Fatima University – Pampanga, Inc.', 'Region III - Central Luzon'),
(1250, 'Our Lady of Fatima University Antipolo', 'Region IV - CALABARZON'),
(1251, 'Our Lady of Fatima University, Inc (Formerly East Central Colleges)', 'Region III - Central Luzon'),
(1252, 'Our Lady of Fatima University, Quezon City', 'National Capital Region (NCR)'),
(1253, 'Our Lady of Guadalupe Colleges, Inc.', 'National Capital Region (NCR)'),
(1254, 'Our Lady of Lourdes College Foundation', 'Region V - Bicol Region'),
(1255, 'Our Lady of Lourdes Technological College', 'National Capital Region (NCR)'),
(1256, 'Our Lady of Manaoag Montessori College, Inc.', 'Region III - Central Luzon'),
(1257, 'Our Lady Of Mercy College', 'Region VIII - Eastern Visayas'),
(1258, 'Our Lady of Peace College Seminary', 'Region III - Central Luzon'),
(1259, 'Our Lady of Perpetual Succor College', 'National Capital Region (NCR)'),
(1260, 'Our Lady of Salvation College', 'Region V - Bicol Region'),
(1261, 'Our Lady of the Most Holy Rosary Seminary', 'Region IV - CALABARZON'),
(1262, 'Our Lady of the Pillar College-Cauayan', 'Region II - Cagayan Valley'),
(1263, 'Our Lady of the Pillar College-Cauayan San Manuel Branch', 'Region II - Cagayan Valley'),
(1264, 'Our Lady of the Sacred Heart College of Guimba, Inc.', 'Region III - Central Luzon'),
(1265, 'Ovilla Technical College', 'Region V - Bicol Region'),
(1266, 'Oxfordian Colleges', 'Region IV - CALABARZON'),
(1267, 'Pacific Inter Continental College', 'National Capital Region (NCR)'),
(1268, 'Paete Science & Business College', 'Region IV - CALABARZON'),
(1269, 'Pagadian Capitol College', 'Region IX - Zamboanga Peninsula'),
(1270, 'Palaris College', 'Region I - Ilocos Region'),
(1271, 'Palawan Polytechnic College, Inc.', 'Region IVB - MIMAROPA'),
(1272, 'Palawan State University – Araceli', 'Region IVB - MIMAROPA'),
(1273, 'Palawan State University – Balabac', 'Region IVB - MIMAROPA'),
(1274, 'Palawan State University – Bataraza', 'Region IVB - MIMAROPA'),
(1275, 'Palawan State University – Brooke’s Point', 'Region IVB - MIMAROPA'),
(1276, 'Palawan State University – Coron', 'Region IVB - MIMAROPA'),
(1277, 'Palawan State University – Cuyo', 'Region IVB - MIMAROPA'),
(1278, 'Palawan State University – Dumaran', 'Region IVB - MIMAROPA'),
(1279, 'Palawan State University – El Nido', 'Region IVB - MIMAROPA'),
(1280, 'Palawan State University – Linapacan', 'Region IVB - MIMAROPA'),
(1281, 'Palawan State University – Main', 'Region IVB - MIMAROPA'),
(1282, 'Palawan State University – Narra', 'Region IVB - MIMAROPA'),
(1283, 'Palawan State University – Quezon', 'Region IVB - MIMAROPA'),
(1284, 'Palawan State University – Rizal', 'Region IVB - MIMAROPA'),
(1285, 'Palawan State University – Roxas', 'Region IVB - MIMAROPA'),
(1286, 'Palawan State University – San Rafael, Puerto Princesa', 'Region IVB - MIMAROPA'),
(1287, 'Palawan State University – San Vicente', 'Region IVB - MIMAROPA'),
(1288, 'Palawan State University – Soforonio Espanola', 'Region IVB - MIMAROPA'),
(1289, 'Palawan State University – Taytay', 'Region IVB - MIMAROPA'),
(1290, 'Palawan Technological College', 'Region IVB - MIMAROPA'),
(1291, 'Palompon Institute Of Technology-Main', 'Region VIII - Eastern Visayas'),
(1292, 'Palompon Institute Of Technology-Tabango', 'Region VIII - Eastern Visayas'),
(1293, 'Pamantasan Ng Araullo (Araullo University), Inc.', 'Region III - Central Luzon'),
(1294, 'Pampanga Colleges, Inc.', 'Region III - Central Luzon'),
(1295, 'Pampanga State Agricultural University ( Formerly: Pampanga Agricultural College)', 'Region III - Central Luzon'),
(1296, 'Panay Technological College', 'Region VI - Western Visayas'),
(1297, 'Pandan Bay Institute, Inc.', 'Region VI - Western Visayas'),
(1298, 'Pangasinan Merchant Marine Academy', 'Region I - Ilocos Region'),
(1299, 'Pangasinan State University', 'Region I - Ilocos Region'),
(1300, 'Pangasinan State University-Alaminos City', 'Region I - Ilocos Region'),
(1301, 'Pangasinan State University-Asingan', 'Region I - Ilocos Region'),
(1302, 'Pangasinan State University-Bayambang', 'Region I - Ilocos Region'),
(1303, 'Pangasinan State University-Binmaley', 'Region I - Ilocos Region'),
(1304, 'Pangasinan State University-Infanta', 'Region I - Ilocos Region'),
(1305, 'Pangasinan State University-San Carlos City', 'Region I - Ilocos Region'),
(1306, 'Pangasinan State University-Sta. Maria', 'Region I - Ilocos Region'),
(1307, 'Pangasinan State University-Urdaneta City', 'Region I - Ilocos Region'),
(1308, 'Panpacific University North Philippines-Tayug', 'Region I - Ilocos Region'),
(1309, 'Panpacific University North Philippines-Urdaneta City', 'Region I - Ilocos Region'),
(1310, 'Paradigm College of Science & Technology', 'Region IVB - MIMAROPA'),
(1311, 'Partido College', 'Region V - Bicol Region'),
(1312, 'Partido State University – San Jose', 'Region V - Bicol Region'),
(1313, 'Partido State University-Main', 'Region V - Bicol Region'),
(1314, 'Partido State University-Sagnay Campus', 'Region V - Bicol Region'),
(1315, 'Partido State University-Salogon Campus', 'Region V - Bicol Region'),
(1316, 'Partido State University-Tinambac Campus', 'Region V - Bicol Region'),
(1317, 'Pasig Catholic College', 'National Capital Region (NCR)'),
(1318, 'Pass College', 'Region I - Ilocos Region'),
(1319, 'Patria Sable Corpus College', 'Region II - Cagayan Valley'),
(1320, 'PATTS College of Aeronautics', 'National Capital Region (NCR)'),
(1321, 'Paul College of Leadership', 'Region IV - CALABARZON'),
(1322, 'Perpetual Help College of Manila', 'National Capital Region (NCR)'),
(1323, 'Perpetual Help College of Pangasinan', 'Region I - Ilocos Region'),
(1324, 'Perpetual Help Paramedical School', 'Region V - Bicol Region'),
(1325, 'Philippine Advent College', 'Region IX - Zamboanga Peninsula'),
(1326, 'Philippine Advent College, Inc. – Salug<', 'Region IX - Zamboanga Peninsula'),
(1327, 'Philippine Best Training System College, Inc.', 'Region IV - CALABARZON'),
(1328, 'Philippine Central Islands College', 'Region IVB - MIMAROPA'),
(1329, 'Philippine Christian University', 'National Capital Region (NCR)'),
(1330, 'Philippine College Foundation', 'Region X - Northern Mindanao'),
(1331, 'Philippine College of Business and Accountancy', 'Region VI - Western Visayas'),
(1332, 'Philippine College of Criminology', 'National Capital Region (NCR)'),
(1333, 'Philippine College of Ministry', 'Cordillera Administrative Region (CAR)');
INSERT INTO `school` (`school_id`, `name`, `description`) VALUES
(1334, 'Philippine College of Northwestern Luzon', 'Region I - Ilocos Region'),
(1335, 'Philippine College of Science and Technology', 'Region I - Ilocos Region'),
(1336, 'Philippine College of Technology', 'Region XI - Davao Region'),
(1337, 'Philippine Computer Foundation College', 'Region V - Bicol Region'),
(1338, 'Philippine Countryville College', 'Region X - Northern Mindanao'),
(1339, 'Philippine Cultural College', 'National Capital Region (NCR)'),
(1340, 'Philippine Darakbang Theological College', 'Region I - Ilocos Region'),
(1341, 'Philippine Electronics And Communication Institute Of Technology', 'CARAGA'),
(1342, 'Philippine Hoteliers International Center for Hospitality Education, Inc.', 'National Capital Region (NCR)'),
(1343, 'Philippine International College', 'Region IV - CALABARZON'),
(1344, 'Philippine Law Enforcement College', 'Region II - Cagayan Valley'),
(1345, 'Philippine Merchant Marine Academy', 'Region III - Central Luzon'),
(1346, 'Philippine Merchant Marine School, Las Piñas', 'National Capital Region (NCR)'),
(1347, 'Philippine Military Academy', 'Cordillera Administrative Region (CAR)'),
(1348, 'Philippine Missionary Institute', 'Region IV - CALABARZON'),
(1349, 'Philippine National Police Academy', 'Region IV - CALABARZON'),
(1350, 'Philippine Nazarene College', 'Cordillera Administrative Region (CAR)'),
(1351, 'Philippine Normal University – Mindanao', 'CARAGA'),
(1352, 'Philippine Normal University – North Luzon Campus', 'Region II - Cagayan Valley'),
(1353, 'Philippine Normal University Lopez', 'Region IV - CALABARZON'),
(1354, 'Philippine Normal University Main', 'National Capital Region (NCR)'),
(1355, 'Philippine Normal University-Alicia', 'Region II - Cagayan Valley'),
(1356, 'Philippine Normal University-Cadiz', 'Negros Island Region (NIR)'),
(1357, 'Philippine Rehabilitation Institute Foundation, Inc.', 'National Capital Region (NCR)'),
(1358, 'Philippine Rehabilitation Institute Foundation, Inc.-Guagua', 'Region III - Central Luzon'),
(1359, 'Philippine School of Interior Design-Ahlen Institute, Inc.', 'National Capital Region (NCR)'),
(1360, 'Philippine State College of Aeronautics – Mactan Air Base', 'Region VII - Central Visayas'),
(1361, 'Philippine State College of Aeronautics Fernando Air Base', 'Region IV - CALABARZON'),
(1362, 'Philippine Technological & Marine Sciences', 'Region IX - Zamboanga Peninsula'),
(1363, 'Philippine Women’s College of Davao', 'Region XI - Davao Region'),
(1364, 'Philippine Women’s University Career Development And Continuing Education Center Bataan, Inc.', 'Region III - Central Luzon'),
(1365, 'Philippine Women’s University Career Development and Continuing Education Center Calamba City', 'Region IV - CALABARZON'),
(1366, 'Philippine Women’s University Career Development And Continuing Education Center Tarlac, Inc.', 'Region III - Central Luzon'),
(1367, 'Philippine Women’s University CDCEC Sta. Cruz', 'Region IV - CALABARZON'),
(1368, 'Philippine Women’s University-CDCEC', 'Cordillera Administrative Region (CAR)'),
(1369, 'PHINMA-UPang College Urdaneta, Inc.', 'Region I - Ilocos Region'),
(1370, 'Picardal Institute of Science and Technology', 'Region X - Northern Mindanao'),
(1371, 'Pilar College of Zamboanga City Inc.', 'Region IX - Zamboanga Peninsula'),
(1372, 'Pilgrim Christian College', 'Region X - Northern Mindanao'),
(1373, 'Pili Capital College', 'Region V - Bicol Region'),
(1374, 'PIMSAT College – Bacoor', 'Region IV - CALABARZON'),
(1375, 'PIMSAT Colleges', 'Region I - Ilocos Region'),
(1376, 'PIMSAT Colleges-San Carlos', 'Region I - Ilocos Region'),
(1377, 'Pinamalayan Maritime Foundation Technological College', 'Region IVB - MIMAROPA'),
(1378, 'Pines City Colleges', 'Cordillera Administrative Region (CAR)'),
(1379, 'Pinnacle College of Bangued', 'Cordillera Administrative Region (CAR)'),
(1380, 'PLT College', 'Region II - Cagayan Valley'),
(1381, 'PMI Colleges-Bohol', 'Region VII - Central Visayas'),
(1382, 'PNTC Colleges', 'Region IV - CALABARZON'),
(1383, 'PNTC Colleges – Manila', 'National Capital Region (NCR)'),
(1384, 'Polytechnic College of Davao Del Sur', 'Region XI - Davao Region'),
(1385, 'Polytechnic College of La Union', 'Region I - Ilocos Region'),
(1386, 'Polytechnic College of Region I', 'Region I - Ilocos Region'),
(1387, 'Polytechnic Institute of Tabaco', 'Region V - Bicol Region'),
(1388, 'Polytechnic University of the Philippines – Ragay Campus', 'Region V - Bicol Region'),
(1389, 'Polytechnic University of the Philippines Lopez', 'Region IV - CALABARZON'),
(1390, 'Polytechnic University of the Philippines Mulanay', 'Region IV - CALABARZON'),
(1391, 'Polytechnic University of the Philippines Sto. Tomas', 'Region IV - CALABARZON'),
(1392, 'Polytechnic University of the Philippines Unisan', 'Region IV - CALABARZON'),
(1393, 'Polytechnic University of the Philippines-Quezon City Campus', 'National Capital Region (NCR)'),
(1394, 'Power School of Technology', 'Region IV - CALABARZON'),
(1395, 'Presbyterian Theological College', 'Region VII - Central Visayas'),
(1396, 'Primasia Foundation College', 'Region XII - SOCCSKSARGEN'),
(1397, 'Prince of Peace College', 'Region IVB - MIMAROPA'),
(1398, 'Professional Academy of the Philippines', 'Region VII - Central Visayas'),
(1399, 'Professional Montessori College', 'Region IV - CALABARZON'),
(1400, 'PTS College and Advanced Studies', 'Region IV - CALABARZON'),
(1401, 'Queen of Apostles College Seminary', 'Region XI - Davao Region'),
(1402, 'Quezon Center for Research and Studies', 'Region IV - CALABARZON'),
(1403, 'Quezon Colleges of Southern Philippines', 'Region XII - SOCCSKSARGEN'),
(1404, 'Quezon Colleges of the North', 'Region II - Cagayan Valley'),
(1405, 'Quezon Institute of Technology', 'Region X - Northern Mindanao'),
(1406, 'Quezon Memorial Institute of Siquijor', 'Region VII - Central Visayas'),
(1407, 'Quezonian Educational College', 'Region IV - CALABARZON'),
(1408, 'Quirino State University-Cabarroguis Campus', 'Region II - Cagayan Valley'),
(1409, 'Quirino State University-Diffun, Quirino', 'Region II - Cagayan Valley'),
(1410, 'Quirino State University-Maddela, Quirino', 'Region II - Cagayan Valley'),
(1411, 'Ramon Magsaysay Memorial Colleges', 'Region XII - SOCCSKSARGEN'),
(1412, 'Ramon Magsaysay Momorial College – Marbel, Inc.', 'Region XII - SOCCSKSARGEN'),
(1413, 'Ramon Magsaysay Technological University (Botolan Campus)', 'Region III - Central Luzon'),
(1414, 'Ramon Magsaysay Technological University (Main Campus)', 'Region III - Central Luzon'),
(1415, 'Ramon Magsaysay Technological University (San Marcelino Campus)', 'Region III - Central Luzon'),
(1416, 'RCC Colegio De San Rafael, Inc.', 'Region III - Central Luzon'),
(1417, 'Read Data Access Computer College', 'Region XII - SOCCSKSARGEN'),
(1418, 'Recoletos School of Theology', 'National Capital Region (NCR)'),
(1419, 'Regency Polytechnic College, Inc.', 'Region XII - SOCCSKSARGEN'),
(1420, 'Regina Mondi College, Inc.', 'Region V - Bicol Region'),
(1421, 'Remnant Christian College', 'Region IVB - MIMAROPA'),
(1422, 'Remnant International College', 'Cordillera Administrative Region (CAR)'),
(1423, 'Republic Central Colleges', 'Region III - Central Luzon'),
(1424, 'Republic Colleges of Guinobatan, Inc.', 'Region V - Bicol Region'),
(1425, 'Richwell Colleges, Inc.', 'Region III - Central Luzon'),
(1426, 'Riverside College', 'Negros Island Region (NIR)'),
(1427, 'Rizal College of Laguna', 'Region IV - CALABARZON'),
(1428, 'Rizal College of Taal', 'Region IV - CALABARZON'),
(1429, 'Rizal Memorial Colleges', 'Region XI - Davao Region'),
(1430, 'Rizal Memorial Institute of Dapitan City, Inc.', 'Region IX - Zamboanga Peninsula'),
(1431, 'Rizal Technological University-Pasig', 'National Capital Region (NCR)'),
(1432, 'Rizwoods College – N. Bacalso', 'Region VII - Central Visayas'),
(1433, 'Rogationist College', 'Region IV - CALABARZON'),
(1434, 'Rogationist Seminary College', 'Region VII - Central Visayas'),
(1435, 'Rogationist Seminary College of Philosophy', 'National Capital Region (NCR)'),
(1436, 'Roman C. Villalon Memorial College', 'Region X - Northern Mindanao'),
(1437, 'Romarinda International College', 'National Capital Region (NCR)'),
(1438, 'Romblon State University – Cajidiocan', 'Region IVB - MIMAROPA'),
(1439, 'Romblon State University – Calatrava', 'Region IVB - MIMAROPA'),
(1440, 'Romblon State University – Main', 'Region IVB - MIMAROPA'),
(1441, 'Romblon State University – San Agustin', 'Region IVB - MIMAROPA'),
(1442, 'Romblon State University – San Andres', 'Region IVB - MIMAROPA'),
(1443, 'Romblon State University – San Jose', 'Region IVB - MIMAROPA'),
(1444, 'Romblon State University – Sawang', 'Region IVB - MIMAROPA'),
(1445, 'Romblon State University – Sta. Fe', 'Region IVB - MIMAROPA'),
(1446, 'Romblon State University – Sta. Maria', 'Region IVB - MIMAROPA'),
(1447, 'Roosevelt College Cainta', 'Region IV - CALABARZON'),
(1448, 'Rosales Wesleyan Bible College, Inc.', 'Region I - Ilocos Region'),
(1449, 'Rosemont Hills Montesorri College', 'Region VII - Central Visayas'),
(1450, 'Royal Christian College', 'Region VII - Central Visayas'),
(1451, 'Sacred Heart College', 'Region IV - CALABARZON'),
(1452, 'Sacred Heart College of Calamba', 'Region X - Northern Mindanao'),
(1453, 'Sacred Heart Seminary', 'Region VIII - Eastern Visayas'),
(1454, 'Sacred Heart Seminary (Bacolod) School of Philosophy', 'Negros Island Region (NIR)'),
(1455, 'Sact St. Mutien College, Inc.', 'Region III - Central Luzon'),
(1456, 'Saint Albert Polytechnic College, Inc', 'Region XII - SOCCSKSARGEN'),
(1457, 'Saint Anthony Mary Claret College', 'National Capital Region (NCR)'),
(1458, 'Saint Anthony’s College', 'Region II - Cagayan Valley'),
(1459, 'Saint Augustine Seminary', 'Region IVB - MIMAROPA'),
(1460, 'Saint Bernadette College of Alabang', 'National Capital Region (NCR)'),
(1461, 'Saint Bridget College', 'Region IV - CALABARZON'),
(1462, 'Saint Catherine’s College', 'Region VII - Central Visayas'),
(1463, 'Saint Columban College', 'Region IX - Zamboanga Peninsula'),
(1464, 'Saint Columban’s College', 'Region I - Ilocos Region'),
(1465, 'Saint Dominic College of Batanes', 'Region II - Cagayan Valley'),
(1466, 'Saint Estanislao Kostka College, Inc.', 'Region IX - Zamboanga Peninsula'),
(1467, 'Saint Ferdinand College – Cabagan', 'Region II - Cagayan Valley'),
(1468, 'Saint Ferdinand College – Ilagan', 'Region II - Cagayan Valley'),
(1469, 'Saint Francis College', 'Region VIII - Eastern Visayas'),
(1470, 'Saint Francis College- Guihulngan', 'Region VII - Central Visayas'),
(1471, 'Saint Francis Institute of Computer Studies', 'Region IV - CALABARZON'),
(1472, 'Saint Francis of Assisi College Cavite', 'Region IV - CALABARZON'),
(1473, 'Saint Francis of Assisi College System', 'National Capital Region (NCR)'),
(1474, 'Saint Francis Xavier College, Inc.', 'CARAGA'),
(1475, 'Saint Gabriel College', 'Region VI - Western Visayas'),
(1476, 'Saint Joseph College of Canlaon', 'Region VII - Central Visayas'),
(1477, 'Saint Joseph College of Sindangan Incorporated', 'Region IX - Zamboanga Peninsula'),
(1478, 'Saint Joseph College, Maasin, Leyte', 'Region VIII - Eastern Visayas'),
(1479, 'Saint Joseph Institute Of Technology', 'CARAGA'),
(1480, 'Saint Joseph’s College of Baggao', 'Region II - Cagayan Valley'),
(1481, 'Saint Louis Anne Colleges', 'Region IV - CALABARZON'),
(1482, 'Saint Louis College of Bulanao', 'Cordillera Administrative Region (CAR)'),
(1483, 'Saint Louis College-Cebu', 'Region VII - Central Visayas'),
(1484, 'Saint Louis College-City of San Fernando', 'Region I - Ilocos Region'),
(1485, 'Saint Louis University', 'Cordillera Administrative Region (CAR)'),
(1486, 'Saint Louise De Marillac College of Sorsogon', 'Region V - Bicol Region'),
(1487, 'Saint Mary’s Angels College of Pampanga, Inc.', 'Region III - Central Luzon'),
(1488, 'Saint Mary’s College Sta. Maria, Ilocos Sur Inc.', 'Region I - Ilocos Region'),
(1489, 'Saint Mary’s University, Bayombong, Nueva Vizcaya', 'Region II - Cagayan Valley'),
(1490, 'Saint Michael College Of Caraga', 'CARAGA'),
(1491, 'Saint Michael College-Hindang, Leyte', 'Region VIII - Eastern Visayas'),
(1492, 'Saint Michael’s College of Laguna', 'Region IV - CALABARZON'),
(1493, 'Saint Paul School Of Professional Studies, Inc.', 'Region VIII - Eastern Visayas'),
(1494, 'Saint Paul University Philippines, Tuguegarao City', 'Region II - Cagayan Valley'),
(1495, 'Saint Pedro Poveda College', 'National Capital Region (NCR)'),
(1496, 'Saint Peter College Seminary', 'CARAGA'),
(1497, 'Saint Peter’s College Seminary', 'Region IV - CALABARZON'),
(1498, 'Saint Scholastica’s College Tacloban', 'Region VIII - Eastern Visayas'),
(1499, 'Saint Theresa College Of Tandag, Inc.', 'CARAGA'),
(1500, 'Saint Therese College Foundation, Inc.', 'Region I - Ilocos Region'),
(1501, 'Saint Thomas Aquinas College', 'Region VIII - Eastern Visayas'),
(1502, 'Saint Tonis College', 'Cordillera Administrative Region (CAR)'),
(1503, 'Saint Vincent De Paul College Seminary', 'Region VIII - Eastern Visayas'),
(1504, 'Saint Vincent De Paul Diocesan College', 'CARAGA'),
(1505, 'Saint Vincent’s College', 'Region IX - Zamboanga Peninsula'),
(1506, 'Saints John and Paul Colleges', 'Region IV - CALABARZON'),
(1507, 'Salazar Colleges of Science and Institute of Technology – Madridejos', 'Region VII - Central Visayas'),
(1508, 'Salazar Colleges of Science and Institute of Technology, N. Bacalso Ave., Cebu City', 'Region VII - Central Visayas'),
(1509, 'Salazar Colleges of Science and Institute of Technology, Talisay City, Cebu', 'Region VII - Central Visayas'),
(1510, 'Samar Colleges, Inc.', 'Region VIII - Eastern Visayas'),
(1511, 'Samar State University-Basey', 'Region VIII - Eastern Visayas'),
(1512, 'Samar State University-Main', 'Region VIII - Eastern Visayas'),
(1513, 'Samar State University-Mercedes', 'Region VIII - Eastern Visayas'),
(1514, 'Samar State University-Paranas', 'Region VIII - Eastern Visayas'),
(1515, 'Samson College of Science and Technology', 'National Capital Region (NCR)'),
(1516, 'Samson Polytechnic College of Davao', 'Region XI - Davao Region'),
(1517, 'San Agustin Institute of Technology', 'Region X - Northern Mindanao'),
(1518, 'San Antonio De Padua College', 'Region IV - CALABARZON'),
(1519, 'San Beda College', 'National Capital Region (NCR)'),
(1520, 'San Beda College Alabang', 'National Capital Region (NCR)'),
(1521, 'San Carlos College', 'Region I - Ilocos Region'),
(1522, 'San Carlos Seminary College', 'Region VII - Central Visayas'),
(1523, 'San Francisco Colleges', 'CARAGA'),
(1524, 'San Francisco Javier College', 'Region IVB - MIMAROPA'),
(1525, 'San Ildefonso College', 'Region IV - CALABARZON'),
(1526, 'San Isidro College', 'Region X - Northern Mindanao'),
(1527, 'San Jose Christian Colleges', 'Region III - Central Luzon'),
(1528, 'San Juan de Dios Educational Foundation, Inc.', 'National Capital Region (NCR)'),
(1529, 'San Lorenzo Ruiz College Of Ormoc, Inc.', 'Region VIII - Eastern Visayas'),
(1530, 'San Lorenzo Ruiz Seminary', 'Region IVB - MIMAROPA'),
(1531, 'San Pablo Colleges', 'Region IV - CALABARZON'),
(1532, 'San Pablo Major Seminary', 'Cordillera Administrative Region (CAR)'),
(1533, 'San Pedro College', 'Region XI - Davao Region'),
(1534, 'San Pedro College of Business Administration', 'Region IV - CALABARZON'),
(1535, 'San Sebastian Colleg Recoletos de Cavite', 'Region IV - CALABARZON'),
(1536, 'San Sebastian College Recoletos Canlubang', 'Region IV - CALABARZON'),
(1537, 'San Sebastian College-Recoletos', 'National Capital Region (NCR)'),
(1538, 'Sancta Maria et Regina Seminarium', 'Region VI - Western Visayas'),
(1539, 'Santa Cruz Institute', 'Region IVB - MIMAROPA'),
(1540, 'Santa Isabel College', 'National Capital Region (NCR)'),
(1541, 'Santa Isabel College of Iloilo City', 'Region VI - Western Visayas'),
(1542, 'Santa Monica Institute of Technology', 'Region X - Northern Mindanao'),
(1543, 'Santa Rita College of Pampanga, Inc.', 'Region III - Central Luzon'),
(1544, 'Santiago City Colleges', 'Region II - Cagayan Valley'),
(1545, 'Sapphire International Aviation Academy', 'National Capital Region (NCR)'),
(1546, 'Schola De San Jose', 'Region XII - SOCCSKSARGEN'),
(1547, 'School of Fashion and the Arts', 'National Capital Region (NCR)'),
(1548, 'Sea and Sky College', 'Region I - Ilocos Region'),
(1549, 'Seminario De San Jose', 'Region IVB - MIMAROPA'),
(1550, 'Senator Ninoy Aquino College Foundation, Inc.', 'Region XII - SOCCSKSARGEN'),
(1551, 'Señor Tesoro College Inc.', 'Region I - Ilocos Region'),
(1552, 'Serapion C Basalo Memorial Foundation Colleges', 'Region XI - Davao Region'),
(1553, 'Shariff Kabunsuan College', 'Region XII - SOCCSKSARGEN'),
(1554, 'Siargao Island Institute Of Technology', 'CARAGA'),
(1555, 'Sibugay Technical Institute, Inc.', 'Region IX - Zamboanga Peninsula'),
(1556, 'Siena College', 'National Capital Region (NCR)'),
(1557, 'Siena College of San Jose, Inc.', 'Region III - Central Luzon'),
(1558, 'Siena College of Taytay', 'Region IV - CALABARZON'),
(1559, 'Siena College Tigaon, Inc.', 'Region V - Bicol Region'),
(1560, 'Sierra College, Bayombong, Nueva Vizcaya', 'Region II - Cagayan Valley'),
(1561, 'Silay Institute', 'Negros Island Region (NIR)'),
(1562, 'Silliman University', 'Region VII - Central Visayas'),
(1563, 'Silliman University - Dumaguete City', 'Negros Island Region (NIR)'),
(1564, 'Siquijor State College', 'Region VII - Central Visayas'),
(1565, 'SISTECH College of Santiago City', 'Region II - Cagayan Valley'),
(1566, 'SJDM Cornerstone College, Inc.', 'Region III - Central Luzon'),
(1567, 'Skill Power Institute', 'Region IV - CALABARZON'),
(1568, 'SMC Agro -Tech', 'Region IX - Zamboanga Peninsula'),
(1569, 'Solis Institute of Technology', 'Region V - Bicol Region'),
(1570, 'Somascan Fathers Seminary', 'Region III - Central Luzon'),
(1571, 'Sorsogon College of Criminology', 'Region V - Bicol Region'),
(1572, 'Sorsogon State College – Castilla Campus', 'Region V - Bicol Region'),
(1573, 'Sorsogon State College – Magallanes Campus', 'Region V - Bicol Region'),
(1574, 'Sorsogon State College-Bulan Campus', 'Region V - Bicol Region'),
(1575, 'Sorsogon State College-Main', 'Region V - Bicol Region'),
(1576, 'South East Asian Institute of Technology', 'Region XII - SOCCSKSARGEN'),
(1577, 'South Forbes City College', 'Region IV - CALABARZON'),
(1578, 'South Ilocandia College of Arts and Technology', 'Region I - Ilocos Region'),
(1579, 'South Mansfield College', 'National Capital Region (NCR)'),
(1580, 'South Philippine Adventist College', 'Region XI - Davao Region'),
(1581, 'South SEED LPDH College', 'National Capital Region (NCR)'),
(1582, 'Southdale International School of Science, Arts and Techology', 'Region IV - CALABARZON'),
(1583, 'Southeast Asia Christian College', 'National Capital Region (NCR)'),
(1584, 'Southeastern College', 'Region XI - Davao Region'),
(1585, 'Southern Baptist College', 'Region XII - SOCCSKSARGEN'),
(1586, 'Southern Bicol Colleges', 'Region V - Bicol Region'),
(1587, 'Southern Bukidnon Foundation Academy', 'Region X - Northern Mindanao'),
(1588, 'Southern Capital College', 'Region X - Northern Mindanao'),
(1589, 'Southern Christian College', 'Region XII - SOCCSKSARGEN'),
(1590, 'Southern City Colleges, Inc.', 'Region IX - Zamboanga Peninsula'),
(1591, 'Southern de Oro Philippines College', 'Region X - Northern Mindanao'),
(1592, 'Southern Leyte State University', 'Region VIII - Eastern Visayas'),
(1593, 'Southern Leyte State University-Bontoc', 'Region VIII - Eastern Visayas'),
(1594, 'Southern Leyte State University-Hinunangan', 'Region VIII - Eastern Visayas'),
(1595, 'Southern Leyte State University-San Juan', 'Region VIII - Eastern Visayas'),
(1596, 'Southern Leyte State University-Tomas Oppus', 'Region VIII - Eastern Visayas'),
(1597, 'Southern Luzon State University', 'Region IV - CALABARZON'),
(1598, 'Southern Luzon State University Alabat', 'Region IV - CALABARZON'),
(1599, 'Southern Luzon State University Infanta', 'Region IV - CALABARZON'),
(1600, 'Southern Luzon State University Judge Guillermo Eleazar Polytechnic College -Tagkawayan', 'Region IV - CALABARZON'),
(1601, 'Southern Luzon State University- Lucena', 'Region IV - CALABARZON'),
(1602, 'Southern Luzon State University Polilio', 'Region IV - CALABARZON'),
(1603, 'Southern Luzon State University Sampaloc', 'Region IV - CALABARZON'),
(1604, 'Southern Luzon State University Tiaong', 'Region IV - CALABARZON'),
(1605, 'Southern Luzon Technological College Foundation Pilar, Inc.', 'Region V - Bicol Region'),
(1606, 'Southern Luzon Technological College Foundation-Legazpi', 'Region V - Bicol Region'),
(1607, 'Southern Maramag Colleges', 'Region X - Northern Mindanao'),
(1608, 'Southern Masbate Roosevelt College', 'Region V - Bicol Region'),
(1609, 'Southern Mindanao Colleges', 'Region IX - Zamboanga Peninsula'),
(1610, 'Southern Mindanao Institute of Technology', 'Region XII - SOCCSKSARGEN'),
(1611, 'Southern Negros College', 'Negros Island Region (NIR)'),
(1612, 'Southern Peninsula College', 'Region IX - Zamboanga Peninsula'),
(1613, 'Southern Philippines Agriculture, Business, Marine and Aquatic School of Technology-Digos', 'Region XI - Davao Region'),
(1614, 'Southern Philippines Agriculture, Business, Marine and Aquatic School of Technology-Malita', 'Region XI - Davao Region'),
(1615, 'Southern Philippines Baptist Theological Seminary', 'Region XI - Davao Region'),
(1616, 'Southern Philippines Institute of Science and Technology', 'Region IV - CALABARZON'),
(1617, 'Southern Philippines Methodist Colleges', 'Region XII - SOCCSKSARGEN'),
(1618, 'Southern Technological Institute Of The Philippines, Inc.', 'CARAGA'),
(1619, 'Southland College of Kabankalan City', 'Negros Island Region (NIR)'),
(1620, 'Southpoint College of Arts and Technology', 'Region XII - SOCCSKSARGEN'),
(1621, 'Southville International School and Colleges', 'National Capital Region (NCR)'),
(1622, 'Southway College Of Technology', 'CARAGA'),
(1623, 'Southwestern Institute of Business and Technology', 'Region IVB - MIMAROPA'),
(1624, 'Southwestern University', 'Region VII - Central Visayas'),
(1625, 'Southwestern University-Matias H.Aznar Memorial College of Medicine, Inc.', 'Region VII - Central Visayas'),
(1626, 'Speed Computer College', 'Region V - Bicol Region'),
(1627, 'St. Alexius College, Inc.', 'Region XII - SOCCSKSARGEN'),
(1628, 'St. Anne College Lucena, Inc.', 'Region IV - CALABARZON'),
(1629, 'St. Anne College of Iloilo', 'Region VI - Western Visayas'),
(1630, 'St. Anthony College', 'Region IVB - MIMAROPA'),
(1631, 'St. Anthony College of Technology', 'Region III - Central Luzon'),
(1632, 'St. Anthony’s College of Antique', 'Region VI - Western Visayas'),
(1633, 'St. Anthony’s College of Roxas City', 'Region VI - Western Visayas'),
(1634, 'St. Augustine Colleges Foundation, Inc.', 'Region III - Central Luzon'),
(1635, 'St. Augustine School of Nursing Lipa', 'Region IV - CALABARZON'),
(1636, 'St. Augustine School of Nursing Lucena', 'Region IV - CALABARZON'),
(1637, 'St. Benedict College of Cotabato', 'Region XII - SOCCSKSARGEN'),
(1638, 'St. Benilde Center For Global Competence, Inc.', 'Region III - Central Luzon'),
(1639, 'St. Bernadette of Lourdes College', 'National Capital Region (NCR)'),
(1640, 'St. Camillus College of Manaoag Foundation, Inc.', 'Region I - Ilocos Region'),
(1641, 'St. Cecilia’s College-Cebu', 'Region VII - Central Visayas'),
(1642, 'St. Clare College of Caloocan', 'National Capital Region (NCR)'),
(1643, 'St. Constantine Institute of Science and Technology', 'Region IV - CALABARZON'),
(1644, 'St. Dominic College of Asia', 'Region IV - CALABARZON'),
(1645, 'St. Dominic Institute', 'National Capital Region (NCR)'),
(1646, 'St. Elizabeth Global Skills Institute, Inc.', 'Region III - Central Luzon'),
(1647, 'St. Ferdinand College-Ilagan', 'Region II - Cagayan Valley'),
(1648, 'St. Francis de Sales Theological Seminary', 'Region IV - CALABARZON'),
(1649, 'St. Francis Xavier College Seminary', 'Region XI - Davao Region'),
(1650, 'St. John Bosco College of Northern Luzon', 'Region I - Ilocos Region'),
(1651, 'St. John College of Buug Foundation, Inc.', 'Region IX - Zamboanga Peninsula'),
(1652, 'St. John Colleges', 'Region IV - CALABARZON'),
(1653, 'St. John Mary Vianney Seminary', 'Negros Island Region (NIR)'),
(1654, 'St. John of Beverly School', 'National Capital Region (NCR)'),
(1655, 'St. Joseph College Cavite City', 'Region IV - CALABARZON'),
(1656, 'St. Joseph College of Bulacan, Inc.', 'Region III - Central Luzon'),
(1657, 'St. Joseph College Rosario, Batangas', 'Region IV - CALABARZON'),
(1658, 'St. Joseph College-Olongapo Inc.', 'Region III - Central Luzon'),
(1659, 'St. Joseph Seminary College', 'Region VII - Central Visayas'),
(1660, 'St. Joseph’s College of Balanga City, Inc.', 'Region III - Central Luzon'),
(1661, 'St. Joseph’s College of Rodriguez', 'Region IV - CALABARZON'),
(1662, 'St. Joseph’s College, Quezon City', 'National Capital Region (NCR)'),
(1663, 'St. Jude College', 'National Capital Region (NCR)'),
(1664, 'St. Jude College-Dasmarinas', 'Region IV - CALABARZON'),
(1665, 'St. Jude Thaddeus Institute of Technology', 'CARAGA'),
(1666, 'St. Luke’s College of Medicine William H. Quasha Memorial', 'National Capital Region (NCR)'),
(1667, 'St. Luke’s Institute', 'Region XII - SOCCSKSARGEN'),
(1668, 'St. Mary Magdalene College of Laguna', 'Region IV - CALABARZON'),
(1669, 'St. Mary’s College', 'National Capital Region (NCR)'),
(1670, 'St. Mary’s College of Baganga', 'Region XI - Davao Region'),
(1671, 'St. Mary’s College of Baliuag', 'Region III - Central Luzon'),
(1672, 'St. Mary’s College of Labason, Inc.', 'Region IX - Zamboanga Peninsula'),
(1673, 'St. Mary’s College of Marinduque', 'Region IVB - MIMAROPA'),
(1674, 'St. Mary’s College of Meycauayan', 'Region III - Central Luzon'),
(1675, 'St. Mary’s College of Tagum', 'Region XI - Davao Region'),
(1676, 'St. Mary’s Maternity and Children Hospital College', 'Region X - Northern Mindanao'),
(1677, 'St. Mary’s of Bansalan College', 'Region XI - Davao Region'),
(1678, 'St. Marys College Of Borongan, Inc.', 'Region VIII - Eastern Visayas'),
(1679, 'St. Marys College Of Catbalogan', 'Region VIII - Eastern Visayas'),
(1680, 'St. Matthew College', 'Region IV - CALABARZON'),
(1681, 'St. Michael’s College', 'Region X - Northern Mindanao'),
(1682, 'St. Nicolas College of Business And Technology, Inc.', 'Region III - Central Luzon'),
(1683, 'St. Paul College Foundation Inc. – Mandaue', 'Region VII - Central Visayas'),
(1684, 'St. Paul College Foundation Inc.- Bulacao', 'Region VII - Central Visayas'),
(1685, 'St. Paul College of Arts And Sciences, Inc.', 'Region III - Central Luzon'),
(1686, 'St. Paul College of Ilocos Sur', 'Region I - Ilocos Region'),
(1687, 'St. Paul College of Technology', 'Region III - Central Luzon'),
(1688, 'St. Paul University at San Miguel', 'Region III - Central Luzon'),
(1689, 'St. Paul University Dumaguete', 'Region VII - Central Visayas'),
(1690, 'St. Paul University Iloilo', 'Region VI - Western Visayas'),
(1691, 'St. Paul University Surigao', 'CARAGA'),
(1692, 'St. Paul University, Manila', 'National Capital Region (NCR)'),
(1693, 'St. Paul University, Quezon City', 'National Capital Region (NCR)'),
(1694, 'St. Peter’s College of Toril', 'Region XI - Davao Region'),
(1695, 'St. Peter’s College-Balingasag', 'Region X - Northern Mindanao'),
(1696, 'St. Peter’s College-Iligan', 'Region X - Northern Mindanao'),
(1697, 'St. Peters College Of Ormoc, Inc.', 'Region VIII - Eastern Visayas'),
(1698, 'St. Rita’s College of Balingasag', 'Region X - Northern Mindanao'),
(1699, 'St. Rose College Educational Foundation, Inc.', 'Region III - Central Luzon'),
(1700, 'St. Scholastica’s College', 'National Capital Region (NCR)'),
(1701, 'St. Theresa’s College', 'Region VII - Central Visayas'),
(1702, 'St. Therese of the Child Jesus Institute of Arts and Sciences', 'National Capital Region (NCR)'),
(1703, 'St. Therese-MTC College-La Fiesta', 'Region VI - Western Visayas'),
(1704, 'St. Therese-MTC College-Magdalo', 'Region VI - Western Visayas'),
(1705, 'St. Therese-MTC College-Tigbauan', 'Region VI - Western Visayas'),
(1706, 'St. Thomas More College', 'Region III - Central Luzon'),
(1707, 'St. Thomas More School of Law And Business', 'Region XI - Davao Region'),
(1708, 'St. Vincenct Ferrer Seminary', 'Region VI - Western Visayas'),
(1709, 'St. Vincent College of Science and Technology', 'Region VI - Western Visayas'),
(1710, 'St. Vincent de Ferrer College of Camarin', 'National Capital Region (NCR)'),
(1711, 'St.Vincent College of Business and Accountancy', 'Region VI - Western Visayas'),
(1712, 'Sta. Cecilia College', 'National Capital Region (NCR)'),
(1713, 'Sta. Cruz Mission School Inc.', 'Region XII - SOCCSKSARGEN'),
(1714, 'Sta. Elena (Camarines Norte) College', 'Region V - Bicol Region'),
(1715, 'Sta. Teresa College', 'Region IV - CALABARZON'),
(1716, 'Sta. Veronica College', 'Region I - Ilocos Region'),
(1717, 'Stella Maris College', 'Region X - Northern Mindanao'),
(1718, 'STI College – Alabang', 'National Capital Region (NCR)'),
(1719, 'STI College – Angeles', 'Region III - Central Luzon'),
(1720, 'STI College – Bacolod', 'Negros Island Region (NIR)'),
(1721, 'STI College – Baguio', 'Cordillera Administrative Region (CAR)'),
(1722, 'STI College – Balagtas', 'Region III - Central Luzon'),
(1723, 'STI College – Baliuag', 'Region III - Central Luzon'),
(1724, 'STI College – Bohol (formerly STI College Tagbilaran)', 'Region VII - Central Visayas'),
(1725, 'STI College – Cagayan de Oro', 'Region X - Northern Mindanao'),
(1726, 'STI College – Caloocan', 'National Capital Region (NCR)'),
(1727, 'STI College – Cebu', 'Region VII - Central Visayas'),
(1728, 'STI College – Cotabato', 'Region XII - SOCCSKSARGEN'),
(1729, 'STI College – Cubao', 'National Capital Region (NCR)'),
(1730, 'STI College – Dagupan', 'Region I - Ilocos Region'),
(1731, 'STI College – Dipolog', 'Region IX - Zamboanga Peninsula'),
(1732, 'STI College – Fairview', 'National Capital Region (NCR)'),
(1733, 'STI College – General Santos City', 'Region XII - SOCCSKSARGEN'),
(1734, 'STI College – Global City', 'National Capital Region (NCR)'),
(1735, 'STI College – Iloilo', 'Region VI - Western Visayas'),
(1736, 'STI College – Kalibo', 'Region VI - Western Visayas'),
(1737, 'STI College – Koronadal', 'Region XII - SOCCSKSARGEN'),
(1738, 'STI College – La Union', 'Region I - Ilocos Region'),
(1739, 'STI College – Las Piñas', 'National Capital Region (NCR)'),
(1740, 'STI College – Makati', 'National Capital Region (NCR)'),
(1741, 'STI College – Malolos', 'Region III - Central Luzon'),
(1742, 'STI College – Meycauayan', 'Region III - Central Luzon'),
(1743, 'STI College – Muñoz-EDSA', 'National Capital Region (NCR)'),
(1744, 'STI College – Naga', 'Region V - Bicol Region'),
(1745, 'STI College – Novaliches', 'National Capital Region (NCR)'),
(1746, 'STI College – Ormoc', 'Region VIII - Eastern Visayas'),
(1747, 'STI College – Parañaque', 'National Capital Region (NCR)'),
(1748, 'STI College – Puerto Princesa', 'Region IVB - MIMAROPA'),
(1749, 'STI College – Quezon Avenue', 'National Capital Region (NCR)'),
(1750, 'STI College – San Fernando', 'Region III - Central Luzon'),
(1751, 'STI College – San Jose', 'Region III - Central Luzon'),
(1752, 'STI College – Shaw', 'National Capital Region (NCR)'),
(1753, 'STI College – Sta. Maria', 'Region III - Central Luzon'),
(1754, 'STI College – Surigao', 'CARAGA'),
(1755, 'STI College – Tacurong', 'Region XII - SOCCSKSARGEN'),
(1756, 'STI College – Taft', 'National Capital Region (NCR)'),
(1757, 'STI College – Tarlac', 'Region III - Central Luzon'),
(1758, 'STI College – Vigan', 'Region I - Ilocos Region'),
(1759, 'STI College Bacoor', 'Region IV - CALABARZON'),
(1760, 'STI College Balayan', 'Region IV - CALABARZON'),
(1761, 'STI College Batangas', 'Region IV - CALABARZON'),
(1762, 'STI College Calamba', 'Region IV - CALABARZON'),
(1763, 'STI College Dasmarinas', 'Region IV - CALABARZON'),
(1764, 'STI College Davao', 'Region XI - Davao Region'),
(1765, 'STI College Dumaguete', 'Negros Island Region (NIR)'),
(1766, 'STI College Lipa', 'Region IV - CALABARZON'),
(1767, 'STI College Lucena', 'Region IV - CALABARZON'),
(1768, 'STI College Ortigas Cainta', 'Region IV - CALABARZON'),
(1769, 'STI College Pagadian', 'Region IX - Zamboanga Peninsula'),
(1770, 'STI College Rosario', 'Region IV - CALABARZON'),
(1771, 'STI College San Pablo', 'Region IV - CALABARZON'),
(1772, 'STI College Sta. Cruz', 'Region IV - CALABARZON'),
(1773, 'STI College Sta. Rosa', 'Region IV - CALABARZON'),
(1774, 'STI College Tagaytay', 'Region IV - CALABARZON'),
(1775, 'STI College Tanauan', 'Region IV - CALABARZON'),
(1776, 'STI College Tanay', 'Region IV - CALABARZON'),
(1777, 'STI College Zamboanga', 'Region IX - Zamboanga Peninsula'),
(1778, 'STI College-Legazpi', 'Region V - Bicol Region'),
(1779, 'STI College-Marikina', 'National Capital Region (NCR)'),
(1780, 'STI College-Recto', 'National Capital Region (NCR)'),
(1781, 'STI College-Tuguegarao', 'Region II - Cagayan Valley'),
(1782, 'STI eCollege Southwoods', 'Region IV - CALABARZON'),
(1783, 'STI Education Services Group', 'Region I - Ilocos Region'),
(1784, 'STI West Negros University', 'Negros Island Region (NIR)'),
(1785, 'Sto. Nino College Of Ormoc, Inc.', 'Region VIII - Eastern Visayas'),
(1786, 'Sto. Niño Seminary', 'Region VI - Western Visayas'),
(1787, 'Sto. Rosario Sapang Palay College', 'Region III - Central Luzon'),
(1788, 'Sto. Tomas College', 'Region VII - Central Visayas'),
(1789, 'Stratford International School', 'Region XII - SOCCSKSARGEN'),
(1790, 'Subic Bay College, Inc.', 'Region III - Central Luzon'),
(1791, 'Sultan Kudarat Educational Institution', 'Region XII - SOCCSKSARGEN'),
(1792, 'Sultan Kudarat State University', 'Region XII - SOCCSKSARGEN'),
(1793, 'Sultan Kudarat State University – Bagumbayan Campus', 'Region XII - SOCCSKSARGEN'),
(1794, 'Sultan Kudarat State University – Glan Campus', 'Region XII - SOCCSKSARGEN'),
(1795, 'Sultan Kudarat State University – Isulan Campus', 'Region XII - SOCCSKSARGEN'),
(1796, 'Sultan Kudarat State University – Kalamansig Campus', 'Region XII - SOCCSKSARGEN'),
(1797, 'Sultan Kudarat State University – Lutayan Campus', 'Region XII - SOCCSKSARGEN'),
(1798, 'Sultan Kudarat State University – Palimbang Campus', 'Region XII - SOCCSKSARGEN'),
(1799, 'Sultan Kudarat State University – SNA Campus', 'Region XII - SOCCSKSARGEN'),
(1800, 'Sultan Kudarat State University – Sunas Campus', 'Region XII - SOCCSKSARGEN'),
(1801, 'Sultan Kudarat State University – Tacurong Campus', 'Region XII - SOCCSKSARGEN'),
(1802, 'Sumulong College of Arts and Sciences', 'Region IV - CALABARZON'),
(1803, 'Surigao Del Sur State University – Cagwait Campus', 'CARAGA'),
(1804, 'Surigao Del Sur State University – Cantilan Campus', 'CARAGA'),
(1805, 'Surigao Del Sur State University – Lianga Campus', 'CARAGA'),
(1806, 'Surigao Del Sur State University – Main', 'CARAGA'),
(1807, 'Surigao Del Sur State University – San Miguel Campus', 'CARAGA'),
(1808, 'Surigao Del Sur State University – Tagbina Campus', 'CARAGA'),
(1809, 'Surigao Doctors’ College', 'CARAGA'),
(1810, 'Surigao Education Center', 'CARAGA'),
(1811, 'Surigao State College Of Technology – Del Carmen Campus', 'CARAGA'),
(1812, 'Surigao State College Of Technology – Main', 'CARAGA'),
(1813, 'Surigao State College Of Technology – Mainit Campus', 'CARAGA'),
(1814, 'Surigao State College Of Technology – Malimono Campus', 'CARAGA'),
(1815, 'Surigao Sur Colleges, Inc.', 'CARAGA'),
(1816, 'System Plus Computer College-Caloocan', 'National Capital Region (NCR)'),
(1817, 'Systems Plus College Foundation, Inc.', 'Region III - Central Luzon'),
(1818, 'Tabaco College', 'Region V - Bicol Region'),
(1819, 'Tabor Hills College', 'Region VII - Central Visayas'),
(1820, 'Tagum City College Of Science And Technology Foundation', 'Region XI - Davao Region'),
(1821, 'Tagum Doctors College', 'Region XI - Davao Region'),
(1822, 'Tan Ting Bing Memorial Colleges Foundation Incorporated', 'Region VIII - Eastern Visayas'),
(1823, 'Tanauan City College', 'Region IV - CALABARZON'),
(1824, 'Tanauan Institute', 'Region IV - CALABARZON'),
(1825, 'Tanchuling College', 'Region V - Bicol Region'),
(1826, 'Tañon College', 'Negros Island Region (NIR)'),
(1827, 'Tarlac Agricultural University (Formerly: Tarlac College of Agriculture)', 'Region III - Central Luzon'),
(1828, 'Tarlac Christian Colleges, Inc.', 'Region III - Central Luzon'),
(1829, 'Tarlac State University – Main Campus', 'Region III - Central Luzon'),
(1830, 'Tarlac State University – San Isidro Campus', 'Region III - Central Luzon'),
(1831, 'Tayabas Western Academy', 'Region IV - CALABARZON'),
(1832, 'Tecarro College Foundation', 'Region XI - Davao Region'),
(1833, 'Technological Institute of the Philippines, Manila', 'National Capital Region (NCR)'),
(1834, 'Technological Institute of the Philippines, Quezon City', 'National Capital Region (NCR)'),
(1835, 'Technological University of the Philippines Cavite', 'Region IV - CALABARZON'),
(1836, 'Technological University of the Philippines, Visayas-Talisay', 'Negros Island Region (NIR)'),
(1837, 'Teodoro M. Luansing College of Rosario', 'Region IV - CALABARZON'),
(1838, 'The Adelphi College', 'Region I - Ilocos Region'),
(1839, 'The Bearer Of Light & Wisdom Colleges', 'Region IV - CALABARZON'),
(1840, 'The College Of Maasin', 'Region VIII - Eastern Visayas'),
(1841, 'The Family Clinic, Inc', 'National Capital Region (NCR)'),
(1842, 'The Fisher Valley College', 'National Capital Region (NCR)'),
(1843, 'The Good Samaritan Colleges, Inc.', 'Region III - Central Luzon'),
(1844, 'The Great Plebeian College', 'Region I - Ilocos Region'),
(1845, 'The Lewis College', 'Region V - Bicol Region'),
(1846, 'The Manila Times College', 'National Capital Region (NCR)'),
(1847, 'The Manila Times College of Subic, Inc.', 'Region III - Central Luzon'),
(1848, 'The National Teacher’s College', 'National Capital Region (NCR)'),
(1849, 'The New El Salvador College', 'Region X - Northern Mindanao'),
(1850, 'The One Nation Entrepreneur School', 'National Capital Region (NCR)'),
(1851, 'The Philippine Women’s University, Manila', 'National Capital Region (NCR)'),
(1852, 'The Philippine Women’s University, Quezon City', 'National Capital Region (NCR)'),
(1853, 'The University of Manila', 'National Capital Region (NCR)'),
(1854, 'Tomas Claudio Colleges', 'Region IV - CALABARZON'),
(1855, 'Tomas Del Rosario College', 'Region III - Central Luzon'),
(1856, 'Trace College', 'Region IV - CALABARZON'),
(1857, 'Trade-Tech International Science Institute', 'Region VII - Central Visayas'),
(1858, 'Treston International College', 'National Capital Region (NCR)'),
(1859, 'Trimex College', 'Region IV - CALABARZON'),
(1860, 'Trinity University of Asia', 'National Capital Region (NCR)'),
(1861, 'UBIX Institute of Technology, Inc.', 'National Capital Region (NCR)'),
(1862, 'UM Bansalan College', 'Region XI - Davao Region'),
(1863, 'UM Digos College', 'Region XI - Davao Region'),
(1864, 'UM Guianga College', 'Region XI - Davao Region'),
(1865, 'UM Panabo College', 'Region XI - Davao Region'),
(1866, 'UM Penaplata College', 'Region XI - Davao Region'),
(1867, 'UM Tagum College', 'Region XI - Davao Region'),
(1868, 'Unciano Colleges', 'Region IV - CALABARZON'),
(1869, 'Unida Christian College', 'Region IV - CALABARZON'),
(1870, 'Union Christian College', 'Region I - Ilocos Region'),
(1871, 'Union College of Laguna', 'Region IV - CALABARZON'),
(1872, 'United School of Science And Technology Colleges', 'Region III - Central Luzon'),
(1873, 'Universal College of Parañaque', 'National Capital Region (NCR)'),
(1874, 'Universidad de Sta. Isabel', 'Region V - Bicol Region'),
(1875, 'Universidad de Zamboanga', 'Region IX - Zamboanga Peninsula'),
(1876, 'Universidad de Zamboanga – Pagadian', 'Region IX - Zamboanga Peninsula'),
(1877, 'Universidad de Zamboanga-Ipil', 'Region IX - Zamboanga Peninsula'),
(1878, 'University of Antique, Hamtic Campus', 'Region VI - Western Visayas'),
(1879, 'University of Antique, Main Campus', 'Region VI - Western Visayas'),
(1880, 'University of Antique, Tibiao Campus', 'Region VI - Western Visayas'),
(1881, 'University of Asia and the Pacific', 'National Capital Region (NCR)'),
(1882, 'University of Baguio', 'Cordillera Administrative Region (CAR)'),
(1883, 'University of Batangas', 'Region IV - CALABARZON'),
(1884, 'University of Batangas Lipa', 'Region IV - CALABARZON'),
(1885, 'University of Bohol', 'Region VII - Central Visayas'),
(1886, 'University of Cagayan Valley', 'Region II - Cagayan Valley'),
(1887, 'University of Cebu', 'Region VII - Central Visayas'),
(1888, 'University of Cebu – Banilad Campus', 'Region VII - Central Visayas'),
(1889, 'University of Cebu – Lapu-lapu and Mandaue', 'Region VII - Central Visayas'),
(1890, 'University Of Eastern Philippines-Catubig', 'Region VIII - Eastern Visayas'),
(1891, 'University Of Eastern Philippines-Laoang', 'Region VIII - Eastern Visayas'),
(1892, 'University Of Eastern Philippines-Main', 'Region VIII - Eastern Visayas'),
(1893, 'University of Iloilo', 'Region VI - Western Visayas'),
(1894, 'University of La Salette, Santiago City, Isabela', 'Region II - Cagayan Valley'),
(1895, 'University of Luzon', 'Region I - Ilocos Region'),
(1896, 'University of Mindanao', 'Region XI - Davao Region'),
(1897, 'University of Negros Occidental – Recoletos', 'Negros Island Region (NIR)'),
(1898, 'University of Northeastern Philippines', 'Region V - Bicol Region'),
(1899, 'University of Northern Philippines', 'Region I - Ilocos Region'),
(1900, 'University of Nueva Caceres', 'Region V - Bicol Region'),
(1901, 'University of Pangasinan', 'Region I - Ilocos Region'),
(1902, 'University of Perpetual Help Dr. Jose G. Tamayo Medical University', 'Region IV - CALABARZON'),
(1903, 'University of Perpetual Help Rizal Molino Campus', 'Region IV - CALABARZON'),
(1904, 'University of Perpetual Help System Cauayan City', 'Region II - Cagayan Valley'),
(1905, 'University of Perpetual Help System Dalta', 'National Capital Region (NCR)'),
(1906, 'University of Perpetual Help System Dalta Calamba', 'Region IV - CALABARZON'),
(1907, 'University of Perpetual Help System GMA Campus', 'Region IV - CALABARZON'),
(1908, 'University of Perpetual Help System Laguna', 'Region IV - CALABARZON'),
(1909, 'University of Rizal System Antipolo', 'Region IV - CALABARZON'),
(1910, 'University of Rizal System Binangonan', 'Region IV - CALABARZON'),
(1911, 'University of Rizal System Cainta', 'Region IV - CALABARZON'),
(1912, 'University of Rizal System Cardona', 'Region IV - CALABARZON'),
(1913, 'University of Rizal System Morong', 'Region IV - CALABARZON'),
(1914, 'University of Rizal System Pililia', 'Region IV - CALABARZON'),
(1915, 'University of Rizal System Rodriguez', 'Region IV - CALABARZON'),
(1916, 'University of Rizal System Tanay', 'Region IV - CALABARZON'),
(1917, 'University of Rizal System Taytay', 'Region IV - CALABARZON'),
(1918, 'University of Rizal System-Angono', 'Region IV - CALABARZON'),
(1919, 'University of Saint Anthony', 'Region V - Bicol Region'),
(1920, 'University of Saint Louis-Tuguegarao', 'Region II - Cagayan Valley'),
(1921, 'University of San Agustin', 'Region VI - Western Visayas'),
(1922, 'University of San Carlos', 'Region VII - Central Visayas'),
(1923, 'University of San Jose-Recoletos', 'Region VII - Central Visayas'),
(1924, 'University of Santo Tomas', 'National Capital Region (NCR)'),
(1925, 'University of Southeastern Philippines-Bislig Campus', 'CARAGA'),
(1926, 'University of Southeastern Philippines-Main', 'Region XI - Davao Region'),
(1927, 'University of Southeastern Philippines-Mintal', 'Region XI - Davao Region'),
(1928, 'University of Southeastern Philippines-Tagum and Mabini Campus', 'Region XI - Davao Region'),
(1929, 'University of Southern Mindanao', 'Region XII - SOCCSKSARGEN'),
(1930, 'University of Southern Mindanao – Kidapawan City Campus', 'Region XII - SOCCSKSARGEN'),
(1931, 'University of St. La Salle', 'Negros Island Region (NIR)'),
(1932, 'University of the Assumption', 'Region III - Central Luzon'),
(1933, 'University of the Cordilleras', 'Cordillera Administrative Region (CAR)'),
(1934, 'University of the East Ramon Magsaysay Memorial Medical Center', 'National Capital Region (NCR)'),
(1935, 'University of the East, Caloocan', 'National Capital Region (NCR)'),
(1936, 'University of the East, Manila', 'National Capital Region (NCR)'),
(1937, 'University of the Immaculate Conception', 'Region XI - Davao Region'),
(1938, 'University of the Philippines – College of Cebu', 'Region VII - Central Visayas'),
(1939, 'University of the Philippines – Diliman (Olongapo City)', 'Region III - Central Luzon'),
(1940, 'University of the Philippines – Diliman (Pampanga)', 'Region III - Central Luzon'),
(1941, 'University of the Philippines – Visayas', 'Region VI - Western Visayas'),
(1942, 'University of the Philippines Baguio', 'Cordillera Administrative Region (CAR)'),
(1943, 'University of the Philippines College of Cebu', 'Region VII - Central Visayas'),
(1944, 'University of the Philippines Diliman', 'National Capital Region (NCR)'),
(1945, 'University of the Philippines Los Banos', 'Region IV - CALABARZON'),
(1946, 'University of the Philippines Manila', 'National Capital Region (NCR)'),
(1947, 'University of the Philippines Manila (Palo Leyte)', 'Region VIII - Eastern Visayas'),
(1948, 'University of the Philippines Open University', 'Region IV - CALABARZON'),
(1949, 'University Of The Philippines Visayas Tacloban College', 'Region VIII - Eastern Visayas'),
(1950, 'University of the Philippines-Mindanao', 'Region XI - Davao Region'),
(1951, 'University of the Southern Philippines Foundation', 'Region VII - Central Visayas'),
(1952, 'University of the Visayas', 'Region VII - Central Visayas'),
(1953, 'University of the Visayas – Dalaguete', 'Region VII - Central Visayas'),
(1954, 'University of the Visayas – Danao City', 'Region VII - Central Visayas'),
(1955, 'University of the Visayas – Gullas College of Medicine', 'Region VII - Central Visayas'),
(1956, 'University of the Visayas – Mandaue', 'Region VII - Central Visayas'),
(1957, 'University of the Visayas-Minglanilla', 'Region VII - Central Visayas'),
(1958, 'University of the Visayas-Toledo City Campus', 'Region VII - Central Visayas'),
(1959, 'Valencia Colleges Inc.', 'Region X - Northern Mindanao'),
(1960, 'Velez College', 'Region VII - Central Visayas'),
(1961, 'Veritas College of Irosin', 'Region V - Bicol Region'),
(1962, 'VillaFlores College', 'Region VII - Central Visayas'),
(1963, 'Villagers Montessori', 'National Capital Region (NCR)'),
(1964, 'Villamor College of Business and Arts', 'Region XII - SOCCSKSARGEN'),
(1965, 'Vineyard International Polytechnic College', 'Region X - Northern Mindanao'),
(1966, 'Virgen Milagrosa Del Rosario College Seminary', 'Region III - Central Luzon'),
(1967, 'Virgen Milagrosa University Foundation', 'Region I - Ilocos Region'),
(1968, 'Visayan Nazarene Bible College', 'Region VII - Central Visayas'),
(1969, 'Visayas Christian Institute Of Technology, Inc', 'Region VIII - Eastern Visayas'),
(1970, 'Visayas State University- Alangalang', 'Region VIII - Eastern Visayas'),
(1971, 'Visayas State University-Isabel', 'Region VIII - Eastern Visayas'),
(1972, 'Visayas State University-Main', 'Region VIII - Eastern Visayas'),
(1973, 'Visayas State University-Tolosa', 'Region VIII - Eastern Visayas'),
(1974, 'Visayas State University-Villaba', 'Region VIII - Eastern Visayas'),
(1975, 'VMA Global College & Training Center', 'Negros Island Region (NIR)'),
(1976, 'VMC Asian College Foundation, Inc.', 'Region XII - SOCCSKSARGEN'),
(1977, 'WCC Aeronautical and Technological College', 'Region I - Ilocos Region'),
(1978, 'WCC Aeronautical and Technological College – North Manila, Inc.', 'National Capital Region (NCR)'),
(1979, 'Wesleyan College of Manila', 'National Capital Region (NCR)'),
(1980, 'Wesleyan University-Philippines-Aurora', 'Region III - Central Luzon'),
(1981, 'Wesleyan University-Philippines-Cabanatuan City', 'Region III - Central Luzon'),
(1982, 'West Bay College, Inc.', 'National Capital Region (NCR)'),
(1983, 'West Celebes College of Technology, Inc', 'Region XII - SOCCSKSARGEN'),
(1984, 'West Coast College', 'Region V - Bicol Region'),
(1985, 'West Point College – Magallanes', 'Region IV - CALABARZON'),
(1986, 'West Visayas State University, Calinog Campus', 'Region VI - Western Visayas'),
(1987, 'West Visayas State University, Janiuay Campus', 'Region VI - Western Visayas'),
(1988, 'West Visayas State University, Lambunao Campus', 'Region VI - Western Visayas'),
(1989, 'West Visayas State University, Main Campus', 'Region VI - Western Visayas'),
(1990, 'West Visayas State University, Pototan Campus', 'Region VI - Western Visayas'),
(1991, 'Western Colleges', 'Region IV - CALABARZON'),
(1992, 'Western Institute of Technology', 'Region VI - Western Visayas'),
(1993, 'Western Leyte College Of Ormoc City, Inc.', 'Region VIII - Eastern Visayas'),
(1994, 'Western Mindanao Foundation College, Inc.', 'Region IX - Zamboanga Peninsula'),
(1995, 'Western Mindanao State University', 'Region IX - Zamboanga Peninsula'),
(1996, 'Western Mindanao State University Alicia External Studies Unit', 'Region IX - Zamboanga Peninsula'),
(1997, 'Western Mindanao State University Aurora External Studies Unit', 'Region IX - Zamboanga Peninsula'),
(1998, 'Western Mindanao State University Curuan External Studies Unit', 'Region IX - Zamboanga Peninsula'),
(1999, 'Western Mindanao State University Diplahan External Studies Unit', 'Region IX - Zamboanga Peninsula'),
(2000, 'Western Mindanao State University Imelda External Studies Unit', 'Region IX - Zamboanga Peninsula'),
(2001, 'Western Mindanao State University Ipil External Studies Unit', 'Region IX - Zamboanga Peninsula'),
(2002, 'Western Mindanao State University Mabuhay External Studies Unit', 'Region IX - Zamboanga Peninsula'),
(2003, 'Western Mindanao State University Malangas External Studies Unit', 'Region IX - Zamboanga Peninsula'),
(2004, 'Western Mindanao State University Molave External Studies Unit', 'Region IX - Zamboanga Peninsula'),
(2005, 'Western Mindanao State University Naga External Studies Unit', 'Region IX - Zamboanga Peninsula'),
(2006, 'Western Mindanao State University Olutanga External Studies Unit', 'Region IX - Zamboanga Peninsula'),
(2007, 'Western Mindanao State University Pagadian External Studies Unit', 'Region IX - Zamboanga Peninsula'),
(2008, 'Western Mindanao State University Siay External Studies Unit', 'Region IX - Zamboanga Peninsula'),
(2009, 'Western Mindanao State University Tungawan External Studies Unit', 'Region IX - Zamboanga Peninsula'),
(2010, 'Western Philippines University – Basuanga', 'Region IVB - MIMAROPA'),
(2011, 'Western Philippines University – Culion', 'Region IVB - MIMAROPA'),
(2012, 'Western Philippines University – El Nido', 'Region IVB - MIMAROPA'),
(2013, 'Western Philippines University – Main', 'Region IVB - MIMAROPA'),
(2014, 'Western Philippines University – Puerto Princesa', 'Region IVB - MIMAROPA');
INSERT INTO `school` (`school_id`, `name`, `description`) VALUES
(2015, 'Western Philippines University – Quezon', 'Region IVB - MIMAROPA'),
(2016, 'Western Philippines University – Rio Tuba', 'Region IVB - MIMAROPA'),
(2017, 'Westfields International College, Inc.', 'Region III - Central Luzon'),
(2018, 'Westmead International School', 'Region IV - CALABARZON'),
(2019, 'World Citi Colleges', 'Region IV - CALABARZON'),
(2020, 'World Citi Colleges-Guimba, Inc.', 'Region III - Central Luzon'),
(2021, 'World Citi Colleges-Quezon City', 'National Capital Region (NCR)'),
(2022, 'Worldtech Resources Foundation-Naga City', 'Region V - Bicol Region'),
(2023, 'Xavier University', 'Region X - Northern Mindanao'),
(2024, 'Xijen College of Mt. Province', 'Cordillera Administrative Region (CAR)'),
(2025, 'Yllana Bay View College', 'Region IX - Zamboanga Peninsula'),
(2026, 'Young Ji College', 'Region IV - CALABARZON'),
(2027, 'Zamboanga City State Polytechnic College', 'Region IX - Zamboanga Peninsula'),
(2028, 'Zamboanga Del Sur Maritime Institute of Technology', 'Region IX - Zamboanga Peninsula'),
(2029, 'Zamboanga State College of Marine Sciences and Technology', 'Region IX - Zamboanga Peninsula'),
(2030, 'Zamora Memorial College', 'Region V - Bicol Region');

-- --------------------------------------------------------

--
-- Table structure for table `site`
--

CREATE TABLE `site` (
  `site_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `system_log`
--

CREATE TABLE `system_log` (
  `entry_id` bigint(20) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `user` varchar(255) NOT NULL,
  `datetime` datetime NOT NULL,
  `action` mediumtext NOT NULL,
  `module` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_log`
--

INSERT INTO `system_log` (`entry_id`, `ip_address`, `user`, `datetime`, `action`, `module`) VALUES
(1, '::1', 'root', '2018-03-09 10:45:02', 'Logged in', '/smic_recruitment/login.php'),
(2, '::1', 'root', '2018-03-09 10:47:53', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php'),
(3, '::1', 'root', '2018-03-09 10:47:58', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php'),
(4, '::1', 'root', '2018-03-09 10:48:08', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(5, '::1', 'root', '2018-04-13 09:03:29', 'Logged in', '/smic_recruitment/login.php'),
(6, '::1', 'root', '2018-04-13 09:05:07', 'Logged out', '/smic_recruitment/end.php'),
(7, '::1', 'root', '2018-04-13 09:06:13', 'Logged in', '/smic_recruitment/login.php'),
(8, '::1', 'root', '2018-04-13 09:07:47', 'Logged out', '/smic_recruitment/end.php'),
(9, '::1', 'root', '2018-05-04 08:33:47', 'Logged in', '/smic_recruitment/login.php'),
(10, '::1', 'root', '2018-05-04 08:34:02', 'Logged out', '/smic_recruitment/end.php'),
(11, '::1', 'root', '2018-05-04 09:04:32', 'Logged in', '/smic_recruitment/login.php'),
(12, '::1', 'root', '2018-05-04 09:07:40', 'Logged out', '/smic_recruitment/end.php'),
(13, '::1', 'root', '2018-05-04 09:15:28', 'Logged in', '/smic_recruitment/login.php'),
(14, '::1', 'root', '2018-05-04 09:16:19', 'Pressed submit button', '/smic_recruitment/sysadmin/add_user_passport_groups.php'),
(15, '::1', 'root', '2018-05-04 09:16:20', 'Query Executed: INSERT INTO user_passport_groups(passport_group, priority, icon) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => Personal Information\n    [2] => 1\n    [3] => blue_folder3.png\n)\n', '/smic_recruitment/sysadmin/add_user_passport_groups.php'),
(16, '::1', 'root', '2018-05-04 09:17:01', 'Pressed submit button', '/smic_recruitment/sysadmin/add_user_role.php'),
(17, '::1', 'root', '2018-05-04 09:17:02', 'Query Executed: INSERT INTO user_role(role, description) VALUES(?,?)\r\nArray\n(\n    [0] => ss\n    [1] => Applicant\n    [2] => Applicant\n)\n', '/smic_recruitment/sysadmin/add_user_role.php'),
(18, '::1', 'root', '2018-05-04 09:19:16', 'Query Executed: DELETE FROM user_role_links WHERE role_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(19, '::1', 'root', '2018-05-04 09:19:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 45\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(20, '::1', 'root', '2018-05-04 09:19:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 49\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(21, '::1', 'root', '2018-05-04 09:19:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 53\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(22, '::1', 'root', '2018-05-04 09:19:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 57\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(23, '::1', 'root', '2018-05-04 09:19:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 61\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(24, '::1', 'root', '2018-05-04 09:19:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 65\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(25, '::1', 'root', '2018-05-04 09:19:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 69\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(26, '::1', 'root', '2018-05-04 09:19:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 73\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(27, '::1', 'root', '2018-05-04 09:19:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 77\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(28, '::1', 'root', '2018-05-04 09:19:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 43\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(29, '::1', 'root', '2018-05-04 09:19:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 47\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(30, '::1', 'root', '2018-05-04 09:19:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 51\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(31, '::1', 'root', '2018-05-04 09:19:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 55\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(32, '::1', 'root', '2018-05-04 09:19:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 59\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(33, '::1', 'root', '2018-05-04 09:19:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 63\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(34, '::1', 'root', '2018-05-04 09:19:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 67\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(35, '::1', 'root', '2018-05-04 09:19:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 71\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(36, '::1', 'root', '2018-05-04 09:19:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 75\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(37, '::1', 'root', '2018-05-04 09:19:19', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 79\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(38, '::1', 'root', '2018-05-04 09:19:19', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 42\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(39, '::1', 'root', '2018-05-04 09:19:19', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 46\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(40, '::1', 'root', '2018-05-04 09:19:19', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 50\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(41, '::1', 'root', '2018-05-04 09:19:19', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 54\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(42, '::1', 'root', '2018-05-04 09:19:19', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 58\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(43, '::1', 'root', '2018-05-04 09:19:19', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 62\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(44, '::1', 'root', '2018-05-04 09:19:19', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 66\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(45, '::1', 'root', '2018-05-04 09:19:19', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 70\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(46, '::1', 'root', '2018-05-04 09:19:19', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 74\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(47, '::1', 'root', '2018-05-04 09:19:20', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 78\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(48, '::1', 'root', '2018-05-04 09:19:46', 'Pressed submit button', '/smic_recruitment/sysadmin/add_user.php'),
(49, '::1', 'root', '2018-05-04 09:19:47', 'Query Executed: INSERT INTO user(username, password, salt, iteration, method, person_id, role_id, skin_id, user_level) VALUES(?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssss\n    [1] => applicant1\n    [2] => $2y$12$pbtkoqAce50nobiS2VtjxeFv1Az6kgN7bDZn5.I9pLkXBOgsSSJ.W\n    [3] => pbtkoqAce50nobiS2Vtjxg\n    [4] => 12\n    [5] => bcrypt\n    [6] => 1\n    [7] => 3\n    [8] => 1\n    [9] => 2\n)\n', '/smic_recruitment/sysadmin/add_user.php'),
(50, '::1', 'root', '2018-05-04 09:19:47', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => applicant1\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/add_user.php'),
(51, '::1', 'root', '2018-05-04 09:19:53', 'Logged out', '/smic_recruitment/end.php'),
(52, '::1', 'applicant1', '2018-05-04 09:20:00', 'Logged in', '/smic_recruitment/login.php'),
(53, '::1', 'applicant1', '2018-05-04 09:20:31', 'Logged out', '/smic_recruitment/end.php'),
(54, '::1', 'root', '2018-05-04 09:20:35', 'Logged in', '/smic_recruitment/login.php'),
(55, '::1', 'root', '2018-05-04 09:21:10', 'Pressed cancel button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(56, '::1', 'root', '2018-05-04 09:25:20', 'Logged out', '/smic_recruitment/end.php'),
(57, '::1', 'root', '2018-05-04 09:25:24', 'Logged in', '/smic_recruitment/login.php'),
(58, '::1', 'root', '2018-05-04 09:25:27', 'Logged out', '/smic_recruitment/end.php'),
(59, '::1', 'applicant1', '2018-05-04 09:25:32', 'Logged in', '/smic_recruitment/login.php'),
(60, '::1', 'applicant1', '2018-05-04 09:25:48', 'Logged out', '/smic_recruitment/end.php'),
(61, '::1', 'applicant1', '2018-05-04 09:28:31', 'Logged in', '/smic_recruitment/login.php'),
(62, '::1', 'applicant1', '2018-05-04 09:34:49', 'Logged out', '/smic_recruitment/end.php'),
(63, '::1', 'applicant1', '2018-05-04 12:44:12', 'Logged in', '/smic_recruitment/login.php'),
(64, '::1', 'root', '2018-05-08 16:07:49', 'Logged in', '/smic_recruitment/login.php'),
(65, '::1', 'root', '2018-05-08 16:41:32', 'Pressed cancel button', '/smic_recruitment/sysadmin/detailview_user_role.php'),
(66, '::1', 'root', '2018-05-08 16:45:19', 'Logged out', '/smic_recruitment/end.php'),
(67, '::1', 'Not Logged In', '2018-05-08 16:47:15', 'Query Executed: INSERT INTO user(personal_email, password, salt, iteration, method, role_id, skin_id, user_level, applicant_id, is_verified) VALUES(?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => 12345678\n    [3] => QPDf/7t6uiA1PC94g150YA\n    [4] => 12\n    [5] => bcrypt\n    [6] => 3\n    [7] => 1\n    [8] => 5\n    [9] => 5\n    [10] => Yes\n)\n', '/smic_recruitment/applicant_registration.php'),
(68, '::1', 'Not Logged In', '2018-05-08 16:47:55', 'Query Executed: INSERT INTO user(personal_email, password, salt, iteration, method, role_id, skin_id, user_level, applicant_id, is_verified) VALUES(?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => $2y$12$uAvQvjM8kfp/66emw2NnlOlSzQuJpoDUUE4pDJ1QinvwyCjQVkNEG\n    [3] => uAvQvjM8kfp/66emw2NnlQ\n    [4] => 12\n    [5] => bcrypt\n    [6] => 3\n    [7] => 1\n    [8] => 5\n    [9] => 5\n    [10] => Yes\n)\n', '/smic_recruitment/applicant_registration.php'),
(69, '::1', 'root', '2018-05-09 06:35:24', 'Logged in', '/smic_recruitment/login.php'),
(70, '::1', 'root', '2018-05-09 06:35:42', 'Logged out', '/smic_recruitment/end.php'),
(71, '::1', 'root', '2018-05-09 06:35:45', 'Logged in', '/smic_recruitment/login.php'),
(72, '::1', 'root', '2018-05-09 06:35:53', 'Logged out', '/smic_recruitment/end.php'),
(73, '::1', 'Not Logged In', '2018-05-09 06:36:24', 'Query Executed: INSERT INTO user(personal_email, password, salt, iteration, method, role_id, skin_id, user_level, applicant_id, is_verified) VALUES(?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => $2y$12$gPMxnov0YbP/rJ6NRJuPXuAVO94D.8fMzs2/3.JYyxC7baDm/BXKi\n    [3] => gPMxnov0YbP/rJ6NRJuPXw\n    [4] => 12\n    [5] => bcrypt\n    [6] => 3\n    [7] => 1\n    [8] => 5\n    [9] => 5\n    [10] => Yes\n)\n', '/smic_recruitment/applicant_registration.php'),
(74, '::1', 'root', '2018-05-09 06:37:12', 'Logged in', '/smic_recruitment/login.php'),
(75, '::1', 'root', '2018-05-09 06:37:17', 'Logged out', '/smic_recruitment/end.php'),
(76, '::1', 'root', '2018-05-09 06:47:09', 'Logged in', '/smic_recruitment/login.php'),
(77, '::1', 'root', '2018-05-09 06:47:53', 'Pressed cancel button', '/smic_recruitment/sysadmin/add_user.php'),
(78, '::1', 'root', '2018-05-09 06:48:23', 'Pressed submit button', '/smic_recruitment/sysadmin/add_user.php'),
(79, '::1', 'root', '2018-05-09 06:48:30', 'Pressed submit button', '/smic_recruitment/sysadmin/add_user.php'),
(80, '::1', 'root', '2018-05-09 06:48:50', 'Logged out', '/smic_recruitment/end.php'),
(81, '::1', '', '2018-05-09 06:53:26', 'Logged in', '/smic_recruitment/login.php'),
(82, '::1', '', '2018-05-09 06:53:43', 'Logged out', '/smic_recruitment/end.php'),
(83, '::1', 'root', '2018-05-09 06:55:28', 'Logged in', '/smic_recruitment/login.php'),
(84, '::1', 'root', '2018-05-09 06:55:35', 'Logged out', '/smic_recruitment/end.php'),
(85, '::1', 'root', '2018-05-09 06:55:39', 'Logged in', '/smic_recruitment/login.php'),
(86, '::1', 'root', '2018-05-09 06:55:47', 'Logged out', '/smic_recruitment/end.php'),
(87, '::1', 'root', '2018-05-09 06:55:50', 'Logged in', '/smic_recruitment/login.php'),
(88, '::1', 'root', '2018-05-09 06:57:42', 'Logged out', '/smic_recruitment/end.php'),
(89, '::1', 'Not Logged In', '2018-05-09 06:58:50', 'Query Executed: INSERT INTO user(username, password, salt, iteration, method, role_id, skin_id, user_level, applicant_id, is_verified) VALUES(?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => $2y$12$hMt66HPLrOO9Ag4XN4Ul0.0A1cJnxA9Zbxx8ZX31EhaorC.suypMm\n    [3] => hMt66HPLrOO9Ag4XN4Ul0A\n    [4] => 12\n    [5] => bcrypt\n    [6] => 3\n    [7] => 1\n    [8] => 5\n    [9] => 5\n    [10] => Yes\n)\n', '/smic_recruitment/applicant_registration.php'),
(90, '::1', 'markdanico.fernandez@gmail.com', '2018-05-09 06:59:11', 'Logged in', '/smic_recruitment/login.php'),
(91, '::1', 'markdanico.fernandez@gmail.com', '2018-05-09 06:59:45', 'Logged out', '/smic_recruitment/end.php'),
(92, '::1', 'Not Logged In', '2018-05-09 07:01:42', 'Query Executed: INSERT INTO user(username, personal_email, password, salt, iteration, method, role_id, skin_id, user_level, applicant_id, is_verified) VALUES(?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssssss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => markdanico.fernandez@gmail.com\n    [3] => $2y$12$zCyytR1/.d9KhnyXj6qg0ulbiFEcPrjNgNrAxgvSCZs9tm8v9azmy\n    [4] => zCyytR1/+d9KhnyXj6qg0w\n    [5] => 12\n    [6] => bcrypt\n    [7] => 3\n    [8] => 1\n    [9] => 5\n    [10] => 5\n    [11] => Yes\n)\n', '/smic_recruitment/applicant_registration.php'),
(93, '::1', 'Not Logged In', '2018-05-09 07:01:43', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => \n    [2] => \n)\n', '/smic_recruitment/applicant_registration.php'),
(94, '::1', 'markdanico.fernandez@gmail.com', '2018-05-09 07:01:59', 'Logged in', '/smic_recruitment/login.php'),
(95, '::1', 'markdanico.fernandez@gmail.com', '2018-05-09 07:02:47', 'Logged out', '/smic_recruitment/end.php'),
(96, '::1', 'Not Logged In', '2018-05-09 07:03:04', 'Query Executed: INSERT INTO user(username, personal_email, password, salt, iteration, method, role_id, skin_id, user_level, applicant_id, is_verified) VALUES(?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssssss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => markdanico.fernandez@gmail.com\n    [3] => $2y$12$I1bWwPua1U.blZrcP0CiwuteBhwpaIZL5.UUiN7nLKy81LijpRYSO\n    [4] => I1bWwPua1U+blZrcP0Ciww\n    [5] => 12\n    [6] => bcrypt\n    [7] => 3\n    [8] => 1\n    [9] => 5\n    [10] => 5\n    [11] => Yes\n)\n', '/smic_recruitment/applicant_registration.php'),
(97, '::1', 'Not Logged In', '2018-05-09 07:03:04', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/applicant_registration.php'),
(98, '::1', 'markdanico.fernandez@gmail.com', '2018-05-09 07:03:19', 'Logged in', '/smic_recruitment/login.php'),
(99, '::1', 'markdanico.fernandez@gmail.com', '2018-05-09 07:03:26', 'Logged out', '/smic_recruitment/end.php'),
(100, '::1', 'Not Logged In', '2018-05-09 08:20:17', 'Query Executed: INSERT INTO applicant(applicant_number, last_name, first_name, middle_name) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => APL-1805-00001\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Albao\n)\n', '/smic_recruitment/applicant_registration.php'),
(101, '::1', 'Not Logged In', '2018-05-09 08:25:56', 'Query Executed: INSERT INTO applicant(applicant_number, last_name, first_name, middle_name) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => APL-1805-00002\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Albao\n)\n', '/smic_recruitment/applicant_registration.php'),
(102, '::1', 'Not Logged In', '2018-05-09 08:26:27', 'Query Executed: INSERT INTO applicant(applicant_number, last_name, first_name, middle_name) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => APL-1805-00003\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Albao\n)\n', '/smic_recruitment/applicant_registration.php'),
(103, '::1', 'Not Logged In', '2018-05-09 08:29:01', 'Query Executed: INSERT INTO applicant(applicant_number, last_name, first_name, middle_name) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => APL-1805-00004\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Albao\n)\n', '/smic_recruitment/applicant_registration.php'),
(104, '::1', 'Not Logged In', '2018-05-09 08:29:01', 'Query Executed: INSERT INTO user(username, personal_email, password, salt, iteration, method, role_id, skin_id, user_level, applicant_id, is_verified) VALUES(?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssssss\n    [1] => markdanico.fernandez1@gmail.com\n    [2] => markdanico.fernandez1@gmail.com\n    [3] => $2y$12$DLvq.wcz/5MV45D1bZVYZ.kWPpyjIKefn7E.9XGh3H85UDlx5K25i\n    [4] => DLvq+wcz/5MV45D1bZVYZA\n    [5] => 12\n    [6] => bcrypt\n    [7] => 3\n    [8] => 1\n    [9] => 5\n    [10] => 4\n    [11] => Yes\n)\n', '/smic_recruitment/applicant_registration.php'),
(105, '::1', 'Not Logged In', '2018-05-09 08:29:01', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez1@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/applicant_registration.php'),
(106, '::1', 'Not Logged In', '2018-05-09 08:30:19', 'Query Executed: INSERT INTO applicant(applicant_number, last_name, first_name, middle_name) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => APL-1805-00005\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Albao\n)\n', '/smic_recruitment/applicant_registration.php'),
(107, '::1', 'Not Logged In', '2018-05-09 08:30:20', 'Query Executed: INSERT INTO user(username, personal_email, password, salt, iteration, method, role_id, skin_id, user_level, applicant_id, is_verified) VALUES(?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssssss\n    [1] => markdanico.fernandez1@gmail.com\n    [2] => markdanico.fernandez1@gmail.com\n    [3] => $2y$12$ZXh4hI84VXfx2wjgApC/lOQWlu9DCzjPDZiFCyCUhbQ1FZRCqmUPi\n    [4] => ZXh4hI84VXfx2wjgApC/lQ\n    [5] => 12\n    [6] => bcrypt\n    [7] => 3\n    [8] => 1\n    [9] => 5\n    [10] => 5\n    [11] => Yes\n)\n', '/smic_recruitment/applicant_registration.php'),
(108, '::1', 'Not Logged In', '2018-05-09 08:32:53', 'Query Executed: INSERT INTO applicant(applicant_number, last_name, first_name, middle_name) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => APL-1805-00006\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Albao\n)\n', '/smic_recruitment/applicant_registration.php'),
(109, '::1', 'Not Logged In', '2018-05-09 08:32:53', 'Query Executed: INSERT INTO user(username, personal_email, password, salt, iteration, method, role_id, skin_id, user_level, applicant_id, is_verified) VALUES(?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssssss\n    [1] => markdanico.fernandez1@gmail.com\n    [2] => markdanico.fernandez1@gmail.com\n    [3] => $2y$12$KGhtLlDjBZI29ctD7a7YxOCzrxB1O695o8NAOy2K04azOsoGEZKxu\n    [4] => KGhtLlDjBZI29ctD7a7YxQ\n    [5] => 12\n    [6] => bcrypt\n    [7] => 3\n    [8] => 1\n    [9] => 5\n    [10] => 6\n    [11] => Yes\n)\n', '/smic_recruitment/applicant_registration.php'),
(110, '::1', 'Not Logged In', '2018-05-09 08:32:53', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez1@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/applicant_registration.php'),
(111, '::1', 'markdanico.fernandez1@gmail.com', '2018-05-09 08:33:44', 'Logged in', '/smic_recruitment/login.php'),
(112, '::1', 'markdanico.fernandez1@gmail.com', '2018-05-09 08:33:56', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(113, '::1', 'markdanico.fernandez1@gmail.com', '2018-05-09 08:34:00', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(114, '::1', 'markdanico.fernandez1@gmail.com', '2018-05-09 08:42:19', 'Logged out', '/smic_recruitment/end.php'),
(115, '::1', 'root', '2018-05-09 08:43:06', 'Logged in', '/smic_recruitment/login.php'),
(116, '::1', 'root', '2018-05-09 08:43:56', 'Pressed submit button', '/smic_recruitment/sysadmin/add_user_links.php'),
(117, '::1', 'root', '2018-05-09 08:43:56', 'Query Executed: INSERT INTO user_links(name, target, descriptive_title, description, passport_group_id, show_in_tasklist, status, icon, priority) VALUES(?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssss\n    [1] => Applicant access\n    [2] => none\n    [3] => Applicant Access\n    [4] => \n    [5] => 3\n    [6] => No\n    [7] => On\n    [8] => none\n    [9] => 0\n)\n', '/smic_recruitment/sysadmin/add_user_links.php'),
(118, '::1', 'root', '2018-05-09 08:44:19', 'Query Executed: DELETE FROM user_role_links WHERE role_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(119, '::1', 'root', '2018-05-09 08:44:19', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 45\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(120, '::1', 'root', '2018-05-09 08:44:19', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 49\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(121, '::1', 'root', '2018-05-09 08:44:19', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 53\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(122, '::1', 'root', '2018-05-09 08:44:20', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 57\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(123, '::1', 'root', '2018-05-09 08:44:20', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 61\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(124, '::1', 'root', '2018-05-09 08:44:20', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 65\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(125, '::1', 'root', '2018-05-09 08:44:20', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 69\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(126, '::1', 'root', '2018-05-09 08:44:20', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 73\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(127, '::1', 'root', '2018-05-09 08:44:20', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 77\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(128, '::1', 'root', '2018-05-09 08:44:20', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 43\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(129, '::1', 'root', '2018-05-09 08:44:21', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 197\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(130, '::1', 'root', '2018-05-09 08:44:21', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 47\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(131, '::1', 'root', '2018-05-09 08:44:21', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 51\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(132, '::1', 'root', '2018-05-09 08:44:21', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 55\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(133, '::1', 'root', '2018-05-09 08:44:21', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 59\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(134, '::1', 'root', '2018-05-09 08:44:21', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 63\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(135, '::1', 'root', '2018-05-09 08:44:21', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 67\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(136, '::1', 'root', '2018-05-09 08:44:21', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 71\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(137, '::1', 'root', '2018-05-09 08:44:21', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 75\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(138, '::1', 'root', '2018-05-09 08:44:21', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 79\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(139, '::1', 'root', '2018-05-09 08:44:21', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 42\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(140, '::1', 'root', '2018-05-09 08:44:21', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 46\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(141, '::1', 'root', '2018-05-09 08:44:22', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 50\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(142, '::1', 'root', '2018-05-09 08:44:22', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 54\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(143, '::1', 'root', '2018-05-09 08:44:22', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 58\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(144, '::1', 'root', '2018-05-09 08:44:22', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 62\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(145, '::1', 'root', '2018-05-09 08:44:22', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 66\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(146, '::1', 'root', '2018-05-09 08:44:22', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 70\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(147, '::1', 'root', '2018-05-09 08:44:22', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 74\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(148, '::1', 'root', '2018-05-09 08:44:22', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 78\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(149, '::1', 'root', '2018-05-09 08:44:31', 'Query Executed: DELETE FROM user_passport WHERE username IN (?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => applicant1\n    [2] => markdanico.fernandez1@gmail.com\n    [3] => markdanico.fernandez@gmail.com\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(150, '::1', 'root', '2018-05-09 08:44:31', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => applicant1\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(151, '::1', 'root', '2018-05-09 08:44:31', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez1@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(152, '::1', 'root', '2018-05-09 08:44:31', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(153, '::1', 'root', '2018-05-09 08:44:33', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(154, '::1', 'root', '2018-05-09 08:47:09', 'Logged out', '/smic_recruitment/end.php'),
(155, '::1', 'markdanico.fernandez1@gmail.com', '2018-05-09 08:49:46', 'Logged in', '/smic_recruitment/login.php'),
(156, '::1', 'markdanico.fernandez1@gmail.com', '2018-05-09 08:49:52', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(157, '::1', 'markdanico.fernandez1@gmail.com', '2018-05-09 09:03:07', 'Logged out', '/smic_recruitment/end.php'),
(158, '::1', 'root', '2018-05-09 09:06:53', 'Logged in', '/smic_recruitment/login.php'),
(159, '::1', 'root', '2018-05-09 09:07:27', 'Pressed submit button', '/smic_recruitment/sysadmin/add_system_settings.php'),
(160, '::1', 'root', '2018-05-09 09:07:27', 'Query Executed: INSERT INTO system_settings(setting, value) VALUES(?,?)\r\nArray\n(\n    [0] => ss\n    [1] => Emailer Address\n    [2] => smic.email.test@gmail.com\n)\n', '/smic_recruitment/sysadmin/add_system_settings.php'),
(161, '::1', 'root', '2018-05-09 09:07:39', 'Pressed submit button', '/smic_recruitment/sysadmin/add_system_settings.php'),
(162, '::1', 'root', '2018-05-09 09:07:39', 'Query Executed: INSERT INTO system_settings(setting, value) VALUES(?,?)\r\nArray\n(\n    [0] => ss\n    [1] => Emailer Password\n    [2] => 56781234\n)\n', '/smic_recruitment/sysadmin/add_system_settings.php'),
(163, '::1', 'root', '2018-05-09 09:15:42', 'Logged out', '/smic_recruitment/end.php'),
(164, '::1', 'root', '2018-05-09 09:34:26', 'Logged in', '/smic_recruitment/login.php'),
(165, '::1', 'root', '2018-05-09 09:38:00', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_system_settings.php'),
(166, '::1', 'root', '2018-05-09 09:38:00', 'Query Executed: UPDATE system_settings SET setting = ?, value = ? WHERE setting = ?\r\nArray\n(\n    [0] => sss\n    [1] => Emailer Address\n    [2] => smic.email.tester@gmail.com\n    [3] => Emailer Address\n)\n', '/smic_recruitment/sysadmin/edit_system_settings.php'),
(167, '::1', 'root', '2018-05-09 09:38:03', 'Logged out', '/smic_recruitment/end.php'),
(168, '::1', 'root', '2018-05-09 09:53:09', 'Logged in', '/smic_recruitment/login.php'),
(169, '::1', 'root', '2018-05-09 09:56:35', 'Logged out', '/smic_recruitment/end.php'),
(170, '::1', 'root', '2018-05-09 09:56:37', 'Logged in', '/smic_recruitment/login.php'),
(171, '::1', 'root', '2018-05-09 09:57:39', 'Logged out', '/smic_recruitment/end.php'),
(172, '::1', 'markdanico.fernandez1@gmail.com', '2018-05-09 09:58:58', 'Logged in', '/smic_recruitment/login.php'),
(173, '::1', 'markdanico.fernandez1@gmail.com', '2018-05-09 10:16:06', 'Logged out', '/smic_recruitment/end.php'),
(174, '::1', 'Not Logged In', '2018-05-09 10:41:07', 'Query Executed: INSERT INTO applicant(applicant_number, last_name, first_name, middle_name) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => APL-1805-00007\n    [2] => Martinez\n    [3] => Lauro\n    [4] => M\n)\n', '/smic_recruitment/applicant_registration.php'),
(175, '::1', 'Not Logged In', '2018-05-09 10:41:08', 'Query Executed: INSERT INTO user(username, personal_email, password, salt, iteration, method, role_id, skin_id, user_level, applicant_id, is_verified) VALUES(?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssssss\n    [1] => smic.email.tester@gmail.com\n    [2] => smic.email.tester@gmail.com\n    [3] => $2y$12$hrY.uunnTN/vkNAUFEQBvOmESFTSTY92sAodSlPpU2z8IrSSDYELK\n    [4] => hrY+uunnTN/vkNAUFEQBvQ\n    [5] => 12\n    [6] => bcrypt\n    [7] => 3\n    [8] => 1\n    [9] => 5\n    [10] => 7\n    [11] => No\n)\n', '/smic_recruitment/applicant_registration.php'),
(176, '::1', 'Not Logged In', '2018-05-09 10:41:08', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => smic.email.tester@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/applicant_registration.php'),
(177, '::1', 'Not Logged In', '2018-05-09 10:58:16', 'Query Executed: UPDATE user SET is_verified = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => ss\n    [1] => Yes\n    [2] => 7\n)\n', '/smic_recruitment/applicant_registration.php'),
(178, '::1', 'smic.email.tester@gmail.com', '2018-05-09 10:58:40', 'Logged in', '/smic_recruitment/login.php'),
(179, '::1', 'smic.email.tester@gmail.com', '2018-05-09 12:47:53', 'Logged out', '/smic_recruitment/end.php'),
(180, '::1', 'markdanico.fernandez1@gmail.com', '2018-05-11 10:40:53', 'Logged in', '/smic_recruitment/login.php'),
(181, '::1', 'markdanico.fernandez1@gmail.com', '2018-05-11 13:11:27', 'Logged out', '/smic_recruitment/end.php'),
(182, '::1', 'Not Logged In', '2018-05-11 13:50:55', 'Query Executed: INSERT INTO applicant(applicant_number, last_name, first_name, middle_name) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => APL-1805-00008\n    [2] => Reyes\n    [3] => Kimberly\n    [4] => Sison\n)\n', '/smic_recruitment/applicant_registration.php'),
(183, '::1', 'Not Logged In', '2018-05-11 13:50:56', 'Query Executed: INSERT INTO user(username, personal_email, password, salt, iteration, method, role_id, skin_id, user_level, applicant_id, is_verified, token) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssss\n    [1] => reyeskimberly018@gmail.com\n    [2] => reyeskimberly018@gmail.com\n    [3] => $2y$12$EDUj0NECTy/l9I8i0sQ/YuHi4YePI9tNnjx6PXVg5N/V7.fW7U86W\n    [4] => EDUj0NECTy/l9I8i0sQ/Yw\n    [5] => 12\n    [6] => bcrypt\n    [7] => 3\n    [8] => 1\n    [9] => 5\n    [10] => 8\n    [11] => No\n    [12] => c61c546aa42cd40ac571320f71ddc383cd28d1a2\n)\n', '/smic_recruitment/applicant_registration.php'),
(184, '::1', 'Not Logged In', '2018-05-11 13:50:56', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => reyeskimberly018@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/applicant_registration.php'),
(185, '::1', 'Not Logged In', '2018-05-11 13:53:54', 'Query Executed: UPDATE user SET is_verified = ? WHERE applicant_id = ? AND token = ?\r\nArray\n(\n    [0] => sss\n    [1] => Yes\n    [2] => 8\n    [3] => c61c546aa42cd40ac571320f71ddc383cd28d1a2\n)\n', '/smic_recruitment/applicant_registration.php'),
(186, '::1', 'reyeskimberly018@gmail.com', '2018-05-11 13:54:13', 'Logged in', '/smic_recruitment/login.php'),
(187, '::1', 'root', '2018-05-24 15:37:15', 'Logged in', '/smic_recruitment/login.php'),
(188, '::1', 'root', '2018-05-24 15:37:23', 'Logged out', '/smic_recruitment/end.php'),
(189, '::1', 'markdanico.fernandez1@gmail.com', '2018-05-24 15:39:20', 'Logged in', '/smic_recruitment/login.php'),
(190, '::1', 'root', '2018-05-31 07:31:44', 'Logged in', '/smic_recruitment/login.php'),
(191, '::1', 'root', '2018-05-31 07:32:07', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(192, '::1', 'root', '2018-05-31 08:01:24', 'Logged out', '/smic_recruitment/end.php'),
(193, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 08:01:44', 'Logged in', '/smic_recruitment/login.php'),
(194, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 08:10:08', 'ILLEGAL ACCESS ATTEMPT - Tried to access ''/smic_recruitment/modules/applicant/applicant/add_applicant.php'' without sufficient privileges.', '/smic_recruitment/modules/applicant/applicant/add_applicant.php'),
(195, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 08:10:20', 'Logged in', '/smic_recruitment/login.php'),
(196, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 13:47:18', 'Logged in', '/smic_recruitment/login.php'),
(197, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:09:54', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(198, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:09:54', 'Query Executed: DELETE FROM applicant_family_members WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(199, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:09:54', 'Query Executed: DELETE FROM applicant_interview WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(200, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:09:54', 'Query Executed: DELETE FROM applicant_languages_proficiency WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(201, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:12:52', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(202, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:12:52', 'Query Executed: DELETE FROM applicant_family_members WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(203, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:12:52', 'Query Executed: DELETE FROM applicant_interview WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(204, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:12:52', 'Query Executed: DELETE FROM applicant_languages_proficiency WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(205, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:13:35', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(206, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:20:17', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(207, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:20:18', 'Query Executed: DELETE FROM applicant_family_members WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(208, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:20:18', 'Query Executed: DELETE FROM applicant_interview WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(209, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:20:18', 'Query Executed: DELETE FROM applicant_languages_proficiency WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(210, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:20:18', 'Query Executed: DELETE FROM applicant_license WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(211, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:20:18', 'Query Executed: DELETE FROM applicant_previous_employers WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(212, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:20:18', 'Query Executed: DELETE FROM applicant_reference WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(213, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:20:18', 'Query Executed: DELETE FROM applicant_school_attended WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(214, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:20:18', 'Query Executed: UPDATE applicant SET last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssss\n    [1] => Ronquillio\n    [2] => Krista\n    [3] => A\n    [4] => Nico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => Male\n    [10] => Single\n    [11] => Filipino\n    [12] => 2015-08-28\n    [13] => 5''9"\n    [14] => 83\n    [15] => O\n    [16] => Sta. Cruz Manila\n    [17] => Block 41 Kenneth Road\n    [18] => Eusebio Avenue Nagpayong\n    [19] => Pinagbuhatan\n    [20] => Pasig\n    [21] => 3rd District\n    [22] => \n    [23] => \n    [24] => \n    [25] => \n    [26] => \n    [27] => 09274728280\n    [28] => 1234567890\n    [29] => 1234567890\n    [30] => 1234567890\n    [31] => 1234567890\n    [32] => 1234567890\n    [33] => Roman Catholic\n    [34] => Mary Joanne Fernandez\n    [35] => Sister\n    [36] => Block 41 Kenneth Road Eusebio Avenue Nagpayong Pinagbuhatan Pasig\n    [37] => 09177920718\n    [38] => APL-1805-00005\n    [39] => 0\n    [40] => 0\n    [41] => \n    [42] => \n    [43] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(215, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:21:18', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(216, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:21:18', 'Query Executed: DELETE FROM applicant_family_members WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(217, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:21:18', 'Query Executed: DELETE FROM applicant_interview WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(218, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:21:18', 'Query Executed: DELETE FROM applicant_languages_proficiency WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(219, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:21:18', 'Query Executed: DELETE FROM applicant_license WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(220, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:21:18', 'Query Executed: DELETE FROM applicant_previous_employers WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(221, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:21:18', 'Query Executed: DELETE FROM applicant_reference WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(222, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:21:18', 'Query Executed: DELETE FROM applicant_school_attended WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(223, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:21:18', 'Query Executed: UPDATE applicant SET last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssss\n    [1] => Fernandez\n    [2] => Mark\n    [3] => Albao\n    [4] => Nico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => Male\n    [10] => Single\n    [11] => Filipino\n    [12] => 2018-05-31\n    [13] => 5''9"\n    [14] => 83\n    [15] => O\n    [16] => Sta. Cruz Manila\n    [17] => Block 41 Kenneth Road\n    [18] => Eusebio Avenue Nagpayong\n    [19] => Pinagbuhatan\n    [20] => Pasig\n    [21] => 3rd District\n    [22] => \n    [23] => \n    [24] => \n    [25] => \n    [26] => \n    [27] => 09274728280\n    [28] => 1234567890\n    [29] => 1234567890\n    [30] => 1234567890\n    [31] => 1234567890\n    [32] => 1234567890\n    [33] => Roman Catholic\n    [34] => Mary Joanne Fernandez\n    [35] => Sister\n    [36] => Block 41 Kenneth Road Eusebio Avenue Nagpayong Pinagbuhatan Pasig\n    [37] => 09177920718\n    [38] => APL-1805-00005\n    [39] => 0\n    [40] => 0\n    [41] => 0000-00-00\n    [42] => \n    [43] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(224, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:22:54', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(225, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:22:54', 'Query Executed: DELETE FROM applicant_family_members WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(226, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:22:54', 'Query Executed: DELETE FROM applicant_interview WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(227, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:22:54', 'Query Executed: DELETE FROM applicant_languages_proficiency WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php');
INSERT INTO `system_log` (`entry_id`, `ip_address`, `user`, `datetime`, `action`, `module`) VALUES
(228, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:22:54', 'Query Executed: DELETE FROM applicant_license WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(229, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:22:54', 'Query Executed: DELETE FROM applicant_previous_employers WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(230, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:22:54', 'Query Executed: DELETE FROM applicant_reference WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(231, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:22:54', 'Query Executed: DELETE FROM applicant_school_attended WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(232, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:22:54', 'Query Executed: UPDATE applicant SET last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssss\n    [1] => Fernandez\n    [2] => Mark\n    [3] => Albao\n    [4] => Nico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => Male\n    [10] => Single\n    [11] => Filipino\n    [12] => 2018-05-31\n    [13] => 5''9"\n    [14] => 83\n    [15] => O\n    [16] => Sta. Cruz Manila\n    [17] => Block 41 Kenneth Road\n    [18] => Eusebio Avenue Nagpayong\n    [19] => Pinagbuhatan\n    [20] => Pasig\n    [21] => 3rd District\n    [22] => \n    [23] => \n    [24] => \n    [25] => \n    [26] => \n    [27] => 09274728280\n    [28] => 1234567890\n    [29] => 1234567890\n    [30] => 1234567890\n    [31] => 1234567890\n    [32] => 1234567890\n    [33] => Roman Catholic\n    [34] => Mary Joanne Fernandez\n    [35] => Sister\n    [36] => Block 41 Kenneth Road Eusebio Avenue Nagpayong Pinagbuhatan Pasig\n    [37] => 09177920718\n    [38] => APL-1805-00005\n    [39] => 0\n    [40] => 0\n    [41] => 0000-00-00\n    [42] => \n    [43] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(233, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:22:59', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(234, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:22:59', 'Query Executed: DELETE FROM applicant_family_members WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(235, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:22:59', 'Query Executed: DELETE FROM applicant_interview WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(236, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:22:59', 'Query Executed: DELETE FROM applicant_languages_proficiency WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(237, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:22:59', 'Query Executed: DELETE FROM applicant_license WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(238, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:23:00', 'Query Executed: DELETE FROM applicant_previous_employers WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(239, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:23:00', 'Query Executed: DELETE FROM applicant_reference WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(240, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:23:00', 'Query Executed: DELETE FROM applicant_school_attended WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(241, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:23:00', 'Query Executed: UPDATE applicant SET last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssss\n    [1] => Fernandez\n    [2] => Mark\n    [3] => Albao\n    [4] => Nico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => Male\n    [10] => Single\n    [11] => Filipino\n    [12] => 2018-05-31\n    [13] => 5''9"\n    [14] => 83\n    [15] => O\n    [16] => Sta. Cruz Manila\n    [17] => Block 41 Kenneth Road\n    [18] => Eusebio Avenue Nagpayong\n    [19] => Pinagbuhatan\n    [20] => Pasig\n    [21] => 3rd District\n    [22] => \n    [23] => \n    [24] => \n    [25] => \n    [26] => \n    [27] => 09274728280\n    [28] => 1234567890\n    [29] => 1234567890\n    [30] => 1234567890\n    [31] => 1234567890\n    [32] => 1234567890\n    [33] => Roman Catholic\n    [34] => Mary Joanne Fernandez\n    [35] => Sister\n    [36] => Block 41 Kenneth Road Eusebio Avenue Nagpayong Pinagbuhatan Pasig\n    [37] => 09177920718\n    [38] => APL-1805-00005\n    [39] => 0\n    [40] => 0\n    [41] => 0000-00-00\n    [42] => \n    [43] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(242, '::1', 'root', '2018-05-31 14:23:34', 'Logged in', '/smic_recruitment/login.php'),
(243, '::1', 'root', '2018-05-31 14:31:30', 'Query Executed: DELETE FROM user_role_links WHERE role_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(244, '::1', 'root', '2018-05-31 14:31:30', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 45\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(245, '::1', 'root', '2018-05-31 14:31:30', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 49\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(246, '::1', 'root', '2018-05-31 14:31:30', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 53\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(247, '::1', 'root', '2018-05-31 14:31:30', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 57\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(248, '::1', 'root', '2018-05-31 14:31:30', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 61\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(249, '::1', 'root', '2018-05-31 14:31:30', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 65\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(250, '::1', 'root', '2018-05-31 14:31:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 69\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(251, '::1', 'root', '2018-05-31 14:31:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 73\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(252, '::1', 'root', '2018-05-31 14:31:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 77\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(253, '::1', 'root', '2018-05-31 14:31:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 197\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(254, '::1', 'root', '2018-05-31 14:31:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 47\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(255, '::1', 'root', '2018-05-31 14:31:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 51\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(256, '::1', 'root', '2018-05-31 14:31:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 55\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(257, '::1', 'root', '2018-05-31 14:31:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 59\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(258, '::1', 'root', '2018-05-31 14:31:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 63\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(259, '::1', 'root', '2018-05-31 14:31:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 67\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(260, '::1', 'root', '2018-05-31 14:31:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 71\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(261, '::1', 'root', '2018-05-31 14:31:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 75\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(262, '::1', 'root', '2018-05-31 14:31:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 79\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(263, '::1', 'root', '2018-05-31 14:31:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 42\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(264, '::1', 'root', '2018-05-31 14:31:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 46\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(265, '::1', 'root', '2018-05-31 14:31:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 50\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(266, '::1', 'root', '2018-05-31 14:31:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 54\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(267, '::1', 'root', '2018-05-31 14:31:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 58\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(268, '::1', 'root', '2018-05-31 14:31:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 62\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(269, '::1', 'root', '2018-05-31 14:31:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 66\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(270, '::1', 'root', '2018-05-31 14:31:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 70\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(271, '::1', 'root', '2018-05-31 14:31:33', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 74\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(272, '::1', 'root', '2018-05-31 14:31:33', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 78\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(273, '::1', 'root', '2018-05-31 14:31:35', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions.php'),
(274, '::1', 'root', '2018-05-31 14:31:38', 'Query Executed: DELETE FROM user_passport WHERE username IN (?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => applicant1\n    [2] => markdanico.fernandez1@gmail.com\n    [3] => markdanico.fernandez@gmail.com\n    [4] => reyeskimberly018@gmail.com\n    [5] => smic.email.tester@gmail.com\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(275, '::1', 'root', '2018-05-31 14:31:38', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => applicant1\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(276, '::1', 'root', '2018-05-31 14:31:38', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez1@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(277, '::1', 'root', '2018-05-31 14:31:38', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(278, '::1', 'root', '2018-05-31 14:31:38', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => reyeskimberly018@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(279, '::1', 'root', '2018-05-31 14:31:38', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => smic.email.tester@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(280, '::1', 'root', '2018-05-31 14:33:03', 'Logged out', '/smic_recruitment/end.php'),
(281, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:43:41', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(282, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:43:41', 'Query Executed: DELETE FROM applicant_family_members WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(283, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:43:41', 'Query Executed: DELETE FROM applicant_interview WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(284, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:43:41', 'Query Executed: DELETE FROM applicant_languages_proficiency WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(285, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:43:42', 'Query Executed: DELETE FROM applicant_license WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(286, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:43:42', 'Query Executed: DELETE FROM applicant_previous_employers WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(287, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:43:42', 'Query Executed: DELETE FROM applicant_reference WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(288, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:43:42', 'Query Executed: DELETE FROM applicant_school_attended WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(289, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:43:42', 'Query Executed: UPDATE applicant SET last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssss\n    [1] => Fernandez\n    [2] => Mark\n    [3] => Albao\n    [4] => Nico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => Male\n    [10] => Single\n    [11] => Filipino\n    [12] => 2018-05-31\n    [13] => 5''9"\n    [14] => 83\n    [15] => O\n    [16] => Sta. Cruz Manila\n    [17] => Block 41 Kenneth Road\n    [18] => Eusebio Avenue Nagpayong\n    [19] => Pinagbuhatan\n    [20] => Pasig\n    [21] => 3rd District\n    [22] => \n    [23] => \n    [24] => \n    [25] => \n    [26] => \n    [27] => 09274728280\n    [28] => 1234567890\n    [29] => 1234567890\n    [30] => 1234567890\n    [31] => 1234567890\n    [32] => 1234567890\n    [33] => Roman Catholic\n    [34] => Mary Joanne Fernandez\n    [35] => Sister\n    [36] => Block 41 Kenneth Road Eusebio Avenue Nagpayong Pinagbuhatan Pasig\n    [37] => 09177920718\n    [38] => APL-1805-00005\n    [39] => 0\n    [40] => 0\n    [41] => 0000-00-00\n    [42] => \n    [43] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(290, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:44:17', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(291, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:44:17', 'Query Executed: DELETE FROM applicant_family_members WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(292, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:44:17', 'Query Executed: DELETE FROM applicant_interview WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(293, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:44:17', 'Query Executed: DELETE FROM applicant_languages_proficiency WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(294, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:44:17', 'Query Executed: DELETE FROM applicant_license WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(295, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:44:17', 'Query Executed: DELETE FROM applicant_previous_employers WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(296, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:44:17', 'Query Executed: DELETE FROM applicant_reference WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(297, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:44:17', 'Query Executed: DELETE FROM applicant_school_attended WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(298, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:44:17', 'Query Executed: UPDATE applicant SET last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssss\n    [1] => Fernandez\n    [2] => Mark\n    [3] => Albao\n    [4] => Nico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => Male\n    [10] => Single\n    [11] => Filipino\n    [12] => 2018-05-31\n    [13] => 5''9"\n    [14] => 83\n    [15] => O\n    [16] => Sta. Cruz Manila\n    [17] => Block 41 Kenneth Road\n    [18] => Eusebio Avenue Nagpayong\n    [19] => Pinagbuhatan\n    [20] => Pasig\n    [21] => 3rd District\n    [22] => \n    [23] => \n    [24] => \n    [25] => \n    [26] => \n    [27] => 09274728280\n    [28] => 1234567890\n    [29] => 1234567890\n    [30] => 1234567890\n    [31] => 1234567890\n    [32] => 1234567890\n    [33] => Roman Catholic\n    [34] => Mary Joanne Fernandez\n    [35] => Sister\n    [36] => Block 41 Kenneth Road Eusebio Avenue Nagpayong Pinagbuhatan Pasig\n    [37] => 09177920718\n    [38] => APL-1805-00005\n    [39] => 0\n    [40] => 0\n    [41] => 0000-00-00\n    [42] => \n    [43] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(299, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:44:41', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(300, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:44:41', 'Query Executed: DELETE FROM applicant_family_members WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(301, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:44:41', 'Query Executed: DELETE FROM applicant_interview WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(302, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:44:41', 'Query Executed: DELETE FROM applicant_languages_proficiency WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(303, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:44:41', 'Query Executed: DELETE FROM applicant_license WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(304, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:44:41', 'Query Executed: DELETE FROM applicant_previous_employers WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(305, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:44:41', 'Query Executed: DELETE FROM applicant_reference WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(306, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:44:41', 'Query Executed: DELETE FROM applicant_school_attended WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(307, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:44:41', 'Query Executed: UPDATE applicant SET last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssss\n    [1] => Fernandez\n    [2] => Mark\n    [3] => Albao\n    [4] => Nico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => Male\n    [10] => Single\n    [11] => Filipino\n    [12] => 2018-05-31\n    [13] => 5''9"\n    [14] => 83\n    [15] => O\n    [16] => Sta. Cruz Manila\n    [17] => Block 41 Kenneth Road\n    [18] => Eusebio Avenue Nagpayong\n    [19] => Pinagbuhatan\n    [20] => Pasig\n    [21] => 3rd District\n    [22] => \n    [23] => \n    [24] => \n    [25] => \n    [26] => \n    [27] => 09274728280\n    [28] => 1234567890\n    [29] => 1234567890\n    [30] => 1234567890\n    [31] => 1234567890\n    [32] => 1234567890\n    [33] => Roman Catholic\n    [34] => Mary Joanne Fernandez\n    [35] => Sister\n    [36] => Block 41 Kenneth Road Eusebio Avenue Nagpayong Pinagbuhatan Pasig\n    [37] => 09177920718\n    [38] => APL-1805-00005\n    [39] => 0\n    [40] => 0\n    [41] => 0000-00-00\n    [42] => \n    [43] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(308, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:45:07', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(309, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:45:07', 'Query Executed: DELETE FROM applicant_family_members WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(310, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:45:07', 'Query Executed: DELETE FROM applicant_interview WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(311, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:45:07', 'Query Executed: DELETE FROM applicant_languages_proficiency WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(312, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:45:08', 'Query Executed: DELETE FROM applicant_license WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(313, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:45:08', 'Query Executed: DELETE FROM applicant_previous_employers WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(314, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:45:08', 'Query Executed: DELETE FROM applicant_reference WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(315, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:45:08', 'Query Executed: DELETE FROM applicant_school_attended WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(316, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 14:45:08', 'Query Executed: UPDATE applicant SET last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssss\n    [1] => Fernandez\n    [2] => Mark\n    [3] => Albao\n    [4] => Nico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => Male\n    [10] => Single\n    [11] => Filipino\n    [12] => 2018-05-31\n    [13] => 5''9"\n    [14] => 83\n    [15] => O\n    [16] => Sta. Cruz Manila\n    [17] => Block 41 Kenneth Road\n    [18] => Eusebio Avenue Nagpayong\n    [19] => Pinagbuhatan\n    [20] => Pasig\n    [21] => 3rd District\n    [22] => \n    [23] => \n    [24] => \n    [25] => \n    [26] => \n    [27] => 09274728280\n    [28] => 1234567890\n    [29] => 1234567890\n    [30] => 1234567890\n    [31] => 1234567890\n    [32] => 1234567890\n    [33] => Roman Catholic\n    [34] => Mary Joanne Fernandez\n    [35] => Sister\n    [36] => Block 41 Kenneth Road Eusebio Avenue Nagpayong Pinagbuhatan Pasig\n    [37] => 09177920718\n    [38] => APL-1805-00005\n    [39] => 0\n    [40] => 0\n    [41] => 0000-00-00\n    [42] => \n    [43] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(317, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:25:13', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(318, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:34:07', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(319, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:40:07', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(320, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:40:07', 'Query Executed: INSERT INTO applicant_family_members(applicant_id, name, birthday, age, relationship, is_dependent) VALUES(?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssss\n    [1] => 5\n    [2] => Amelia Fernandez\n    [3] => 1952-04-04\n    [4] => 56\n    [5] => Mother\n    [6] => Yes\n)\n', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(321, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:40:15', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/edit_applicant_family_members.php'),
(322, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:40:21', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/edit_applicant_family_members.php'),
(323, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:40:28', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(324, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:40:46', 'Logged out', '/smic_recruitment/end.php'),
(325, '::1', 'markdanico.fernandez1@gmail.com', '2018-05-31 15:41:08', 'Logged in', '/smic_recruitment/login.php'),
(326, '::1', 'markdanico.fernandez1@gmail.com', '2018-05-31 15:44:09', 'Logged out', '/smic_recruitment/end.php'),
(327, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:44:21', 'Logged in', '/smic_recruitment/login.php'),
(328, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:45:45', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/detailview_applicant_family_members.php'),
(329, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:45:48', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/edit_applicant_family_members.php'),
(330, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:46:15', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(331, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:47:01', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/edit_applicant_family_members.php'),
(332, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:48:36', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php'),
(333, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:48:45', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php'),
(334, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:48:45', 'Query Executed: INSERT INTO applicant_languages_proficiency(applicant_id, language, speaking_proficiency, writing_proficiency) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 5\n    [2] => Tagalog\n    [3] => 4\n    [4] => 4\n)\n', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php'),
(335, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:48:52', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(336, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:48:53', 'Query Executed: UPDATE applicant_languages_proficiency SET applicant_id = ?, language = ?, speaking_proficiency = ?, writing_proficiency = ? WHERE applicant_language_proficiency_id = ?\r\nArray\n(\n    [0] => sssss\n    [1] => 5\n    [2] => Tagalog\n    [3] => 4\n    [4] => 4\n    [5] => 1\n)\n', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(337, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:54:22', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php'),
(338, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:56:24', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_license/add_applicant_license.php'),
(339, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:57:12', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_license/add_applicant_license.php'),
(340, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:57:18', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_license/add_applicant_license.php'),
(341, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:57:27', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_license/add_applicant_license.php'),
(342, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:57:27', 'Query Executed: INSERT INTO applicant_license(applicant_id, license, license_number, license_expiry) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 5\n    [2] => Engineer License\n    [3] => 12345676543\n    [4] => 2018-10-31\n)\n', '/smic_recruitment/modules/applicant/applicant_license/add_applicant_license.php'),
(343, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:57:30', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_license/edit_applicant_license.php'),
(344, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 15:57:30', 'Query Executed: UPDATE applicant_license SET applicant_id = ?, license = ?, license_number = ?, license_expiry = ? WHERE applicant_license_id = ?\r\nArray\n(\n    [0] => sssss\n    [1] => 5\n    [2] => Engineer License\n    [3] => 12345676543\n    [4] => 2018-05-31\n    [5] => 1\n)\n', '/smic_recruitment/modules/applicant/applicant_license/edit_applicant_license.php'),
(345, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:01:54', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(346, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:01:54', 'Query Executed: INSERT INTO applicant_previous_employers(applicant_id, previous_employer_name, previous_employer_position, previous_employer_date_from, previous_employer_date_to) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 5\n    [2] => Total Dev Multipurpose Cooperative\n    [3] => Junior IT Consultant\n    [4] => 2016-06-16\n    [5] => 2018-05-31\n)\n', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(347, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:02:19', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/edit_applicant_previous_employers.php'),
(348, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:02:19', 'Query Executed: UPDATE applicant_previous_employers SET applicant_id = ?, previous_employer_name = ?, previous_employer_position = ?, previous_employer_date_from = ?, previous_employer_date_to = ? WHERE applicant_previous_employer_id = ?\r\nArray\n(\n    [0] => ssssss\n    [1] => 5\n    [2] => Total Dev Multipurpose Cooperative\n    [3] => Junior IT Consultant\n    [4] => 2018-05-31\n    [5] => 2018-05-31\n    [6] => 1\n)\n', '/smic_recruitment/modules/applicant/applicant_previous_employers/edit_applicant_previous_employers.php'),
(349, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:02:36', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/edit_applicant_previous_employers.php'),
(350, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:02:36', 'Query Executed: UPDATE applicant_previous_employers SET applicant_id = ?, previous_employer_name = ?, previous_employer_position = ?, previous_employer_date_from = ?, previous_employer_date_to = ? WHERE applicant_previous_employer_id = ?\r\nArray\n(\n    [0] => ssssss\n    [1] => 5\n    [2] => Total Dev Multipurpose Cooperative\n    [3] => Junior IT Consultant\n    [4] => 2016-06-16\n    [5] => 2018-05-31\n    [6] => 1\n)\n', '/smic_recruitment/modules/applicant/applicant_previous_employers/edit_applicant_previous_employers.php'),
(351, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:09:02', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_reference/add_applicant_reference.php'),
(352, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:09:02', 'Query Executed: INSERT INTO applicant_reference(applicant_id, reference_name, reference_occupation, reference_relationship, reference_additional_information, reference_contact_number) VALUES(?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssss\n    [1] => 5\n    [2] => Eunice V. Gatdula\n    [3] => Graphic Designer\n    [4] => Office Mate\n    [5] => Asia Pacific College\n    [6] => 09876545678\n)\n', '/smic_recruitment/modules/applicant/applicant_reference/add_applicant_reference.php'),
(353, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:09:06', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_reference/edit_applicant_reference.php'),
(354, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:13:28', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_school_attended/add_applicant_school_attended.php'),
(355, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:14:07', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_school_attended/add_applicant_school_attended.php'),
(356, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:14:07', 'Query Executed: INSERT INTO applicant_school_attended(applicant_id, school_id, address, date_from, date_to) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 5\n    [2] => National University\n    [3] => M.F Jhocson Sampaloc Manila\n    [4] => 2018-05-31\n    [5] => 2018-05-31\n)\n', '/smic_recruitment/modules/applicant/applicant_school_attended/add_applicant_school_attended.php'),
(357, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:17:04', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_skills/add_applicant_skills.php'),
(358, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:19:45', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_skills/add_applicant_skills.php'),
(359, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:20:01', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_skills/add_applicant_skills.php'),
(360, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:20:07', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_skills/add_applicant_skills.php'),
(361, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:20:07', 'Query Executed: INSERT INTO applicant_skills(applicant_id, skill, proficiency, years_of_experience) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 5\n    [2] => PHP\n    [3] => 4\n    [4] => 2\n)\n', '/smic_recruitment/modules/applicant/applicant_skills/add_applicant_skills.php'),
(362, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:20:32', 'Logged out', '/smic_recruitment/end.php'),
(363, '::1', 'markdanico.fernandez1@gmail.com', '2018-05-31 16:20:56', 'Logged in', '/smic_recruitment/login.php'),
(364, '::1', 'markdanico.fernandez1@gmail.com', '2018-05-31 16:22:18', 'Logged out', '/smic_recruitment/end.php'),
(365, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:22:26', 'Logged in', '/smic_recruitment/login.php'),
(366, '::1', 'root', '2018-05-31 16:22:44', 'Logged in', '/smic_recruitment/login.php'),
(367, '::1', 'root', '2018-05-31 16:23:58', 'Query Executed: DELETE FROM user_role_links WHERE role_id = ? AND link_id IN (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssssssssssssssssssssssssssss\n    [1] => 3\n    [2] => 41\n    [3] => 42\n    [4] => 43\n    [5] => 44\n    [6] => 45\n    [7] => 46\n    [8] => 47\n    [9] => 48\n    [10] => 49\n    [11] => 50\n    [12] => 51\n    [13] => 52\n    [14] => 53\n    [15] => 54\n    [16] => 55\n    [17] => 56\n    [18] => 57\n    [19] => 58\n    [20] => 59\n    [21] => 60\n    [22] => 61\n    [23] => 62\n    [24] => 63\n    [25] => 64\n    [26] => 65\n    [27] => 66\n    [28] => 67\n    [29] => 68\n    [30] => 69\n    [31] => 70\n    [32] => 71\n    [33] => 72\n    [34] => 73\n    [35] => 74\n    [36] => 75\n    [37] => 76\n    [38] => 77\n    [39] => 78\n    [40] => 79\n    [41] => 80\n    [42] => 197\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(368, '::1', 'root', '2018-05-31 16:23:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 49\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(369, '::1', 'root', '2018-05-31 16:23:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 57\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(370, '::1', 'root', '2018-05-31 16:23:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 61\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(371, '::1', 'root', '2018-05-31 16:23:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 65\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(372, '::1', 'root', '2018-05-31 16:23:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 69\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(373, '::1', 'root', '2018-05-31 16:23:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 73\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(374, '::1', 'root', '2018-05-31 16:23:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 77\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(375, '::1', 'root', '2018-05-31 16:23:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 197\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(376, '::1', 'root', '2018-05-31 16:23:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 51\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(377, '::1', 'root', '2018-05-31 16:23:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 59\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(378, '::1', 'root', '2018-05-31 16:23:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 63\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(379, '::1', 'root', '2018-05-31 16:23:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 67\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(380, '::1', 'root', '2018-05-31 16:23:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 71\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(381, '::1', 'root', '2018-05-31 16:23:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 75\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(382, '::1', 'root', '2018-05-31 16:23:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 79\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(383, '::1', 'root', '2018-05-31 16:23:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 42\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(384, '::1', 'root', '2018-05-31 16:24:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 50\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(385, '::1', 'root', '2018-05-31 16:24:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 58\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(386, '::1', 'root', '2018-05-31 16:24:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 62\n)\n', '/smic_recruitment/sysadmin/role_permissions.php');
INSERT INTO `system_log` (`entry_id`, `ip_address`, `user`, `datetime`, `action`, `module`) VALUES
(387, '::1', 'root', '2018-05-31 16:24:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 66\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(388, '::1', 'root', '2018-05-31 16:24:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 70\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(389, '::1', 'root', '2018-05-31 16:24:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 74\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(390, '::1', 'root', '2018-05-31 16:24:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 78\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(391, '::1', 'root', '2018-05-31 16:24:05', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions.php'),
(392, '::1', 'root', '2018-05-31 16:24:07', 'Query Executed: DELETE FROM user_passport WHERE username IN (?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => applicant1\n    [2] => markdanico.fernandez1@gmail.com\n    [3] => markdanico.fernandez@gmail.com\n    [4] => reyeskimberly018@gmail.com\n    [5] => smic.email.tester@gmail.com\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(393, '::1', 'root', '2018-05-31 16:24:07', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => applicant1\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(394, '::1', 'root', '2018-05-31 16:24:07', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez1@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(395, '::1', 'root', '2018-05-31 16:24:07', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(396, '::1', 'root', '2018-05-31 16:24:08', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => reyeskimberly018@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(397, '::1', 'root', '2018-05-31 16:24:08', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => smic.email.tester@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(398, '::1', 'root', '2018-05-31 16:24:09', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(399, '::1', 'root', '2018-05-31 16:24:14', 'Logged out', '/smic_recruitment/end.php'),
(400, '::1', 'markdanico.fernandez@gmail.com', '2018-05-31 16:24:57', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_skills/add_applicant_skills.php'),
(401, '::1', 'root', '2018-06-01 06:38:16', 'Logged in', '/smic_recruitment/login.php'),
(402, '::1', 'root', '2018-06-01 06:38:20', 'Logged out', '/smic_recruitment/end.php'),
(403, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:38:29', 'Logged in', '/smic_recruitment/login.php'),
(404, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:39:30', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(405, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:39:31', 'Query Executed: DELETE FROM applicant_family_members WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(406, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:39:31', 'Query Executed: DELETE FROM applicant_interview WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(407, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:39:31', 'Query Executed: DELETE FROM applicant_languages_proficiency WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(408, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:39:31', 'Query Executed: DELETE FROM applicant_license WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(409, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:39:31', 'Query Executed: DELETE FROM applicant_previous_employers WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(410, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:39:31', 'Query Executed: DELETE FROM applicant_reference WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(411, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:39:31', 'Query Executed: DELETE FROM applicant_school_attended WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(412, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:40:05', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(413, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:40:05', 'Query Executed: DELETE FROM applicant_family_members WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(414, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:40:05', 'Query Executed: DELETE FROM applicant_interview WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(415, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:40:05', 'Query Executed: DELETE FROM applicant_languages_proficiency WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(416, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:40:06', 'Query Executed: DELETE FROM applicant_license WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(417, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:40:06', 'Query Executed: DELETE FROM applicant_previous_employers WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(418, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:40:06', 'Query Executed: DELETE FROM applicant_reference WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(419, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:40:06', 'Query Executed: DELETE FROM applicant_school_attended WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(420, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:40:06', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => ssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 711f2553ea6b767cb32c389b45574e3751df6c3c75324f46eb13af479c555305e3b0400dcfc4924eduolingo_profile_pic_italian.png\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Albao\n    [5] => Nico\n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => Single\n    [12] => Filipino\n    [13] => 2018-06-01\n    [14] => 5''9"\n    [15] => 83\n    [16] => O\n    [17] => Sta. Cruz Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue Nagpayong\n    [20] => Pinagbuhatan\n    [21] => Pasig\n    [22] => 3rd District\n    [23] => \n    [24] => \n    [25] => \n    [26] => \n    [27] => \n    [28] => 09274728280\n    [29] => 1234567890\n    [30] => 1234567890\n    [31] => 1234567890\n    [32] => 1234567890\n    [33] => 1234567890\n    [34] => Roman Catholic\n    [35] => Mary Joanne Fernandez\n    [36] => Sister\n    [37] => Block 41 Kenneth Road Eusebio Avenue Nagpayong Pinagbuhatan Pasig\n    [38] => 09177920718\n    [39] => APL-1805-00005\n    [40] => 0\n    [41] => 0\n    [42] => 0000-00-00\n    [43] => \n    [44] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(421, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:47:50', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(422, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:47:50', 'Query Executed: DELETE FROM applicant_family_members WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(423, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:47:50', 'Query Executed: DELETE FROM applicant_interview WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(424, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:47:50', 'Query Executed: DELETE FROM applicant_languages_proficiency WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(425, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:47:50', 'Query Executed: DELETE FROM applicant_license WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(426, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:47:50', 'Query Executed: DELETE FROM applicant_previous_employers WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(427, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:47:50', 'Query Executed: DELETE FROM applicant_reference WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(428, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:47:50', 'Query Executed: DELETE FROM applicant_school_attended WHERE applicant_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(429, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:47:50', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => ssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 711f2553ea6b767cb32c389b45574e3751df6c3c75324f46eb13af479c555305e3b0400dcfc4924eduolingo_profile_pic_italian.png\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Albao\n    [5] => Nico\n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => Single\n    [12] => Filipino\n    [13] => 2018-06-01\n    [14] => 5''9"\n    [15] => 83\n    [16] => O\n    [17] => Sta. Cruz Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue Nagpayong\n    [20] => Pinagbuhatan\n    [21] => Pasig\n    [22] => 3rd District\n    [23] => \n    [24] => \n    [25] => \n    [26] => \n    [27] => \n    [28] => 09274728280\n    [29] => 1234567890\n    [30] => 1234567890\n    [31] => 1234567890\n    [32] => 1234567890\n    [33] => 1234567890\n    [34] => Roman Catholic\n    [35] => Mary Joanne Fernandez\n    [36] => Sister\n    [37] => Block 41 Kenneth Road Eusebio Avenue Nagpayong Pinagbuhatan Pasig\n    [38] => 09177920718\n    [39] => APL-1805-00005\n    [40] => 0\n    [41] => 0\n    [42] => 0000-00-00\n    [43] => \n    [44] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(430, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:52:33', 'Logged out', '/smic_recruitment/end.php'),
(431, '::1', 'markdanico.fernandez1@gmail.com', '2018-06-01 06:52:48', 'Logged in', '/smic_recruitment/login.php'),
(432, '::1', 'markdanico.fernandez1@gmail.com', '2018-06-01 06:52:57', 'Logged out', '/smic_recruitment/end.php'),
(433, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:54:49', 'Logged in', '/smic_recruitment/login.php'),
(434, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:55:21', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(435, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:55:21', 'Query Executed: INSERT INTO applicant_family_members(applicant_id, name, birthday, age, relationship, is_dependent) VALUES(?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssss\n    [1] => 5\n    [2] => Amelia Fernandez\n    [3] => 1956-04-04\n    [4] => 56\n    [5] => Mother\n    [6] => Yes\n)\n', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(436, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:55:30', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php'),
(437, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:55:31', 'Query Executed: INSERT INTO applicant_languages_proficiency(applicant_id, language, speaking_proficiency, writing_proficiency) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 5\n    [2] => Tagalog\n    [3] => 4\n    [4] => 4\n)\n', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php'),
(438, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:55:41', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_license/add_applicant_license.php'),
(439, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:55:41', 'Query Executed: INSERT INTO applicant_license(applicant_id, license, license_number, license_expiry) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 5\n    [2] => Engineer License\n    [3] => 12345676543\n    [4] => 2022-06-01\n)\n', '/smic_recruitment/modules/applicant/applicant_license/add_applicant_license.php'),
(440, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:56:04', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(441, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:56:04', 'Query Executed: INSERT INTO applicant_previous_employers(applicant_id, previous_employer_name, previous_employer_position, previous_employer_date_from, previous_employer_date_to) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 5\n    [2] => Total Dev Multipurpose Cooperative\n    [3] => Junior IT Consultant\n    [4] => 2016-06-16\n    [5] => 2018-05-31\n)\n', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(442, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:56:19', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_reference/add_applicant_reference.php'),
(443, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:56:20', 'Query Executed: INSERT INTO applicant_reference(applicant_id, reference_name, reference_occupation, reference_relationship, reference_additional_information, reference_contact_number) VALUES(?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssss\n    [1] => 5\n    [2] => Eunice V. Gatdula\n    [3] => Graphic Designer\n    [4] => Office Mate\n    [5] => Asia Pacific College\n    [6] => 09876545678\n)\n', '/smic_recruitment/modules/applicant/applicant_reference/add_applicant_reference.php'),
(444, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:56:56', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_school_attended/add_applicant_school_attended.php'),
(445, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:56:56', 'Query Executed: INSERT INTO applicant_school_attended(applicant_id, school_id, address, date_from, date_to) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 5\n    [2] => National University\n    [3] => M.F Jhocson Sampaloc Manila\n    [4] => 2012-06-01\n    [5] => 2016-06-01\n)\n', '/smic_recruitment/modules/applicant/applicant_school_attended/add_applicant_school_attended.php'),
(446, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:57:10', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(447, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 06:57:10', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => ssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 711f2553ea6b767cb32c389b45574e3751df6c3c75324f46eb13af479c555305e3b0400dcfc4924eduolingo_profile_pic_italian.png\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Albao\n    [5] => Nico\n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => Single\n    [12] => Filipino\n    [13] => 2018-06-01\n    [14] => 5''9"\n    [15] => 83\n    [16] => O\n    [17] => Sta. Cruz Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue Nagpayong\n    [20] => Pinagbuhatan\n    [21] => Pasig\n    [22] => 3rd District\n    [23] => \n    [24] => \n    [25] => \n    [26] => \n    [27] => \n    [28] => 09274728280\n    [29] => 1234567890\n    [30] => 1234567890\n    [31] => 1234567890\n    [32] => 1234567890\n    [33] => 1234567890\n    [34] => Roman Catholic\n    [35] => Mary Joanne Fernandez\n    [36] => Sister\n    [37] => Block 41 Kenneth Road Eusebio Avenue Nagpayong Pinagbuhatan Pasig\n    [38] => 09177920718\n    [39] => APL-1805-00005\n    [40] => 0\n    [41] => 0\n    [42] => 0000-00-00\n    [43] => \n    [44] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(448, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 07:36:07', 'Logged out', '/smic_recruitment/end.php'),
(449, '::1', 'Not Logged In', '2018-06-01 07:36:48', 'Query Executed: INSERT INTO applicant(applicant_number, last_name, first_name, middle_name) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => APL-1806-00009\n    [2] => FErnandez\n    [3] => Mark\n    [4] => Danico \n)\n', '/smic_recruitment/applicant_registration.php'),
(450, '::1', 'Not Logged In', '2018-06-01 07:37:17', 'Query Executed: INSERT INTO applicant(applicant_number, last_name, first_name, middle_name) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => APL-1806-00010\n    [2] => FErnandez\n    [3] => Mark\n    [4] => Danico \n)\n', '/smic_recruitment/applicant_registration.php'),
(451, '::1', 'Not Logged In', '2018-06-01 07:37:17', 'Query Executed: INSERT INTO user(username, personal_email, password, salt, iteration, method, role_id, skin_id, user_level, applicant_id, is_verified, token) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssss\n    [1] => markdanico.fernandezz@gmail.com\n    [2] => markdanico.fernandezz@gmail.com\n    [3] => $2y$12$Wg0bSubz.JOnyJIJyobP1.1LRMi3q6pThlHdIN/rCCevCvd8Hrdse\n    [4] => Wg0bSubz+JOnyJIJyobP1A\n    [5] => 12\n    [6] => bcrypt\n    [7] => 3\n    [8] => 1\n    [9] => 5\n    [10] => 10\n    [11] => No\n    [12] => 447bb366354d19ccffc17918530988de54e5768c\n)\n', '/smic_recruitment/applicant_registration.php'),
(452, '::1', 'Not Logged In', '2018-06-01 07:37:17', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandezz@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/applicant_registration.php'),
(453, '::1', 'markdanico.fernandez1@gmail.com', '2018-06-01 07:50:30', 'Logged in', '/smic_recruitment/login.php'),
(454, '::1', 'markdanico.fernandez1@gmail.com', '2018-06-01 07:53:35', 'Logged out', '/smic_recruitment/end.php'),
(455, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 07:53:44', 'Logged in', '/smic_recruitment/login.php'),
(456, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 07:54:31', 'Logged out', '/smic_recruitment/end.php'),
(457, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 08:16:45', 'Logged in', '/smic_recruitment/login.php'),
(458, '::1', 'markdanico.fernandez@gmail.com', '2018-06-01 11:42:01', 'Logged out', '/smic_recruitment/end.php'),
(459, '::1', 'markdanico.fernandez@gmail.com', '2018-06-05 07:13:49', 'Logged in', '/smic_recruitment/login.php'),
(460, '::1', 'markdanico.fernandez1@gmail.com', '2018-06-14 09:27:41', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(461, '::1', 'markdanico.fernandez1@gmail.com', '2018-06-14 09:27:46', 'Logged out', '/smic_recruitment/end.php'),
(462, '::1', 'markdanico.fernandez@gmail.com', '2018-06-14 09:29:47', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(463, '::1', 'markdanico.fernandez@gmail.com', '2018-06-14 09:29:50', 'Logged out', '/smic_recruitment/end.php'),
(464, '::1', 'root', '2018-06-22 09:14:55', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(465, '::1', 'root', '2018-06-22 09:15:13', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_passport_groups.php'),
(466, '::1', 'root', '2018-06-22 09:15:13', 'Query Executed: UPDATE user_passport_groups SET passport_group = ?, priority = ?, icon = ? WHERE passport_group_id = ?\r\nArray\n(\n    [0] => ssss\n    [1] => Applicant Information\n    [2] => 1\n    [3] => blue_folder3.png\n    [4] => 3\n)\n', '/smic_recruitment/sysadmin/edit_user_passport_groups.php'),
(467, '::1', 'root', '2018-06-22 09:15:43', 'Pressed cancel button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(468, '::1', 'root', '2018-06-22 09:16:13', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(469, '::1', 'root', '2018-06-22 09:16:14', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => View applicant family members\n    [2] => modules/applicant/applicant_family_members/listview_applicant_family_members.php\n    [3] => Family Members\n    [4] => \n    [5] => 3\n    [6] => Yes\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n    [10] => 51\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(470, '::1', 'root', '2018-06-22 09:16:24', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(471, '::1', 'root', '2018-06-22 09:16:24', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => View applicant languages proficiency\n    [2] => modules/applicant/applicant_languages_proficiency/listview_applicant_languages_proficiency.php\n    [3] => Languages Proficiency\n    [4] => \n    [5] => 3\n    [6] => Yes\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n    [10] => 59\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(472, '::1', 'root', '2018-06-22 09:16:32', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(473, '::1', 'root', '2018-06-22 09:16:32', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => View applicant license\n    [2] => modules/applicant/applicant_license/listview_applicant_license.php\n    [3] => Licenses\n    [4] => \n    [5] => 3\n    [6] => Yes\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n    [10] => 63\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(474, '::1', 'root', '2018-06-22 09:16:39', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(475, '::1', 'root', '2018-06-22 09:16:39', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => View applicant previous employers\n    [2] => modules/applicant/applicant_previous_employers/listview_applicant_previous_employers.php\n    [3] => Previous Employers\n    [4] => \n    [5] => 3\n    [6] => Yes\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n    [10] => 67\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(476, '::1', 'root', '2018-06-22 09:16:47', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(477, '::1', 'root', '2018-06-22 09:16:47', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => View applicant reference\n    [2] => modules/applicant/applicant_reference/listview_applicant_reference.php\n    [3] => References\n    [4] => \n    [5] => 3\n    [6] => Yes\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n    [10] => 71\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(478, '::1', 'root', '2018-06-22 09:16:55', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(479, '::1', 'root', '2018-06-22 09:16:55', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => View applicant school attended\n    [2] => modules/applicant/applicant_school_attended/listview_applicant_school_attended.php\n    [3] => Schools Attended\n    [4] => \n    [5] => 3\n    [6] => Yes\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n    [10] => 75\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(480, '::1', 'root', '2018-06-22 09:17:00', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(481, '::1', 'root', '2018-06-22 09:17:00', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => View applicant skills\n    [2] => modules/applicant/applicant_skills/listview_applicant_skills.php\n    [3] => Skills\n    [4] => \n    [5] => 3\n    [6] => Yes\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n    [10] => 79\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(482, '::1', 'root', '2018-06-22 09:18:24', 'Logged out', '/smic_recruitment/end.php'),
(483, '::1', 'markdanico.fernandez1@gmail.com', '2018-06-22 09:20:35', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(484, '::1', 'markdanico.fernandez1@gmail.com', '2018-06-22 09:22:00', 'Logged out', '/smic_recruitment/end.php'),
(485, '::1', 'markdanico.fernandez@gmail.com', '2018-06-22 09:22:13', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(486, '::1', 'markdanico.fernandez@gmail.com', '2018-06-22 09:22:39', 'Logged out', '/smic_recruitment/end.php'),
(487, '::1', 'markdanico.fernandez1@gmail.com', '2018-06-22 09:22:47', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(488, '::1', 'markdanico.fernandez1@gmail.com', '2018-06-22 12:04:51', 'Logged out', '/smic_recruitment/end.php'),
(489, '::1', 'markdanico.fernandez1@gmail.com', '2018-06-22 12:15:05', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(490, '::1', 'markdanico.fernandez1@gmail.com', '2018-06-22 12:16:34', 'Logged out', '/smic_recruitment/end.php'),
(491, '::1', 'markdanico.fernandez@gmail.com', '2018-06-28 14:22:30', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(492, '::1', 'markdanico.fernandez@gmail.com', '2018-06-28 14:31:01', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(493, '::1', 'markdanico.fernandez@gmail.com', '2018-06-28 14:31:01', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => ssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 711f2553ea6b767cb32c389b45574e3751df6c3c75324f46eb13af479c555305e3b0400dcfc4924eduolingo_profile_pic_italian.png\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Albao\n    [5] => Nico\n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => Single\n    [12] => Filipino\n    [13] => 2000-06-28\n    [14] => 5''9"\n    [15] => 83\n    [16] => O\n    [17] => Sta. Cruz Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue Nagpayong\n    [20] => Pinagbuhatan\n    [21] => Pasig\n    [22] => 3rd District\n    [23] => \n    [24] => \n    [25] => \n    [26] => \n    [27] => \n    [28] => 09274728280\n    [29] => 1234567890\n    [30] => 1234567890\n    [31] => 1234567890\n    [32] => 1234567890\n    [33] => 1234567890\n    [34] => Roman Catholic\n    [35] => Mary Joanne Fernandez\n    [36] => Sister\n    [37] => Block 41 Kenneth Road Eusebio Avenue Nagpayong Pinagbuhatan Pasig\n    [38] => 09177920718\n    [39] => APL-1805-00005\n    [40] => 0\n    [41] => 0\n    [42] => 0000-00-00\n    [43] => \n    [44] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(494, '::1', 'markdanico.fernandez@gmail.com', '2018-06-28 14:39:50', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/detailview_applicant_family_members.php'),
(495, '::1', 'markdanico.fernandez@gmail.com', '2018-06-28 14:39:58', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/edit_applicant_family_members.php'),
(496, '::1', 'markdanico.fernandez@gmail.com', '2018-06-28 14:40:02', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(497, '::1', 'markdanico.fernandez@gmail.com', '2018-06-28 14:49:49', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(498, '::1', 'markdanico.fernandez@gmail.com', '2018-06-28 14:51:47', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_skills/detailview_applicant_skills.php'),
(499, '::1', 'markdanico.fernandez1@gmail.com', '2018-06-29 09:46:11', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(500, '::1', 'markdanico.fernandez@gmail.com', '2018-07-02 15:14:27', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(501, '::1', 'markdanico.fernandez1@gmail.com', '2018-07-09 11:23:49', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(502, '::1', 'markdanico.fernandez1@gmail.com', '2018-07-09 11:32:37', 'Logged out', '/smic_recruitment/end.php'),
(503, '::1', 'markdanico.fernandez1@gmail.com', '2018-07-09 11:39:06', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(504, '::1', 'root', '2018-07-13 08:14:37', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(505, '::1', 'root', '2018-07-13 08:15:17', 'Pressed submit button', '/smic_recruitment/modules/organization/company/add_company.php'),
(506, '::1', 'root', '2018-07-13 08:15:20', 'Pressed submit button', '/smic_recruitment/modules/organization/company/add_company.php'),
(507, '::1', 'root', '2018-07-13 08:15:20', 'Query Executed: INSERT INTO company(official_name, short_name, head, assistant) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => SM Investments Corporation\n    [2] => SMIC\n    [3] => 1\n    [4] => 2\n)\n', '/smic_recruitment/modules/organization/company/add_company.php'),
(508, '::1', 'root', '2018-07-13 08:17:15', 'Pressed submit button', '/smic_recruitment/modules/organization/department/add_department.php'),
(509, '::1', 'root', '2018-07-13 08:17:21', 'Pressed submit button', '/smic_recruitment/modules/organization/department/add_department.php'),
(510, '::1', 'root', '2018-07-13 08:17:38', 'Pressed submit button', '/smic_recruitment/modules/organization/department/add_department.php'),
(511, '::1', 'root', '2018-07-13 08:18:31', 'Pressed submit button', '/smic_recruitment/modules/organization/department/add_department.php'),
(512, '::1', 'root', '2018-07-13 08:18:31', 'Query Executed: INSERT INTO department(branch_id, floor_id, name, code, description, head, assistant) VALUES(?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssss\n    [1] => \n    [2] => \n    [3] => Information Technology\n    [4] => IT\n    [5] => Sample\n    [6] => \n    [7] => \n)\n', '/smic_recruitment/modules/organization/department/add_department.php'),
(513, '::1', 'root', '2018-07-13 08:19:10', 'Pressed submit button', '/smic_recruitment/modules/organization/department/add_department.php'),
(514, '::1', 'root', '2018-07-13 08:19:10', 'Query Executed: INSERT INTO department(branch_id, floor_id, name, code, description, head, assistant) VALUES(?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssss\n    [1] => \n    [2] => \n    [3] => Human Resources\n    [4] => HR\n    [5] => sampe\n    [6] => \n    [7] => \n)\n', '/smic_recruitment/modules/organization/department/add_department.php'),
(515, '::1', 'root', '2018-07-13 08:19:18', 'Pressed submit button', '/smic_recruitment/modules/organization/department/add_department.php'),
(516, '::1', 'root', '2018-07-13 08:19:18', 'Query Executed: INSERT INTO department(branch_id, floor_id, name, code, description, head, assistant) VALUES(?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssss\n    [1] => \n    [2] => \n    [3] => Accounting\n    [4] => Acctg\n    [5] => ss\n    [6] => \n    [7] => \n)\n', '/smic_recruitment/modules/organization/department/add_department.php'),
(517, '::1', 'root', '2018-07-13 08:19:26', 'Pressed submit button', '/smic_recruitment/modules/organization/department/add_department.php'),
(518, '::1', 'root', '2018-07-13 08:19:26', 'Query Executed: INSERT INTO department(branch_id, floor_id, name, code, description, head, assistant) VALUES(?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssss\n    [1] => \n    [2] => \n    [3] => Payroll\n    [4] => PRoll\n    [5] => a\n    [6] => \n    [7] => \n)\n', '/smic_recruitment/modules/organization/department/add_department.php'),
(519, '::1', 'root', '2018-07-13 08:27:35', 'Pressed submit button', '/smic_recruitment/modules/plantilla/add_plantilla.php'),
(520, '::1', 'root', '2018-07-13 08:28:13', 'Pressed submit button', '/smic_recruitment/modules/plantilla/add_plantilla.php'),
(521, '::1', 'root', '2018-07-13 08:28:13', 'Query Executed: INSERT INTO plantilla(parent_plantilla_id, branch_id, department_id, company_id, position_title, job_description, skill_requirements, recruitment_notes) VALUES(?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssss\n    [1] => \n    [2] => \n    [3] => 5\n    [4] => 1\n    [5] => Accounting Assistant\n    [6] => Sample Sample Sample\n    [7] => Clerical\n    [8] => none\n)\n', '/smic_recruitment/modules/plantilla/add_plantilla.php'),
(522, '::1', 'root', '2018-07-13 08:29:34', 'Pressed submit button', '/smic_recruitment/modules/plantilla/add_plantilla.php'),
(523, '::1', 'root', '2018-07-13 08:29:34', 'Query Executed: INSERT INTO plantilla(parent_plantilla_id, branch_id, department_id, company_id, position_title, job_description, skill_requirements, recruitment_notes) VALUES(?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssss\n    [1] => \n    [2] => \n    [3] => 3\n    [4] => 1\n    [5] => Desktop Support\n    [6] => Tedious job\n    [7] => Knowledgable in supporting PCs\n    [8] => none\n)\n', '/smic_recruitment/modules/plantilla/add_plantilla.php'),
(524, '::1', 'root', '2018-07-13 08:29:54', 'Logged out', '/smic_recruitment/end.php'),
(525, '::1', 'root', '2018-07-13 15:41:06', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(526, '::1', 'markdanico.fernandez1@gmail.com', '2018-07-19 16:00:23', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(527, '::1', 'Not Logged In', '2018-07-23 21:58:35', 'Query Executed: INSERT INTO forgot_pass_table(email_address, token, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => markdanico.fernandez1@gmail.com\n    [2] => c2cf527c9431e29ef78fcd566380ded31925b250\n    [3] => 2018-07-23\n)\n', '/smic_recruitment/forgot_password.php'),
(528, '::1', 'Not Logged In', '2018-07-23 21:59:12', 'Query Executed: INSERT INTO forgot_pass_table(email_address, token, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => markdanico.fernandez1@gmail.com\n    [2] => 60647b5da54d213777af7cd59be4e2ac091dd11e\n    [3] => 2018-07-23\n)\n', '/smic_recruitment/forgot_password.php'),
(529, '::1', 'Not Logged In', '2018-07-23 22:02:56', 'Query Executed: INSERT INTO forgot_pass_table(email_address, token, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => 565e90bc47db4e4f5906526857fc5190e05d6202\n    [3] => 2018-07-23\n)\n', '/smic_recruitment/forgot_password.php'),
(530, '::1', 'Not Logged In', '2018-07-23 22:29:57', 'Query Executed: UPDATE user SET password = ? WHERE username = ?\r\nArray\n(\n    [0] => ss\n    [1] => \n    [2] => markdanico.fernandez@gmail.com\n)\n', '/smic_recruitment/reset_password.php'),
(531, '::1', 'Not Logged In', '2018-07-23 22:30:54', 'Query Executed: INSERT INTO forgot_pass_table(email_address, token, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => 566621a826329295af87ee821cfd7d27eb99ecb1\n    [3] => 2018-07-23\n)\n', '/smic_recruitment/forgot_password.php'),
(532, '::1', 'Not Logged In', '2018-07-23 22:31:12', 'Query Executed: UPDATE user SET password = ? WHERE username = ?\r\nArray\n(\n    [0] => ss\n    [1] => $2y$12$N2y.jdOlaoi6Ps9QMgVgVOwY9XkFDV./27JgG7c4kWjZj10diml7O\n    [2] => markdanico.fernandez@gmail.com\n)\n', '/smic_recruitment/reset_password.php'),
(533, '::1', 'Not Logged In', '2018-07-23 22:34:34', 'Query Executed: INSERT INTO forgot_pass_table(email_address, token, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => e00c991228962350610fdb0d5706e42b1dd1acf4\n    [3] => 2018-07-23\n)\n', '/smic_recruitment/forgot_password.php'),
(534, '::1', 'Not Logged In', '2018-07-23 22:35:50', 'Query Executed: UPDATE user SET password = ?, salt = ?, iteration = ?, method = ? WHERE username = ?\r\nArray\n(\n    [0] => sssss\n    [1] => $2y$12$kf0.BSF3ClvdZHAi/5OrOO5uq0RI8Ls.h0gxcyCivPtFXe1C9lULm\n    [2] => kf0+BSF3ClvdZHAi/5OrOQ\n    [3] => 12\n    [4] => bcrypt\n    [5] => markdanico.fernandez@gmail.com\n)\n', '/smic_recruitment/reset_password.php'),
(535, '::1', 'markdanico.fernandez@gmail.com', '2018-07-23 22:36:13', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(536, '::1', 'markdanico.fernandez@gmail.com', '2018-07-23 22:36:16', 'Logged out', '/smic_recruitment/end.php'),
(537, '::1', 'markdanico.fernandez@gmail.com', '2018-07-24 09:27:26', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(538, '::1', 'markdanico.fernandez@gmail.com', '2018-07-24 09:28:19', 'Logged out', '/smic_recruitment/end.php'),
(539, '::1', 'markdanico.fernandez1@gmail.com', '2018-07-24 09:28:30', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(540, '::1', 'root', '2018-08-01 08:59:11', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(541, '::1', 'root', '2018-08-01 08:59:15', 'Logged out', '/smic_recruitment/end.php'),
(542, '::1', 'markdanico.fernandez1@gmail.com', '2018-08-01 08:59:53', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(543, '::1', 'markdanico.fernandez1@gmail.com', '2018-08-01 10:43:22', 'Logged out', '/smic_recruitment/end.php'),
(544, '::1', 'markdanico.fernandez1@gmail.com', '2018-08-01 10:54:01', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(545, '::1', 'markdanico.fernandez1@gmail.com', '2018-08-01 10:54:05', 'Logged out', '/smic_recruitment/end.php'),
(546, '::1', 'markdanico.fernandez@gmail.com', '2018-08-01 10:54:22', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(547, '::1', 'markdanico.fernandez1@gmail.com', '2018-09-19 13:44:13', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(548, '::1', 'markdanico.fernandez1@gmail.com', '2018-09-19 14:09:05', 'Logged out', '/smic_recruitment/end.php'),
(549, '::1', 'markdanico.fernandez1@gmail.com', '2018-09-19 15:29:08', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(550, '::1', 'markdanico.fernandez1@gmail.com', '2018-09-19 15:30:51', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_school_attended/add_applicant_school_attended.php'),
(551, '::1', 'markdanico.fernandez1@gmail.com', '2018-09-19 15:37:54', 'Logged out', '/smic_recruitment/end.php'),
(552, '::1', 'markdanico.fernandez1@gmail.com', '2018-09-21 12:06:48', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(553, '::1', 'markdanico.fernandez1@gmail.com', '2018-09-25 11:16:59', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(554, '::1', 'markdanico.fernandez1@gmail.com', '2018-09-25 11:22:27', 'Logged out', '/smic_recruitment/end.php'),
(555, '::1', 'root', '2018-09-25 11:22:58', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(556, '::1', 'root', '2018-09-25 11:24:33', 'Pressed cancel button', '/smic_recruitment/modules/organization/company_policy/add_company_policy.php'),
(557, '::1', 'root', '2018-09-25 11:29:20', 'Logged out', '/smic_recruitment/end.php'),
(558, '::1', 'root', '2018-09-25 11:33:03', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(559, '::1', 'root', '2018-09-25 11:35:30', 'Logged out', '/smic_recruitment/end.php'),
(560, '::1', 'Not Logged In', '2018-09-27 08:41:32', 'Query Executed: INSERT INTO temp_user(username, personal_email, password, salt, iteration, method, token, first_name, middle_name, last_name, gender) VALUES(?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssssss\n    [1] => reyeskimberly018@gmail.com\n    [2] => reyeskimberly018@gmail.com\n    [3] => $2y$12$XFJkzguXxWtFiuFbCq4WseN9GWiAsGDz/Pk7aWgciXt72C4oYPoqi\n    [4] => XFJkzguXxWtFiuFbCq4Wsg\n    [5] => 12\n    [6] => bcrypt\n    [7] => ebc74b365cc8bdd21b1f1e5604572e531ca8071d\n    [8] => Kim\n    [9] => Sison\n    [10] => Reyes\n    [11] => Female\n)\n', '/smic_recruitment/applicant_registration.php'),
(561, '::1', 'Not Logged In', '2018-09-27 08:43:19', 'Query Executed: INSERT INTO temp_user(username, personal_email, password, salt, iteration, method, token, first_name, middle_name, last_name, gender) VALUES(?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssssss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => markdanico.fernandez@gmail.com\n    [3] => $2y$12$6cBziRth3X/apdudla0yluFfShu2OsJ.UnvNvvOHzHSwk89SbiygW\n    [4] => 6cBziRth3X/apdudla0ylw\n    [5] => 12\n    [6] => bcrypt\n    [7] => 0a4295af75dbaddb6203df5d5fff4c27997b36ed\n    [8] => Krista\n    [9] => A\n    [10] => Ronquillio\n    [11] => Female\n)\n', '/smic_recruitment/applicant_registration.php'),
(562, '::1', 'Not Logged In', '2018-09-27 08:47:04', 'Query Executed: INSERT INTO temp_user(personal_email, password, salt, iteration, method, token, first_name, middle_name, last_name, gender) VALUES(?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => $2y$12$7oNo.TBwp0.SnRp7fRQCgO1Bhwg2FQYC0lCT8aH14ISp8xxaZXtcW\n    [3] => 7oNo+TBwp0+SnRp7fRQCgQ\n    [4] => 12\n    [5] => bcrypt\n    [6] => 6f6261d6102b8a99ef01905d1b0b8e419687d796\n    [7] => 1\n    [8] => 1\n    [9] => 1\n    [10] => Male\n)\n', '/smic_recruitment/applicant_registration.php'),
(563, '::1', 'Not Logged In', '2018-09-27 08:47:27', 'Query Executed: INSERT INTO temp_user(personal_email, password, salt, iteration, method, token, first_name, middle_name, last_name, gender) VALUES(?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => $2y$12$0zwDy1lAtqdf7.VSNI0l.OVyUnuJco6Ma.OXzGO5C7Z.q1VP6/uCK\n    [3] => 0zwDy1lAtqdf7+VSNI0l+Q\n    [4] => 12\n    [5] => bcrypt\n    [6] => df85eb995e5df95e72ad8aa07595bc71b9878ff0\n    [7] => mark\n    [8] => mark\n    [9] => mark\n    [10] => Male\n)\n', '/smic_recruitment/applicant_registration.php'),
(564, '::1', 'Not Logged In', '2018-09-27 08:52:27', 'Query Executed: INSERT INTO temp_user(personal_email, password, salt, iteration, method, token, first_name, middle_name, last_name, gender) VALUES(?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => $2y$12$TnKqrGWVUQ9cx0V3GvO1qu9duj2uOyJtiXGhufKMMPqnfdDkuUaFy\n    [3] => TnKqrGWVUQ9cx0V3GvO1qw\n    [4] => 12\n    [5] => bcrypt\n    [6] => 8b25496ed6fab8ffdb8eb5afa05a21120a95ce8e\n    [7] => Mark\n    [8] => Danico\n    [9] => Fernandez\n    [10] => Male\n)\n', '/smic_recruitment/applicant_registration.php'),
(565, '::1', 'root', '2018-09-27 09:03:10', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(566, '::1', 'root', '2018-09-27 09:04:22', 'Query Executed: INSERT INTO applicant(applicant_number, last_name, first_name, middle_name) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => APL-1809-00011\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n)\n', '/smic_recruitment/applicant_registration.php'),
(567, '::1', 'root', '2018-09-27 09:04:41', 'Query Executed: INSERT INTO applicant(applicant_number, last_name, first_name, middle_name) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => APL-1809-00012\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n)\n', '/smic_recruitment/applicant_registration.php'),
(568, '::1', 'root', '2018-09-27 09:05:28', 'Query Executed: INSERT INTO applicant(applicant_number, last_name, first_name, middle_name) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => APL-1809-00013\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n)\n', '/smic_recruitment/applicant_registration.php'),
(569, '::1', 'root', '2018-09-27 09:05:28', 'Query Executed: INSERT INTO user(username, password, salt, iteration, method, role_id, skin_id, user_level) VALUES(?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => $2y$12$TnKqrGWVUQ9cx0V3GvO1qu9duj2uOyJtiXGhufKMMPqnfdDkuUaFy\n    [3] => TnKqrGWVUQ9cx0V3GvO1qw\n    [4] => 12\n    [5] => bcrypt\n    [6] => 3\n    [7] => 1\n    [8] => 5\n)\n', '/smic_recruitment/applicant_registration.php'),
(570, '::1', 'root', '2018-09-27 09:05:28', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/applicant_registration.php');
INSERT INTO `system_log` (`entry_id`, `ip_address`, `user`, `datetime`, `action`, `module`) VALUES
(571, '::1', 'root', '2018-09-27 09:05:28', 'Query Executed: DELETE FROM temp_user WHERE temp_user_id = ?\r\nArray\n(\n    [0] => s\n    [1] => \n)\n', '/smic_recruitment/applicant_registration.php'),
(572, '::1', 'root', '2018-09-27 09:05:45', 'Logged out', '/smic_recruitment/end.php'),
(573, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 09:09:12', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(574, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 09:13:55', 'Logged out', '/smic_recruitment/end.php'),
(575, '::1', 'Not Logged In', '2018-09-27 09:14:02', 'Query Executed: INSERT INTO applicant(applicant_number, last_name, first_name, middle_name) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => APL-1809-00014\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n)\n', '/smic_recruitment/applicant_registration.php'),
(576, '::1', 'Not Logged In', '2018-09-27 09:14:03', 'Query Executed: INSERT INTO user(username, password, salt, iteration, method, role_id, skin_id, user_level) VALUES(?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => $2y$12$TnKqrGWVUQ9cx0V3GvO1qu9duj2uOyJtiXGhufKMMPqnfdDkuUaFy\n    [3] => TnKqrGWVUQ9cx0V3GvO1qw\n    [4] => 12\n    [5] => bcrypt\n    [6] => 3\n    [7] => 1\n    [8] => 5\n)\n', '/smic_recruitment/applicant_registration.php'),
(577, '::1', 'Not Logged In', '2018-09-27 09:14:03', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/applicant_registration.php'),
(578, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 09:14:17', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(579, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 09:14:49', 'Query Executed: INSERT INTO applicant(applicant_number, last_name, first_name, middle_name) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => APL-1809-00015\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n)\n', '/smic_recruitment/applicant_registration.php'),
(580, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 09:14:59', 'Query Executed: INSERT INTO applicant(applicant_number, last_name, first_name, middle_name) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => APL-1809-00016\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n)\n', '/smic_recruitment/applicant_registration.php'),
(581, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 09:17:11', 'Query Executed: INSERT INTO applicant(applicant_number, last_name, first_name, middle_name) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => APL-1809-00017\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n)\n', '/smic_recruitment/applicant_registration.php'),
(582, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 09:17:11', 'Query Executed: INSERT INTO user(username, password, salt, iteration, method, applicant_id, role_id, skin_id, user_level) VALUES(?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => $2y$12$TnKqrGWVUQ9cx0V3GvO1qu9duj2uOyJtiXGhufKMMPqnfdDkuUaFy\n    [3] => TnKqrGWVUQ9cx0V3GvO1qw\n    [4] => 12\n    [5] => bcrypt\n    [6] => 17\n    [7] => 3\n    [8] => 1\n    [9] => 5\n)\n', '/smic_recruitment/applicant_registration.php'),
(583, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 09:17:11', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/applicant_registration.php'),
(584, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 09:17:11', 'Query Executed: DELETE FROM temp_user WHERE temp_user_id = ?\r\nArray\n(\n    [0] => s\n    [1] => Array\n        (\n            [applicant_number] => APL-1809-00017\n            [first_name] => Mark\n            [last_name] => Fernandez\n            [middle_name] => Danico\n            [gender] => Male\n            [username] => markdanico.fernandez@gmail.com\n            [personal_email] => markdanico.fernandez@gmail.com\n            [password] => $2y$12$TnKqrGWVUQ9cx0V3GvO1qu9duj2uOyJtiXGhufKMMPqnfdDkuUaFy\n            [salt] => TnKqrGWVUQ9cx0V3GvO1qw\n            [iteration] => 12\n            [method] => bcrypt\n            [role_id] => 3\n            [skin_id] => 1\n            [user_level] => 5\n            [applicant_id] => 17\n        )\n\n)\n', '/smic_recruitment/applicant_registration.php'),
(585, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 09:17:32', 'Logged out', '/smic_recruitment/end.php'),
(586, '::1', 'Not Logged In', '2018-09-27 09:18:36', 'Query Executed: INSERT INTO applicant(applicant_number, last_name, first_name, middle_name) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => APL-1809-00018\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n)\n', '/smic_recruitment/applicant_registration.php'),
(587, '::1', 'Not Logged In', '2018-09-27 09:18:36', 'Query Executed: INSERT INTO user(username, password, salt, iteration, method, applicant_id, role_id, skin_id, user_level) VALUES(?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => $2y$12$TnKqrGWVUQ9cx0V3GvO1qu9duj2uOyJtiXGhufKMMPqnfdDkuUaFy\n    [3] => TnKqrGWVUQ9cx0V3GvO1qw\n    [4] => 12\n    [5] => bcrypt\n    [6] => 18\n    [7] => 3\n    [8] => 1\n    [9] => 5\n)\n', '/smic_recruitment/applicant_registration.php'),
(588, '::1', 'Not Logged In', '2018-09-27 09:18:36', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/applicant_registration.php'),
(589, '::1', 'Not Logged In', '2018-09-27 09:18:36', 'Query Executed: DELETE FROM temp_user WHERE temp_user_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 3\n)\n', '/smic_recruitment/applicant_registration.php'),
(590, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 09:19:09', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(591, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 09:19:27', 'Logged out', '/smic_recruitment/end.php'),
(592, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 09:39:15', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(593, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 09:58:39', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(594, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 10:02:57', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(595, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 10:02:57', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => ssssssssssssssssssssssssssssssssssssssssssss\n    [1] => \n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => \n    [11] => \n    [12] => \n    [13] => 2018-09-27\n    [14] => \n    [15] => \n    [16] => \n    [17] => \n    [18] => \n    [19] => \n    [20] => \n    [21] => \n    [22] => \n    [23] => \n    [24] => \n    [25] => \n    [26] => \n    [27] => \n    [28] => \n    [29] => \n    [30] => \n    [31] => \n    [32] => \n    [33] => \n    [34] => \n    [35] => \n    [36] => \n    [37] => \n    [38] => \n    [39] => APL-1809-00018\n    [40] => 0\n    [41] => 0\n    [42] => \n    [43] => \n    [44] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(596, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 10:07:37', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(597, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 10:07:37', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => ssssssssssssssssssssssssssssssssssssssssssss\n    [1] => \n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => \n    [11] => \n    [12] => \n    [13] => 1911-01-27\n    [14] => \n    [15] => \n    [16] => \n    [17] => \n    [18] => \n    [19] => \n    [20] => \n    [21] => \n    [22] => \n    [23] => \n    [24] => \n    [25] => \n    [26] => \n    [27] => \n    [28] => \n    [29] => \n    [30] => \n    [31] => \n    [32] => \n    [33] => \n    [34] => \n    [35] => \n    [36] => \n    [37] => \n    [38] => \n    [39] => APL-1809-00018\n    [40] => 0\n    [41] => 0\n    [42] => 0000-00-00\n    [43] => \n    [44] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(598, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 10:07:52', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(599, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 10:07:52', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => ssssssssssssssssssssssssssssssssssssssssssss\n    [1] => \n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => \n    [11] => \n    [12] => \n    [13] => 2000-09-27\n    [14] => \n    [15] => \n    [16] => \n    [17] => \n    [18] => \n    [19] => \n    [20] => \n    [21] => \n    [22] => \n    [23] => \n    [24] => \n    [25] => \n    [26] => \n    [27] => \n    [28] => \n    [29] => \n    [30] => \n    [31] => \n    [32] => \n    [33] => \n    [34] => \n    [35] => \n    [36] => \n    [37] => \n    [38] => \n    [39] => APL-1809-00018\n    [40] => 0\n    [41] => 0\n    [42] => 0000-00-00\n    [43] => \n    [44] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(600, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 10:09:50', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(601, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 10:09:51', 'ILLEGAL ACCESS ATTEMPT - Tried to access ''/smic_recruitment/modules/applicant/applicant/listview_applicant.php'' without sufficient privileges.', '/smic_recruitment/modules/applicant/applicant/listview_applicant.php'),
(602, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 10:11:00', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(603, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 10:11:03', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(604, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 10:11:19', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(605, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 10:34:26', 'Logged out', '/smic_recruitment/end.php'),
(606, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 10:41:50', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(607, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 10:56:38', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php'),
(608, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 10:56:46', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_license/add_applicant_license.php'),
(609, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:06:09', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(610, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:06:21', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(611, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:06:33', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(612, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:06:54', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(613, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:06:54', 'Query Executed: INSERT INTO applicant_previous_employers(applicant_id, previous_employer_name, address, job_description, basic_salary, reason_for_leaving, previous_employer_position, previous_employer_date_from, previous_employer_date_to) VALUES(?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssss\n    [1] => 18\n    [2] => Hey\n    [3] => Hey\n    [4] => Jude\n    [5] => 28,000.25\n    [6] => not happy anymore T_T\n    [7] => Jude\n    [8] => 2018-09-27\n    [9] => 2018-09-27\n)\n', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(614, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:13:33', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(615, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:13:33', 'Query Executed: INSERT INTO applicant_previous_employers(applicant_id, previous_employer_name, address, job_description, basic_salary, reason_for_leaving, previous_employer_position, previous_employer_date_from, previous_employer_date_to) VALUES(?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssss\n    [1] => 18\n    [2] => 12345\n    [3] => 23456\n    [4] => 23456\n    [5] => 28,000.25\n    [6] => s\n    [7] => 123456\n    [8] => 2018-09-27\n    [9] => 2018-09-27\n)\n', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(616, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:13:55', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(617, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:13:55', 'Query Executed: INSERT INTO applicant_previous_employers(applicant_id, previous_employer_name, address, job_description, basic_salary, reason_for_leaving, previous_employer_position, previous_employer_date_from, previous_employer_date_to) VALUES(?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssss\n    [1] => 18\n    [2] => 2345\n    [3] => 1\n    [4] => 2345\n    [5] => 28,000.25\n    [6] => 1234\n    [7] => 23456\n    [8] => 2018-09-27\n    [9] => 2018-09-27\n)\n', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(618, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:14:24', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(619, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:14:24', 'Query Executed: INSERT INTO applicant_previous_employers(applicant_id, previous_employer_name, address, job_description, basic_salary, reason_for_leaving, previous_employer_position, previous_employer_date_from, previous_employer_date_to) VALUES(?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssss\n    [1] => 18\n    [2] => 123456\n    [3] => 3456\n    [4] => 23456\n    [5] => 28,000.25\n    [6] => s\n    [7] => 23q456\n    [8] => 2018-09-27\n    [9] => 2018-09-27\n)\n', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(620, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:14:50', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(621, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:14:50', 'Query Executed: INSERT INTO applicant_previous_employers(applicant_id, previous_employer_name, address, job_description, basic_salary, reason_for_leaving, previous_employer_position, previous_employer_date_from, previous_employer_date_to) VALUES(?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssss\n    [1] => 18\n    [2] => 1234\n    [3] => F233 Holt Street\r\nSummitville, Putatan Muntinlupa City\n    [4] => 1234\n    [5] => 28,000.25\n    [6] => s\n    [7] => 234\n    [8] => 2018-09-27\n    [9] => 2018-09-27\n)\n', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(622, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:15:48', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(623, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:15:49', 'Query Executed: INSERT INTO applicant_previous_employers(applicant_id, previous_employer_name, address, job_description, basic_salary, reason_for_leaving, previous_employer_position, previous_employer_date_from, previous_employer_date_to) VALUES(?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssss\n    [1] => 18\n    [2] => 1234\n    [3] => 1234\n    [4] => 234\n    [5] => 28,000.25\n    [6] => s\n    [7] => 1234\n    [8] => 2018-09-27\n    [9] => 2018-09-27\n)\n', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(624, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:17:22', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(625, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:17:22', 'Query Executed: INSERT INTO applicant_previous_employers(applicant_id, previous_employer_name, address, job_description, basic_salary, reason_for_leaving, previous_employer_position, previous_employer_date_from, previous_employer_date_to) VALUES(?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssss\n    [1] => 18\n    [2] => 1234\n    [3] => 2345\n    [4] => 345\n    [5] => 28,000.25\n    [6] => s\n    [7] => 2345\n    [8] => 2018-09-27\n    [9] => 2018-09-27\n)\n', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(626, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:18:18', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(627, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:18:25', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(628, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:18:34', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(629, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:18:34', 'Query Executed: INSERT INTO applicant_previous_employers(applicant_id, previous_employer_name, address, job_description, basic_salary, reason_for_leaving, previous_employer_position, previous_employer_date_from, previous_employer_date_to) VALUES(?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssss\n    [1] => 18\n    [2] => 1234\n    [3] => 1234\n    [4] => 234\n    [5] => 28,000.25\n    [6] => 1\n    [7] => 2\n    [8] => 2018-09-27\n    [9] => 2018-09-27\n)\n', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(630, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:19:16', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(631, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 11:19:17', 'Query Executed: INSERT INTO applicant_previous_employers(applicant_id, previous_employer_name, address, job_description, basic_salary, reason_for_leaving, previous_employer_position, previous_employer_date_from, previous_employer_date_to) VALUES(?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssss\n    [1] => 18\n    [2] => 2\n    [3] => 2\n    [4] => 2\n    [5] => 28000.25\n    [6] => s\n    [7] => 2\n    [8] => 2018-09-27\n    [9] => 2018-09-27\n)\n', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(632, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 13:23:22', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_school_attended/add_applicant_school_attended.php'),
(633, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 13:29:37', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_school_attended/add_applicant_school_attended.php'),
(634, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 13:29:37', 'Query Executed: INSERT INTO applicant_school_attended(applicant_id, school_id, address, educational_attainment, awards, date_from, date_to) VALUES(?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssss\n    [1] => 18\n    [2] => s\n    [3] => Elementary - Undergraduate\n    [4] => aw\n    [5] => s\n    [6] => 2018-09-27\n    [7] => 2018-09-27\n)\n', '/smic_recruitment/modules/applicant/applicant_school_attended/add_applicant_school_attended.php'),
(635, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 13:33:13', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_school_attended/edit_applicant_school_attended.php'),
(636, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 13:33:13', 'Query Executed: UPDATE applicant_school_attended SET applicant_id = ?, school_id = ?, educational_attainment = ?, awards = ?, address = ?, date_from = ?, date_to = ? WHERE applicant_school_attended_id = ?\r\nArray\n(\n    [0] => ssssssss\n    [1] => 18\n    [2] => s\n    [3] => Elementary - Undergraduate\n    [4] => s\n    [5] => aw\n    [6] => 2018-09-27\n    [7] => 2018-09-27\n    [8] => 3\n)\n', '/smic_recruitment/modules/applicant/applicant_school_attended/edit_applicant_school_attended.php'),
(637, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 14:06:21', 'Logged out', '/smic_recruitment/end.php'),
(638, '::1', 'root', '2018-09-27 14:06:26', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(639, '::1', 'root', '2018-09-27 14:08:06', 'Pressed submit button', '/smic_recruitment/sysadmin/add_user_links.php'),
(640, '::1', 'root', '2018-09-27 14:08:07', 'Query Executed: INSERT INTO user_links(name, target, descriptive_title, description, passport_group_id, show_in_tasklist, status, icon, priority) VALUES(?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssss\n    [1] => Other declarations\n    [2] => modules/applicant_other_declarations/add_applicant_other_declarations.php\n    [3] => Other Declarations\n    [4] => a\n    [5] => 3\n    [6] => Yes\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n)\n', '/smic_recruitment/sysadmin/add_user_links.php'),
(641, '::1', 'root', '2018-09-27 14:08:30', 'Query Executed: DELETE FROM user_role_links WHERE role_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(642, '::1', 'root', '2018-09-27 14:08:30', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 49\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(643, '::1', 'root', '2018-09-27 14:08:30', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 57\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(644, '::1', 'root', '2018-09-27 14:08:30', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 61\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(645, '::1', 'root', '2018-09-27 14:08:30', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 65\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(646, '::1', 'root', '2018-09-27 14:08:30', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 69\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(647, '::1', 'root', '2018-09-27 14:08:30', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 73\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(648, '::1', 'root', '2018-09-27 14:08:30', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 77\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(649, '::1', 'root', '2018-09-27 14:08:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 197\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(650, '::1', 'root', '2018-09-27 14:08:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 42\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(651, '::1', 'root', '2018-09-27 14:08:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 50\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(652, '::1', 'root', '2018-09-27 14:08:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 58\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(653, '::1', 'root', '2018-09-27 14:08:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 62\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(654, '::1', 'root', '2018-09-27 14:08:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 66\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(655, '::1', 'root', '2018-09-27 14:08:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 70\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(656, '::1', 'root', '2018-09-27 14:08:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 74\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(657, '::1', 'root', '2018-09-27 14:08:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 78\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(658, '::1', 'root', '2018-09-27 14:08:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 51\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(659, '::1', 'root', '2018-09-27 14:08:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 59\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(660, '::1', 'root', '2018-09-27 14:08:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 63\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(661, '::1', 'root', '2018-09-27 14:08:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 202\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(662, '::1', 'root', '2018-09-27 14:08:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 67\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(663, '::1', 'root', '2018-09-27 14:08:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 71\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(664, '::1', 'root', '2018-09-27 14:08:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 75\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(665, '::1', 'root', '2018-09-27 14:08:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 79\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(666, '::1', 'root', '2018-09-27 14:08:36', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions.php'),
(667, '::1', 'root', '2018-09-27 14:08:40', 'Query Executed: DELETE FROM user_passport WHERE username IN (?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssss\n    [1] => applicant1\n    [2] => markdanico.fernandez1@gmail.com\n    [3] => markdanico.fernandez@gmail.com\n    [4] => markdanico.fernandezz@gmail.com\n    [5] => reyeskimberly018@gmail.com\n    [6] => smic.email.tester@gmail.com\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(668, '::1', 'root', '2018-09-27 14:08:40', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => applicant1\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(669, '::1', 'root', '2018-09-27 14:08:40', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez1@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(670, '::1', 'root', '2018-09-27 14:08:41', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(671, '::1', 'root', '2018-09-27 14:08:41', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandezz@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(672, '::1', 'root', '2018-09-27 14:08:41', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => reyeskimberly018@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(673, '::1', 'root', '2018-09-27 14:08:42', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => smic.email.tester@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(674, '::1', 'root', '2018-09-27 14:08:44', 'Logged out', '/smic_recruitment/end.php'),
(675, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 14:08:52', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(676, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 14:09:06', 'Logged out', '/smic_recruitment/end.php'),
(677, '::1', 'root', '2018-09-27 14:09:09', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(678, '::1', 'root', '2018-09-27 14:09:37', 'Pressed cancel button', '/smic_recruitment/sysadmin/detailview_user_links.php'),
(679, '::1', 'root', '2018-09-27 14:09:47', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(680, '::1', 'root', '2018-09-27 14:09:47', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => Other declarations\n    [2] => modules/applicant_other_declarations/add_applicant_other_declarations.php\n    [3] => Other Declarations\n    [4] => a\n    [5] => 3\n    [6] => Yes\n    [7] => On\n    [8] => form3.png\n    [9] => 99\n    [10] => 202\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(681, '::1', 'root', '2018-09-27 14:10:07', 'Query Executed: DELETE FROM user_role_links WHERE role_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 1\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(682, '::1', 'root', '2018-09-27 14:10:08', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 39\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(683, '::1', 'root', '2018-09-27 14:10:09', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 37\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(684, '::1', 'root', '2018-09-27 14:10:10', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 41\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(685, '::1', 'root', '2018-09-27 14:10:10', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 45\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(686, '::1', 'root', '2018-09-27 14:10:11', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 49\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(687, '::1', 'root', '2018-09-27 14:10:11', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 53\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(688, '::1', 'root', '2018-09-27 14:10:12', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 57\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(689, '::1', 'root', '2018-09-27 14:10:12', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 61\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(690, '::1', 'root', '2018-09-27 14:10:12', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 198\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(691, '::1', 'root', '2018-09-27 14:10:12', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 65\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(692, '::1', 'root', '2018-09-27 14:10:13', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 69\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(693, '::1', 'root', '2018-09-27 14:10:14', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 73\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(694, '::1', 'root', '2018-09-27 14:10:15', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 77\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(695, '::1', 'root', '2018-09-27 14:10:15', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 81\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(696, '::1', 'root', '2018-09-27 14:10:16', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 85\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(697, '::1', 'root', '2018-09-27 14:10:16', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 89\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(698, '::1', 'root', '2018-09-27 14:10:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 93\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(699, '::1', 'root', '2018-09-27 14:10:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 97\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(700, '::1', 'root', '2018-09-27 14:10:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 101\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(701, '::1', 'root', '2018-09-27 14:10:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 105\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(702, '::1', 'root', '2018-09-27 14:10:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 109\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(703, '::1', 'root', '2018-09-27 14:10:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 33\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(704, '::1', 'root', '2018-09-27 14:10:19', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 113\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(705, '::1', 'root', '2018-09-27 14:10:20', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 117\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(706, '::1', 'root', '2018-09-27 14:10:20', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 121\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(707, '::1', 'root', '2018-09-27 14:10:21', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 125\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(708, '::1', 'root', '2018-09-27 14:10:21', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 129\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(709, '::1', 'root', '2018-09-27 14:10:22', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 133\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(710, '::1', 'root', '2018-09-27 14:10:22', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 137\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(711, '::1', 'root', '2018-09-27 14:10:22', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 141\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(712, '::1', 'root', '2018-09-27 14:10:22', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 145\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(713, '::1', 'root', '2018-09-27 14:10:23', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 149\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(714, '::1', 'root', '2018-09-27 14:10:23', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 153\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(715, '::1', 'root', '2018-09-27 14:10:24', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 157\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(716, '::1', 'root', '2018-09-27 14:10:24', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 161\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(717, '::1', 'root', '2018-09-27 14:10:25', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 165\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(718, '::1', 'root', '2018-09-27 14:10:25', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 169\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(719, '::1', 'root', '2018-09-27 14:10:26', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 4\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(720, '::1', 'root', '2018-09-27 14:10:26', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 173\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(721, '::1', 'root', '2018-09-27 14:10:26', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 177\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(722, '::1', 'root', '2018-09-27 14:10:27', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 181\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(723, '::1', 'root', '2018-09-27 14:10:28', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 185\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(724, '::1', 'root', '2018-09-27 14:10:28', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 189\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(725, '::1', 'root', '2018-09-27 14:10:28', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 16\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(726, '::1', 'root', '2018-09-27 14:10:29', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 28\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(727, '::1', 'root', '2018-09-27 14:10:30', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 193\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(728, '::1', 'root', '2018-09-27 14:10:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 8\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(729, '::1', 'root', '2018-09-27 14:10:31', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 20\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(730, '::1', 'root', '2018-09-27 14:10:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 24\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(731, '::1', 'root', '2018-09-27 14:10:32', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 12\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(732, '::1', 'root', '2018-09-27 14:10:33', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 43\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(733, '::1', 'root', '2018-09-27 14:10:33', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 197\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(734, '::1', 'root', '2018-09-27 14:10:33', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 47\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(735, '::1', 'root', '2018-09-27 14:10:34', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 55\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(736, '::1', 'root', '2018-09-27 14:10:34', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 200\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(737, '::1', 'root', '2018-09-27 14:10:35', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 83\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(738, '::1', 'root', '2018-09-27 14:10:35', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 87\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(739, '::1', 'root', '2018-09-27 14:10:35', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 91\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(740, '::1', 'root', '2018-09-27 14:10:36', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 95\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(741, '::1', 'root', '2018-09-27 14:10:36', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 99\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(742, '::1', 'root', '2018-09-27 14:10:36', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 103\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(743, '::1', 'root', '2018-09-27 14:10:37', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 107\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(744, '::1', 'root', '2018-09-27 14:10:37', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 111\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(745, '::1', 'root', '2018-09-27 14:10:37', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 35\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(746, '::1', 'root', '2018-09-27 14:10:38', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 115\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(747, '::1', 'root', '2018-09-27 14:10:38', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 119\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(748, '::1', 'root', '2018-09-27 14:10:39', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 123\n)\n', '/smic_recruitment/sysadmin/role_permissions.php');
INSERT INTO `system_log` (`entry_id`, `ip_address`, `user`, `datetime`, `action`, `module`) VALUES
(749, '::1', 'root', '2018-09-27 14:10:39', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 40\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(750, '::1', 'root', '2018-09-27 14:10:39', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 44\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(751, '::1', 'root', '2018-09-27 14:10:40', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 48\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(752, '::1', 'root', '2018-09-27 14:10:40', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 52\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(753, '::1', 'root', '2018-09-27 14:10:40', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 56\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(754, '::1', 'root', '2018-09-27 14:10:40', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 60\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(755, '::1', 'root', '2018-09-27 14:10:41', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 64\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(756, '::1', 'root', '2018-09-27 14:10:41', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 201\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(757, '::1', 'root', '2018-09-27 14:10:42', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 68\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(758, '::1', 'root', '2018-09-27 14:10:42', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 72\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(759, '::1', 'root', '2018-09-27 14:10:42', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 76\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(760, '::1', 'root', '2018-09-27 14:10:43', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 80\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(761, '::1', 'root', '2018-09-27 14:10:43', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 84\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(762, '::1', 'root', '2018-09-27 14:10:43', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 88\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(763, '::1', 'root', '2018-09-27 14:10:44', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 92\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(764, '::1', 'root', '2018-09-27 14:10:44', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 96\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(765, '::1', 'root', '2018-09-27 14:10:44', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 100\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(766, '::1', 'root', '2018-09-27 14:10:44', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 104\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(767, '::1', 'root', '2018-09-27 14:10:45', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 108\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(768, '::1', 'root', '2018-09-27 14:10:45', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 112\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(769, '::1', 'root', '2018-09-27 14:10:46', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 36\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(770, '::1', 'root', '2018-09-27 14:10:46', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 116\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(771, '::1', 'root', '2018-09-27 14:10:46', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 120\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(772, '::1', 'root', '2018-09-27 14:10:47', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 124\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(773, '::1', 'root', '2018-09-27 14:10:47', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 128\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(774, '::1', 'root', '2018-09-27 14:10:47', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 132\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(775, '::1', 'root', '2018-09-27 14:10:47', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 136\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(776, '::1', 'root', '2018-09-27 14:10:48', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 140\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(777, '::1', 'root', '2018-09-27 14:10:48', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 144\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(778, '::1', 'root', '2018-09-27 14:10:48', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 148\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(779, '::1', 'root', '2018-09-27 14:10:49', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 152\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(780, '::1', 'root', '2018-09-27 14:10:49', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 156\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(781, '::1', 'root', '2018-09-27 14:10:49', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 160\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(782, '::1', 'root', '2018-09-27 14:10:49', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 164\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(783, '::1', 'root', '2018-09-27 14:10:50', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 168\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(784, '::1', 'root', '2018-09-27 14:10:50', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 172\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(785, '::1', 'root', '2018-09-27 14:10:50', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 7\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(786, '::1', 'root', '2018-09-27 14:10:50', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 176\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(787, '::1', 'root', '2018-09-27 14:10:51', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 180\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(788, '::1', 'root', '2018-09-27 14:10:51', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 184\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(789, '::1', 'root', '2018-09-27 14:10:51', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 188\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(790, '::1', 'root', '2018-09-27 14:10:51', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 192\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(791, '::1', 'root', '2018-09-27 14:10:52', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 19\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(792, '::1', 'root', '2018-09-27 14:10:52', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 31\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(793, '::1', 'root', '2018-09-27 14:10:53', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 196\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(794, '::1', 'root', '2018-09-27 14:10:53', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 11\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(795, '::1', 'root', '2018-09-27 14:10:54', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 23\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(796, '::1', 'root', '2018-09-27 14:10:55', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 27\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(797, '::1', 'root', '2018-09-27 14:10:56', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 15\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(798, '::1', 'root', '2018-09-27 14:10:56', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 127\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(799, '::1', 'root', '2018-09-27 14:10:56', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 38\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(800, '::1', 'root', '2018-09-27 14:10:57', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 42\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(801, '::1', 'root', '2018-09-27 14:10:57', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 46\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(802, '::1', 'root', '2018-09-27 14:10:57', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 50\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(803, '::1', 'root', '2018-09-27 14:10:57', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 54\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(804, '::1', 'root', '2018-09-27 14:10:57', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 58\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(805, '::1', 'root', '2018-09-27 14:10:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 62\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(806, '::1', 'root', '2018-09-27 14:10:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 199\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(807, '::1', 'root', '2018-09-27 14:10:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 66\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(808, '::1', 'root', '2018-09-27 14:10:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 70\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(809, '::1', 'root', '2018-09-27 14:10:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 74\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(810, '::1', 'root', '2018-09-27 14:10:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 78\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(811, '::1', 'root', '2018-09-27 14:10:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 82\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(812, '::1', 'root', '2018-09-27 14:10:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 86\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(813, '::1', 'root', '2018-09-27 14:11:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 90\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(814, '::1', 'root', '2018-09-27 14:11:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 94\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(815, '::1', 'root', '2018-09-27 14:11:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 98\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(816, '::1', 'root', '2018-09-27 14:11:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 102\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(817, '::1', 'root', '2018-09-27 14:11:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 106\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(818, '::1', 'root', '2018-09-27 14:11:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 110\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(819, '::1', 'root', '2018-09-27 14:11:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 34\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(820, '::1', 'root', '2018-09-27 14:11:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 114\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(821, '::1', 'root', '2018-09-27 14:11:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 118\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(822, '::1', 'root', '2018-09-27 14:11:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 122\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(823, '::1', 'root', '2018-09-27 14:11:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 126\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(824, '::1', 'root', '2018-09-27 14:11:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 130\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(825, '::1', 'root', '2018-09-27 14:11:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 134\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(826, '::1', 'root', '2018-09-27 14:11:04', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 138\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(827, '::1', 'root', '2018-09-27 14:11:04', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 142\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(828, '::1', 'root', '2018-09-27 14:11:04', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 146\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(829, '::1', 'root', '2018-09-27 14:11:05', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 150\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(830, '::1', 'root', '2018-09-27 14:11:05', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 154\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(831, '::1', 'root', '2018-09-27 14:11:05', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 158\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(832, '::1', 'root', '2018-09-27 14:11:06', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 162\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(833, '::1', 'root', '2018-09-27 14:11:06', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 166\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(834, '::1', 'root', '2018-09-27 14:11:07', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 170\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(835, '::1', 'root', '2018-09-27 14:11:07', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 5\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(836, '::1', 'root', '2018-09-27 14:11:07', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 174\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(837, '::1', 'root', '2018-09-27 14:11:07', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 178\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(838, '::1', 'root', '2018-09-27 14:11:08', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 182\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(839, '::1', 'root', '2018-09-27 14:11:09', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 186\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(840, '::1', 'root', '2018-09-27 14:11:09', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 190\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(841, '::1', 'root', '2018-09-27 14:11:10', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 17\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(842, '::1', 'root', '2018-09-27 14:11:10', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 29\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(843, '::1', 'root', '2018-09-27 14:11:10', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 194\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(844, '::1', 'root', '2018-09-27 14:11:11', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 9\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(845, '::1', 'root', '2018-09-27 14:11:11', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 21\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(846, '::1', 'root', '2018-09-27 14:11:12', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 25\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(847, '::1', 'root', '2018-09-27 14:11:12', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 13\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(848, '::1', 'root', '2018-09-27 14:11:13', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 131\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(849, '::1', 'root', '2018-09-27 14:11:13', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 135\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(850, '::1', 'root', '2018-09-27 14:11:14', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 139\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(851, '::1', 'root', '2018-09-27 14:11:14', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 143\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(852, '::1', 'root', '2018-09-27 14:11:15', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 147\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(853, '::1', 'root', '2018-09-27 14:11:15', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 151\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(854, '::1', 'root', '2018-09-27 14:11:15', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 51\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(855, '::1', 'root', '2018-09-27 14:11:16', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 155\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(856, '::1', 'root', '2018-09-27 14:11:16', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 159\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(857, '::1', 'root', '2018-09-27 14:11:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 163\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(858, '::1', 'root', '2018-09-27 14:11:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 59\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(859, '::1', 'root', '2018-09-27 14:11:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 63\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(860, '::1', 'root', '2018-09-27 14:11:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 1\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(861, '::1', 'root', '2018-09-27 14:11:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 202\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(862, '::1', 'root', '2018-09-27 14:11:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 167\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(863, '::1', 'root', '2018-09-27 14:11:19', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 171\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(864, '::1', 'root', '2018-09-27 14:11:19', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 6\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(865, '::1', 'root', '2018-09-27 14:11:19', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 175\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(866, '::1', 'root', '2018-09-27 14:11:19', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 179\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(867, '::1', 'root', '2018-09-27 14:11:20', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 67\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(868, '::1', 'root', '2018-09-27 14:11:20', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 71\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(869, '::1', 'root', '2018-09-27 14:11:21', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 32\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(870, '::1', 'root', '2018-09-27 14:11:22', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 183\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(871, '::1', 'root', '2018-09-27 14:11:23', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 187\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(872, '::1', 'root', '2018-09-27 14:11:23', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 75\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(873, '::1', 'root', '2018-09-27 14:11:24', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(874, '::1', 'root', '2018-09-27 14:11:24', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 2\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(875, '::1', 'root', '2018-09-27 14:11:24', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 191\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(876, '::1', 'root', '2018-09-27 14:11:25', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 79\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(877, '::1', 'root', '2018-09-27 14:11:26', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 18\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(878, '::1', 'root', '2018-09-27 14:11:27', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 30\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(879, '::1', 'root', '2018-09-27 14:11:27', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 195\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(880, '::1', 'root', '2018-09-27 14:11:27', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 10\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(881, '::1', 'root', '2018-09-27 14:11:28', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 22\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(882, '::1', 'root', '2018-09-27 14:11:29', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 26\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(883, '::1', 'root', '2018-09-27 14:11:30', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 14\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(884, '::1', 'root', '2018-09-27 14:11:36', 'Query Executed: DELETE FROM user_passport WHERE username IN (?)\r\nArray\n(\n    [0] => s\n    [1] => root\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(885, '::1', 'root', '2018-09-27 14:11:36', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => root\n    [2] => 1\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(886, '::1', 'root', '2018-09-27 14:12:26', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(887, '::1', 'root', '2018-09-27 14:12:26', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => Other declarations\n    [2] => modules/applicant_other_declarations/add_applicant_other_declarations.php\n    [3] => Other Declarations\n    [4] => a\n    [5] => 3\n    [6] => Yes\n    [7] => On\n    [8] => form3.png\n    [9] => -1\n    [10] => 202\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(888, '::1', 'root', '2018-09-27 14:12:33', 'Logged out', '/smic_recruitment/end.php'),
(889, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 14:13:12', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(890, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 14:18:22', 'ILLEGAL ACCESS ATTEMPT - Tried to access ''/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'' without sufficient privileges.', '/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'),
(891, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 14:18:48', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(892, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 14:18:52', 'ILLEGAL ACCESS ATTEMPT - Tried to access ''/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'' without sufficient privileges.', '/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'),
(893, '::1', 'root', '2018-09-27 14:18:59', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(894, '::1', 'root', '2018-09-27 14:19:06', 'ILLEGAL ACCESS ATTEMPT - Tried to access ''/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'' without sufficient privileges.', '/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'),
(895, '::1', 'root', '2018-09-27 14:19:18', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(896, '::1', 'root', '2018-09-27 14:19:41', 'Query Executed: DELETE FROM user_role_links WHERE role_id = ? AND link_id IN (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 3\n    [2] => 37\n    [3] => 38\n    [4] => 39\n    [5] => 40\n    [6] => 81\n    [7] => 82\n    [8] => 83\n    [9] => 84\n    [10] => 85\n    [11] => 86\n    [12] => 87\n    [13] => 88\n    [14] => 89\n    [15] => 90\n    [16] => 91\n    [17] => 92\n    [18] => 93\n    [19] => 94\n    [20] => 95\n    [21] => 96\n    [22] => 97\n    [23] => 98\n    [24] => 99\n    [25] => 100\n    [26] => 101\n    [27] => 102\n    [28] => 103\n    [29] => 104\n    [30] => 105\n    [31] => 106\n    [32] => 107\n    [33] => 108\n    [34] => 109\n    [35] => 110\n    [36] => 111\n    [37] => 112\n    [38] => 113\n    [39] => 114\n    [40] => 115\n    [41] => 116\n    [42] => 117\n    [43] => 118\n    [44] => 119\n    [45] => 120\n    [46] => 121\n    [47] => 122\n    [48] => 123\n    [49] => 124\n    [50] => 125\n    [51] => 126\n    [52] => 127\n    [53] => 128\n    [54] => 129\n    [55] => 130\n    [56] => 131\n    [57] => 132\n    [58] => 133\n    [59] => 134\n    [60] => 135\n    [61] => 136\n    [62] => 137\n    [63] => 138\n    [64] => 139\n    [65] => 140\n    [66] => 141\n    [67] => 142\n    [68] => 143\n    [69] => 144\n    [70] => 145\n    [71] => 146\n    [72] => 147\n    [73] => 148\n    [74] => 149\n    [75] => 150\n    [76] => 151\n    [77] => 152\n    [78] => 153\n    [79] => 154\n    [80] => 155\n    [81] => 156\n    [82] => 157\n    [83] => 158\n    [84] => 159\n    [85] => 160\n    [86] => 161\n    [87] => 162\n    [88] => 163\n    [89] => 164\n    [90] => 165\n    [91] => 166\n    [92] => 167\n    [93] => 168\n    [94] => 169\n    [95] => 170\n    [96] => 171\n    [97] => 172\n    [98] => 173\n    [99] => 174\n    [100] => 175\n    [101] => 176\n    [102] => 177\n    [103] => 178\n    [104] => 179\n    [105] => 180\n    [106] => 181\n    [107] => 182\n    [108] => 183\n    [109] => 184\n    [110] => 185\n    [111] => 186\n    [112] => 187\n    [113] => 188\n    [114] => 189\n    [115] => 190\n    [116] => 191\n    [117] => 192\n    [118] => 193\n    [119] => 194\n    [120] => 195\n    [121] => 196\n    [122] => 203\n    [123] => 204\n    [124] => 205\n    [125] => 206\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(897, '::1', 'root', '2018-09-27 14:19:41', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 203\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(898, '::1', 'root', '2018-09-27 14:19:45', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions.php'),
(899, '::1', 'root', '2018-09-27 14:19:48', 'Query Executed: DELETE FROM user_passport WHERE username IN (?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssss\n    [1] => applicant1\n    [2] => markdanico.fernandez1@gmail.com\n    [3] => markdanico.fernandez@gmail.com\n    [4] => markdanico.fernandezz@gmail.com\n    [5] => reyeskimberly018@gmail.com\n    [6] => smic.email.tester@gmail.com\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(900, '::1', 'root', '2018-09-27 14:19:48', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => applicant1\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(901, '::1', 'root', '2018-09-27 14:19:48', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez1@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(902, '::1', 'root', '2018-09-27 14:19:49', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(903, '::1', 'root', '2018-09-27 14:19:49', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandezz@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(904, '::1', 'root', '2018-09-27 14:19:49', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => reyeskimberly018@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(905, '::1', 'root', '2018-09-27 14:19:49', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => smic.email.tester@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(906, '::1', 'root', '2018-09-27 14:19:50', 'Logged out', '/smic_recruitment/end.php'),
(907, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 14:19:58', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(908, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 14:27:18', 'Pressed cancel button', '/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'),
(909, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 14:27:18', 'ILLEGAL ACCESS ATTEMPT - Tried to access ''/smic_recruitment/modules/applicant_other_declarations/listview_applicant_other_declarations.php'' without sufficient privileges.', '/smic_recruitment/modules/applicant_other_declarations/listview_applicant_other_declarations.php'),
(910, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 14:28:06', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(911, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 14:28:09', 'Pressed cancel button', '/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'),
(912, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 14:28:22', 'Pressed cancel button', '/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'),
(913, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 14:52:32', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'),
(914, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 14:53:06', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'),
(915, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:00:28', 'Logged out', '/smic_recruitment/end.php'),
(916, '::1', 'root', '2018-09-27 16:00:33', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(917, '::1', 'root', '2018-09-27 16:01:08', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(918, '::1', 'root', '2018-09-27 16:01:08', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => Other declarations\n    [2] => modules/applicant_other_declarations/edit_applicant_other_declarations.php\n    [3] => Other Declarations\n    [4] => a\n    [5] => 3\n    [6] => Yes\n    [7] => On\n    [8] => form3.png\n    [9] => -1\n    [10] => 202\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(919, '::1', 'root', '2018-09-27 16:02:12', 'Logged out', '/smic_recruitment/end.php'),
(920, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:02:22', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(921, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:02:24', 'ILLEGAL ACCESS ATTEMPT - Tried to access ''/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'' without sufficient privileges.', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(922, '::1', 'root', '2018-09-27 16:02:36', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(923, '::1', 'root', '2018-09-27 16:02:58', 'Query Executed: DELETE FROM user_role_links WHERE role_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(924, '::1', 'root', '2018-09-27 16:02:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 49\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(925, '::1', 'root', '2018-09-27 16:02:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 57\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(926, '::1', 'root', '2018-09-27 16:02:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 61\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(927, '::1', 'root', '2018-09-27 16:02:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 203\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(928, '::1', 'root', '2018-09-27 16:02:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 65\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(929, '::1', 'root', '2018-09-27 16:02:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 69\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(930, '::1', 'root', '2018-09-27 16:02:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 73\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(931, '::1', 'root', '2018-09-27 16:02:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 77\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(932, '::1', 'root', '2018-09-27 16:02:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 197\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(933, '::1', 'root', '2018-09-27 16:02:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 42\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(934, '::1', 'root', '2018-09-27 16:02:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 50\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(935, '::1', 'root', '2018-09-27 16:02:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 58\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(936, '::1', 'root', '2018-09-27 16:02:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 62\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(937, '::1', 'root', '2018-09-27 16:02:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 204\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(938, '::1', 'root', '2018-09-27 16:02:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 66\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(939, '::1', 'root', '2018-09-27 16:02:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 70\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(940, '::1', 'root', '2018-09-27 16:03:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 74\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(941, '::1', 'root', '2018-09-27 16:03:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 78\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(942, '::1', 'root', '2018-09-27 16:03:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 51\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(943, '::1', 'root', '2018-09-27 16:03:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 59\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(944, '::1', 'root', '2018-09-27 16:03:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 63\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(945, '::1', 'root', '2018-09-27 16:03:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 202\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(946, '::1', 'root', '2018-09-27 16:03:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 67\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(947, '::1', 'root', '2018-09-27 16:03:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 71\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(948, '::1', 'root', '2018-09-27 16:03:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 75\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(949, '::1', 'root', '2018-09-27 16:03:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 79\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(950, '::1', 'root', '2018-09-27 16:03:05', 'Logged out', '/smic_recruitment/end.php'),
(951, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:03:13', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(952, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:03:14', 'ILLEGAL ACCESS ATTEMPT - Tried to access ''/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'' without sufficient privileges.', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(953, '::1', 'root', '2018-09-27 16:03:22', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(954, '::1', 'root', '2018-09-27 16:03:31', 'Query Executed: DELETE FROM user_passport WHERE username IN (?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssss\n    [1] => applicant1\n    [2] => markdanico.fernandez1@gmail.com\n    [3] => markdanico.fernandez@gmail.com\n    [4] => markdanico.fernandezz@gmail.com\n    [5] => reyeskimberly018@gmail.com\n    [6] => smic.email.tester@gmail.com\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(955, '::1', 'root', '2018-09-27 16:03:32', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => applicant1\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(956, '::1', 'root', '2018-09-27 16:03:32', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez1@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php');
INSERT INTO `system_log` (`entry_id`, `ip_address`, `user`, `datetime`, `action`, `module`) VALUES
(957, '::1', 'root', '2018-09-27 16:03:32', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(958, '::1', 'root', '2018-09-27 16:03:32', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandezz@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(959, '::1', 'root', '2018-09-27 16:03:32', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => reyeskimberly018@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(960, '::1', 'root', '2018-09-27 16:03:32', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => smic.email.tester@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(961, '::1', 'root', '2018-09-27 16:03:34', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(962, '::1', 'root', '2018-09-27 16:03:35', 'Logged out', '/smic_recruitment/end.php'),
(963, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:03:43', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(964, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:04:24', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(965, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:04:57', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(966, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:12:41', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(967, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:12:44', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(968, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:12:44', 'Query Executed: DELETE FROM other_declarations_name_smgroup WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => s\n    [1] => \n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(969, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:12:44', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name, company) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => \n    [2] => hey\n    [3] => h\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(970, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:12:45', 'Query Executed: DELETE FROM other_declarations_name_smic_employ WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => s\n    [1] => \n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(971, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:12:45', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name) VALUES(?,?)\r\nArray\n(\n    [0] => ss\n    [1] => \n    [2] => hey\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(972, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:12:45', 'Query Executed: UPDATE applicant_other_declarations SET applicant_id = ?, crime_convict = ?, details = ? WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => ssss\n    [1] => 18\n    [2] => No\n    [3] => a\n    [4] => \n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(973, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:18:40', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'),
(974, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:18:40', 'Query Executed: INSERT INTO applicant_other_declarations(applicant_id, crime_convict, details) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 18\n    [2] => No\n    [3] => s\n)\n', '/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'),
(975, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:18:40', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name, company) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 1\n    [2] => as\n    [3] => s\n)\n', '/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'),
(976, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:18:40', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name) VALUES(?,?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => a\n)\n', '/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'),
(977, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:18:40', 'ILLEGAL ACCESS ATTEMPT - Tried to access ''/smic_recruitment/modules/applicant_other_declarations/listview_applicant_other_declarations.php'' without sufficient privileges.', '/smic_recruitment/modules/applicant_other_declarations/listview_applicant_other_declarations.php'),
(978, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:19:11', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(979, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:21:19', 'Logged out', '/smic_recruitment/end.php'),
(980, '::1', 'root', '2018-09-27 16:21:21', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(981, '::1', 'root', '2018-09-27 16:22:31', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(982, '::1', 'root', '2018-09-27 16:22:31', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => Other declarations\n    [2] => modules/applicant_other_declarations/process_declaration.php\n    [3] => Other Declarations\n    [4] => a\n    [5] => 3\n    [6] => Yes\n    [7] => On\n    [8] => form3.png\n    [9] => -1\n    [10] => 202\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(983, '::1', 'root', '2018-09-27 16:22:35', 'ILLEGAL ACCESS ATTEMPT - Tried to access ''/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'' without sufficient privileges.', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(984, '::1', 'root', '2018-09-27 16:22:44', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(985, '::1', 'root', '2018-09-27 16:23:13', 'Logged out', '/smic_recruitment/end.php'),
(986, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:23:19', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(987, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:27:16', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(988, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:27:17', 'Query Executed: DELETE FROM other_declarations_name_smgroup WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(989, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:27:17', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name, company) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 1\n    [2] => as\n    [3] => s\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(990, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:27:17', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name, company) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 1\n    [2] => s\n    [3] => s\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(991, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:27:17', 'Query Executed: DELETE FROM other_declarations_name_smic_employ WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(992, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:27:17', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name) VALUES(?,?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => a\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(993, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:27:17', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name) VALUES(?,?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(994, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:27:18', 'Query Executed: UPDATE applicant_other_declarations SET applicant_id = ?, crime_convict = ?, details = ? WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => ssss\n    [1] => 18\n    [2] => No\n    [3] => s\n    [4] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(995, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:54:34', 'Logged out', '/smic_recruitment/end.php'),
(996, '::1', 'root', '2018-09-27 16:54:37', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(997, '::1', 'root', '2018-09-27 16:54:53', 'ILLEGAL ACCESS ATTEMPT - Tried to access ''/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'' without sufficient privileges.', '/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'),
(998, '::1', 'root', '2018-09-27 16:55:12', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(999, '::1', 'root', '2018-09-27 16:56:46', 'Pressed cancel button', '/smic_recruitment/modules/plantilla/detailview_plantilla.php'),
(1000, '::1', 'root', '2018-09-27 16:57:03', 'Logged out', '/smic_recruitment/end.php'),
(1001, '::1', 'root', '2018-09-27 16:57:11', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1002, '::1', 'root', '2018-09-27 16:57:14', 'Logged out', '/smic_recruitment/end.php'),
(1003, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:57:24', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1004, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:58:20', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'),
(1005, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:58:20', 'Query Executed: INSERT INTO applicant_other_declarations(applicant_id, crime_convict, details) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 18\n    [2] => Yes\n    [3] => s\n)\n', '/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'),
(1006, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:58:20', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name, company) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 1\n    [2] => sa\n    [3] => a\n)\n', '/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'),
(1007, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:58:20', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name) VALUES(?,?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => sa\n)\n', '/smic_recruitment/modules/applicant_other_declarations/add_applicant_other_declarations.php'),
(1008, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 16:59:44', 'Pressed cancel button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1009, '::1', 'markdanico.fernandez@gmail.com', '2018-09-27 17:00:04', 'Logged out', '/smic_recruitment/end.php'),
(1010, '::1', 'markdanico.fernandez@gmail.com', '2018-09-28 14:12:52', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1011, '::1', 'markdanico.fernandez@gmail.com', '2018-09-28 14:20:35', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_school_attended/add_applicant_school_attended.php'),
(1012, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 09:27:25', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1013, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 10:14:30', 'Logged out', '/smic_recruitment/end.php'),
(1014, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 10:23:16', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1015, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 10:33:21', 'Logged out', '/smic_recruitment/end.php'),
(1016, '::1', 'root', '2018-10-02 10:33:27', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1017, '::1', 'root', '2018-10-02 10:35:54', 'Query Executed: DELETE FROM user_role_links WHERE role_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 1\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1018, '::1', 'root', '2018-10-02 10:35:54', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 39\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1019, '::1', 'root', '2018-10-02 10:35:54', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 37\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1020, '::1', 'root', '2018-10-02 10:35:54', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 41\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1021, '::1', 'root', '2018-10-02 10:35:54', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 45\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1022, '::1', 'root', '2018-10-02 10:35:54', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 49\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1023, '::1', 'root', '2018-10-02 10:35:54', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 53\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1024, '::1', 'root', '2018-10-02 10:35:55', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 57\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1025, '::1', 'root', '2018-10-02 10:35:55', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 61\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1026, '::1', 'root', '2018-10-02 10:35:55', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 203\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1027, '::1', 'root', '2018-10-02 10:35:55', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 65\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1028, '::1', 'root', '2018-10-02 10:35:55', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 69\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1029, '::1', 'root', '2018-10-02 10:35:55', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 73\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1030, '::1', 'root', '2018-10-02 10:35:55', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 77\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1031, '::1', 'root', '2018-10-02 10:35:55', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 81\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1032, '::1', 'root', '2018-10-02 10:35:55', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 85\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1033, '::1', 'root', '2018-10-02 10:35:56', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 89\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1034, '::1', 'root', '2018-10-02 10:35:56', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 93\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1035, '::1', 'root', '2018-10-02 10:35:56', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 97\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1036, '::1', 'root', '2018-10-02 10:35:56', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 101\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1037, '::1', 'root', '2018-10-02 10:35:56', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 105\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1038, '::1', 'root', '2018-10-02 10:35:56', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 109\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1039, '::1', 'root', '2018-10-02 10:35:56', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 33\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1040, '::1', 'root', '2018-10-02 10:35:56', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 113\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1041, '::1', 'root', '2018-10-02 10:35:56', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 117\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1042, '::1', 'root', '2018-10-02 10:35:57', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 121\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1043, '::1', 'root', '2018-10-02 10:35:57', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 125\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1044, '::1', 'root', '2018-10-02 10:35:57', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 129\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1045, '::1', 'root', '2018-10-02 10:35:57', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 133\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1046, '::1', 'root', '2018-10-02 10:35:57', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 137\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1047, '::1', 'root', '2018-10-02 10:35:57', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 141\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1048, '::1', 'root', '2018-10-02 10:35:57', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 145\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1049, '::1', 'root', '2018-10-02 10:35:57', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 149\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1050, '::1', 'root', '2018-10-02 10:35:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 153\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1051, '::1', 'root', '2018-10-02 10:35:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 157\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1052, '::1', 'root', '2018-10-02 10:35:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 161\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1053, '::1', 'root', '2018-10-02 10:35:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 207\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1054, '::1', 'root', '2018-10-02 10:35:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 211\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1055, '::1', 'root', '2018-10-02 10:35:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 165\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1056, '::1', 'root', '2018-10-02 10:35:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 169\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1057, '::1', 'root', '2018-10-02 10:35:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 4\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1058, '::1', 'root', '2018-10-02 10:35:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 173\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1059, '::1', 'root', '2018-10-02 10:35:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 177\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1060, '::1', 'root', '2018-10-02 10:35:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 181\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1061, '::1', 'root', '2018-10-02 10:35:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 185\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1062, '::1', 'root', '2018-10-02 10:35:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 189\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1063, '::1', 'root', '2018-10-02 10:35:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 16\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1064, '::1', 'root', '2018-10-02 10:35:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 28\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1065, '::1', 'root', '2018-10-02 10:36:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 193\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1066, '::1', 'root', '2018-10-02 10:36:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 8\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1067, '::1', 'root', '2018-10-02 10:36:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 20\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1068, '::1', 'root', '2018-10-02 10:36:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 24\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1069, '::1', 'root', '2018-10-02 10:36:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 12\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1070, '::1', 'root', '2018-10-02 10:36:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 43\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1071, '::1', 'root', '2018-10-02 10:36:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 197\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1072, '::1', 'root', '2018-10-02 10:36:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 47\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1073, '::1', 'root', '2018-10-02 10:36:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 55\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1074, '::1', 'root', '2018-10-02 10:36:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 205\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1075, '::1', 'root', '2018-10-02 10:36:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 83\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1076, '::1', 'root', '2018-10-02 10:36:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 87\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1077, '::1', 'root', '2018-10-02 10:36:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 91\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1078, '::1', 'root', '2018-10-02 10:36:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 95\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1079, '::1', 'root', '2018-10-02 10:36:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 99\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1080, '::1', 'root', '2018-10-02 10:36:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 103\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1081, '::1', 'root', '2018-10-02 10:36:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 107\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1082, '::1', 'root', '2018-10-02 10:36:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 111\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1083, '::1', 'root', '2018-10-02 10:36:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 35\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1084, '::1', 'root', '2018-10-02 10:36:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 115\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1085, '::1', 'root', '2018-10-02 10:36:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 119\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1086, '::1', 'root', '2018-10-02 10:36:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 123\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1087, '::1', 'root', '2018-10-02 10:36:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 40\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1088, '::1', 'root', '2018-10-02 10:36:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 44\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1089, '::1', 'root', '2018-10-02 10:36:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 48\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1090, '::1', 'root', '2018-10-02 10:36:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 52\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1091, '::1', 'root', '2018-10-02 10:36:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 56\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1092, '::1', 'root', '2018-10-02 10:36:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 60\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1093, '::1', 'root', '2018-10-02 10:36:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 64\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1094, '::1', 'root', '2018-10-02 10:36:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 206\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1095, '::1', 'root', '2018-10-02 10:36:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 68\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1096, '::1', 'root', '2018-10-02 10:36:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 72\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1097, '::1', 'root', '2018-10-02 10:36:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 76\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1098, '::1', 'root', '2018-10-02 10:36:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 80\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1099, '::1', 'root', '2018-10-02 10:36:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 84\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1100, '::1', 'root', '2018-10-02 10:36:04', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 88\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1101, '::1', 'root', '2018-10-02 10:36:04', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 92\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1102, '::1', 'root', '2018-10-02 10:36:04', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 96\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1103, '::1', 'root', '2018-10-02 10:36:04', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 100\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1104, '::1', 'root', '2018-10-02 10:36:04', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 104\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1105, '::1', 'root', '2018-10-02 10:36:04', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 108\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1106, '::1', 'root', '2018-10-02 10:36:04', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 112\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1107, '::1', 'root', '2018-10-02 10:36:04', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 36\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1108, '::1', 'root', '2018-10-02 10:36:05', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 116\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1109, '::1', 'root', '2018-10-02 10:36:05', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 120\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1110, '::1', 'root', '2018-10-02 10:36:05', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 124\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1111, '::1', 'root', '2018-10-02 10:36:05', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 128\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1112, '::1', 'root', '2018-10-02 10:36:05', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 132\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1113, '::1', 'root', '2018-10-02 10:36:05', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 136\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1114, '::1', 'root', '2018-10-02 10:36:05', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 140\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1115, '::1', 'root', '2018-10-02 10:36:05', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 144\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1116, '::1', 'root', '2018-10-02 10:36:05', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 148\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1117, '::1', 'root', '2018-10-02 10:36:05', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 152\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1118, '::1', 'root', '2018-10-02 10:36:05', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 156\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1119, '::1', 'root', '2018-10-02 10:36:05', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 160\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1120, '::1', 'root', '2018-10-02 10:36:06', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 164\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1121, '::1', 'root', '2018-10-02 10:36:06', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 210\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1122, '::1', 'root', '2018-10-02 10:36:06', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 214\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1123, '::1', 'root', '2018-10-02 10:36:06', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 168\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1124, '::1', 'root', '2018-10-02 10:36:06', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 172\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1125, '::1', 'root', '2018-10-02 10:36:06', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 7\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1126, '::1', 'root', '2018-10-02 10:36:06', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 176\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1127, '::1', 'root', '2018-10-02 10:36:06', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 180\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1128, '::1', 'root', '2018-10-02 10:36:06', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 184\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1129, '::1', 'root', '2018-10-02 10:36:06', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 188\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1130, '::1', 'root', '2018-10-02 10:36:07', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 192\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1131, '::1', 'root', '2018-10-02 10:36:07', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 19\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1132, '::1', 'root', '2018-10-02 10:36:07', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 31\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1133, '::1', 'root', '2018-10-02 10:36:07', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 196\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1134, '::1', 'root', '2018-10-02 10:36:07', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 11\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1135, '::1', 'root', '2018-10-02 10:36:07', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 23\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1136, '::1', 'root', '2018-10-02 10:36:07', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 27\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1137, '::1', 'root', '2018-10-02 10:36:07', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 15\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1138, '::1', 'root', '2018-10-02 10:36:07', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 127\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1139, '::1', 'root', '2018-10-02 10:36:07', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 38\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1140, '::1', 'root', '2018-10-02 10:36:07', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 42\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1141, '::1', 'root', '2018-10-02 10:36:08', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 46\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1142, '::1', 'root', '2018-10-02 10:36:08', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 50\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1143, '::1', 'root', '2018-10-02 10:36:08', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 54\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1144, '::1', 'root', '2018-10-02 10:36:08', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 58\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1145, '::1', 'root', '2018-10-02 10:36:08', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 62\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1146, '::1', 'root', '2018-10-02 10:36:08', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 204\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1147, '::1', 'root', '2018-10-02 10:36:08', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 66\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1148, '::1', 'root', '2018-10-02 10:36:08', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 70\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1149, '::1', 'root', '2018-10-02 10:36:09', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 74\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1150, '::1', 'root', '2018-10-02 10:36:09', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 78\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1151, '::1', 'root', '2018-10-02 10:36:09', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 82\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1152, '::1', 'root', '2018-10-02 10:36:09', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 86\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1153, '::1', 'root', '2018-10-02 10:36:09', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 90\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1154, '::1', 'root', '2018-10-02 10:36:09', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 94\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1155, '::1', 'root', '2018-10-02 10:36:09', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 98\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1156, '::1', 'root', '2018-10-02 10:36:09', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 102\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1157, '::1', 'root', '2018-10-02 10:36:09', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 106\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1158, '::1', 'root', '2018-10-02 10:36:10', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 110\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1159, '::1', 'root', '2018-10-02 10:36:10', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 34\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1160, '::1', 'root', '2018-10-02 10:36:10', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 114\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1161, '::1', 'root', '2018-10-02 10:36:10', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 118\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1162, '::1', 'root', '2018-10-02 10:36:10', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 122\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1163, '::1', 'root', '2018-10-02 10:36:10', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 126\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1164, '::1', 'root', '2018-10-02 10:36:10', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 130\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1165, '::1', 'root', '2018-10-02 10:36:11', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 134\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1166, '::1', 'root', '2018-10-02 10:36:11', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 138\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1167, '::1', 'root', '2018-10-02 10:36:11', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 142\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1168, '::1', 'root', '2018-10-02 10:36:11', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 146\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1169, '::1', 'root', '2018-10-02 10:36:11', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 150\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1170, '::1', 'root', '2018-10-02 10:36:11', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 154\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1171, '::1', 'root', '2018-10-02 10:36:11', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 158\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1172, '::1', 'root', '2018-10-02 10:36:11', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 162\n)\n', '/smic_recruitment/sysadmin/role_permissions.php');
INSERT INTO `system_log` (`entry_id`, `ip_address`, `user`, `datetime`, `action`, `module`) VALUES
(1173, '::1', 'root', '2018-10-02 10:36:11', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 208\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1174, '::1', 'root', '2018-10-02 10:36:12', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 212\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1175, '::1', 'root', '2018-10-02 10:36:12', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 166\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1176, '::1', 'root', '2018-10-02 10:36:12', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 170\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1177, '::1', 'root', '2018-10-02 10:36:12', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 5\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1178, '::1', 'root', '2018-10-02 10:36:12', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 174\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1179, '::1', 'root', '2018-10-02 10:36:12', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 178\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1180, '::1', 'root', '2018-10-02 10:36:12', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 182\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1181, '::1', 'root', '2018-10-02 10:36:12', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 186\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1182, '::1', 'root', '2018-10-02 10:36:13', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 190\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1183, '::1', 'root', '2018-10-02 10:36:13', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 17\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1184, '::1', 'root', '2018-10-02 10:36:13', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 29\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1185, '::1', 'root', '2018-10-02 10:36:13', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 194\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1186, '::1', 'root', '2018-10-02 10:36:13', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 9\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1187, '::1', 'root', '2018-10-02 10:36:13', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 21\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1188, '::1', 'root', '2018-10-02 10:36:13', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 25\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1189, '::1', 'root', '2018-10-02 10:36:14', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 13\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1190, '::1', 'root', '2018-10-02 10:36:14', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 131\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1191, '::1', 'root', '2018-10-02 10:36:14', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 135\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1192, '::1', 'root', '2018-10-02 10:36:14', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 139\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1193, '::1', 'root', '2018-10-02 10:36:14', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 143\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1194, '::1', 'root', '2018-10-02 10:36:14', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 147\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1195, '::1', 'root', '2018-10-02 10:36:14', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 151\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1196, '::1', 'root', '2018-10-02 10:36:14', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 51\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1197, '::1', 'root', '2018-10-02 10:36:15', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 155\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1198, '::1', 'root', '2018-10-02 10:36:15', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 159\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1199, '::1', 'root', '2018-10-02 10:36:15', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 163\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1200, '::1', 'root', '2018-10-02 10:36:16', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 209\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1201, '::1', 'root', '2018-10-02 10:36:16', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 213\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1202, '::1', 'root', '2018-10-02 10:36:16', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 59\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1203, '::1', 'root', '2018-10-02 10:36:16', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 63\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1204, '::1', 'root', '2018-10-02 10:36:16', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 1\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1205, '::1', 'root', '2018-10-02 10:36:16', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 202\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1206, '::1', 'root', '2018-10-02 10:36:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 167\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1207, '::1', 'root', '2018-10-02 10:36:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 171\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1208, '::1', 'root', '2018-10-02 10:36:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 6\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1209, '::1', 'root', '2018-10-02 10:36:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 175\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1210, '::1', 'root', '2018-10-02 10:36:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 179\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1211, '::1', 'root', '2018-10-02 10:36:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 67\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1212, '::1', 'root', '2018-10-02 10:36:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 71\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1213, '::1', 'root', '2018-10-02 10:36:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 32\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1214, '::1', 'root', '2018-10-02 10:36:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 183\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1215, '::1', 'root', '2018-10-02 10:36:17', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 187\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1216, '::1', 'root', '2018-10-02 10:36:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 75\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1217, '::1', 'root', '2018-10-02 10:36:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1218, '::1', 'root', '2018-10-02 10:36:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 2\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1219, '::1', 'root', '2018-10-02 10:36:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 191\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1220, '::1', 'root', '2018-10-02 10:36:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 79\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1221, '::1', 'root', '2018-10-02 10:36:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 18\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1222, '::1', 'root', '2018-10-02 10:36:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 30\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1223, '::1', 'root', '2018-10-02 10:36:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 195\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1224, '::1', 'root', '2018-10-02 10:36:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 10\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1225, '::1', 'root', '2018-10-02 10:36:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 22\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1226, '::1', 'root', '2018-10-02 10:36:18', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 26\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1227, '::1', 'root', '2018-10-02 10:36:19', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => 14\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1228, '::1', 'root', '2018-10-02 10:36:41', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions.php'),
(1229, '::1', 'root', '2018-10-02 10:36:43', 'Query Executed: DELETE FROM user_passport WHERE username IN (?)\r\nArray\n(\n    [0] => s\n    [1] => root\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1230, '::1', 'root', '2018-10-02 10:36:44', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => root\n    [2] => 1\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1231, '::1', 'root', '2018-10-02 10:36:45', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1232, '::1', 'root', '2018-10-02 10:57:58', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(1233, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 11:34:45', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1234, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 11:35:13', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1235, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 11:35:21', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1236, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 11:37:17', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1237, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 11:41:18', 'ILLEGAL ACCESS ATTEMPT - Tried to access ''/smic_recruitment/modules/applicant/applicant/listview_applicant.php'' without sufficient privileges.', '/smic_recruitment/modules/applicant/applicant/listview_applicant.php'),
(1238, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 11:41:30', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1239, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 11:43:51', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1240, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 11:44:32', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1241, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 11:44:32', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => \n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => \n    [11] => \n    [12] => \n    [13] => 2000-09-27\n    [14] => \n    [15] => \n    [16] => \n    [17] => \n    [18] => \n    [19] => \n    [20] => \n    [21] => \n    [22] => \n    [23] => \n    [24] => \n    [25] => \n    [26] => \n    [27] => \n    [28] => \n    [29] => \n    [30] => \n    [31] => \n    [32] => \n    [33] => \n    [34] => \n    [35] => \n    [36] => \n    [37] => \n    [38] => \n    [39] => \n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1242, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 11:44:49', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1243, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 11:44:49', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => \n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => \n    [11] => \n    [12] => \n    [13] => 2000-09-27\n    [14] => \n    [15] => \n    [16] => \n    [17] => \n    [18] => \n    [19] => \n    [20] => \n    [21] => \n    [22] => \n    [23] => hey\n    [24] => \n    [25] => \n    [26] => \n    [27] => \n    [28] => \n    [29] => \n    [30] => \n    [31] => \n    [32] => \n    [33] => \n    [34] => \n    [35] => \n    [36] => \n    [37] => \n    [38] => \n    [39] => \n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1244, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 11:56:41', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_previous_employers/detailview_applicant_previous_employers.php'),
(1245, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 11:58:02', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_previous_employers/detailview_applicant_previous_employers.php'),
(1246, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 11:58:12', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_previous_employers/edit_applicant_previous_employers.php'),
(1247, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 12:14:42', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(1248, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 12:14:51', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(1249, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 12:16:34', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(1250, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 12:17:55', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/edit_applicant_previous_employers.php'),
(1251, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 12:17:55', 'Query Executed: UPDATE applicant_previous_employers SET applicant_id = ?, previous_employer_name = ?, address = ?, job_description = ?, basic_salary = ?, reason_for_leaving = ?, previous_employer_position = ?, previous_employer_date_from = ?, previous_employer_date_to = ? WHERE applicant_previous_employer_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => 18\n    [2] => 2\n    [3] => 2\n    [4] => 2\n    [5] => 28000.25\n    [6] => s\n    [7] => 2\n    [8] => 2018-12-27\n    [9] => 2018-09-27\n    [10] => 11\n)\n', '/smic_recruitment/modules/applicant/applicant_previous_employers/edit_applicant_previous_employers.php'),
(1252, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 12:18:21', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/edit_applicant_previous_employers.php'),
(1253, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 12:18:26', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/edit_applicant_previous_employers.php'),
(1254, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 12:18:27', 'Query Executed: UPDATE applicant_previous_employers SET applicant_id = ?, previous_employer_name = ?, address = ?, job_description = ?, basic_salary = ?, reason_for_leaving = ?, previous_employer_position = ?, previous_employer_date_from = ?, previous_employer_date_to = ? WHERE applicant_previous_employer_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => 18\n    [2] => 1234\n    [3] => 1234\n    [4] => 234\n    [5] => 28.00\n    [6] => 1\n    [7] => 2\n    [8] => 2018-12-27\n    [9] => 2018-12-31\n    [10] => 10\n)\n', '/smic_recruitment/modules/applicant/applicant_previous_employers/edit_applicant_previous_employers.php'),
(1255, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 12:20:04', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_reference/add_applicant_reference.php'),
(1256, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 12:20:34', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_reference/add_applicant_reference.php'),
(1257, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 12:20:37', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_reference/add_applicant_reference.php'),
(1258, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 12:22:06', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_school_attended/detailview_applicant_school_attended.php'),
(1259, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 13:33:27', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_school_attended/add_applicant_school_attended.php'),
(1260, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 13:35:43', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_school_attended/add_applicant_school_attended.php'),
(1261, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 13:35:47', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_school_attended/add_applicant_school_attended.php'),
(1262, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 13:35:59', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(1263, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 13:36:03', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(1264, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 14:13:28', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1265, '::1', 'markdanico.fernandez@gmail.com', '2018-10-02 14:17:12', 'Logged out', '/smic_recruitment/end.php'),
(1266, '::1', 'root', '2018-10-04 21:08:41', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1267, '::1', 'root', '2018-10-04 21:09:53', 'Pressed submit button', '/smic_recruitment/sysadmin/add_user_links.php'),
(1268, '::1', 'root', '2018-10-04 21:09:54', 'Query Executed: INSERT INTO user_links(name, target, descriptive_title, description, passport_group_id, show_in_tasklist, status, icon, priority) VALUES(?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssss\n    [1] => Applicant access\n    [2] => none\n    [3] => Applicant Access\n    [4] => none\n    [5] => 3\n    [6] => No\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n)\n', '/smic_recruitment/sysadmin/add_user_links.php'),
(1269, '::1', 'root', '2018-10-04 21:10:20', 'Pressed submit button', '/smic_recruitment/sysadmin/add_user_role.php'),
(1270, '::1', 'root', '2018-10-04 21:10:20', 'Query Executed: INSERT INTO user_role(role, description) VALUES(?,?)\r\nArray\n(\n    [0] => ss\n    [1] => Recruiter Access\n    [2] => a\n)\n', '/smic_recruitment/sysadmin/add_user_role.php'),
(1271, '::1', 'root', '2018-10-04 21:11:24', 'Query Executed: DELETE FROM user_role_links WHERE role_id = ? AND link_id IN (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 4\n    [2] => 37\n    [3] => 38\n    [4] => 39\n    [5] => 40\n    [6] => 81\n    [7] => 82\n    [8] => 83\n    [9] => 84\n    [10] => 85\n    [11] => 86\n    [12] => 87\n    [13] => 88\n    [14] => 89\n    [15] => 90\n    [16] => 91\n    [17] => 92\n    [18] => 93\n    [19] => 94\n    [20] => 95\n    [21] => 96\n    [22] => 97\n    [23] => 98\n    [24] => 99\n    [25] => 100\n    [26] => 101\n    [27] => 102\n    [28] => 103\n    [29] => 104\n    [30] => 105\n    [31] => 106\n    [32] => 107\n    [33] => 108\n    [34] => 109\n    [35] => 110\n    [36] => 111\n    [37] => 112\n    [38] => 113\n    [39] => 114\n    [40] => 115\n    [41] => 116\n    [42] => 117\n    [43] => 118\n    [44] => 119\n    [45] => 120\n    [46] => 121\n    [47] => 122\n    [48] => 123\n    [49] => 124\n    [50] => 125\n    [51] => 126\n    [52] => 127\n    [53] => 128\n    [54] => 129\n    [55] => 130\n    [56] => 131\n    [57] => 132\n    [58] => 133\n    [59] => 134\n    [60] => 135\n    [61] => 136\n    [62] => 137\n    [63] => 138\n    [64] => 139\n    [65] => 140\n    [66] => 141\n    [67] => 142\n    [68] => 143\n    [69] => 144\n    [70] => 145\n    [71] => 146\n    [72] => 147\n    [73] => 148\n    [74] => 149\n    [75] => 150\n    [76] => 151\n    [77] => 152\n    [78] => 153\n    [79] => 154\n    [80] => 155\n    [81] => 156\n    [82] => 157\n    [83] => 158\n    [84] => 159\n    [85] => 160\n    [86] => 161\n    [87] => 162\n    [88] => 163\n    [89] => 164\n    [90] => 165\n    [91] => 166\n    [92] => 167\n    [93] => 168\n    [94] => 169\n    [95] => 170\n    [96] => 171\n    [97] => 172\n    [98] => 173\n    [99] => 174\n    [100] => 175\n    [101] => 176\n    [102] => 177\n    [103] => 178\n    [104] => 179\n    [105] => 180\n    [106] => 181\n    [107] => 182\n    [108] => 183\n    [109] => 184\n    [110] => 185\n    [111] => 186\n    [112] => 187\n    [113] => 188\n    [114] => 189\n    [115] => 190\n    [116] => 191\n    [117] => 192\n    [118] => 193\n    [119] => 194\n    [120] => 195\n    [121] => 196\n    [122] => 203\n    [123] => 204\n    [124] => 205\n    [125] => 206\n    [126] => 207\n    [127] => 208\n    [128] => 209\n    [129] => 210\n    [130] => 211\n    [131] => 212\n    [132] => 213\n    [133] => 214\n    [134] => 215\n    [135] => 216\n    [136] => 217\n    [137] => 218\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1272, '::1', 'root', '2018-10-04 21:11:24', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 4\n    [2] => 215\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1273, '::1', 'root', '2018-10-04 21:11:24', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 4\n    [2] => 216\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1274, '::1', 'root', '2018-10-04 21:11:25', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 4\n    [2] => 217\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1275, '::1', 'root', '2018-10-04 21:11:27', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions.php'),
(1276, '::1', 'root', '2018-10-04 21:11:55', 'Pressed submit button', '/smic_recruitment/sysadmin/add_user.php'),
(1277, '::1', 'root', '2018-10-04 21:13:43', 'Logged out', '/smic_recruitment/end.php'),
(1278, '::1', 'recruiter1', '2018-10-04 21:13:46', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1279, '::1', 'recruiter1', '2018-10-04 21:13:50', 'Logged out', '/smic_recruitment/end.php'),
(1280, '::1', 'root', '2018-10-04 21:13:58', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1281, '::1', 'root', '2018-10-04 21:14:20', 'Query Executed: DELETE FROM user_passport WHERE username IN (?)\r\nArray\n(\n    [0] => s\n    [1] => recruiter1\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1282, '::1', 'root', '2018-10-04 21:14:21', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => recruiter1\n    [2] => 4\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1283, '::1', 'root', '2018-10-04 21:14:21', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1284, '::1', 'root', '2018-10-04 21:14:29', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions.php'),
(1285, '::1', 'root', '2018-10-04 21:14:31', 'Logged out', '/smic_recruitment/end.php'),
(1286, '::1', 'recruiter1', '2018-10-04 21:14:34', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1287, '::1', 'recruiter1', '2018-10-04 21:18:20', 'Pressed submit button', '/smic_recruitment/modules/upload_material/add_upload_material.php'),
(1288, '::1', 'recruiter1', '2018-10-04 21:18:57', 'Pressed submit button', '/smic_recruitment/modules/upload_material/add_upload_material.php'),
(1289, '::1', 'recruiter1', '2018-10-04 21:18:57', 'Query Executed: INSERT INTO upload_material(file_name, date_uploaded, active) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 6230d27a73dec8f7bce57802b0f4568dd48f1642download.jpg\n    [2] => 2018-10-04\n    [3] => Yes\n)\n', '/smic_recruitment/modules/upload_material/add_upload_material.php'),
(1290, '::1', 'recruiter1', '2018-10-04 21:24:09', 'Logged out', '/smic_recruitment/end.php'),
(1291, '::1', 'recruiter1', '2018-10-04 21:57:52', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1292, '::1', 'recruiter1', '2018-10-04 21:58:17', 'Pressed submit button', '/smic_recruitment/modules/upload_material/add_upload_material.php'),
(1293, '::1', 'recruiter1', '2018-10-04 21:58:17', 'Query Executed: INSERT INTO upload_material(file_name, date_uploaded, active) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 063a4eb9beedeee783972e38b6c2d51ee9ca0383a1dX0bG_460s.jpg\n    [2] => 2018-10-04\n    [3] => Yes\n)\n', '/smic_recruitment/modules/upload_material/add_upload_material.php'),
(1294, '::1', 'recruiter1', '2018-10-04 21:58:20', 'Logged out', '/smic_recruitment/end.php'),
(1295, '::1', 'recruiter1', '2018-10-05 14:41:24', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1296, '::1', 'recruiter1', '2018-10-05 14:41:42', 'Pressed submit button', '/smic_recruitment/modules/upload_material/add_upload_material.php'),
(1297, '::1', 'recruiter1', '2018-10-05 14:41:42', 'Query Executed: INSERT INTO upload_material(file_name, date_uploaded, active) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 7e29a455ab80775a1d3132ad9f366aea975a4e6316512865-Rank-Icons-Stock-Vector-icon.jpg\n    [2] => 2018-10-05\n    [3] => Yes\n)\n', '/smic_recruitment/modules/upload_material/add_upload_material.php'),
(1298, '::1', 'recruiter1', '2018-10-05 14:41:45', 'Logged out', '/smic_recruitment/end.php'),
(1299, '::1', 'markdanico.fernandez@gmail.com', '2018-10-05 14:44:45', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1300, '::1', 'markdanico.fernandez@gmail.com', '2018-10-05 14:45:19', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_previous_employers/detailview_applicant_previous_employers.php'),
(1301, '::1', 'markdanico.fernandez@gmail.com', '2018-10-05 14:46:32', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(1302, '::1', 'markdanico.fernandez@gmail.com', '2018-10-05 14:46:50', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_school_attended/add_applicant_school_attended.php'),
(1303, '::1', 'markdanico.fernandez@gmail.com', '2018-10-05 14:47:45', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_reference/add_applicant_reference.php'),
(1304, '::1', 'markdanico.fernandez@gmail.com', '2018-10-05 14:48:04', 'Logged out', '/smic_recruitment/end.php'),
(1305, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 06:43:07', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1306, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:05:26', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1307, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:05:27', 'Query Executed: DELETE FROM other_declarations_name_smgroup WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1308, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:05:27', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => as\n    [3] => hey\n    [4] => \n    [5] => s\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1309, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:05:27', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => s\n    [3] => \n    [4] => sup\n    [5] => s\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1310, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:05:27', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => sa\n    [3] => yow\n    [4] => \n    [5] => a\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1311, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:05:27', 'Query Executed: DELETE FROM other_declarations_name_smic_employ WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1312, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:05:27', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => a\n    [3] => hey\n    [4] => yow\n    [5] => sup\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1313, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:05:27', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => 1\n    [3] => \n    [4] => \n    [5] => \n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1314, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:05:27', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => sa\n    [3] => \n    [4] => s\n    [5] => \n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1315, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:05:28', 'Query Executed: UPDATE applicant_other_declarations SET applicant_id = ?, crime_convict = ?, details = ? WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => ssss\n    [1] => 18\n    [2] => Yes\n    [3] => s\n    [4] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1316, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:08:08', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_license/add_applicant_license.php'),
(1317, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:08:08', 'Query Executed: INSERT INTO applicant_license(applicant_id, license, license_number, license_expiry) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 18\n    [2] => 12345\n    [3] => 123456\n    [4] => 2018-10-09\n)\n', '/smic_recruitment/modules/applicant/applicant_license/add_applicant_license.php'),
(1318, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:08:19', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_license/detailview_applicant_license.php'),
(1319, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:09:14', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(1320, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:09:14', 'Query Executed: INSERT INTO applicant_family_members(applicant_id, name, birthday, age, relationship, is_dependent) VALUES(?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssss\n    [1] => 18\n    [2] => hey\n    [3] => 2001-10-09\n    [4] => 17\n    [5] => Mother\n    [6] => Yes\n)\n', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(1321, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:09:50', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(1322, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:09:50', 'Query Executed: INSERT INTO applicant_family_members(applicant_id, name, birthday, age, relationship, is_dependent) VALUES(?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssss\n    [1] => 18\n    [2] => hey\n    [3] => 2001-10-09\n    [4] => 17\n    [5] => Mother\n    [6] => Yes\n)\n', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(1323, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:10:00', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(1324, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:10:00', 'Query Executed: INSERT INTO applicant_family_members(applicant_id, name, birthday, age, relationship, is_dependent) VALUES(?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssss\n    [1] => 18\n    [2] => hey\n    [3] => 2001-10-09\n    [4] => 17\n    [5] => Mother\n    [6] => Yes\n)\n', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(1325, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:10:04', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/detailview_applicant_family_members.php'),
(1326, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:10:15', 'Logged out', '/smic_recruitment/end.php'),
(1327, '::1', 'root', '2018-10-09 08:10:21', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1328, '::1', 'root', '2018-10-09 08:10:58', 'Query Executed: DELETE FROM user_role_links WHERE role_id = ? AND link_id IN (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 3\n    [2] => 41\n    [3] => 42\n    [4] => 43\n    [5] => 44\n    [6] => 45\n    [7] => 46\n    [8] => 47\n    [9] => 48\n    [10] => 49\n    [11] => 50\n    [12] => 51\n    [13] => 52\n    [14] => 53\n    [15] => 54\n    [16] => 55\n    [17] => 56\n    [18] => 57\n    [19] => 58\n    [20] => 59\n    [21] => 60\n    [22] => 61\n    [23] => 62\n    [24] => 63\n    [25] => 64\n    [26] => 65\n    [27] => 66\n    [28] => 67\n    [29] => 68\n    [30] => 69\n    [31] => 70\n    [32] => 71\n    [33] => 72\n    [34] => 73\n    [35] => 74\n    [36] => 75\n    [37] => 76\n    [38] => 77\n    [39] => 78\n    [40] => 79\n    [41] => 80\n    [42] => 197\n    [43] => 202\n    [44] => 219\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1329, '::1', 'root', '2018-10-09 08:10:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 49\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1330, '::1', 'root', '2018-10-09 08:10:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 57\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1331, '::1', 'root', '2018-10-09 08:10:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 61\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1332, '::1', 'root', '2018-10-09 08:10:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 65\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1333, '::1', 'root', '2018-10-09 08:10:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 69\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1334, '::1', 'root', '2018-10-09 08:10:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 73\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1335, '::1', 'root', '2018-10-09 08:10:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 77\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1336, '::1', 'root', '2018-10-09 08:10:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 197\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1337, '::1', 'root', '2018-10-09 08:10:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 52\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1338, '::1', 'root', '2018-10-09 08:10:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 60\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1339, '::1', 'root', '2018-10-09 08:11:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 64\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1340, '::1', 'root', '2018-10-09 08:11:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 68\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1341, '::1', 'root', '2018-10-09 08:11:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 72\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1342, '::1', 'root', '2018-10-09 08:11:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 76\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1343, '::1', 'root', '2018-10-09 08:11:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 80\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1344, '::1', 'root', '2018-10-09 08:11:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 42\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1345, '::1', 'root', '2018-10-09 08:11:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 50\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1346, '::1', 'root', '2018-10-09 08:11:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 58\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1347, '::1', 'root', '2018-10-09 08:11:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 62\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1348, '::1', 'root', '2018-10-09 08:11:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 66\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1349, '::1', 'root', '2018-10-09 08:11:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 70\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1350, '::1', 'root', '2018-10-09 08:11:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 74\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1351, '::1', 'root', '2018-10-09 08:11:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 78\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1352, '::1', 'root', '2018-10-09 08:11:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 51\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1353, '::1', 'root', '2018-10-09 08:11:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 59\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1354, '::1', 'root', '2018-10-09 08:11:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 63\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1355, '::1', 'root', '2018-10-09 08:11:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 202\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1356, '::1', 'root', '2018-10-09 08:11:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 67\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1357, '::1', 'root', '2018-10-09 08:11:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 71\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1358, '::1', 'root', '2018-10-09 08:11:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 75\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1359, '::1', 'root', '2018-10-09 08:11:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 79\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1360, '::1', 'root', '2018-10-09 08:11:04', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions.php'),
(1361, '::1', 'root', '2018-10-09 08:11:18', 'Query Executed: DELETE FROM user_passport WHERE username IN (?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssss\n    [1] => applicant1\n    [2] => markdanico.fernandez1@gmail.com\n    [3] => markdanico.fernandez@gmail.com\n    [4] => markdanico.fernandezz@gmail.com\n    [5] => reyeskimberly018@gmail.com\n    [6] => smic.email.tester@gmail.com\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1362, '::1', 'root', '2018-10-09 08:11:19', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => applicant1\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php');
INSERT INTO `system_log` (`entry_id`, `ip_address`, `user`, `datetime`, `action`, `module`) VALUES
(1363, '::1', 'root', '2018-10-09 08:11:19', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez1@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1364, '::1', 'root', '2018-10-09 08:11:19', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1365, '::1', 'root', '2018-10-09 08:11:19', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandezz@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1366, '::1', 'root', '2018-10-09 08:11:19', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => reyeskimberly018@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1367, '::1', 'root', '2018-10-09 08:11:19', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => smic.email.tester@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1368, '::1', 'root', '2018-10-09 08:11:22', 'Logged out', '/smic_recruitment/end.php'),
(1369, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:11:29', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1370, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:11:33', 'Pressed delete button', '/smic_recruitment/modules/applicant/applicant_family_members/delete_applicant_family_members.php'),
(1371, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:11:33', 'Query Executed: DELETE FROM applicant_family_members WHERE applicant_family_member_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant_family_members/delete_applicant_family_members.php'),
(1372, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:11:36', 'Pressed delete button', '/smic_recruitment/modules/applicant/applicant_family_members/delete_applicant_family_members.php'),
(1373, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 08:11:36', 'Query Executed: DELETE FROM applicant_family_members WHERE applicant_family_member_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 4\n)\n', '/smic_recruitment/modules/applicant/applicant_family_members/delete_applicant_family_members.php'),
(1374, '::1', 'recruiter1', '2018-10-09 09:09:49', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1375, '::1', 'recruiter1', '2018-10-09 09:09:54', 'Logged out', '/smic_recruitment/end.php'),
(1376, '::1', 'root', '2018-10-09 09:09:58', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1377, '::1', 'root', '2018-10-09 09:11:14', 'Query Executed: DELETE FROM user_role_links WHERE role_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 4\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1378, '::1', 'root', '2018-10-09 09:11:15', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 4\n    [2] => 215\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1379, '::1', 'root', '2018-10-09 09:11:15', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 4\n    [2] => 43\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1380, '::1', 'root', '2018-10-09 09:11:15', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 4\n    [2] => 216\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1381, '::1', 'root', '2018-10-09 09:11:15', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 4\n    [2] => 217\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1382, '::1', 'root', '2018-10-09 09:11:29', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions.php'),
(1383, '::1', 'root', '2018-10-09 09:11:33', 'Query Executed: DELETE FROM user_passport WHERE username IN (?)\r\nArray\n(\n    [0] => s\n    [1] => recruiter1\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1384, '::1', 'root', '2018-10-09 09:11:33', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => recruiter1\n    [2] => 4\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1385, '::1', 'root', '2018-10-09 09:11:36', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1386, '::1', 'root', '2018-10-09 09:11:40', 'Logged out', '/smic_recruitment/end.php'),
(1387, '::1', 'recruiter1', '2018-10-09 09:11:43', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1388, '::1', 'recruiter1', '2018-10-09 09:19:02', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1389, '::1', 'recruiter1', '2018-10-09 09:22:59', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1390, '::1', 'recruiter1', '2018-10-09 09:25:46', 'Pressed cancel button', '/smic_recruitment/modules/upload_material/detailview_upload_material.php'),
(1391, '::1', 'recruiter1', '2018-10-09 10:01:43', 'Logged out', '/smic_recruitment/end.php'),
(1392, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 10:06:53', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1393, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 10:07:24', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_skills/add_applicant_skills.php'),
(1394, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 10:07:24', 'Query Executed: INSERT INTO applicant_skills(applicant_id, skill, proficiency, years_of_experience) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 18\n    [2] => yow yow hey\n    [3] => 2\n    [4] => 3\n)\n', '/smic_recruitment/modules/applicant/applicant_skills/add_applicant_skills.php'),
(1395, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 10:07:27', 'Logged out', '/smic_recruitment/end.php'),
(1396, '::1', 'recruiter1', '2018-10-09 10:07:31', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1397, '::1', 'recruiter1', '2018-10-09 10:07:39', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1398, '::1', 'recruiter1', '2018-10-09 10:08:08', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1399, '::1', 'recruiter1', '2018-10-09 10:08:13', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1400, '::1', 'recruiter1', '2018-10-09 10:08:18', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1401, '::1', 'recruiter1', '2018-10-09 10:14:06', 'Logged out', '/smic_recruitment/end.php'),
(1402, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 10:14:16', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1403, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 10:14:31', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1404, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 10:14:31', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => \n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Female\n    [11] => \n    [12] => \n    [13] => 2000-09-27\n    [14] => \n    [15] => \n    [16] => \n    [17] => \n    [18] => \n    [19] => \n    [20] => \n    [21] => \n    [22] => \n    [23] => hey\n    [24] => \n    [25] => \n    [26] => \n    [27] => \n    [28] => \n    [29] => \n    [30] => \n    [31] => \n    [32] => \n    [33] => \n    [34] => \n    [35] => \n    [36] => \n    [37] => \n    [38] => \n    [39] => \n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1405, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 10:14:32', 'Logged out', '/smic_recruitment/end.php'),
(1406, '::1', 'recruiter1', '2018-10-09 10:14:35', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1407, '::1', 'recruiter1', '2018-10-09 10:26:00', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1408, '::1', 'recruiter1', '2018-10-09 10:26:02', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/listview_applicant.php'),
(1409, '::1', 'recruiter1', '2018-10-09 10:26:08', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1410, '::1', 'recruiter1', '2018-10-09 10:26:15', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1411, '::1', 'recruiter1', '2018-10-09 10:26:56', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1412, '::1', 'recruiter1', '2018-10-09 10:27:04', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1413, '::1', 'recruiter1', '2018-10-09 10:34:14', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1414, '::1', 'recruiter1', '2018-10-09 10:34:25', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1415, '::1', 'recruiter1', '2018-10-09 10:45:23', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1416, '::1', 'recruiter1', '2018-10-09 11:07:39', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1417, '::1', 'recruiter1', '2018-10-09 11:54:00', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1418, '::1', 'recruiter1', '2018-10-09 11:54:15', 'Logged out', '/smic_recruitment/end.php'),
(1419, '::1', 'recruiter1', '2018-10-09 13:25:32', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1420, '::1', 'recruiter1', '2018-10-09 13:26:16', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1421, '::1', 'recruiter1', '2018-10-09 13:26:33', 'Logged out', '/smic_recruitment/end.php'),
(1422, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 13:26:46', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1423, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 13:31:06', 'Logged out', '/smic_recruitment/end.php'),
(1424, '::1', 'recruiter1', '2018-10-09 13:31:19', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1425, '::1', 'recruiter1', '2018-10-09 13:31:48', 'Logged out', '/smic_recruitment/end.php'),
(1426, '::1', 'root', '2018-10-09 15:22:55', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1427, '::1', 'root', '2018-10-09 15:27:35', 'Query Executed: DELETE FROM user_role_links WHERE role_id = ? AND link_id IN (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 3\n    [2] => 37\n    [3] => 38\n    [4] => 39\n    [5] => 40\n    [6] => 81\n    [7] => 82\n    [8] => 83\n    [9] => 84\n    [10] => 85\n    [11] => 86\n    [12] => 87\n    [13] => 88\n    [14] => 89\n    [15] => 90\n    [16] => 91\n    [17] => 92\n    [18] => 93\n    [19] => 94\n    [20] => 95\n    [21] => 96\n    [22] => 97\n    [23] => 98\n    [24] => 99\n    [25] => 100\n    [26] => 101\n    [27] => 102\n    [28] => 103\n    [29] => 104\n    [30] => 105\n    [31] => 106\n    [32] => 107\n    [33] => 108\n    [34] => 109\n    [35] => 110\n    [36] => 111\n    [37] => 112\n    [38] => 113\n    [39] => 114\n    [40] => 115\n    [41] => 116\n    [42] => 117\n    [43] => 118\n    [44] => 119\n    [45] => 120\n    [46] => 121\n    [47] => 122\n    [48] => 123\n    [49] => 124\n    [50] => 125\n    [51] => 126\n    [52] => 127\n    [53] => 128\n    [54] => 129\n    [55] => 130\n    [56] => 131\n    [57] => 132\n    [58] => 133\n    [59] => 134\n    [60] => 135\n    [61] => 136\n    [62] => 137\n    [63] => 138\n    [64] => 139\n    [65] => 140\n    [66] => 141\n    [67] => 142\n    [68] => 143\n    [69] => 144\n    [70] => 145\n    [71] => 146\n    [72] => 147\n    [73] => 148\n    [74] => 149\n    [75] => 150\n    [76] => 151\n    [77] => 152\n    [78] => 153\n    [79] => 154\n    [80] => 155\n    [81] => 156\n    [82] => 157\n    [83] => 158\n    [84] => 159\n    [85] => 160\n    [86] => 161\n    [87] => 162\n    [88] => 163\n    [89] => 164\n    [90] => 165\n    [91] => 166\n    [92] => 167\n    [93] => 168\n    [94] => 169\n    [95] => 170\n    [96] => 171\n    [97] => 172\n    [98] => 173\n    [99] => 174\n    [100] => 175\n    [101] => 176\n    [102] => 177\n    [103] => 178\n    [104] => 179\n    [105] => 180\n    [106] => 181\n    [107] => 182\n    [108] => 183\n    [109] => 184\n    [110] => 185\n    [111] => 186\n    [112] => 187\n    [113] => 188\n    [114] => 189\n    [115] => 190\n    [116] => 191\n    [117] => 192\n    [118] => 193\n    [119] => 194\n    [120] => 195\n    [121] => 196\n    [122] => 203\n    [123] => 204\n    [124] => 205\n    [125] => 206\n    [126] => 207\n    [127] => 208\n    [128] => 209\n    [129] => 210\n    [130] => 211\n    [131] => 212\n    [132] => 213\n    [133] => 214\n    [134] => 215\n    [135] => 216\n    [136] => 217\n    [137] => 218\n    [138] => 220\n    [139] => 221\n    [140] => 222\n    [141] => 223\n    [142] => 224\n    [143] => 225\n    [144] => 226\n    [145] => 227\n    [146] => 228\n    [147] => 229\n    [148] => 230\n    [149] => 231\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1428, '::1', 'root', '2018-10-09 15:27:35', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 220\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1429, '::1', 'root', '2018-10-09 15:27:35', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 203\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1430, '::1', 'root', '2018-10-09 15:27:35', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 224\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1431, '::1', 'root', '2018-10-09 15:27:35', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 228\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1432, '::1', 'root', '2018-10-09 15:27:35', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 222\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1433, '::1', 'root', '2018-10-09 15:27:35', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 226\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1434, '::1', 'root', '2018-10-09 15:27:36', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 223\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1435, '::1', 'root', '2018-10-09 15:27:36', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 227\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1436, '::1', 'root', '2018-10-09 15:27:36', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 231\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1437, '::1', 'root', '2018-10-09 15:27:36', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 221\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1438, '::1', 'root', '2018-10-09 15:27:36', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 204\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1439, '::1', 'root', '2018-10-09 15:27:36', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 225\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1440, '::1', 'root', '2018-10-09 15:27:36', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 229\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1441, '::1', 'root', '2018-10-09 15:27:36', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 230\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1442, '::1', 'root', '2018-10-09 15:28:12', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1443, '::1', 'root', '2018-10-09 15:28:12', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => Add applicant attachments\n    [2] => modules/applicant_attachments/add_applicant_attachments.php\n    [3] => Add Applicant Attachments\n    [4] => \n    [5] => 3\n    [6] => No\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n    [10] => 220\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1444, '::1', 'root', '2018-10-09 15:28:15', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1445, '::1', 'root', '2018-10-09 15:28:16', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => Edit applicant attachments\n    [2] => modules/applicant_attachments/edit_applicant_attachments.php\n    [3] => Edit Applicant Attachments\n    [4] => \n    [5] => 3\n    [6] => No\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n    [10] => 221\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1446, '::1', 'root', '2018-10-09 15:28:19', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1447, '::1', 'root', '2018-10-09 15:28:20', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => View applicant attachments\n    [2] => modules/applicant_attachments/listview_applicant_attachments.php\n    [3] => Applicant Attachments\n    [4] => \n    [5] => 3\n    [6] => Yes\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n    [10] => 222\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1448, '::1', 'root', '2018-10-09 15:28:24', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1449, '::1', 'root', '2018-10-09 15:28:24', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => Delete applicant attachments\n    [2] => modules/applicant_attachments/delete_applicant_attachments.php\n    [3] => Delete Applicant Attachments\n    [4] => \n    [5] => 3\n    [6] => No\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n    [10] => 223\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1450, '::1', 'root', '2018-10-09 15:28:51', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1451, '::1', 'root', '2018-10-09 15:28:51', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => Add applicant preferred positions\n    [2] => modules/applicant_preferred_positions/add_applicant_preferred_positions.php\n    [3] => Add Applicant Preferred Positions\n    [4] => \n    [5] => 3\n    [6] => No\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n    [10] => 224\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1452, '::1', 'root', '2018-10-09 15:28:55', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1453, '::1', 'root', '2018-10-09 15:28:55', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => Edit applicant preferred positions\n    [2] => modules/applicant_preferred_positions/edit_applicant_preferred_positions.php\n    [3] => Edit Applicant Preferred Positions\n    [4] => \n    [5] => 3\n    [6] => No\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n    [10] => 225\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1454, '::1', 'root', '2018-10-09 15:29:00', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1455, '::1', 'root', '2018-10-09 15:29:00', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => View applicant preferred positions\n    [2] => modules/applicant_preferred_positions/listview_applicant_preferred_positions.php\n    [3] => Applicant Preferred Positions\n    [4] => \n    [5] => 3\n    [6] => Yes\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n    [10] => 226\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1456, '::1', 'root', '2018-10-09 15:29:04', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1457, '::1', 'root', '2018-10-09 15:29:04', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => Delete applicant preferred positions\n    [2] => modules/applicant_preferred_positions/delete_applicant_preferred_positions.php\n    [3] => Delete Applicant Preferred Positions\n    [4] => \n    [5] => 3\n    [6] => No\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n    [10] => 227\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1458, '::1', 'root', '2018-10-09 15:29:09', 'Logged out', '/smic_recruitment/end.php'),
(1459, '::1', 'root', '2018-10-09 15:29:19', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1460, '::1', 'root', '2018-10-09 15:31:30', 'Logged out', '/smic_recruitment/end.php'),
(1461, '::1', 'recruiter1', '2018-10-09 15:31:33', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1462, '::1', 'recruiter1', '2018-10-09 15:31:41', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1463, '::1', 'recruiter1', '2018-10-09 15:31:45', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1464, '::1', 'recruiter1', '2018-10-09 15:31:47', 'Logged out', '/smic_recruitment/end.php'),
(1465, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 15:31:58', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1466, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 15:32:05', 'Logged out', '/smic_recruitment/end.php'),
(1467, '::1', 'root', '2018-10-09 15:32:10', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1468, '::1', 'root', '2018-10-09 15:32:22', 'Query Executed: DELETE FROM user_passport WHERE username IN (?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssss\n    [1] => applicant1\n    [2] => markdanico.fernandez1@gmail.com\n    [3] => markdanico.fernandez@gmail.com\n    [4] => markdanico.fernandezz@gmail.com\n    [5] => reyeskimberly018@gmail.com\n    [6] => smic.email.tester@gmail.com\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1469, '::1', 'root', '2018-10-09 15:32:22', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => applicant1\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1470, '::1', 'root', '2018-10-09 15:32:22', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez1@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1471, '::1', 'root', '2018-10-09 15:32:22', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1472, '::1', 'root', '2018-10-09 15:32:22', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandezz@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1473, '::1', 'root', '2018-10-09 15:32:23', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => reyeskimberly018@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1474, '::1', 'root', '2018-10-09 15:32:23', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => smic.email.tester@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1475, '::1', 'root', '2018-10-09 15:32:24', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1476, '::1', 'root', '2018-10-09 15:32:29', 'Logged out', '/smic_recruitment/end.php'),
(1477, '::1', 'markdanico.fernandez@gmail.com', '2018-10-09 15:32:37', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1478, '::1', 'markdanico.fernandez@gmail.com', '2018-10-12 16:11:29', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1479, '::1', 'markdanico.fernandez@gmail.com', '2018-10-12 16:13:15', 'Logged out', '/smic_recruitment/end.php'),
(1480, '::1', 'recruiter1', '2018-10-12 16:13:21', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1481, '::1', 'recruiter1', '2018-10-12 17:08:18', 'Logged out', '/smic_recruitment/end.php'),
(1482, '::1', 'markdanico.fernandez@gmail.com', '2018-10-12 17:08:27', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1483, '::1', 'recruiter1', '2018-10-16 08:12:17', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1484, '::1', 'recruiter1', '2018-10-16 08:12:47', 'Logged out', '/smic_recruitment/end.php'),
(1485, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 08:12:56', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1486, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 09:24:36', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1487, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 09:29:44', 'Logged out', '/smic_recruitment/end.php'),
(1488, '::1', 'root', '2018-10-16 09:29:47', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1489, '::1', 'root', '2018-10-16 09:30:31', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1490, '::1', 'root', '2018-10-16 09:30:31', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => View applicant preferred positions\n    [2] => modules/applicant_preferred_positions/listview_applicant_preferred_positions.php\n    [3] => Preferred Positions\n    [4] => \n    [5] => 3\n    [6] => Yes\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n    [10] => 226\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1491, '::1', 'root', '2018-10-16 09:30:50', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1492, '::1', 'root', '2018-10-16 09:30:50', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => View applicant attachments\n    [2] => modules/applicant_attachments/listview_applicant_attachments.php\n    [3] => Document Attachments\n    [4] => \n    [5] => 3\n    [6] => Yes\n    [7] => On\n    [8] => form3.png\n    [9] => 99\n    [10] => 222\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1493, '::1', 'root', '2018-10-16 12:00:07', 'Logged out', '/smic_recruitment/end.php'),
(1494, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 12:01:11', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1495, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 12:08:52', 'Logged out', '/smic_recruitment/end.php'),
(1496, '::1', 'recruiter1', '2018-10-16 12:12:34', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1497, '::1', 'recruiter1', '2018-10-16 12:12:51', 'Logged out', '/smic_recruitment/end.php'),
(1498, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 12:13:05', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1499, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 12:19:11', 'Logged out', '/smic_recruitment/end.php'),
(1500, '::1', 'recruiter1', '2018-10-16 12:19:15', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1501, '::1', 'recruiter1', '2018-10-16 12:19:19', 'Logged out', '/smic_recruitment/end.php'),
(1502, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 12:19:31', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1503, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 12:21:00', 'Logged out', '/smic_recruitment/end.php'),
(1504, '::1', 'recruiter1', '2018-10-16 12:21:03', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1505, '::1', 'recruiter1', '2018-10-16 12:22:04', 'Logged out', '/smic_recruitment/end.php'),
(1506, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 12:22:12', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1507, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 12:24:54', 'Logged out', '/smic_recruitment/end.php'),
(1508, '::1', 'root', '2018-10-16 12:24:56', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1509, '::1', 'root', '2018-10-16 12:37:15', 'Logged out', '/smic_recruitment/end.php'),
(1510, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 12:38:39', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1511, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 12:43:03', 'Pressed cancel button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1512, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 12:55:16', 'Logged out', '/smic_recruitment/end.php'),
(1513, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 12:58:22', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1514, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 12:59:45', 'Logged out', '/smic_recruitment/end.php'),
(1515, '::1', 'root', '2018-10-16 13:02:14', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1516, '::1', 'root', '2018-10-16 13:02:21', 'Logged out', '/smic_recruitment/end.php'),
(1517, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 13:02:29', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1518, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 13:52:56', 'Logged out', '/smic_recruitment/end.php'),
(1519, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 13:53:20', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1520, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 14:01:29', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(1521, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 14:01:43', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/edit_applicant_family_members.php'),
(1522, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 14:01:44', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/listview_applicant_family_members.php'),
(1523, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 14:16:44', 'Logged out', '/smic_recruitment/end.php'),
(1524, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 14:17:00', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1525, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 14:17:28', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/detailview_applicant_family_members.php'),
(1526, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 14:20:19', 'Logged out', '/smic_recruitment/end.php'),
(1527, '::1', 'recruiter1', '2018-10-16 14:20:23', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1528, '::1', 'recruiter1', '2018-10-16 14:28:55', 'ILLEGAL ACCESS ATTEMPT - Tried to access ''/smic_recruitment/modules/applicant_attachments/listview_applicant_attachments.php'' without sufficient privileges.', '/smic_recruitment/modules/applicant_attachments/listview_applicant_attachments.php'),
(1529, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 14:29:09', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1530, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 14:32:00', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_skills/add_applicant_skills.php'),
(1531, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 14:43:00', 'Logged out', '/smic_recruitment/end.php'),
(1532, '::1', 'recruiter1', '2018-10-16 14:43:08', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1533, '::1', 'recruiter1', '2018-10-16 14:43:45', 'Logged out', '/smic_recruitment/end.php'),
(1534, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 14:43:53', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1535, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 14:44:39', 'Logged out', '/smic_recruitment/end.php'),
(1536, '::1', 'recruiter1', '2018-10-16 14:44:50', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1537, '::1', 'recruiter1', '2018-10-16 14:45:04', 'Logged out', '/smic_recruitment/end.php'),
(1538, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 14:45:12', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1539, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 14:46:44', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1540, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 14:47:04', 'Logged out', '/smic_recruitment/end.php'),
(1541, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 15:02:05', 'Logged out', '/smic_recruitment/end.php'),
(1542, '::1', 'recruiter1', '2018-10-16 15:02:09', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1543, '::1', 'recruiter1', '2018-10-16 15:03:01', 'Pressed cancel button', '/smic_recruitment/modules/upload_material/detailview_upload_material.php'),
(1544, '::1', 'recruiter1', '2018-10-16 15:03:04', 'Pressed cancel button', '/smic_recruitment/modules/upload_material/detailview_upload_material.php'),
(1545, '::1', 'recruiter1', '2018-10-16 15:06:34', 'Pressed cancel button', '/smic_recruitment/modules/upload_material/detailview_upload_material.php'),
(1546, '::1', 'recruiter1', '2018-10-16 15:06:37', 'Pressed cancel button', '/smic_recruitment/modules/upload_material/listview_upload_material.php'),
(1547, '::1', 'recruiter1', '2018-10-16 15:06:47', 'Logged out', '/smic_recruitment/end.php'),
(1548, '::1', 'recruiter1', '2018-10-16 15:07:03', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1549, '::1', 'recruiter1', '2018-10-16 15:18:11', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/listview_applicant.php'),
(1550, '::1', 'recruiter1', '2018-10-16 15:18:21', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/listview_applicant.php'),
(1551, '::1', 'recruiter1', '2018-10-16 15:21:04', 'Logged out', '/smic_recruitment/end.php'),
(1552, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 15:29:29', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1553, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 15:33:05', 'Logged out', '/smic_recruitment/end.php'),
(1554, '::1', 'recruiter1', '2018-10-16 15:33:09', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1555, '::1', 'recruiter1', '2018-10-16 16:15:31', 'Logged out', '/smic_recruitment/end.php'),
(1556, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 16:15:40', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1557, '::1', 'markdanico.fernandez@gmail.com', '2018-10-16 16:15:46', 'Logged out', '/smic_recruitment/end.php'),
(1558, '::1', 'markdanico.fernandez@gmail.com', '2018-10-17 08:02:54', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1559, '::1', 'markdanico.fernandez@gmail.com', '2018-10-17 08:03:19', 'Logged out', '/smic_recruitment/end.php'),
(1560, '::1', 'recruiter1', '2018-10-17 08:03:26', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1561, '::1', 'recruiter1', '2018-10-17 08:03:39', 'Logged out', '/smic_recruitment/end.php'),
(1562, '::1', 'markdanico.fernandez@gmail.com', '2018-10-17 08:03:50', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1563, '::1', 'markdanico.fernandez@gmail.com', '2018-10-18 05:37:07', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1564, '::1', 'markdanico.fernandez@gmail.com', '2018-10-18 07:57:16', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_school_attended/detailview_applicant_school_attended.php'),
(1565, '::1', 'markdanico.fernandez@gmail.com', '2018-10-18 07:57:28', 'Logged out', '/smic_recruitment/end.php'),
(1566, '::1', 'markdanico.fernandez@gmail.com', '2018-10-19 06:27:04', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1567, '::1', 'recruiter1', '2018-10-19 10:49:15', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1568, '::1', 'recruiter1', '2018-10-19 10:49:31', 'Logged out', '/smic_recruitment/end.php'),
(1569, '::1', 'markdanico.fernandez@gmail.com', '2018-10-19 10:50:54', 'Logged out', '/smic_recruitment/end.php'),
(1570, '::1', 'root', '2018-10-19 10:50:56', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1571, '::1', 'root', '2018-10-19 10:51:48', 'Logged out', '/smic_recruitment/end.php'),
(1572, '::1', 'root', '2018-10-19 10:52:18', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1573, '::1', 'root', '2018-10-19 11:33:37', 'Logged out', '/smic_recruitment/end.php'),
(1574, '::1', 'recruiter1', '2018-10-19 11:33:43', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1575, '::1', 'recruiter1', '2018-10-19 15:23:31', 'Logged out', '/smic_recruitment/end.php'),
(1576, '::1', 'recruiter1', '2018-10-19 15:23:34', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1577, '::1', 'markdanico.fernandez@gmail.com', '2018-10-22 10:03:10', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1578, '::1', 'Not Logged In', '2018-10-22 10:56:27', 'Query Executed: INSERT INTO rank(name) VALUES(?)\r\nArray\n(\n    [0] => s\n    [1] => Rank and File\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1579, '::1', 'Not Logged In', '2018-10-22 10:58:07', 'Query Executed: INSERT INTO rank(name) VALUES(?)\r\nArray\n(\n    [0] => s\n    [1] => Rank and File\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1580, '::1', 'Not Logged In', '2018-10-22 11:17:50', 'Query Executed: INSERT INTO company(company_id, official_name) VALUES(?,?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => SM Investments Corporation\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1581, '::1', 'Not Logged In', '2018-10-22 11:17:50', 'Query Executed: INSERT INTO rank(rank_id, name) VALUES(?,?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => Rank and File\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1582, '::1', 'markdanico.fernandez@gmail.com', '2018-10-22 11:24:09', 'Query Executed: INSERT INTO `position`(position_id, title) VALUES(?,?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => Compensation and Benefits\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1583, '::1', 'markdanico.fernandez@gmail.com', '2018-10-22 11:24:09', 'Query Executed: INSERT INTO classification(classification_id, name) VALUES(?,?)\r\nArray\n(\n    [0] => ss\n    [1] => 1\n    [2] => Rank and File\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1584, '::1', 'markdanico.fernandez@gmail.com', '2018-10-22 11:31:21', 'Query Executed: INSERT INTO iprf_staffrequest(iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssss\n    [1] => 1\n    [2] => PRF-2018-00001\n    [3] => 1\n    [4] => 1\n    [5] => 1\n    [6] => 1\n    [7] => 1\n    [8] => In progress\n    [9] => Sample Duration Data\n    [10] => Collects and process compensation and benefits of employees\n    [11] => Collects and process compensation and benefits of employees in general\n    [12] => Collects and process compensation and benefits of employees in detailed\n    [13] => Must have be college graduate\n    [14] => Must clerical and attentive\n    [15] => Nothing in Particular.\n    [16] => N/A\n    [17] => 2018-11-01\n    [18] => 2018-10-20\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1585, '::1', 'Not Logged In', '2018-10-22 11:41:41', 'Query Executed: INSERT INTO iprf_staffrequest(iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssss\n    [1] => 2\n    [2] => PRF-2018-00001\n    [3] => 1\n    [4] => 1\n    [5] => 1\n    [6] => 1\n    [7] => 1\n    [8] => In progress\n    [9] => Sample Duration Data\n    [10] => Collects and process compensation and benefits of employees\n    [11] => Collects and process compensation and benefits of employees in general\n    [12] => Collects and process compensation and benefits of employees in detailed\n    [13] => Must have be college graduate\n    [14] => Must clerical and attentive\n    [15] => Nothing in Particular.\n    [16] => N/A\n    [17] => 2018-11-01\n    [18] => 2018-10-20\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1586, '::1', 'Not Logged In', '2018-10-22 11:48:28', 'Query Executed: INSERT INTO iprf_staffrequest(iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssss\n    [1] => 3\n    [2] => PRF-2018-00001\n    [3] => 1\n    [4] => 1\n    [5] => 1\n    [6] => 1\n    [7] => 1\n    [8] => In progress\n    [9] => Sample Duration Data\n    [10] => Collects and process compensation and benefits of employees\n    [11] => Collects and process compensation and benefits of employees in general\n    [12] => Collects and process compensation and benefits of employees in detailed\n    [13] => Must have be college graduate\n    [14] => Must clerical and attentive\n    [15] => Nothing in Particular.\n    [16] => N/A\n    [17] => 2018-11-01\n    [18] => 2018-10-20\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1587, '::1', 'Not Logged In', '2018-10-22 12:00:17', 'Query Executed: INSERT INTO iprf_staffrequest(iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssss\n    [1] => 4\n    [2] => PRF-2018-00008\n    [3] => 1\n    [4] => 1\n    [5] => 1\n    [6] => 1\n    [7] => 1\n    [8] => In progress\n    [9] => Sample Duration Data\n    [10] => Collects and process compensation and benefits of employees\n    [11] => Collects and process compensation and benefits of employees in general\n    [12] => Collects and process compensation and benefits of employees in detailed\n    [13] => Must have be college graduate\n    [14] => Must clerical and attentive\n    [15] => Nothing in Particular.\n    [16] => N/A\n    [17] => 2018-11-01\n    [18] => 2018-10-20\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1588, '::1', 'Not Logged In', '2018-10-22 12:02:52', 'Query Executed: INSERT INTO iprf_staffrequest(iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssss\n    [1] => 5\n    [2] => PRF-2018-00008\n    [3] => 1\n    [4] => 1\n    [5] => 1\n    [6] => 1\n    [7] => 1\n    [8] => In progress\n    [9] => Sample Duration Data\n    [10] => Collects and process compensation and benefits of employees\n    [11] => Collects and process compensation and benefits of employees in general\n    [12] => Collects and process compensation and benefits of employees in detailed\n    [13] => Must have be college graduate\n    [14] => Must clerical and attentive\n    [15] => Nothing in Particular.\n    [16] => N/A\n    [17] => 2018-11-01\n    [18] => 2018-10-20\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1589, '::1', 'Not Logged In', '2018-10-22 12:03:50', 'Query Executed: INSERT INTO iprf_staffrequest(iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssss\n    [1] => 6\n    [2] => PRF-2018-00008\n    [3] => 1\n    [4] => 1\n    [5] => 1\n    [6] => 1\n    [7] => 1\n    [8] => In progress\n    [9] => Sample Duration Data\n    [10] => Collects and process compensation and benefits of employees\n    [11] => Collects and process compensation and benefits of employees in general\n    [12] => Collects and process compensation and benefits of employees in detailed\n    [13] => Must have be college graduate\n    [14] => Must clerical and attentive\n    [15] => Nothing in Particular.\n    [16] => N/A\n    [17] => 2018-11-01\n    [18] => 2018-10-20\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1590, '::1', 'Not Logged In', '2018-10-22 12:04:50', 'Query Executed: INSERT INTO iprf_staffrequest(iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssss\n    [1] => 7\n    [2] => PRF-2018-00008\n    [3] => 1\n    [4] => 1\n    [5] => 1\n    [6] => 1\n    [7] => 1\n    [8] => In progress\n    [9] => Sample Duration Data\n    [10] => Collects and process compensation and benefits of employees\n    [11] => Collects and process compensation and benefits of employees in general\n    [12] => Collects and process compensation and benefits of employees in detailed\n    [13] => Must have be college graduate\n    [14] => Must clerical and attentive\n    [15] => Nothing in Particular.\n    [16] => N/A\n    [17] => 2018-11-01\n    [18] => 2018-10-20\n)\n', '/smic_recruitment/recruitment_soap_server.php');
INSERT INTO `system_log` (`entry_id`, `ip_address`, `user`, `datetime`, `action`, `module`) VALUES
(1591, '::1', 'Not Logged In', '2018-10-22 12:05:41', 'Query Executed: INSERT INTO iprf_staffrequest(iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssss\n    [1] => 8\n    [2] => PRF-2018-00008\n    [3] => 1\n    [4] => 1\n    [5] => 1\n    [6] => 1\n    [7] => 1\n    [8] => In progress\n    [9] => Sample Duration Data\n    [10] => Collects and process compensation and benefits of employees\n    [11] => Collects and process compensation and benefits of employees in general\n    [12] => Collects and process compensation and benefits of employees in detailed\n    [13] => Must have be college graduate\n    [14] => Must clerical and attentive\n    [15] => Nothing in Particular.\n    [16] => N/A\n    [17] => 2018-11-01\n    [18] => 2018-10-20\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1592, '::1', 'Not Logged In', '2018-10-22 12:06:34', 'Query Executed: INSERT INTO iprf_staffrequest(iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssss\n    [1] => 9\n    [2] => PRF-2018-00008\n    [3] => 1\n    [4] => 1\n    [5] => 1\n    [6] => 1\n    [7] => 1\n    [8] => In progress\n    [9] => Sample Duration Data\n    [10] => Collects and process compensation and benefits of employees\n    [11] => Collects and process compensation and benefits of employees in general\n    [12] => Collects and process compensation and benefits of employees in detailed\n    [13] => Must have be college graduate\n    [14] => Must clerical and attentive\n    [15] => Nothing in Particular.\n    [16] => N/A\n    [17] => 2018-11-01\n    [18] => 2018-10-20\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1593, '::1', 'Not Logged In', '2018-10-22 12:07:12', 'Query Executed: INSERT INTO iprf_staffrequest(iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssss\n    [1] => 10\n    [2] => PRF-2018-00008\n    [3] => 1\n    [4] => 1\n    [5] => 1\n    [6] => 1\n    [7] => 1\n    [8] => In progress\n    [9] => Sample Duration Data\n    [10] => Collects and process compensation and benefits of employees\n    [11] => Collects and process compensation and benefits of employees in general\n    [12] => Collects and process compensation and benefits of employees in detailed\n    [13] => Must have be college graduate\n    [14] => Must clerical and attentive\n    [15] => Nothing in Particular.\n    [16] => N/A\n    [17] => 2018-11-01\n    [18] => 2018-10-20\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1594, '::1', 'Not Logged In', '2018-10-22 12:08:16', 'Query Executed: INSERT INTO iprf_staffrequest(iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssss\n    [1] => 11\n    [2] => PRF-2018-00008\n    [3] => 1\n    [4] => 1\n    [5] => 1\n    [6] => 1\n    [7] => 1\n    [8] => In progress\n    [9] => Sample Duration Data\n    [10] => Collects and process compensation and benefits of employees\n    [11] => Collects and process compensation and benefits of employees in general\n    [12] => Collects and process compensation and benefits of employees in detailed\n    [13] => Must have be college graduate\n    [14] => Must clerical and attentive\n    [15] => Nothing in Particular.\n    [16] => N/A\n    [17] => 2018-11-01\n    [18] => 2018-10-20\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1595, '::1', 'Not Logged In', '2018-10-22 12:09:27', 'Query Executed: INSERT INTO iprf_staffrequest(iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssss\n    [1] => 12\n    [2] => PRF-2018-00008\n    [3] => 1\n    [4] => 1\n    [5] => 1\n    [6] => 1\n    [7] => 1\n    [8] => In progress\n    [9] => Sample Duration Data\n    [10] => Collects and process compensation and benefits of employees\n    [11] => Collects and process compensation and benefits of employees in general\n    [12] => Collects and process compensation and benefits of employees in detailed\n    [13] => Must have be college graduate\n    [14] => Must clerical and attentive\n    [15] => Nothing in Particular.\n    [16] => N/A\n    [17] => 2018-11-01\n    [18] => 2018-10-20\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1596, '::1', 'markdanico.fernandez@gmail.com', '2018-10-22 12:13:06', 'Logged out', '/smic_recruitment/end.php'),
(1597, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 07:42:01', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1598, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 07:42:31', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1599, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 07:42:31', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 953c75ecb6471a71c60e31b72f17d07d6640be20download_(1).jpg\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Female\n    [11] => \n    [12] => \n    [13] => 2000-09-27\n    [14] => \n    [15] => \n    [16] => \n    [17] => \n    [18] => \n    [19] => \n    [20] => \n    [21] => \n    [22] => \n    [23] => hey\n    [24] => \n    [25] => \n    [26] => \n    [27] => \n    [28] => \n    [29] => \n    [30] => \n    [31] => \n    [32] => \n    [33] => \n    [34] => \n    [35] => \n    [36] => \n    [37] => \n    [38] => \n    [39] => \n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1600, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 07:56:03', 'Logged out', '/smic_recruitment/end.php'),
(1601, '::1', 'root', '2018-10-23 07:56:06', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1602, '::1', 'root', '2018-10-23 07:56:59', 'Query Executed: DELETE FROM user_role_links WHERE role_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1603, '::1', 'root', '2018-10-23 07:56:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 220\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1604, '::1', 'root', '2018-10-23 07:56:59', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 49\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1605, '::1', 'root', '2018-10-23 07:57:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 57\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1606, '::1', 'root', '2018-10-23 07:57:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 61\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1607, '::1', 'root', '2018-10-23 07:57:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 203\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1608, '::1', 'root', '2018-10-23 07:57:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 224\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1609, '::1', 'root', '2018-10-23 07:57:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 65\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1610, '::1', 'root', '2018-10-23 07:57:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 69\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1611, '::1', 'root', '2018-10-23 07:57:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 73\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1612, '::1', 'root', '2018-10-23 07:57:00', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 77\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1613, '::1', 'root', '2018-10-23 07:57:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 228\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1614, '::1', 'root', '2018-10-23 07:57:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 197\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1615, '::1', 'root', '2018-10-23 07:57:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 223\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1616, '::1', 'root', '2018-10-23 07:57:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 52\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1617, '::1', 'root', '2018-10-23 07:57:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 60\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1618, '::1', 'root', '2018-10-23 07:57:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 64\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1619, '::1', 'root', '2018-10-23 07:57:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 227\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1620, '::1', 'root', '2018-10-23 07:57:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 68\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1621, '::1', 'root', '2018-10-23 07:57:01', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 72\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1622, '::1', 'root', '2018-10-23 07:57:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 76\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1623, '::1', 'root', '2018-10-23 07:57:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 80\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1624, '::1', 'root', '2018-10-23 07:57:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 231\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1625, '::1', 'root', '2018-10-23 07:57:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 222\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1626, '::1', 'root', '2018-10-23 07:57:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 42\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1627, '::1', 'root', '2018-10-23 07:57:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 221\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1628, '::1', 'root', '2018-10-23 07:57:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 50\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1629, '::1', 'root', '2018-10-23 07:57:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 58\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1630, '::1', 'root', '2018-10-23 07:57:02', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 62\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1631, '::1', 'root', '2018-10-23 07:57:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 204\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1632, '::1', 'root', '2018-10-23 07:57:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 225\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1633, '::1', 'root', '2018-10-23 07:57:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 66\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1634, '::1', 'root', '2018-10-23 07:57:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 70\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1635, '::1', 'root', '2018-10-23 07:57:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 74\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1636, '::1', 'root', '2018-10-23 07:57:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 78\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1637, '::1', 'root', '2018-10-23 07:57:03', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 229\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1638, '::1', 'root', '2018-10-23 07:57:04', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 51\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1639, '::1', 'root', '2018-10-23 07:57:04', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 59\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1640, '::1', 'root', '2018-10-23 07:57:04', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 63\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1641, '::1', 'root', '2018-10-23 07:57:04', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 202\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1642, '::1', 'root', '2018-10-23 07:57:04', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 67\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1643, '::1', 'root', '2018-10-23 07:57:04', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 71\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1644, '::1', 'root', '2018-10-23 07:57:04', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 75\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1645, '::1', 'root', '2018-10-23 07:57:04', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 79\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1646, '::1', 'root', '2018-10-23 07:57:15', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions.php'),
(1647, '::1', 'root', '2018-10-23 07:57:19', 'Query Executed: DELETE FROM user_passport WHERE username IN (?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssss\n    [1] => applicant1\n    [2] => markdanico.fernandez1@gmail.com\n    [3] => markdanico.fernandez@gmail.com\n    [4] => markdanico.fernandezz@gmail.com\n    [5] => reyeskimberly018@gmail.com\n    [6] => smic.email.tester@gmail.com\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1648, '::1', 'root', '2018-10-23 07:57:19', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => applicant1\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1649, '::1', 'root', '2018-10-23 07:57:19', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez1@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1650, '::1', 'root', '2018-10-23 07:57:19', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1651, '::1', 'root', '2018-10-23 07:57:19', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandezz@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1652, '::1', 'root', '2018-10-23 07:57:19', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => reyeskimberly018@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1653, '::1', 'root', '2018-10-23 07:57:20', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => smic.email.tester@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1654, '::1', 'root', '2018-10-23 07:57:21', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1655, '::1', 'root', '2018-10-23 07:57:49', 'Logged out', '/smic_recruitment/end.php'),
(1656, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 07:57:58', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1657, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 07:59:46', 'Pressed cancel button', '/smic_recruitment/modules/applicant_attachments/add_applicant_attachments.php'),
(1658, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 07:59:48', 'Pressed cancel button', '/smic_recruitment/modules/applicant_attachments/listview_applicant_attachments.php'),
(1659, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 08:00:02', 'Pressed cancel button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1660, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 08:14:32', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_reference/add_applicant_reference.php'),
(1661, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 08:20:55', 'Logged out', '/smic_recruitment/end.php'),
(1662, '::1', 'recruiter1', '2018-10-23 08:26:45', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1663, '::1', 'recruiter1', '2018-10-23 08:27:29', 'Logged out', '/smic_recruitment/end.php'),
(1664, '::1', 'recruiter1', '2018-10-23 08:27:34', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1665, '::1', 'recruiter1', '2018-10-23 08:27:54', 'Logged out', '/smic_recruitment/end.php'),
(1666, '::1', 'root', '2018-10-23 08:27:56', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1667, '::1', 'root', '2018-10-23 08:28:45', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions.php'),
(1668, '::1', 'root', '2018-10-23 08:29:40', 'Pressed submit button', '/smic_recruitment/sysadmin/add_user_links.php'),
(1669, '::1', 'root', '2018-10-23 08:29:41', 'Query Executed: INSERT INTO user_links(name, target, descriptive_title, description, passport_group_id, show_in_tasklist, status, icon, priority) VALUES(?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssss\n    [1] => Recruiter access\n    [2] => none\n    [3] => Recruiter Access\n    [4] => a\n    [5] => 1\n    [6] => No\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n)\n', '/smic_recruitment/sysadmin/add_user_links.php'),
(1670, '::1', 'root', '2018-10-23 08:29:57', 'Query Executed: DELETE FROM user_role_links WHERE role_id = ? AND link_id IN (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 4\n    [2] => 37\n    [3] => 38\n    [4] => 39\n    [5] => 40\n    [6] => 81\n    [7] => 82\n    [8] => 83\n    [9] => 84\n    [10] => 85\n    [11] => 86\n    [12] => 87\n    [13] => 88\n    [14] => 89\n    [15] => 90\n    [16] => 91\n    [17] => 92\n    [18] => 93\n    [19] => 94\n    [20] => 95\n    [21] => 96\n    [22] => 97\n    [23] => 98\n    [24] => 99\n    [25] => 100\n    [26] => 101\n    [27] => 102\n    [28] => 103\n    [29] => 104\n    [30] => 105\n    [31] => 106\n    [32] => 107\n    [33] => 108\n    [34] => 109\n    [35] => 110\n    [36] => 111\n    [37] => 112\n    [38] => 113\n    [39] => 114\n    [40] => 115\n    [41] => 116\n    [42] => 117\n    [43] => 118\n    [44] => 119\n    [45] => 120\n    [46] => 121\n    [47] => 122\n    [48] => 123\n    [49] => 124\n    [50] => 125\n    [51] => 126\n    [52] => 127\n    [53] => 128\n    [54] => 129\n    [55] => 130\n    [56] => 131\n    [57] => 132\n    [58] => 133\n    [59] => 134\n    [60] => 135\n    [61] => 136\n    [62] => 137\n    [63] => 138\n    [64] => 139\n    [65] => 140\n    [66] => 141\n    [67] => 142\n    [68] => 143\n    [69] => 144\n    [70] => 145\n    [71] => 146\n    [72] => 147\n    [73] => 148\n    [74] => 149\n    [75] => 150\n    [76] => 151\n    [77] => 152\n    [78] => 153\n    [79] => 154\n    [80] => 155\n    [81] => 156\n    [82] => 157\n    [83] => 158\n    [84] => 159\n    [85] => 160\n    [86] => 161\n    [87] => 162\n    [88] => 163\n    [89] => 164\n    [90] => 165\n    [91] => 166\n    [92] => 167\n    [93] => 168\n    [94] => 169\n    [95] => 170\n    [96] => 171\n    [97] => 172\n    [98] => 173\n    [99] => 174\n    [100] => 175\n    [101] => 176\n    [102] => 177\n    [103] => 178\n    [104] => 179\n    [105] => 180\n    [106] => 181\n    [107] => 182\n    [108] => 183\n    [109] => 184\n    [110] => 185\n    [111] => 186\n    [112] => 187\n    [113] => 188\n    [114] => 189\n    [115] => 190\n    [116] => 191\n    [117] => 192\n    [118] => 193\n    [119] => 194\n    [120] => 195\n    [121] => 196\n    [122] => 203\n    [123] => 204\n    [124] => 205\n    [125] => 206\n    [126] => 207\n    [127] => 208\n    [128] => 209\n    [129] => 210\n    [130] => 211\n    [131] => 212\n    [132] => 213\n    [133] => 214\n    [134] => 215\n    [135] => 216\n    [136] => 217\n    [137] => 218\n    [138] => 228\n    [139] => 229\n    [140] => 230\n    [141] => 231\n    [142] => 232\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1671, '::1', 'root', '2018-10-23 08:29:57', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 4\n    [2] => 215\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1672, '::1', 'root', '2018-10-23 08:29:57', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 4\n    [2] => 216\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1673, '::1', 'root', '2018-10-23 08:29:57', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 4\n    [2] => 232\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1674, '::1', 'root', '2018-10-23 08:29:58', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 4\n    [2] => 217\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1675, '::1', 'root', '2018-10-23 08:30:00', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions.php'),
(1676, '::1', 'root', '2018-10-23 08:30:02', 'Query Executed: DELETE FROM user_passport WHERE username IN (?)\r\nArray\n(\n    [0] => s\n    [1] => recruiter1\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1677, '::1', 'root', '2018-10-23 08:30:02', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => recruiter1\n    [2] => 4\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1678, '::1', 'root', '2018-10-23 08:30:04', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1679, '::1', 'root', '2018-10-23 08:30:05', 'Logged out', '/smic_recruitment/end.php'),
(1680, '::1', 'recruiter1', '2018-10-23 08:30:10', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1681, '::1', 'recruiter1', '2018-10-23 10:07:32', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1682, '::1', 'recruiter1', '2018-10-23 10:07:46', 'Logged out', '/smic_recruitment/end.php'),
(1683, '::1', 'recruiter1', '2018-10-23 10:07:49', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1684, '::1', 'recruiter1', '2018-10-23 10:07:59', 'Logged out', '/smic_recruitment/end.php'),
(1685, '::1', 'Not Logged In', '2018-10-23 10:19:53', 'Query Executed: INSERT INTO iprf_staffrequest(iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssss\n    [1] => 2\n    [2] => PRF-2018-00001\n    [3] => 1\n    [4] => 1\n    [5] => 1\n    [6] => 1\n    [7] => 1\n    [8] => In progress\n    [9] => Sample Duration Data\n    [10] => Collects and process compensation and benefits of employees\n    [11] => Collects and process compensation and benefits of employees in general\n    [12] => Collects and process compensation and benefits of employees in detailed\n    [13] => Must have be college graduate\n    [14] => Must clerical and attentive\n    [15] => Nothing in Particular.\n    [16] => N/A\n    [17] => 2018-11-01\n    [18] => 2018-10-20\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1686, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 10:22:39', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1687, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 10:23:58', 'Logged out', '/smic_recruitment/end.php'),
(1688, '::1', 'recruiter1', '2018-10-23 10:24:02', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1689, '::1', 'recruiter1', '2018-10-23 10:24:48', 'Logged out', '/smic_recruitment/end.php'),
(1690, '::1', 'recruiter1', '2018-10-23 10:24:53', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1691, '::1', 'recruiter1', '2018-10-23 10:26:01', 'Logged out', '/smic_recruitment/end.php'),
(1692, '::1', 'recruiter1', '2018-10-23 10:26:04', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1693, '::1', 'recruiter1', '2018-10-23 10:48:08', 'Logged out', '/smic_recruitment/end.php'),
(1694, '::1', 'Not Logged In', '2018-10-23 11:15:38', 'Query Executed: INSERT INTO `position`(position_id, title) VALUES(?,?)\r\nArray\n(\n    [0] => ss\n    [1] => 2\n    [2] => Software Developer\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1695, '::1', 'Not Logged In', '2018-10-23 11:15:38', 'Query Executed: INSERT INTO iprf_staffrequest(iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssss\n    [1] => 3\n    [2] => PRF-2018-00002\n    [3] => 1\n    [4] => 1\n    [5] => 1\n    [6] => 1\n    [7] => 1\n    [8] => In progress\n    [9] => Sample Duration Data\n    [10] => Collects and process compensation and benefits of employees\n    [11] => Collects and process compensation and benefits of employees in general\n    [12] => Collects and process compensation and benefits of employees in detailed\n    [13] => Must have be college graduate\n    [14] => Must clerical and attentive\n    [15] => Nothing in Particular.\n    [16] => N/A\n    [17] => 2018-11-01\n    [18] => 2018-10-20\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1696, '::1', 'Not Logged In', '2018-10-23 11:16:35', 'Query Executed: INSERT INTO iprf_staffrequest(iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssss\n    [1] => 4\n    [2] => PRF-2018-00002\n    [3] => 1\n    [4] => 2\n    [5] => 2\n    [6] => 1\n    [7] => 1\n    [8] => In progress\n    [9] => Sample Duration Data\n    [10] => Collects and process compensation and benefits of employees\n    [11] => Collects and process compensation and benefits of employees in general\n    [12] => Collects and process compensation and benefits of employees in detailed\n    [13] => Must have be college graduate\n    [14] => Must clerical and attentive\n    [15] => Nothing in Particular.\n    [16] => N/A\n    [17] => 2018-11-01\n    [18] => 2018-10-20\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1697, '::1', 'Not Logged In', '2018-10-23 14:07:27', 'Query Executed: INSERT INTO iprf_staffrequest(iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssss\n    [1] => 5\n    [2] => PRF-2018-00002\n    [3] => 1\n    [4] => 1\n    [5] => 1\n    [6] => 1\n    [7] => 1\n    [8] => In progress\n    [9] => Sample Duration Data\n    [10] => Collects and process compensation and benefits of employees\n    [11] => Collects and process compensation and benefits of employees in general\n    [12] => Collects and process compensation and benefits of employees in detailed\n    [13] => Must have be college graduate\n    [14] => Must clerical and attentive\n    [15] => Nothing in Particular.\n    [16] => N/A\n    [17] => 2018-11-01\n    [18] => 2018-10-20\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1698, '::1', 'Not Logged In', '2018-10-23 14:09:14', 'Query Executed: INSERT INTO iprf_staffrequest(iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssss\n    [1] => 6\n    [2] => PRF-2018-00002\n    [3] => 1\n    [4] => 1\n    [5] => 1\n    [6] => 1\n    [7] => 1\n    [8] => In progress\n    [9] => Sample Duration Data\n    [10] => Collects and process compensation and benefits of employees\n    [11] => Collects and process compensation and benefits of employees in general\n    [12] => Collects and process compensation and benefits of employees in detailed\n    [13] => Must have be college graduate\n    [14] => Must clerical and attentive\n    [15] => Nothing in Particular.\n    [16] => N/A\n    [17] => 2018-11-01\n    [18] => 2018-10-20\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1699, '::1', 'Not Logged In', '2018-10-23 14:39:55', 'Query Executed: INSERT INTO iprf_staffrequest(iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssss\n    [1] => 7\n    [2] => PRF-2018-00007\n    [3] => 1\n    [4] => 1\n    [5] => 1\n    [6] => 1\n    [7] => 1\n    [8] => In progress\n    [9] => Sample Duration Data\n    [10] => Collects and process compensation and benefits of employees\n    [11] => Collects and process compensation and benefits of employees in general\n    [12] => Collects and process compensation and benefits of employees in detailed\n    [13] => Must have be college graduate\n    [14] => Must clerical and attentive\n    [15] => Nothing in Particular.\n    [16] => N/A\n    [17] => 2018-11-01\n    [18] => 2018-10-20\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1700, '::1', 'recruiter1', '2018-10-23 14:50:54', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1701, '::1', 'recruiter1', '2018-10-23 15:09:07', 'Logged out', '/smic_recruitment/end.php'),
(1702, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:17:03', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1703, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:20:48', 'Logged out', '/smic_recruitment/end.php'),
(1704, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:21:04', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1705, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:25:15', 'Logged out', '/smic_recruitment/end.php'),
(1706, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:27:20', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1707, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:44:28', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 4\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1708, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:44:28', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 4\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1709, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:45:13', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 4\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1710, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:45:13', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 4\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1711, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:46:45', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1712, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:46:45', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1713, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:47:18', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1714, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:47:52', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1715, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:48:31', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1716, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:48:35', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1717, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:48:40', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1718, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:48:46', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1719, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:48:51', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1720, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:51:53', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1721, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:52:29', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1722, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:53:00', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1723, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:53:06', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1724, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:54:33', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1725, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:54:51', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1726, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:54:58', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1727, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 15:55:57', 'Logged out', '/smic_recruitment/end.php'),
(1728, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 16:03:54', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1729, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 16:06:44', 'Logged out', '/smic_recruitment/end.php'),
(1730, '::1', 'recruiter1', '2018-10-23 16:06:47', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1731, '::1', 'recruiter1', '2018-10-23 16:17:28', 'Logged out', '/smic_recruitment/end.php'),
(1732, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 16:17:36', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1733, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 16:17:56', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1734, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 16:18:29', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 5\n    [2] => 18\n    [3] => 2018-10-23\n)\n', '/smic_recruitment/carreer_page.php'),
(1735, '::1', 'markdanico.fernandez@gmail.com', '2018-10-23 16:18:33', 'Logged out', '/smic_recruitment/end.php'),
(1736, '::1', 'recruiter1', '2018-10-23 16:18:39', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1737, '::1', 'markdanico.fernandez@gmail.com', '2018-10-24 08:35:03', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1738, '::1', 'markdanico.fernandez@gmail.com', '2018-10-24 08:35:08', 'Logged out', '/smic_recruitment/end.php'),
(1739, '::1', 'recruiter1', '2018-10-24 08:41:30', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1740, '::1', 'recruiter1', '2018-10-24 08:41:35', 'Logged out', '/smic_recruitment/end.php'),
(1741, '::1', 'markdanico.fernandez@gmail.com', '2018-10-24 08:41:43', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1742, '::1', 'recruiter1', '2018-10-26 05:32:24', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1743, '::1', 'recruiter1', '2018-10-26 05:33:48', 'Logged out', '/smic_recruitment/end.php'),
(1744, '::1', 'recruiter1', '2018-10-26 05:33:52', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1745, '::1', 'recruiter1', '2018-10-26 05:33:58', 'Logged out', '/smic_recruitment/end.php'),
(1746, '::1', 'recruiter1', '2018-10-26 05:34:33', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1747, '::1', 'recruiter1', '2018-10-26 05:34:49', 'Logged out', '/smic_recruitment/end.php'),
(1748, '::1', 'root', '2018-10-26 05:34:52', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1749, '::1', 'root', '2018-10-26 05:36:24', 'Logged out', '/smic_recruitment/end.php'),
(1750, '::1', 'recruiter1', '2018-10-26 05:36:32', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1751, '::1', 'recruiter1', '2018-10-26 05:54:15', 'Logged out', '/smic_recruitment/end.php'),
(1752, '::1', 'markdanico.fernandez@gmail.com', '2018-10-26 05:54:22', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1753, '::1', 'markdanico.fernandez@gmail.com', '2018-10-26 05:54:27', 'Logged out', '/smic_recruitment/end.php'),
(1754, '::1', 'recruiter1', '2018-10-26 05:54:38', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1755, '::1', 'recruiter1', '2018-10-26 05:58:21', 'ILLEGAL ACCESS ATTEMPT - Tried to access ''/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'' without sufficient privileges.', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(1756, '::1', 'root', '2018-10-26 05:58:38', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1757, '::1', 'root', '2018-10-26 05:59:38', 'Query Executed: DELETE FROM user_role_links WHERE role_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 4\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1758, '::1', 'root', '2018-10-26 05:59:38', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 4\n    [2] => 215\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1759, '::1', 'root', '2018-10-26 05:59:38', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 4\n    [2] => 43\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1760, '::1', 'root', '2018-10-26 05:59:38', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 4\n    [2] => 216\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1761, '::1', 'root', '2018-10-26 05:59:38', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 4\n    [2] => 213\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1762, '::1', 'root', '2018-10-26 05:59:38', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 4\n    [2] => 232\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1763, '::1', 'root', '2018-10-26 05:59:39', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 4\n    [2] => 217\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(1764, '::1', 'root', '2018-10-26 05:59:57', 'Query Executed: DELETE FROM user_passport WHERE username IN (?)\r\nArray\n(\n    [0] => s\n    [1] => recruiter1\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1765, '::1', 'root', '2018-10-26 05:59:58', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => recruiter1\n    [2] => 4\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(1766, '::1', 'root', '2018-10-26 06:00:00', 'Logged out', '/smic_recruitment/end.php'),
(1767, '::1', 'recruiter1', '2018-10-26 06:00:02', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1768, '::1', 'recruiter1', '2018-10-26 06:04:24', 'Logged out', '/smic_recruitment/end.php'),
(1769, '::1', 'recruiter1', '2018-10-26 06:04:31', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1770, '::1', 'recruiter1', '2018-10-26 06:23:35', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1771, '::1', 'recruiter1', '2018-10-26 06:25:04', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1772, '::1', 'recruiter1', '2018-10-26 06:25:40', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1773, '::1', 'recruiter1', '2018-10-26 06:26:02', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1774, '::1', 'recruiter1', '2018-10-26 06:26:06', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1775, '::1', 'recruiter1', '2018-10-26 06:30:11', 'Pressed cancel button', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php');
INSERT INTO `system_log` (`entry_id`, `ip_address`, `user`, `datetime`, `action`, `module`) VALUES
(1776, '::1', 'recruiter1', '2018-10-26 06:30:18', 'Pressed cancel button', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(1777, '::1', 'recruiter1', '2018-10-26 06:31:06', 'Pressed cancel button', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(1778, '::1', 'recruiter1', '2018-10-26 06:31:22', 'Pressed cancel button', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(1779, '::1', 'recruiter1', '2018-10-26 06:31:39', 'Pressed cancel button', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(1780, '::1', 'recruiter1', '2018-10-26 06:31:52', 'Logged out', '/smic_recruitment/end.php'),
(1781, '::1', 'root', '2018-10-26 06:31:56', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1782, '::1', 'root', '2018-10-26 06:33:20', 'Pressed submit button', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1783, '::1', 'root', '2018-10-26 06:33:21', 'Query Executed: UPDATE user_links SET name = ?, target = ?, descriptive_title = ?, description = ?, passport_group_id = ?, show_in_tasklist = ?, status = ?, icon = ?, priority = ? WHERE link_id = ?\r\nArray\n(\n    [0] => ssssssssss\n    [1] => View iprf staffrequest applicants\n    [2] => modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php\n    [3] => Iprf Staffrequest Applicants\n    [4] => \n    [5] => 1\n    [6] => No\n    [7] => On\n    [8] => form3.png\n    [9] => 0\n    [10] => 213\n)\n', '/smic_recruitment/sysadmin/edit_user_links.php'),
(1784, '::1', 'root', '2018-10-26 06:33:31', 'Logged out', '/smic_recruitment/end.php'),
(1785, '::1', 'markdanico.fernandez@gmail.com', '2018-10-26 06:33:37', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1786, '::1', 'markdanico.fernandez@gmail.com', '2018-10-26 06:57:17', 'ILLEGAL ACCESS ATTEMPT - Tried to access ''/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'' without sufficient privileges.', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(1787, '::1', 'recruiter1', '2018-10-26 06:57:29', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1788, '::1', 'recruiter1', '2018-10-26 06:57:43', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1789, '::1', 'recruiter1', '2018-10-26 06:57:44', 'Pressed cancel button', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(1790, '::1', 'recruiter1', '2018-10-26 06:59:05', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1791, '::1', 'recruiter1', '2018-10-26 07:12:53', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1792, '::1', 'recruiter1', '2018-10-26 07:13:33', 'Logged out', '/smic_recruitment/end.php'),
(1793, '::1', 'root', '2018-10-26 07:13:46', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1794, '::1', 'root', '2018-10-26 07:13:50', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 0\n    [3] => 2018-10-26\n)\n', '/smic_recruitment/carreer_page.php'),
(1795, '::1', 'root', '2018-10-26 07:13:53', 'Logged out', '/smic_recruitment/end.php'),
(1796, '::1', 'recruiter1', '2018-10-26 07:14:04', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1797, '::1', 'recruiter1', '2018-10-26 07:14:17', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1798, '::1', 'recruiter1', '2018-10-26 09:42:02', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(1799, '::1', 'recruiter1', '2018-10-26 09:43:17', 'Pressed cancel button', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(1800, '::1', 'Not Logged In', '2018-10-29 13:31:56', 'Query Executed: INSERT INTO iprf_staffrequest(iprf_staffrequest_id,PRF_No, company_id, department_id, position_id, rank_id, classification_id, status, duration, responsibility, general_duties, detailed_duties, education, professional_eligibility_skills, experience, skills, date_needed, date_filed) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssss\n    [1] => 7\n    [2] => PRF-2018-00007\n    [3] => 1\n    [4] => 1\n    [5] => 1\n    [6] => 1\n    [7] => 1\n    [8] => In progress\n    [9] => Sample Duration Data\n    [10] => Collects and process compensation and benefits of employees\n    [11] => Collects and process compensation and benefits of employees in general\n    [12] => Collects and process compensation and benefits of employees in detailed\n    [13] => Must have be college graduate\n    [14] => Must clerical and attentive\n    [15] => Nothing in Particular.\n    [16] => N/A\n    [17] => 2018-11-01\n    [18] => 2018-10-20\n)\n', '/smic_recruitment/recruitment_soap_server.php'),
(1801, '::1', 'recruiter1', '2018-10-30 07:39:14', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1802, '::1', 'recruiter1', '2018-10-30 09:42:18', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1803, '::1', 'recruiter1', '2018-10-30 10:24:35', 'Logged out', '/smic_recruitment/end.php'),
(1804, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 10:24:43', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1805, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 10:25:47', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1806, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 10:25:47', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 953c75ecb6471a71c60e31b72f17d07d6640be20download_(1).jpg\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => Nico\n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => Single\n    [12] => Filipino\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => O\n    [17] => Sta. Cruz, Manila\n    [18] => \n    [19] => \n    [20] => \n    [21] => \n    [22] => \n    [23] => hey\n    [24] => \n    [25] => \n    [26] => \n    [27] => \n    [28] => \n    [29] => 952-62-35\n    [30] => \n    [31] => \n    [32] => \n    [33] => \n    [34] => \n    [35] => Roman Catholic\n    [36] => \n    [37] => \n    [38] => \n    [39] => \n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1807, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 10:27:23', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1808, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 10:27:43', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1809, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 10:27:43', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 953c75ecb6471a71c60e31b72f17d07d6640be20download_(1).jpg\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => Nico\n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => Single\n    [12] => Filipino\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => O\n    [17] => Sta. Cruz, Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue\n    [20] => Pinagbuhatan\n    [21] => Pasig\n    [22] => \n    [23] => 952-62-35\n    [24] => Block 41 Kenneth Road\n    [25] => Eusebio Avenue\n    [26] => Pinagbuhatan\n    [27] => Pasig\n    [28] => \n    [29] => 952-62-35\n    [30] => 123452345612\n    [31] => 123445764567\n    [32] => 234553411\n    [33] => 1234563456\n    [34] => \n    [35] => Roman Catholic\n    [36] => Amelia Fernandez\n    [37] => Mother\n    [38] => Same\n    [39] => 952-62-35\n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1810, '::1', 'recruiter1', '2018-10-30 10:28:03', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1811, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 10:41:32', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1812, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 10:41:32', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 953c75ecb6471a71c60e31b72f17d07d6640be20download_(1).jpg\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => Single\n    [12] => Filipino\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => O\n    [17] => Sta. Cruz, Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue\n    [20] => Pinagbuhatan\n    [21] => Pasig\n    [22] => \n    [23] => 952-62-35\n    [24] => Block 41 Kenneth Road\n    [25] => Eusebio Avenue\n    [26] => Pinagbuhatan\n    [27] => Pasig\n    [28] => \n    [29] => 952-62-35\n    [30] => 123452345612\n    [31] => 123445764567\n    [32] => 234553411\n    [33] => 1234563456\n    [34] => \n    [35] => Roman Catholic\n    [36] => Amelia Fernandez\n    [37] => Mother\n    [38] => Same\n    [39] => 952-62-35\n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1813, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 10:44:31', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/detailview_applicant_family_members.php'),
(1814, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 10:53:59', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_family_members/edit_applicant_family_members.php'),
(1815, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 10:53:59', 'Query Executed: UPDATE applicant_family_members SET applicant_id = ?, name = ?, birthday = ?, age = ?, relationship = ?, is_dependent = ? WHERE applicant_family_member_id = ?\r\nArray\n(\n    [0] => sssssss\n    [1] => 18\n    [2] => hey\n    [3] => 1986-10-05\n    [4] => 32\n    [5] => Mother\n    [6] => Yes\n    [7] => 3\n)\n', '/smic_recruitment/modules/applicant/applicant_family_members/edit_applicant_family_members.php'),
(1816, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 10:55:08', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(1817, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 10:55:08', 'Query Executed: INSERT INTO applicant_family_members(applicant_id, name, birthday, age, relationship, is_dependent) VALUES(?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssss\n    [1] => 18\n    [2] => Amelia Fernandez\n    [3] => 1954-10-30\n    [4] => 64\n    [5] => Mother\n    [6] => No\n)\n', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(1818, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 10:55:20', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_family_members/edit_applicant_family_members.php'),
(1819, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 10:55:20', 'Query Executed: UPDATE applicant_family_members SET applicant_id = ?, name = ?, birthday = ?, age = ?, relationship = ?, is_dependent = ? WHERE applicant_family_member_id = ?\r\nArray\n(\n    [0] => sssssss\n    [1] => 18\n    [2] => Danilo Fernandez\n    [3] => 1967-10-05\n    [4] => 51\n    [5] => Mother\n    [6] => Yes\n    [7] => 3\n)\n', '/smic_recruitment/modules/applicant/applicant_family_members/edit_applicant_family_members.php'),
(1820, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 11:04:53', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_school_attended/detailview_applicant_school_attended.php'),
(1821, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 11:14:27', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_school_attended/edit_applicant_school_attended.php'),
(1822, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 11:14:27', 'Query Executed: UPDATE applicant_school_attended SET applicant_id = ?, school_id = ?, educational_attainment = ?, course = ?, awards = ?, address = ?, date_from = ?, date_to = ? WHERE applicant_school_attended_id = ?\r\nArray\n(\n    [0] => sssssssss\n    [1] => 18\n    [2] => 1\n    [3] => College\n    [4] => Bachelor of Science in Information Technology\n    [5] => \n    [6] => aw\n    [7] => 2012-06-01\n    [8] => 2016-05-31\n    [9] => 3\n)\n', '/smic_recruitment/modules/applicant/applicant_school_attended/edit_applicant_school_attended.php'),
(1823, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 11:21:09', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_school_attended/add_applicant_school_attended.php'),
(1824, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 11:21:09', 'Query Executed: INSERT INTO applicant_school_attended(applicant_id, school_id, educational_attainment, course, awards, address, date_from, date_to) VALUES(?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssss\n    [1] => 18\n    [2] => 2\n    [3] => High School\n    [4] => \n    [5] => \n    [6] => a\n    [7] => 2004-10-30\n    [8] => 2008-10-30\n)\n', '/smic_recruitment/modules/applicant/applicant_school_attended/add_applicant_school_attended.php'),
(1825, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 11:41:40', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_previous_employers/detailview_applicant_previous_employers.php'),
(1826, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 11:50:40', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_reference/add_applicant_reference.php'),
(1827, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 11:50:40', 'Query Executed: INSERT INTO applicant_reference(applicant_id, reference_name, reference_occupation, reference_relationship, reference_additional_information, reference_address, reference_contact_number) VALUES(?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssss\n    [1] => 18\n    [2] => Kim Reyes\n    [3] => Junior IT Consultant\n    [4] => Office Mate\n    [5] => \n    [6] => Sample\n    [7] => 123456789\n)\n', '/smic_recruitment/modules/applicant/applicant_reference/add_applicant_reference.php'),
(1828, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 12:15:46', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php'),
(1829, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 12:15:46', 'Query Executed: INSERT INTO applicant_languages_proficiency(applicant_id, language, speaking_proficiency, writing_proficiency) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 18\n    [2] => English\n    [3] => 3\n    [4] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php'),
(1830, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 13:53:30', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/detailview_applicant_languages_proficiency.php'),
(1831, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 14:10:01', 'Pressed submit button', '/smic_recruitment/modules/applicant_attachments/add_applicant_attachments.php'),
(1832, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 14:10:58', 'Pressed submit button', '/smic_recruitment/modules/applicant_attachments/add_applicant_attachments.php'),
(1833, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 14:11:00', 'Pressed submit button', '/smic_recruitment/modules/applicant_attachments/add_applicant_attachments.php'),
(1834, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 14:11:32', 'Pressed submit button', '/smic_recruitment/modules/applicant_attachments/add_applicant_attachments.php'),
(1835, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 14:11:34', 'Pressed cancel button', '/smic_recruitment/modules/applicant_attachments/add_applicant_attachments.php'),
(1836, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 14:11:39', 'Pressed submit button', '/smic_recruitment/modules/applicant_attachments/add_applicant_attachments.php'),
(1837, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 14:11:39', 'Query Executed: INSERT INTO applicant_attachments(applicant_id, attachments, date_uploaded) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 18\n    [2] => 53ccdda33860dba7cc59779210dcc4c5bf234f4b14947643_1818918724986328_1710167908974488298_n.jpg\n    [3] => \n)\n', '/smic_recruitment/modules/applicant_attachments/add_applicant_attachments.php'),
(1838, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 14:11:43', 'Pressed cancel button', '/smic_recruitment/modules/applicant_attachments/detailview_applicant_attachments.php'),
(1839, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 14:11:59', 'Pressed cancel button', '/smic_recruitment/modules/applicant_attachments/detailview_applicant_attachments.php'),
(1840, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 14:13:44', 'Pressed submit button', '/smic_recruitment/modules/applicant_attachments/edit_applicant_attachments.php'),
(1841, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 14:13:44', 'Query Executed: UPDATE applicant_attachments SET applicant_id = ?, attachments = ?, date_uploaded = ? WHERE applicant_attachment_id = ?\r\nArray\n(\n    [0] => ssss\n    [1] => 18\n    [2] => cf1a511fe40014b5d771c1db3b01a6640c4fdbb916512865-Rank-Icons-Stock-Vector-icon.jpg\n    [3] => \n    [4] => 1\n)\n', '/smic_recruitment/modules/applicant_attachments/edit_applicant_attachments.php'),
(1842, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 14:13:47', 'Pressed cancel button', '/smic_recruitment/modules/applicant_attachments/detailview_applicant_attachments.php'),
(1843, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 14:15:28', 'Pressed cancel button', '/smic_recruitment/modules/applicant_attachments/detailview_applicant_attachments.php'),
(1844, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 14:15:40', 'Pressed cancel button', '/smic_recruitment/modules/applicant_attachments/listview_applicant_attachments.php'),
(1845, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 14:15:57', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_school_attended/listview_applicant_school_attended.php'),
(1846, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 14:16:39', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_school_attended/listview_applicant_school_attended.php'),
(1847, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 14:16:44', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_reference/listview_applicant_reference.php'),
(1848, '::1', 'recruiter1', '2018-10-30 14:16:56', 'Pressed cancel button', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(1849, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 14:45:13', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(1850, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 14:48:40', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/detailview_applicant_family_members.php'),
(1851, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 15:02:11', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1852, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 15:02:11', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => \n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => Single\n    [12] => Filipino\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => O\n    [17] => Sta. Cruz, Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue\n    [20] => Pinagbuhatan\n    [21] => Pasig\n    [22] => \n    [23] => 952-62-35\n    [24] => Block 41 Kenneth Road\n    [25] => Eusebio Avenue\n    [26] => Pinagbuhatan\n    [27] => Pasig\n    [28] => \n    [29] => 952-62-35\n    [30] => 123452345612\n    [31] => 123445764567\n    [32] => 234553411\n    [33] => 1234563456\n    [34] => \n    [35] => Roman Catholic\n    [36] => Amelia Fernandez\n    [37] => Mother\n    [38] => Same\n    [39] => 952-62-35\n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1853, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 15:12:14', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1854, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 15:12:14', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => \n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => Single\n    [12] => Filipino\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => O\n    [17] => Sta. Cruz, Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue\n    [20] => Pinagbuhatan\n    [21] => Pasig\n    [22] => \n    [23] => 952-62-35\n    [24] => Block 41 Kenneth Road\n    [25] => Eusebio Avenue\n    [26] => Pinagbuhatan\n    [27] => Pasig\n    [28] => \n    [29] => 952-62-35\n    [30] => 123452345612\n    [31] => 123445764567\n    [32] => 234553411\n    [33] => 1234563456\n    [34] => \n    [35] => Roman Catholic\n    [36] => Amelia Fernandez\n    [37] => Mother\n    [38] => Same\n    [39] => 952-62-35\n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1855, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 15:13:19', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1856, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 15:13:19', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality = ?, present_address_province = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality = ?, provincial_address_province = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 42431d51342b28d2bebb0673abe846200cdeb9b015134615_1778595462469230_9126298284826496227_n.jpg\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => Single\n    [12] => Filipino\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => O\n    [17] => Sta. Cruz, Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue\n    [20] => Pinagbuhatan\n    [21] => Pasig\n    [22] => \n    [23] => 952-62-35\n    [24] => Block 41 Kenneth Road\n    [25] => Eusebio Avenue\n    [26] => Pinagbuhatan\n    [27] => Pasig\n    [28] => \n    [29] => 952-62-35\n    [30] => 123452345612\n    [31] => 123445764567\n    [32] => 234553411\n    [33] => 1234563456\n    [34] => \n    [35] => Roman Catholic\n    [36] => Amelia Fernandez\n    [37] => Mother\n    [38] => Same\n    [39] => 952-62-35\n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1857, '::1', 'markdanico.fernandez@gmail.com', '2018-10-30 16:22:14', 'Logged out', '/smic_recruitment/end.php'),
(1858, '::1', 'Not Logged In', '2018-10-30 16:22:38', 'Query Executed: INSERT INTO temp_user(personal_email, password, salt, iteration, method, token, first_name, middle_name, last_name, gender) VALUES(?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssss\n    [1] => sumimurayoshi28@gmail.com\n    [2] => $2y$12$Bi74a3XCkFGNOfQy2yZyIeNvcK.pjYS42GmLtRbAVdMtrafVMz3tW\n    [3] => Bi74a3XCkFGNOfQy2yZyIg\n    [4] => 12\n    [5] => bcrypt\n    [6] => c9df64009ed4d0959d7060124ef8ae301335d48d\n    [7] => Nico\n    [8] => A\n    [9] => Fernan\n    [10] => Male\n)\n', '/smic_recruitment/applicant_registration.php'),
(1859, '::1', 'Not Logged In', '2018-10-30 16:23:05', 'Query Executed: INSERT INTO applicant(applicant_number, last_name, first_name, middle_name) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => APL-1810-00002\n    [2] => Fernan\n    [3] => Nico\n    [4] => A\n)\n', '/smic_recruitment/applicant_registration.php'),
(1860, '::1', 'Not Logged In', '2018-10-30 16:23:05', 'Query Executed: INSERT INTO user(username, password, salt, iteration, method, applicant_id, role_id, skin_id, user_level) VALUES(?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssssss\n    [1] => sumimurayoshi28@gmail.com\n    [2] => $2y$12$Bi74a3XCkFGNOfQy2yZyIeNvcK.pjYS42GmLtRbAVdMtrafVMz3tW\n    [3] => Bi74a3XCkFGNOfQy2yZyIg\n    [4] => 12\n    [5] => bcrypt\n    [6] => 19\n    [7] => 3\n    [8] => 1\n    [9] => 5\n)\n', '/smic_recruitment/applicant_registration.php'),
(1861, '::1', 'Not Logged In', '2018-10-30 16:23:06', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => sumimurayoshi28@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/applicant_registration.php'),
(1862, '::1', 'Not Logged In', '2018-10-30 16:23:06', 'Query Executed: DELETE FROM temp_user WHERE temp_user_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 1\n)\n', '/smic_recruitment/applicant_registration.php'),
(1863, '::1', 'sumimurayoshi28@gmail.com', '2018-10-30 16:23:16', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1864, '::1', 'sumimurayoshi28@gmail.com', '2018-10-30 16:24:27', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-10-30\n)\n', '/smic_recruitment/carreer_page.php'),
(1865, '::1', 'sumimurayoshi28@gmail.com', '2018-10-30 16:35:59', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-10-30\n)\n', '/smic_recruitment/carreer_page.php'),
(1866, '::1', 'sumimurayoshi28@gmail.com', '2018-10-30 16:36:05', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-10-30\n)\n', '/smic_recruitment/carreer_page.php'),
(1867, '::1', 'sumimurayoshi28@gmail.com', '2018-10-30 16:36:18', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-10-30\n)\n', '/smic_recruitment/carreer_page.php'),
(1868, '::1', 'sumimurayoshi28@gmail.com', '2018-10-30 16:39:53', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-10-30\n)\n', '/smic_recruitment/carreer_page.php'),
(1869, '::1', 'sumimurayoshi28@gmail.com', '2018-10-30 16:40:01', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied) VALUES(?,?,?)\r\nArray\n(\n    [0] => sss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-10-30\n)\n', '/smic_recruitment/carreer_page.php'),
(1870, '::1', 'recruiter1', '2018-11-05 10:49:03', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1871, '::1', 'recruiter1', '2018-11-05 11:03:18', 'Logged out', '/smic_recruitment/end.php'),
(1872, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 11:03:29', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1873, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 13:19:16', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1874, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 13:19:16', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality_id = ?, present_address_province_id = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality_id = ?, provincial_address_province_id = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 42431d51342b28d2bebb0673abe846200cdeb9b015134615_1778595462469230_9126298284826496227_n.jpg\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => Single\n    [12] => Filipino\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => O\n    [17] => Sta. Cruz, Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue\n    [20] => Pinagbuhatan\n    [21] => 671\n    [22] => 64\n    [23] => 952-62-35\n    [24] => Block 41 Kenneth Road\n    [25] => Eusebio Avenue\n    [26] => Pinagbuhatan\n    [27] => 698\n    [28] => 57\n    [29] => 952-62-35\n    [30] => 123452345612\n    [31] => 123445764567\n    [32] => 234553411\n    [33] => 1234563456\n    [34] => \n    [35] => Roman Catholic\n    [36] => Amelia Fernandez\n    [37] => Mother\n    [38] => Same\n    [39] => 952-62-35\n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1875, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 13:21:49', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1876, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 13:21:59', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1877, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 13:28:19', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1878, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 13:28:40', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1879, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 13:29:31', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1880, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 13:29:31', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality_id = ?, present_address_province_id = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality_id = ?, provincial_address_province_id = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 42431d51342b28d2bebb0673abe846200cdeb9b015134615_1778595462469230_9126298284826496227_n.jpg\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => Single\n    [12] => Filipino\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => O\n    [17] => Sta. Cruz, Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue\n    [20] => Pinagbuhatan\n    [21] => 671\n    [22] => 64\n    [23] => 952-62-35\n    [24] => Block 41 Kenneth Road\n    [25] => Eusebio Avenue\n    [26] => Pinagbuhatan\n    [27] => \n    [28] => 57\n    [29] => 952-62-35\n    [30] => 123452345612\n    [31] => 123445764567\n    [32] => 234553411\n    [33] => 1234563456\n    [34] => \n    [35] => Roman Catholic\n    [36] => Amelia Fernandez\n    [37] => Mother\n    [38] => Same\n    [39] => 952-62-35\n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1881, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:01:12', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1882, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:12:12', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1883, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:14:20', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1884, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:14:20', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality_id = ?, present_address_province_id = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality_id = ?, provincial_address_province_id = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 42431d51342b28d2bebb0673abe846200cdeb9b015134615_1778595462469230_9126298284826496227_n.jpg\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => Single\n    [12] => Filipino\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => O\n    [17] => Sta. Cruz, Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue\n    [20] => Pinagbuhatan\n    [21] => 2965\n    [22] => 64\n    [23] => 952-62-35\n    [24] => Block 41 Kenneth Road\n    [25] => Eusebio Avenue\n    [26] => Pinagbuhatan\n    [27] => 2797\n    [28] => 57\n    [29] => 952-62-35\n    [30] => 123452345612\n    [31] => 123445764567\n    [32] => 234553411\n    [33] => 1234563456\n    [34] => \n    [35] => Roman Catholic\n    [36] => Amelia Fernandez\n    [37] => Mother\n    [38] => Same\n    [39] => 952-62-35\n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1885, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:14:32', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1886, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:14:33', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality_id = ?, present_address_province_id = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality_id = ?, provincial_address_province_id = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 42431d51342b28d2bebb0673abe846200cdeb9b015134615_1778595462469230_9126298284826496227_n.jpg\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => Single\n    [12] => Filipino\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => O\n    [17] => Sta. Cruz, Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue\n    [20] => Pinagbuhatan\n    [21] => 2965\n    [22] => 64\n    [23] => 952-62-35\n    [24] => Block 41 Kenneth Road\n    [25] => Eusebio Avenue\n    [26] => Pinagbuhatan\n    [27] => \n    [28] => 57\n    [29] => 952-62-35\n    [30] => 123452345612\n    [31] => 123445764567\n    [32] => 234553411\n    [33] => 1234563456\n    [34] => \n    [35] => Roman Catholic\n    [36] => Amelia Fernandez\n    [37] => Mother\n    [38] => Same\n    [39] => 952-62-35\n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(1887, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:01', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1888, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:01', 'Query Executed: DELETE FROM other_declarations_name_smgroup WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1889, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:01', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => as\n    [3] => hey\n    [4] => \n    [5] => s\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1890, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:01', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => s\n    [3] => \n    [4] => sup\n    [5] => s\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1891, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:01', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => sa\n    [3] => yow\n    [4] => \n    [5] => a\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1892, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:02', 'Query Executed: DELETE FROM other_declarations_name_smic_employ WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1893, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:02', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => a\n    [3] => hey\n    [4] => yow\n    [5] => sup\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1894, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:02', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => 1\n    [3] => \n    [4] => \n    [5] => \n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1895, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:02', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => sa\n    [3] => \n    [4] => s\n    [5] => \n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1896, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:02', 'Query Executed: UPDATE applicant_other_declarations SET applicant_id = ?, crime_convict = ?, details = ? WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => ssss\n    [1] => 18\n    [2] => No\n    [3] => s\n    [4] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1897, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:15', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1898, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:15', 'Query Executed: DELETE FROM other_declarations_name_smgroup WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1899, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:15', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => as\n    [3] => hey\n    [4] => \n    [5] => s\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php');
INSERT INTO `system_log` (`entry_id`, `ip_address`, `user`, `datetime`, `action`, `module`) VALUES
(1900, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:15', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => s\n    [3] => \n    [4] => sup\n    [5] => s\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1901, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:15', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => sa\n    [3] => yow\n    [4] => \n    [5] => a\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1902, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:16', 'Query Executed: DELETE FROM other_declarations_name_smic_employ WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1903, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:16', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => a\n    [3] => hey\n    [4] => yow\n    [5] => sup\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1904, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:16', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => 1\n    [3] => \n    [4] => \n    [5] => \n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1905, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:16', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => sa\n    [3] => \n    [4] => s\n    [5] => \n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1906, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:17', 'Query Executed: UPDATE applicant_other_declarations SET applicant_id = ?, crime_convict = ?, details = ? WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => ssss\n    [1] => 18\n    [2] => No\n    [3] => \n    [4] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1907, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:40', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1908, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:41', 'Query Executed: DELETE FROM other_declarations_name_smgroup WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1909, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:41', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => as\n    [3] => hey\n    [4] => \n    [5] => s\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1910, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:41', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => s\n    [3] => \n    [4] => sup\n    [5] => s\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1911, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:41', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => sa\n    [3] => yow\n    [4] => \n    [5] => a\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1912, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:41', 'Query Executed: DELETE FROM other_declarations_name_smic_employ WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1913, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:41', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => a\n    [3] => hey\n    [4] => yow\n    [5] => sup\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1914, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:42', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => 1\n    [3] => \n    [4] => \n    [5] => \n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1915, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:42', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => sa\n    [3] => \n    [4] => s\n    [5] => \n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1916, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:22:42', 'Query Executed: UPDATE applicant_other_declarations SET applicant_id = ?, crime_convict = ?, details = ? WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => ssss\n    [1] => 18\n    [2] => No\n    [3] => \n    [4] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1917, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:24:38', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1918, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:24:38', 'Query Executed: DELETE FROM other_declarations_name_smgroup WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1919, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:24:38', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => as\n    [3] => hey\n    [4] => \n    [5] => s\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1920, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:24:38', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => s\n    [3] => \n    [4] => sup\n    [5] => s\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1921, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:24:38', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => sa\n    [3] => yow\n    [4] => \n    [5] => a\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1922, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:24:39', 'Query Executed: DELETE FROM other_declarations_name_smic_employ WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1923, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:24:39', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => a\n    [3] => hey\n    [4] => yow\n    [5] => sup\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1924, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:24:39', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => 1\n    [3] => \n    [4] => \n    [5] => \n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1925, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:24:39', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => sa\n    [3] => \n    [4] => s\n    [5] => \n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1926, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:24:39', 'Query Executed: UPDATE applicant_other_declarations SET applicant_id = ?, crime_convict = ?, details = ? WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => ssss\n    [1] => 18\n    [2] => No\n    [3] => \n    [4] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1927, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:26:17', 'Pressed cancel button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1928, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:27:02', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1929, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:27:39', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1930, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:27:42', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1931, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:27:52', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1932, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:28:08', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1933, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:28:48', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1934, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:28:50', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1935, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:29:09', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1936, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:29:19', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1937, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:29:36', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1938, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:30:02', 'Pressed cancel button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1939, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:30:09', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1940, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:31:41', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1941, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:34:42', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1942, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:34:44', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1943, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:10', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1944, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:12', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1945, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:31', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1946, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:31', 'Query Executed: DELETE FROM other_declarations_name_smgroup WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1947, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:32', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => as\n    [3] => hey\n    [4] => \n    [5] => s\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1948, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:32', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => s\n    [3] => \n    [4] => sup\n    [5] => s\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1949, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:32', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => sa\n    [3] => yow\n    [4] => \n    [5] => a\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1950, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:32', 'Query Executed: DELETE FROM other_declarations_name_smic_employ WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1951, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:32', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => a\n    [3] => hey\n    [4] => yow\n    [5] => sup\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1952, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:33', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => 1\n    [3] => \n    [4] => \n    [5] => \n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1953, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:33', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => sa\n    [3] => \n    [4] => s\n    [5] => \n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1954, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:33', 'Query Executed: UPDATE applicant_other_declarations SET applicant_id = ?, crime_convict = ?, details = ? WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => ssss\n    [1] => 18\n    [2] => No\n    [3] => 1\n    [4] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1955, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:53', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1956, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:53', 'Query Executed: DELETE FROM other_declarations_name_smgroup WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1957, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:53', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => as\n    [3] => hey\n    [4] => \n    [5] => s\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1958, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:53', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => s\n    [3] => \n    [4] => sup\n    [5] => s\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1959, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:53', 'Query Executed: INSERT INTO other_declarations_name_smgroup(applicant_other_declaration_id, name,  relationship, position, company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => sa\n    [3] => yow\n    [4] => \n    [5] => a\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1960, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:53', 'Query Executed: DELETE FROM other_declarations_name_smic_employ WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1961, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:53', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => a\n    [3] => hey\n    [4] => yow\n    [5] => sup\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1962, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:54', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => 1\n    [3] => \n    [4] => \n    [5] => \n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1963, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:54', 'Query Executed: INSERT INTO other_declarations_name_smic_employ(applicant_other_declaration_id, name, relationship, position, department_company) VALUES(?,?,?,?,?)\r\nArray\n(\n    [0] => sssss\n    [1] => 1\n    [2] => sa\n    [3] => \n    [4] => s\n    [5] => \n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1964, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:54', 'Query Executed: UPDATE applicant_other_declarations SET applicant_id = ?, crime_convict = ?, details = ? WHERE applicant_other_declaration_id = ?\r\nArray\n(\n    [0] => ssss\n    [1] => 18\n    [2] => No\n    [3] => \n    [4] => 1\n)\n', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1965, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:36:58', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1966, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 14:37:22', 'Pressed submit button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(1967, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 15:16:48', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php'),
(1968, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 15:16:48', 'Query Executed: INSERT INTO applicant_languages_proficiency(applicant_id, language, speaking_proficiency, writing_proficiency) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 18\n    [2] => 10\n    [3] => 4\n    [4] => 4\n)\n', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php'),
(1969, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 15:16:55', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(1970, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 15:16:56', 'Query Executed: UPDATE applicant_languages_proficiency SET applicant_id = ?, language = ?, speaking_proficiency = ?, writing_proficiency = ? WHERE applicant_language_proficiency_id = ?\r\nArray\n(\n    [0] => sssss\n    [1] => 18\n    [2] => 16\n    [3] => 4\n    [4] => 4\n    [5] => 4\n)\n', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(1971, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 15:17:08', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(1972, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 15:17:08', 'Query Executed: UPDATE applicant_languages_proficiency SET applicant_id = ?, language = ?, speaking_proficiency = ?, writing_proficiency = ? WHERE applicant_language_proficiency_id = ?\r\nArray\n(\n    [0] => sssss\n    [1] => 18\n    [2] => 141\n    [3] => 4\n    [4] => 4\n    [5] => 4\n)\n', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(1973, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 15:17:32', 'Pressed delete button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/delete_applicant_languages_proficiency.php'),
(1974, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 15:17:33', 'Query Executed: DELETE FROM applicant_languages_proficiency WHERE applicant_language_proficiency_id = ?\r\nArray\n(\n    [0] => s\n    [1] => 4\n)\n', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/delete_applicant_languages_proficiency.php'),
(1975, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 15:17:38', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(1976, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 15:17:47', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php'),
(1977, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 15:17:47', 'Query Executed: INSERT INTO applicant_languages_proficiency(applicant_id, language, speaking_proficiency, writing_proficiency) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 18\n    [2] => 13\n    [3] => 2\n    [4] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php'),
(1978, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 15:18:34', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(1979, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 15:18:34', 'Query Executed: UPDATE applicant_languages_proficiency SET applicant_id = ?, language = ?, speaking_proficiency = ?, writing_proficiency = ? WHERE applicant_language_proficiency_id = ?\r\nArray\n(\n    [0] => sssss\n    [1] => 18\n    [2] => 14\n    [3] => 2\n    [4] => 5\n    [5] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(1980, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 15:19:50', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(1981, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 15:19:51', 'Query Executed: UPDATE applicant_languages_proficiency SET applicant_id = ?, language = ?, speaking_proficiency = ?, writing_proficiency = ? WHERE applicant_language_proficiency_id = ?\r\nArray\n(\n    [0] => sssss\n    [1] => 18\n    [2] => 142\n    [3] => 3\n    [4] => 5\n    [5] => 3\n)\n', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(1982, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 15:25:49', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(1983, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 15:25:49', 'Query Executed: UPDATE applicant_languages_proficiency SET applicant_id = ?, language = ?, speaking_proficiency = ?, writing_proficiency = ? WHERE applicant_language_proficiency_id = ?\r\nArray\n(\n    [0] => sssss\n    [1] => 18\n    [2] => 141\n    [3] => 3\n    [4] => 5\n    [5] => 3\n)\n', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(1984, '::1', 'markdanico.fernandez@gmail.com', '2018-11-05 15:32:18', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(1985, '::1', 'markdanico.fernandez@gmail.com', '2018-11-06 10:09:45', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1986, '::1', 'markdanico.fernandez@gmail.com', '2018-11-06 10:13:46', 'Logged out', '/smic_recruitment/end.php'),
(1987, '::1', 'recruiter1', '2018-11-06 10:13:50', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1988, '::1', 'recruiter1', '2018-11-06 10:49:12', 'Logged out', '/smic_recruitment/end.php'),
(1989, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 10:49:24', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(1990, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 10:50:37', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 4\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(1991, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 10:52:32', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 4\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(1992, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 10:54:33', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 4\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(1993, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 10:54:33', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 4\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(1994, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 10:55:07', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 4\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(1995, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 10:55:28', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 4\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(1996, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 10:55:40', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 4\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(1997, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 10:56:01', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 4\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(1998, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 10:56:02', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 4\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(1999, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 10:57:08', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2000, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 10:57:36', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2001, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 10:58:08', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2002, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 10:59:55', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2003, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 11:01:32', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2004, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 11:01:39', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2005, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 11:01:43', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2006, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 11:01:52', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2007, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 11:04:19', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2008, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 11:05:02', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2009, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 11:05:12', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2010, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 11:05:36', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2011, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 11:05:38', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2012, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 11:06:05', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2013, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 11:06:09', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2014, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 11:06:20', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2015, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 11:06:30', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2016, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 11:06:39', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2017, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 11:06:42', 'Logged out', '/smic_recruitment/end.php'),
(2018, '::1', 'recruiter1', '2018-11-06 11:10:21', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2019, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 11:10:56', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2020, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 11:12:10', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2021, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 11:12:40', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => \n)\n', '/smic_recruitment/carreer_page.php'),
(2022, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 11:29:53', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 7\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => Pending Application\n)\n', '/smic_recruitment/detailed_prf.php'),
(2023, '::1', 'recruiter1', '2018-11-06 14:16:20', 'Pressed cancel button', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(2024, '::1', 'recruiter1', '2018-11-06 14:22:30', 'Logged out', '/smic_recruitment/end.php'),
(2025, '::1', 'recruiter1', '2018-11-06 14:22:35', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2026, '::1', 'recruiter1', '2018-11-06 14:27:20', 'Pressed cancel button', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(2027, '::1', 'recruiter1', '2018-11-06 14:29:24', 'Pressed cancel button', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(2028, '::1', 'recruiter1', '2018-11-06 14:53:26', 'Pressed cancel button', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(2029, '::1', 'recruiter1', '2018-11-06 15:02:18', 'Logged out', '/smic_recruitment/end.php'),
(2030, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 15:02:25', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2031, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 15:02:44', 'Logged out', '/smic_recruitment/end.php'),
(2032, '::1', 'recruiter1', '2018-11-06 15:02:48', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2033, '::1', 'recruiter1', '2018-11-06 15:14:03', 'Query Executed: UPDATE iprf_staffrequest_applicants SET updates = ?  WHERE iprf_staffrequest_applicant_id = ?\r\nArray\n(\n    [0] => ss\n    [1] => Pending Application\n    [2] => 58\n)\n', '/smic_recruitment/applicant_portal_modules/send_email_to_applicant.php'),
(2034, '::1', 'recruiter1', '2018-11-06 15:32:35', 'Query Executed: UPDATE iprf_staffrequest_applicants SET updates = ?  WHERE iprf_staffrequest_applicant_id = ?\r\nArray\n(\n    [0] => ss\n    [1] => Invitation Sent - 2018-11-23\n    [2] => 58\n)\n', '/smic_recruitment/applicant_portal_modules/send_email_to_applicant.php'),
(2035, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 16:05:59', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2036, '::1', 'sumimurayoshi28@gmail.com', '2018-11-06 16:09:22', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 2\n    [2] => 19\n    [3] => 2018-11-06\n    [4] => Pending Application\n)\n', '/smic_recruitment/detailed_prf.php'),
(2037, '::1', 'recruiter1', '2018-11-07 08:34:38', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2038, '::1', 'recruiter1', '2018-11-07 08:35:55', 'Pressed cancel button', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(2039, '::1', 'recruiter1', '2018-11-07 09:02:03', 'Query Executed: UPDATE iprf_staffrequest_applicants SET updates = ?  WHERE iprf_staffrequest_applicant_id = ?\r\nArray\n(\n    [0] => ss\n    [1] => Invitation Sent - 2018-11-15\n    [2] => \n)\n', '/smic_recruitment/applicant_portal_modules/send_email_to_applicant.php'),
(2040, '::1', 'recruiter1', '2018-11-07 09:02:27', 'Query Executed: UPDATE iprf_staffrequest_applicants SET updates = ?  WHERE iprf_staffrequest_applicant_id = ?\r\nArray\n(\n    [0] => ss\n    [1] => Invitation Sent - 2018-11-16\n    [2] => 58\n)\n', '/smic_recruitment/applicant_portal_modules/send_email_to_applicant.php'),
(2041, '::1', 'recruiter1', '2018-11-07 09:14:24', 'Pressed cancel button', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(2042, '::1', 'recruiter1', '2018-11-07 10:06:30', 'Query Executed: UPDATE iprf_staffrequest_applicants SET updates = ?  WHERE iprf_staffrequest_applicant_id = ?\r\nArray\n(\n    [0] => ss\n    [1] => Invitation Sent - 2018-11-30 - 01:00\n    [2] => 58\n)\n', '/smic_recruitment/applicant_portal_modules/send_email_to_applicant.php'),
(2043, '::1', 'markdanico.fernandez@gmail.com', '2018-11-13 10:31:06', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2044, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 09:46:28', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2045, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:09:43', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2046, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:09:44', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality_id = ?, present_address_province_id = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality_id = ?, provincial_address_province_id = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 42431d51342b28d2bebb0673abe846200cdeb9b015134615_1778595462469230_9126298284826496227_n.jpg\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => 1\n    [12] => Filipino\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => O\n    [17] => Sta. Cruz, Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue\n    [20] => Pinagbuhatan\n    [21] => 2965\n    [22] => 64\n    [23] => 952-62-35\n    [24] => Block 41 Kenneth Road\n    [25] => Eusebio Avenue\n    [26] => Pinagbuhatan\n    [27] => \n    [28] => 57\n    [29] => 952-62-35\n    [30] => 123452345612\n    [31] => 123445764567\n    [32] => 234553411\n    [33] => 1234563456\n    [34] => \n    [35] => Roman Catholic\n    [36] => Amelia Fernandez\n    [37] => Mother\n    [38] => Same\n    [39] => 952-62-35\n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2047, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:09:56', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2048, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:09:56', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality_id = ?, present_address_province_id = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality_id = ?, provincial_address_province_id = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 42431d51342b28d2bebb0673abe846200cdeb9b015134615_1778595462469230_9126298284826496227_n.jpg\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => 1\n    [12] => Filipino\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => O\n    [17] => Sta. Cruz, Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue\n    [20] => Pinagbuhatan\n    [21] => 2965\n    [22] => 64\n    [23] => 952-62-35\n    [24] => Block 41 Kenneth Road\n    [25] => Eusebio Avenue\n    [26] => Pinagbuhatan\n    [27] => \n    [28] => 57\n    [29] => 952-62-35\n    [30] => 123452345612\n    [31] => 123445764567\n    [32] => 234553411\n    [33] => 1234563456\n    [34] => \n    [35] => Roman Catholic\n    [36] => Amelia Fernandez\n    [37] => Mother\n    [38] => Same\n    [39] => 952-62-35\n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2049, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:10:03', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php');
INSERT INTO `system_log` (`entry_id`, `ip_address`, `user`, `datetime`, `action`, `module`) VALUES
(2050, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:10:03', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality_id = ?, present_address_province_id = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality_id = ?, provincial_address_province_id = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 42431d51342b28d2bebb0673abe846200cdeb9b015134615_1778595462469230_9126298284826496227_n.jpg\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => 4\n    [12] => Filipino\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => O\n    [17] => Sta. Cruz, Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue\n    [20] => Pinagbuhatan\n    [21] => 2965\n    [22] => 64\n    [23] => 952-62-35\n    [24] => Block 41 Kenneth Road\n    [25] => Eusebio Avenue\n    [26] => Pinagbuhatan\n    [27] => \n    [28] => 57\n    [29] => 952-62-35\n    [30] => 123452345612\n    [31] => 123445764567\n    [32] => 234553411\n    [33] => 1234563456\n    [34] => \n    [35] => Roman Catholic\n    [36] => Amelia Fernandez\n    [37] => Mother\n    [38] => Same\n    [39] => 952-62-35\n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2051, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:10:17', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2052, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:11:43', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2053, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:11:43', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality_id = ?, present_address_province_id = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality_id = ?, provincial_address_province_id = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 42431d51342b28d2bebb0673abe846200cdeb9b015134615_1778595462469230_9126298284826496227_n.jpg\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => 4\n    [12] => 1\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => O\n    [17] => Sta. Cruz, Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue\n    [20] => Pinagbuhatan\n    [21] => 2965\n    [22] => 64\n    [23] => 952-62-35\n    [24] => Block 41 Kenneth Road\n    [25] => Eusebio Avenue\n    [26] => Pinagbuhatan\n    [27] => \n    [28] => 57\n    [29] => 952-62-35\n    [30] => 123452345612\n    [31] => 123445764567\n    [32] => 234553411\n    [33] => 1234563456\n    [34] => \n    [35] => Roman Catholic\n    [36] => Amelia Fernandez\n    [37] => Mother\n    [38] => Same\n    [39] => 952-62-35\n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2054, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:17:42', 'Logged out', '/smic_recruitment/end.php'),
(2055, '::1', 'root', '2018-11-15 10:17:44', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2056, '::1', 'root', '2018-11-15 10:21:43', 'Logged out', '/smic_recruitment/end.php'),
(2057, '::1', 'root', '2018-11-15 10:21:47', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2058, '::1', 'root', '2018-11-15 10:21:50', 'Logged out', '/smic_recruitment/end.php'),
(2059, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:21:57', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2060, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:22:45', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2061, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:22:46', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality_id = ?, present_address_province_id = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality_id = ?, provincial_address_province_id = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 42431d51342b28d2bebb0673abe846200cdeb9b015134615_1778595462469230_9126298284826496227_n.jpg\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => 4\n    [12] => 5\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => O\n    [17] => Sta. Cruz, Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue\n    [20] => Pinagbuhatan\n    [21] => 2965\n    [22] => 64\n    [23] => 952-62-35\n    [24] => Block 41 Kenneth Road\n    [25] => Eusebio Avenue\n    [26] => Pinagbuhatan\n    [27] => \n    [28] => 57\n    [29] => 952-62-35\n    [30] => 123452345612\n    [31] => 123445764567\n    [32] => 234553411\n    [33] => 1234563456\n    [34] => \n    [35] => Roman Catholic\n    [36] => Amelia Fernandez\n    [37] => Mother\n    [38] => Same\n    [39] => 952-62-35\n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2062, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:27:18', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2063, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:27:18', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality_id = ?, present_address_province_id = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality_id = ?, provincial_address_province_id = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 42431d51342b28d2bebb0673abe846200cdeb9b015134615_1778595462469230_9126298284826496227_n.jpg\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => \n    [6] => \n    [7] => \n    [8] => \n    [9] => \n    [10] => Male\n    [11] => 4\n    [12] => 5\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => 1\n    [17] => Sta. Cruz, Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue\n    [20] => Pinagbuhatan\n    [21] => 2965\n    [22] => 64\n    [23] => 952-62-35\n    [24] => Block 41 Kenneth Road\n    [25] => Eusebio Avenue\n    [26] => Pinagbuhatan\n    [27] => \n    [28] => 57\n    [29] => 952-62-35\n    [30] => 123452345612\n    [31] => 123445764567\n    [32] => 234553411\n    [33] => 1234563456\n    [34] => \n    [35] => Roman Catholic\n    [36] => Amelia Fernandez\n    [37] => Mother\n    [38] => Same\n    [39] => 952-62-35\n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2064, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:30:05', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(2065, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:30:07', 'Logged out', '/smic_recruitment/end.php'),
(2066, '::1', 'recruiter1', '2018-11-15 10:30:09', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2067, '::1', 'recruiter1', '2018-11-15 10:30:20', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(2068, '::1', 'recruiter1', '2018-11-15 10:31:54', 'Logged out', '/smic_recruitment/end.php'),
(2069, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:32:39', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2070, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:36:47', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(2071, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:36:59', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_family_members/edit_applicant_family_members.php'),
(2072, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:36:59', 'Query Executed: UPDATE applicant_family_members SET applicant_id = ?, name = ?, birthday = ?, age = ?, relationship = ?, is_dependent = ? WHERE applicant_family_member_id = ?\r\nArray\n(\n    [0] => sssssss\n    [1] => 18\n    [2] => Danilo Fernandez\n    [3] => 1967-10-05\n    [4] => 51\n    [5] => Father\n    [6] => Yes\n    [7] => 3\n)\n', '/smic_recruitment/modules/applicant/applicant_family_members/edit_applicant_family_members.php'),
(2073, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:38:48', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_family_members/edit_applicant_family_members.php'),
(2074, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:38:48', 'Query Executed: UPDATE applicant_family_members SET applicant_id = ?, name = ?, birthday = ?, age = ?, relationship = ?, is_dependent = ? WHERE applicant_family_member_id = ?\r\nArray\n(\n    [0] => sssssss\n    [1] => 18\n    [2] => Amelia Fernandez\n    [3] => 1954-10-30\n    [4] => 64\n    [5] => 3\n    [6] => No\n    [7] => 4\n)\n', '/smic_recruitment/modules/applicant/applicant_family_members/edit_applicant_family_members.php'),
(2075, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:38:52', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_family_members/edit_applicant_family_members.php'),
(2076, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:38:52', 'Query Executed: UPDATE applicant_family_members SET applicant_id = ?, name = ?, birthday = ?, age = ?, relationship = ?, is_dependent = ? WHERE applicant_family_member_id = ?\r\nArray\n(\n    [0] => sssssss\n    [1] => 18\n    [2] => Danilo Fernandez\n    [3] => 1967-10-05\n    [4] => 51\n    [5] => 1\n    [6] => Yes\n    [7] => 3\n)\n', '/smic_recruitment/modules/applicant/applicant_family_members/edit_applicant_family_members.php'),
(2077, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:38:56', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_family_members/edit_applicant_family_members.php'),
(2078, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:38:56', 'Query Executed: UPDATE applicant_family_members SET applicant_id = ?, name = ?, birthday = ?, age = ?, relationship = ?, is_dependent = ? WHERE applicant_family_member_id = ?\r\nArray\n(\n    [0] => sssssss\n    [1] => 18\n    [2] => Amelia Fernandez\n    [3] => 1954-10-30\n    [4] => 64\n    [5] => 2\n    [6] => No\n    [7] => 4\n)\n', '/smic_recruitment/modules/applicant/applicant_family_members/edit_applicant_family_members.php'),
(2079, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:39:08', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/listview_applicant_family_members.php'),
(2080, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:47:55', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php'),
(2081, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:47:55', 'Query Executed: INSERT INTO applicant_languages_proficiency(applicant_id, language, speaking_proficiency, writing_proficiency) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 18\n    [2] => 1\n    [3] => 1\n    [4] => 1\n)\n', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php'),
(2082, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:48:50', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(2083, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:48:50', 'Query Executed: UPDATE applicant_languages_proficiency SET applicant_id = ?, language = ?, speaking_proficiency = ?, writing_proficiency = ? WHERE applicant_language_proficiency_id = ?\r\nArray\n(\n    [0] => sssss\n    [1] => 18\n    [2] => 14\n    [3] => 1\n    [4] => 2\n    [5] => 3\n)\n', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(2084, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:49:00', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(2085, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:49:00', 'Query Executed: UPDATE applicant_languages_proficiency SET applicant_id = ?, language = ?, speaking_proficiency = ?, writing_proficiency = ? WHERE applicant_language_proficiency_id = ?\r\nArray\n(\n    [0] => sssss\n    [1] => 18\n    [2] => 17\n    [3] => 2\n    [4] => 3\n    [5] => 3\n)\n', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(2086, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:49:07', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(2087, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:49:08', 'Query Executed: UPDATE applicant_languages_proficiency SET applicant_id = ?, language = ?, speaking_proficiency = ?, writing_proficiency = ? WHERE applicant_language_proficiency_id = ?\r\nArray\n(\n    [0] => sssss\n    [1] => 18\n    [2] => 15\n    [3] => 1\n    [4] => 2\n    [5] => 3\n)\n', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(2088, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:50:36', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(2089, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:50:36', 'Query Executed: UPDATE applicant_languages_proficiency SET applicant_id = ?, language = ?, speaking_proficiency = ?, writing_proficiency = ? WHERE applicant_language_proficiency_id = ?\r\nArray\n(\n    [0] => sssss\n    [1] => 18\n    [2] => 14\n    [3] => 1\n    [4] => 2\n    [5] => 5\n)\n', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(2090, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:50:41', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(2091, '::1', 'markdanico.fernandez@gmail.com', '2018-11-15 10:50:42', 'Query Executed: UPDATE applicant_languages_proficiency SET applicant_id = ?, language = ?, speaking_proficiency = ?, writing_proficiency = ? WHERE applicant_language_proficiency_id = ?\r\nArray\n(\n    [0] => sssss\n    [1] => 18\n    [2] => 15\n    [3] => 1\n    [4] => 3\n    [5] => 3\n)\n', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php'),
(2092, '::1', 'markdanico.fernandez@gmail.com', '2018-11-20 07:56:19', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2093, '::1', 'markdanico.fernandez@gmail.com', '2018-11-20 07:56:44', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2094, '::1', 'markdanico.fernandez@gmail.com', '2018-11-20 07:56:50', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2095, '::1', 'markdanico.fernandez@gmail.com', '2018-11-20 07:58:55', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php'),
(2096, '::1', 'markdanico.fernandez@gmail.com', '2018-11-20 07:59:14', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_license/add_applicant_license.php'),
(2097, '::1', 'markdanico.fernandez@gmail.com', '2018-11-20 08:01:11', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php'),
(2098, '::1', 'markdanico.fernandez@gmail.com', '2018-11-20 08:01:24', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_reference/add_applicant_reference.php'),
(2099, '::1', 'markdanico.fernandez@gmail.com', '2018-11-20 08:20:01', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_school_attended/add_applicant_school_attended.php'),
(2100, '::1', 'markdanico.fernandez@gmail.com', '2018-11-20 08:20:09', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_school_attended/edit_applicant_school_attended.php'),
(2101, '::1', 'markdanico.fernandez@gmail.com', '2018-11-20 08:20:09', 'Query Executed: UPDATE applicant_school_attended SET applicant_id = ?, school_id = ?, educational_attainment = ?, course = ?, awards = ?, address = ?, date_from = ?, date_to = ? WHERE applicant_school_attended_id = ?\r\nArray\n(\n    [0] => sssssssss\n    [1] => 18\n    [2] => 2\n    [3] => High School\n    [4] => 18\n    [5] => \n    [6] => a\n    [7] => 2004-10-30\n    [8] => 2008-10-30\n    [9] => 4\n)\n', '/smic_recruitment/modules/applicant/applicant_school_attended/edit_applicant_school_attended.php'),
(2102, '::1', 'markdanico.fernandez@gmail.com', '2018-11-20 09:01:01', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_skills/add_applicant_skills.php'),
(2103, '::1', 'markdanico.fernandez@gmail.com', '2018-11-20 09:01:08', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_skills/edit_applicant_skills.php'),
(2104, '::1', 'markdanico.fernandez@gmail.com', '2018-11-20 09:01:08', 'Query Executed: UPDATE applicant_skills SET applicant_id = ?, skill = ?, proficiency = ?, years_of_experience = ? WHERE applicant_skill_id = ?\r\nArray\n(\n    [0] => sssss\n    [1] => 18\n    [2] => yow yow hey\n    [3] => 3\n    [4] => 3\n    [5] => 2\n)\n', '/smic_recruitment/modules/applicant/applicant_skills/edit_applicant_skills.php'),
(2105, '::1', 'markdanico.fernandez@gmail.com', '2018-11-20 09:04:52', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_skills/add_applicant_skills.php'),
(2106, '::1', 'markdanico.fernandez@gmail.com', '2018-11-20 09:04:53', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_skills/listview_applicant_skills.php'),
(2107, '::1', 'markdanico.fernandez@gmail.com', '2018-11-20 09:05:03', 'Pressed cancel button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(2108, '::1', 'markdanico.fernandez@gmail.com', '2018-11-20 09:26:50', 'Query Executed: INSERT INTO iprf_staffrequest_applicants(iprf_staffrequest_id, applicant_id, date_applied, updates) VALUES(?,?,?,?)\r\nArray\n(\n    [0] => ssss\n    [1] => 7\n    [2] => 18\n    [3] => 2018-11-20\n    [4] => Pending Application\n)\n', '/smic_recruitment/detailed_prf.php'),
(2109, '::1', 'markdanico.fernandez@gmail.com', '2018-11-20 10:08:09', 'Logged out', '/smic_recruitment/end.php'),
(2110, '::1', 'recruiter1', '2018-11-20 10:08:12', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2111, '::1', 'recruiter1', '2018-11-20 10:11:25', 'Pressed cancel button', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(2112, '::1', 'recruiter1', '2018-11-20 10:11:30', 'Pressed cancel button', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(2113, '::1', 'recruiter1', '2018-11-20 10:11:42', 'Pressed cancel button', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(2114, '::1', 'recruiter1', '2018-11-20 10:13:29', 'Query Executed: UPDATE iprf_staffrequest_applicants SET updates = ?  WHERE iprf_staffrequest_applicant_id = ?\r\nArray\n(\n    [0] => ss\n    [1] => Invitation Sent - 2018-11-21 - 13:00\n    [2] => 60\n)\n', '/smic_recruitment/applicant_portal_modules/send_email_to_applicant.php'),
(2115, '::1', 'recruiter1', '2018-11-20 10:13:47', 'Query Executed: UPDATE iprf_staffrequest_applicants SET updates = ?  WHERE iprf_staffrequest_applicant_id = ?\r\nArray\n(\n    [0] => ss\n    [1] => Invitation Sent - 2018-11-22 - 13:00\n    [2] => 60\n)\n', '/smic_recruitment/applicant_portal_modules/send_email_to_applicant.php'),
(2116, '::1', 'recruiter1', '2018-11-20 14:51:02', 'Pressed cancel button', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(2117, '::1', 'recruiter1', '2018-11-21 13:33:01', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2118, '::1', 'markdanico.fernandez@gmail.com', '2018-11-21 14:13:51', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2119, '::1', 'markdanico.fernandez@gmail.com', '2018-11-21 14:17:39', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2120, '::1', 'markdanico.fernandez@gmail.com', '2018-11-21 14:17:39', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality_id = ?, present_address_province_id = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality_id = ?, provincial_address_province_id = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 42431d51342b28d2bebb0673abe846200cdeb9b015134615_1778595462469230_9126298284826496227_n.jpg\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => Nico\n    [6] => \n    [7] => 0\n    [8] => \n    [9] => \n    [10] => 0\n    [11] => 4\n    [12] => 5\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => 1\n    [17] => Sta. Cruz, Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue\n    [20] => Pinagbuhatan\n    [21] => 2\n    [22] => 7\n    [23] => 952-62-35\n    [24] => Block 41 Kenneth Road\n    [25] => Eusebio Avenue\n    [26] => Pinagbuhatan\n    [27] => 2\n    [28] => 7\n    [29] => 952-62-35\n    [30] => 123452345612\n    [31] => 123445764567\n    [32] => 234553411\n    [33] => 1234563456\n    [34] => \n    [35] => Taoist\n    [36] => Amelia Fernandez\n    [37] => Mother\n    [38] => Same\n    [39] => 952-62-35\n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2121, '::1', 'markdanico.fernandez@gmail.com', '2018-11-21 14:17:52', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2122, '::1', 'markdanico.fernandez@gmail.com', '2018-11-21 14:17:52', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality_id = ?, present_address_province_id = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality_id = ?, provincial_address_province_id = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 42431d51342b28d2bebb0673abe846200cdeb9b015134615_1778595462469230_9126298284826496227_n.jpg\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => Nico\n    [6] => \n    [7] => 0\n    [8] => \n    [9] => \n    [10] => 0\n    [11] => 4\n    [12] => 5\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => 1\n    [17] => Sta. Cruz, Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue\n    [20] => Pinagbuhatan\n    [21] => 2\n    [22] => 7\n    [23] => 952-62-35\n    [24] => Block 41 Kenneth Road\n    [25] => Eusebio Avenue\n    [26] => Pinagbuhatan\n    [27] => 2\n    [28] => 7\n    [29] => 952-62-35\n    [30] => 123452345612\n    [31] => 123445764567\n    [32] => 234553411\n    [33] => 1234563456\n    [34] => \n    [35] => Confucianism\n    [36] => Amelia Fernandez\n    [37] => Mother\n    [38] => Same\n    [39] => 952-62-35\n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2123, '::1', 'recruiter1', '2018-11-22 07:52:14', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2124, '::1', 'recruiter1', '2018-11-22 08:37:42', 'Logged out', '/smic_recruitment/end.php'),
(2125, '::1', 'markdanico.fernandez@gmail.com', '2018-11-22 08:37:48', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2126, '::1', 'markdanico.fernandez@gmail.com', '2018-11-22 08:38:02', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2127, '::1', 'markdanico.fernandez@gmail.com', '2018-11-22 08:38:02', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality_id = ?, present_address_province_id = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality_id = ?, provincial_address_province_id = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 42431d51342b28d2bebb0673abe846200cdeb9b015134615_1778595462469230_9126298284826496227_n.jpg\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => Nico\n    [6] => \n    [7] => 0\n    [8] => \n    [9] => \n    [10] => 0\n    [11] => 4\n    [12] => 5\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => 1\n    [17] => Sta. Cruz, Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue\n    [20] => Pinagbuhatan\n    [21] => 2\n    [22] => 7\n    [23] => 952-62-35\n    [24] => Block 41 Kenneth Road\n    [25] => Eusebio Avenue\n    [26] => Pinagbuhatan\n    [27] => 2\n    [28] => 7\n    [29] => 952-62-35\n    [30] => 123452345612\n    [31] => 123445764567\n    [32] => 234553411\n    [33] => 1234563456\n    [34] => \n    [35] => \n    [36] => Amelia Fernandez\n    [37] => 2\n    [38] => Same\n    [39] => 952-62-35\n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2128, '::1', 'markdanico.fernandez@gmail.com', '2018-11-22 08:38:16', 'Logged out', '/smic_recruitment/end.php'),
(2129, '::1', 'recruiter1', '2018-11-22 08:38:20', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2130, '::1', 'markdanico.fernandez@gmail.com', '2018-11-22 09:12:59', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2131, '::1', 'markdanico.fernandez@gmail.com', '2018-11-22 09:15:30', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_school_attended/add_applicant_school_attended.php'),
(2132, '::1', 'markdanico.fernandez@gmail.com', '2018-11-22 09:15:36', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant_school_attended/edit_applicant_school_attended.php'),
(2133, '::1', 'markdanico.fernandez@gmail.com', '2018-11-22 09:15:37', 'Query Executed: UPDATE applicant_school_attended SET applicant_id = ?, school_id = ?, educational_attainment = ?, course = ?, awards = ?, address = ?, date_from = ?, date_to = ? WHERE applicant_school_attended_id = ?\r\nArray\n(\n    [0] => sssssssss\n    [1] => 18\n    [2] => 1\n    [3] => 4\n    [4] => 11\n    [5] => \n    [6] => aw\n    [7] => 2012-06-01\n    [8] => 2016-05-31\n    [9] => 3\n)\n', '/smic_recruitment/modules/applicant/applicant_school_attended/edit_applicant_school_attended.php'),
(2134, '::1', 'markdanico.fernandez@gmail.com', '2018-11-22 11:24:50', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_skills/edit_applicant_skills.php'),
(2135, '::1', 'recruiter1', '2018-11-28 10:00:24', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2136, '::1', 'recruiter1', '2018-11-28 10:09:35', 'Query Executed: UPDATE iprf_staffrequest_applicants SET updates = ?  WHERE iprf_staffrequest_applicant_id = ?\r\nArray\n(\n    [0] => ss\n    [1] => Exported to iHRIS - November 28, 2018 - 10:09 AM\n    [2] => 60\n)\n', '/smic_recruitment/applicant_portal_modules/export_to_ihris.php'),
(2137, '::1', 'recruiter1', '2018-11-28 10:10:29', 'Query Executed: UPDATE iprf_staffrequest_applicants SET updates = ?  WHERE iprf_staffrequest_applicant_id = ?\r\nArray\n(\n    [0] => ss\n    [1] => Exported to iHRIS - November 28, 2018 - 10:10 AM\n    [2] => 60\n)\n', '/smic_recruitment/applicant_portal_modules/export_to_ihris.php'),
(2138, '::1', 'recruiter1', '2018-11-28 10:14:11', 'Query Executed: UPDATE iprf_staffrequest_applicants SET updates = ?  WHERE iprf_staffrequest_applicant_id = ?\r\nArray\n(\n    [0] => ss\n    [1] => Exported to iHRIS - November 28, 2018 - 10:14 AM\n    [2] => 60\n)\n', '/smic_recruitment/applicant_portal_modules/export_to_ihris.php'),
(2139, '::1', 'recruiter1', '2018-11-28 10:14:26', 'Logged out', '/smic_recruitment/end.php'),
(2140, '::1', 'markdanico.fernandez@gmail.com', '2018-11-28 10:14:37', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2141, '::1', 'markdanico.fernandez@gmail.com', '2018-11-28 10:14:50', 'Pressed submit button', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2142, '::1', 'markdanico.fernandez@gmail.com', '2018-11-28 10:14:51', 'Query Executed: UPDATE applicant SET image = ?, last_name = ?, first_name = ?, middle_name = ?, nickname = ?, current_application_status = ?, company_id = ?, source = ?, email_address = ?, gender = ?, civil_status = ?, citizenship = ?, date_of_birth = ?, height = ?, weight = ?, blood_type = ?, birth_place = ?, present_address_line_1 = ?, present_address_line_2 = ?, present_address_barangay = ?, present_address_city_municipality_id = ?, present_address_province_id = ?, provincial_address_contact_number = ?, provincial_address_line_1 = ?, provincial_address_line_2 = ?, provincial_address_barangay = ?, provincial_address_city_municipality_id = ?, provincial_address_province_id = ?, contact_number = ?, sss_number = ?, tin = ?, philhealth_id_number = ?, hdmf_number = ?, umid_number = ?, religion = ?, contact_name = ?, contact_relationship = ?, contact_address = ?, contact_contact_number = ?, applicant_number = ?, employee_id = ?, personnel_requisition_id = ?, application_date = ?, application_status = ? WHERE applicant_id = ?\r\nArray\n(\n    [0] => sssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 42431d51342b28d2bebb0673abe846200cdeb9b015134615_1778595462469230_9126298284826496227_n.jpg\n    [2] => Fernandez\n    [3] => Mark\n    [4] => Danico\n    [5] => Nico\n    [6] => \n    [7] => 0\n    [8] => \n    [9] => \n    [10] => 0\n    [11] => 4\n    [12] => 5\n    [13] => 2000-09-27\n    [14] => 175\n    [15] => 62\n    [16] => 1\n    [17] => Sta. Cruz, Manila\n    [18] => Block 41 Kenneth Road\n    [19] => Eusebio Avenue\n    [20] => Pinagbuhatan\n    [21] => 2\n    [22] => 7\n    [23] => 952-62-35\n    [24] => Block 41 Kenneth Road\n    [25] => Eusebio Avenue\n    [26] => Pinagbuhatan\n    [27] => 2\n    [28] => 7\n    [29] => 952-62-35\n    [30] => 123452345612\n    [31] => 123445764567\n    [32] => 234553411\n    [33] => 1234563456\n    [34] => \n    [35] => Daoist\n    [36] => Amelia Fernandez\n    [37] => 2\n    [38] => Same\n    [39] => 952-62-35\n    [40] => APL-1809-00018\n    [41] => 0\n    [42] => 0\n    [43] => 0000-00-00\n    [44] => \n    [45] => 18\n)\n', '/smic_recruitment/modules/applicant/applicant/edit_applicant.php'),
(2143, '::1', 'markdanico.fernandez@gmail.com', '2018-11-28 10:14:57', 'Pressed cancel button', '/smic_recruitment/modules/applicant_other_declarations/edit_applicant_other_declarations.php'),
(2144, '::1', 'recruiter1', '2018-11-28 17:08:12', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2145, '::1', 'recruiter1', '2018-11-28 17:08:17', 'Logged out', '/smic_recruitment/end.php'),
(2146, '::1', 'markdanico.fernandez@gmail.com', '2018-11-28 17:08:27', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2147, '::1', 'markdanico.fernandez@gmail.com', '2018-11-29 13:24:36', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2148, '::1', 'markdanico.fernandez@gmail.com', '2018-11-29 14:22:36', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant_family_members/add_applicant_family_members.php'),
(2149, '::1', 'root', '2018-12-04 14:41:33', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2150, '::1', 'root', '2018-12-04 14:42:26', 'Query Executed: DELETE FROM user_role_links WHERE role_id = ? AND link_id IN (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss\n    [1] => 3\n    [2] => 37\n    [3] => 38\n    [4] => 39\n    [5] => 40\n    [6] => 81\n    [7] => 82\n    [8] => 83\n    [9] => 84\n    [10] => 85\n    [11] => 86\n    [12] => 87\n    [13] => 88\n    [14] => 89\n    [15] => 90\n    [16] => 91\n    [17] => 92\n    [18] => 93\n    [19] => 94\n    [20] => 95\n    [21] => 96\n    [22] => 97\n    [23] => 98\n    [24] => 99\n    [25] => 100\n    [26] => 101\n    [27] => 102\n    [28] => 103\n    [29] => 104\n    [30] => 105\n    [31] => 106\n    [32] => 107\n    [33] => 108\n    [34] => 109\n    [35] => 110\n    [36] => 111\n    [37] => 112\n    [38] => 113\n    [39] => 114\n    [40] => 115\n    [41] => 116\n    [42] => 117\n    [43] => 118\n    [44] => 119\n    [45] => 120\n    [46] => 121\n    [47] => 122\n    [48] => 123\n    [49] => 124\n    [50] => 125\n    [51] => 126\n    [52] => 127\n    [53] => 128\n    [54] => 129\n    [55] => 130\n    [56] => 131\n    [57] => 132\n    [58] => 133\n    [59] => 134\n    [60] => 135\n    [61] => 136\n    [62] => 137\n    [63] => 138\n    [64] => 139\n    [65] => 140\n    [66] => 141\n    [67] => 142\n    [68] => 143\n    [69] => 144\n    [70] => 145\n    [71] => 146\n    [72] => 147\n    [73] => 148\n    [74] => 149\n    [75] => 150\n    [76] => 151\n    [77] => 152\n    [78] => 153\n    [79] => 154\n    [80] => 155\n    [81] => 156\n    [82] => 157\n    [83] => 158\n    [84] => 159\n    [85] => 160\n    [86] => 161\n    [87] => 162\n    [88] => 163\n    [89] => 164\n    [90] => 165\n    [91] => 166\n    [92] => 167\n    [93] => 168\n    [94] => 169\n    [95] => 170\n    [96] => 171\n    [97] => 172\n    [98] => 173\n    [99] => 174\n    [100] => 175\n    [101] => 176\n    [102] => 177\n    [103] => 178\n    [104] => 179\n    [105] => 180\n    [106] => 181\n    [107] => 182\n    [108] => 183\n    [109] => 184\n    [110] => 185\n    [111] => 186\n    [112] => 187\n    [113] => 188\n    [114] => 189\n    [115] => 190\n    [116] => 191\n    [117] => 192\n    [118] => 193\n    [119] => 194\n    [120] => 195\n    [121] => 196\n    [122] => 203\n    [123] => 204\n    [124] => 205\n    [125] => 206\n    [126] => 207\n    [127] => 208\n    [128] => 209\n    [129] => 210\n    [130] => 211\n    [131] => 212\n    [132] => 213\n    [133] => 214\n    [134] => 215\n    [135] => 216\n    [136] => 217\n    [137] => 218\n    [138] => 228\n    [139] => 229\n    [140] => 230\n    [141] => 231\n    [142] => 232\n    [143] => 233\n    [144] => 234\n    [145] => 235\n    [146] => 236\n    [147] => 237\n    [148] => 238\n    [149] => 239\n    [150] => 240\n    [151] => 249\n    [152] => 250\n    [153] => 251\n    [154] => 252\n    [155] => 253\n    [156] => 254\n    [157] => 255\n    [158] => 256\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(2151, '::1', 'root', '2018-12-04 14:42:26', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 203\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(2152, '::1', 'root', '2018-12-04 14:42:26', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 253\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(2153, '::1', 'root', '2018-12-04 14:42:26', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 228\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(2154, '::1', 'root', '2018-12-04 14:42:27', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 255\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(2155, '::1', 'root', '2018-12-04 14:42:27', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 256\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(2156, '::1', 'root', '2018-12-04 14:42:27', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 231\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(2157, '::1', 'root', '2018-12-04 14:42:27', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 204\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(2158, '::1', 'root', '2018-12-04 14:42:27', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 254\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(2159, '::1', 'root', '2018-12-04 14:42:27', 'Query Executed: INSERT INTO user_role_links(role_id, link_id) VALUES(?, ?)\r\nArray\n(\n    [0] => ss\n    [1] => 3\n    [2] => 229\n)\n', '/smic_recruitment/sysadmin/role_permissions.php'),
(2160, '::1', 'root', '2018-12-04 14:42:34', 'Pressed cancel button', '/smic_recruitment/sysadmin/role_permissions.php'),
(2161, '::1', 'root', '2018-12-04 14:42:39', 'Query Executed: DELETE FROM user_passport WHERE username IN (?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssss\n    [1] => applicant1\n    [2] => markdanico.fernandez1@gmail.com\n    [3] => markdanico.fernandez@gmail.com\n    [4] => markdanico.fernandezz@gmail.com\n    [5] => reyeskimberly018@gmail.com\n    [6] => smic.email.tester@gmail.com\n    [7] => sumimurayoshi28@gmail.com\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(2162, '::1', 'root', '2018-12-04 14:42:39', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => applicant1\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(2163, '::1', 'root', '2018-12-04 14:42:39', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez1@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(2164, '::1', 'root', '2018-12-04 14:42:39', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandez@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(2165, '::1', 'root', '2018-12-04 14:42:39', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => markdanico.fernandezz@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(2166, '::1', 'root', '2018-12-04 14:42:40', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => reyeskimberly018@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(2167, '::1', 'root', '2018-12-04 14:42:40', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => smic.email.tester@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(2168, '::1', 'root', '2018-12-04 14:42:40', 'Query Executed: \r\nArray\n(\n    [0] => ss\n    [1] => sumimurayoshi28@gmail.com\n    [2] => 3\n)\n', '/smic_recruitment/sysadmin/role_permissions_cascade.php'),
(2169, '::1', 'root', '2018-12-04 14:43:44', 'Logged out', '/smic_recruitment/end.php'),
(2170, '::1', 'markdanico.fernandez@gmail.com', '2018-12-04 14:43:57', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2171, '::1', 'markdanico.fernandez@gmail.com', '2018-12-04 14:44:11', 'Pressed cancel button', '/smic_recruitment/modules/applicant_trainings/add_applicant_trainings.php'),
(2172, '::1', 'markdanico.fernandez@gmail.com', '2018-12-04 14:46:11', 'Pressed submit button', '/smic_recruitment/modules/applicant_trainings/add_applicant_trainings.php'),
(2173, '::1', 'markdanico.fernandez@gmail.com', '2018-12-04 14:46:12', 'Query Executed: INSERT INTO applicant_trainings(applicant_id, training_title, training_provider, training_date, venue, no_of_hours, remarks) VALUES(?,?,?,?,?,?,?)\r\nArray\n(\n    [0] => sssssss\n    [1] => 18\n    [2] => Title Sample\n    [3] => Title sample\n    [4] => Title sample\n    [5] => Title\n    [6] => 28\n    [7] => Sample\n)\n', '/smic_recruitment/modules/applicant_trainings/add_applicant_trainings.php'),
(2174, '::1', 'markdanico.fernandez@gmail.com', '2018-12-04 15:01:16', 'Pressed cancel button', '/smic_recruitment/modules/applicant_trainings/edit_applicant_trainings.php'),
(2175, '::1', 'markdanico.fernandez@gmail.com', '2018-12-04 15:01:39', 'Pressed cancel button', '/smic_recruitment/modules/applicant_trainings/listview_applicant_trainings.php'),
(2176, '::1', 'markdanico.fernandez@gmail.com', '2018-12-04 15:04:34', 'Pressed cancel button', '/smic_recruitment/modules/applicant_trainings/listview_applicant_trainings.php'),
(2177, '::1', 'markdanico.fernandez@gmail.com', '2018-12-04 15:07:26', 'Logged out', '/smic_recruitment/end.php'),
(2178, '::1', 'recruiter1', '2018-12-04 15:07:33', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2179, '::1', 'recruiter1', '2018-12-04 15:10:00', 'Pressed cancel button', '/smic_recruitment/modules/applicant/applicant/detailview_applicant.php'),
(2180, '::1', 'recruiter1', '2018-12-04 15:10:03', 'Pressed cancel button', '/smic_recruitment/modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php'),
(2181, '::1', 'recruiter1', '2018-12-04 16:04:51', 'Logged out', '/smic_recruitment/end.php'),
(2182, '::1', 'markdanico.fernandez@gmail.com', '2018-12-04 16:05:06', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2183, '::1', 'markdanico.fernandez@gmail.com', '2018-12-04 16:05:23', 'Pressed submit button', '/smic_recruitment/modules/applicant_trainings/edit_applicant_trainings.php'),
(2184, '::1', 'markdanico.fernandez@gmail.com', '2018-12-04 16:05:24', 'Query Executed: UPDATE applicant_trainings SET applicant_id = ?, training_title = ?, training_provider = ?, training_date = ?, venue = ?, no_of_hours = ?, remarks = ? WHERE applicant_training_id = ?\r\nArray\n(\n    [0] => ssssssss\n    [1] => 18\n    [2] => Title Sample\n    [3] => Title sample\n    [4] => 2018-12-04\n    [5] => Title\n    [6] => 28\n    [7] => Sample\n    [8] => 1\n)\n', '/smic_recruitment/modules/applicant_trainings/edit_applicant_trainings.php'),
(2185, '::1', 'markdanico.fernandez@gmail.com', '2018-12-04 16:05:26', 'Logged out', '/smic_recruitment/end.php'),
(2186, '::1', 'recruiter1', '2018-12-04 16:05:35', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2187, '::1', 'recruiter1', '2018-12-04 16:06:04', 'Query Executed: UPDATE iprf_staffrequest_applicants SET updates = ?  WHERE iprf_staffrequest_applicant_id = ?\r\nArray\n(\n    [0] => ss\n    [1] => Exported to iHRIS - December 04, 2018 - 04:06 PM\n    [2] => 60\n)\n', '/smic_recruitment/applicant_portal_modules/export_to_ihris.php'),
(2188, '::1', 'markdanico.fernandez@gmail.com', '2018-12-07 07:02:21', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2189, '::1', 'markdanico.fernandez@gmail.com', '2018-12-07 07:02:34', 'Logged out', '/smic_recruitment/end.php'),
(2190, '::1', 'recruiter1', '2018-12-11 15:07:21', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2191, '::1', 'markdanico.fernandez@gmail.com', '2018-12-11 15:11:41', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2192, '::1', 'recruiter1', '2018-12-18 09:25:20', 'Logged in', '/smic_recruitment/applicant_portal.php'),
(2193, '::1', 'recruiter1', '2018-12-18 09:30:18', 'Query Executed: UPDATE iprf_staffrequest_applicants SET updates = ?  WHERE iprf_staffrequest_applicant_id = ?\r\nArray\n(\n    [0] => ss\n    [1] => Exported to iHRIS - December 18, 2018 - 09:30 AM\n    [2] => 60\n)\n', '/smic_recruitment/applicant_portal_modules/export_to_ihris.php');

-- --------------------------------------------------------

--
-- Table structure for table `system_settings`
--

CREATE TABLE `system_settings` (
  `setting` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_settings`
--

INSERT INTO `system_settings` (`setting`, `value`) VALUES
('Emailer Address', 'smic.email.tester@gmail.com'),
('Emailer Password', '56781234'),
('Max Attachment Height', '0'),
('Max Attachment Size (MB)', '0'),
('Max Attachment Width', '0'),
('Security Level', 'HIGH');

-- --------------------------------------------------------

--
-- Table structure for table `system_skins`
--

CREATE TABLE `system_skins` (
  `skin_id` int(11) NOT NULL,
  `skin_name` varchar(255) NOT NULL,
  `header` varchar(255) NOT NULL,
  `footer` varchar(255) NOT NULL,
  `master_css` varchar(255) NOT NULL,
  `colors_css` varchar(255) NOT NULL,
  `fonts_css` varchar(255) NOT NULL,
  `override_css` varchar(255) NOT NULL,
  `icon_set` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_skins`
--

INSERT INTO `system_skins` (`skin_id`, `skin_name`, `header`, `footer`, `master_css`, `colors_css`, `fonts_css`, `override_css`, `icon_set`) VALUES
(1, 'Cobalt Default', 'skins/default_header.php', 'skins/default_footer.php', 'cobalt_master.css', 'cobalt_colors.css', 'cobalt_fonts.css', 'cobalt_override.css', 'cobalt'),
(2, 'Cobalt Minimal', 'skins/minimal_header.php', 'skins/minimal_footer.php', 'cobalt_minimal.css', 'cobalt_minimal.css', 'cobalt_minimal.css', 'cobalt_minimal.css', 'cobalt'),
(3, 'After Sunset', 'skins/default_header.php', 'skins/default_footer.php', 'after_sunset_master.css', 'after_sunset_colors.css', 'after_sunset_fonts.css', 'after_sunset_override.css', 'cobalt'),
(4, 'Hello There', 'skins/default_header.php', 'skins/default_footer.php', 'hello_there_master.css', 'hello_there_colors.css', 'hello_there_fonts.css', 'hello_there_override.css', 'cobalt'),
(5, 'Gold Titanium', 'skins/default_header.php', 'skins/default_footer.php', 'gold_titanium_master.css', 'gold_titanium_colors.css', 'gold_titanium_fonts.css', 'gold_titanium_override.css', 'cobalt'),
(6, 'Summer Rain', 'skins/default_header.php', 'skins/default_footer.php', 'summer_rain_master.css', 'summer_rain_colors.css', 'summer_rain_fonts.css', 'summer_rain_override.css', 'cobalt'),
(7, 'Salmon Impression', 'skins/default_header.php', 'skins/default_footer.php', 'salmon_impression_master.css', 'salmon_impression_colors.css', 'salmon_impression_fonts.css', 'salmon_impression_override.css', 'cobalt'),
(8, 'Royal Amethyst', 'skins/default_header.php', 'skins/default_footer.php', 'royal_amethyst_master.css', 'royal_amethyst_colors.css', 'royal_amethyst_fonts.css', 'royal_amethyst_override.css', 'cobalt'),
(9, 'Red Decadence', 'skins/default_header.php', 'skins/default_footer.php', 'red_decadence_master.css', 'red_decadence_colors.css', 'red_decadence_fonts.css', 'red_decadence_override.css', 'cobalt'),
(10, 'Modern Eden', 'skins/default_header.php', 'skins/default_footer.php', 'modern_eden_master.css', 'modern_eden_colors.css', 'modern_eden_fonts.css', 'modern_eden_override.css', 'cobalt'),
(11, 'Warm Teal', 'skins/default_header.php', 'skins/default_footer.php', 'warm_teal_master.css', 'warm_teal_colors.css', 'warm_teal_fonts.css', 'warm_teal_override.css', 'cobalt'),
(12, 'Purple Rain', 'skins/default_header.php', 'skins/default_footer.php', 'purple_rain_master.css', 'purple_rain_colors.css', 'purple_rain_fonts.css', 'purple_rain_override.css', 'cobalt');

-- --------------------------------------------------------

--
-- Table structure for table `temp_user`
--

CREATE TABLE `temp_user` (
  `temp_user_id` int(11) NOT NULL,
  `personal_email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `iteration` int(11) NOT NULL,
  `method` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `training`
--

CREATE TABLE `training` (
  `training_id` int(11) NOT NULL,
  `training_name` varchar(255) DEFAULT NULL,
  `training_date` date DEFAULT NULL,
  `training_details` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `upload_material`
--

CREATE TABLE `upload_material` (
  `upload_material_id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `date_uploaded` date NOT NULL,
  `active` char(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `upload_material`
--

INSERT INTO `upload_material` (`upload_material_id`, `file_name`, `date_uploaded`, `active`) VALUES
(1, '6230d27a73dec8f7bce57802b0f4568dd48f1642download.jpg', '2018-10-04', 'Yes'),
(2, '063a4eb9beedeee783972e38b6c2d51ee9ca0383a1dX0bG_460s.jpg', '2018-10-04', 'Yes'),
(3, '7e29a455ab80775a1d3132ad9f366aea975a4e6316512865-Rank-Icons-Stock-Vector-icon.jpg', '2018-10-05', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(255) NOT NULL,
  `personal_email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `iteration` int(11) NOT NULL,
  `method` varchar(255) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `skin_id` int(11) NOT NULL,
  `user_level` tinyint(4) NOT NULL,
  `is_verified` varchar(3) NOT NULL,
  `token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `personal_email`, `password`, `salt`, `iteration`, `method`, `applicant_id`, `person_id`, `role_id`, `skin_id`, `user_level`, `is_verified`, `token`) VALUES
('applicant1', '', '$2y$12$pbtkoqAce50nobiS2VtjxeFv1Az6kgN7bDZn5.I9pLkXBOgsSSJ.W', 'pbtkoqAce50nobiS2Vtjxg', 12, 'bcrypt', 0, 1, 3, 1, 2, '', ''),
('markdanico.fernandez1@gmail.com', 'markdanico.fernandez1@gmail.com', '$2y$12$KGhtLlDjBZI29ctD7a7YxOCzrxB1O695o8NAOy2K04azOsoGEZKxu', 'KGhtLlDjBZI29ctD7a7YxQ', 12, 'bcrypt', 6, 0, 3, 1, 5, 'Yes', ''),
('markdanico.fernandez@gmail.com', '', '$2y$12$TnKqrGWVUQ9cx0V3GvO1qu9duj2uOyJtiXGhufKMMPqnfdDkuUaFy', 'TnKqrGWVUQ9cx0V3GvO1qw', 12, 'bcrypt', 18, 0, 3, 1, 5, '', ''),
('markdanico.fernandezz@gmail.com', 'markdanico.fernandezz@gmail.com', '$2y$12$Wg0bSubz.JOnyJIJyobP1.1LRMi3q6pThlHdIN/rCCevCvd8Hrdse', 'Wg0bSubz+JOnyJIJyobP1A', 12, 'bcrypt', 10, 0, 3, 1, 5, 'No', '447bb366354d19ccffc17918530988de54e5768c'),
('recruiter1', '', '$2y$12$hJllllSg2hmdK99qgYi8ROrduX.FdzurONbKL8e7cPmxIjiAsIb62', 'hJllllSg2hmdK99qgYi8RQ', 12, 'bcrypt', 0, 1, 4, 1, 5, '', ''),
('reyeskimberly018@gmail.com', 'reyeskimberly018@gmail.com', '$2y$12$EDUj0NECTy/l9I8i0sQ/YuHi4YePI9tNnjx6PXVg5N/V7.fW7U86W', 'EDUj0NECTy/l9I8i0sQ/Yw', 12, 'bcrypt', 8, 0, 3, 1, 5, 'Yes', 'c61c546aa42cd40ac571320f71ddc383cd28d1a2'),
('root', '', '$2y$12$u4q6Mwkf5Iw2KtHYRdfKZuXzXibBEbk08oGl1R0z2MQKXN3HxP/6e', 'u4q6Mwkf5Iw2KtHYRdfKZw', 12, 'bcrypt', 0, 1, 1, 1, 5, 'Yes', ''),
('smic.email.tester@gmail.com', 'smic.email.tester@gmail.com', '$2y$12$hrY.uunnTN/vkNAUFEQBvOmESFTSTY92sAodSlPpU2z8IrSSDYELK', 'hrY+uunnTN/vkNAUFEQBvQ', 12, 'bcrypt', 7, 0, 3, 1, 5, 'Yes', ''),
('sumimurayoshi28@gmail.com', '', '$2y$12$Bi74a3XCkFGNOfQy2yZyIeNvcK.pjYS42GmLtRbAVdMtrafVMz3tW', 'Bi74a3XCkFGNOfQy2yZyIg', 12, 'bcrypt', 19, 0, 3, 1, 5, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_links`
--

CREATE TABLE `user_links` (
  `link_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `target` varchar(255) NOT NULL,
  `descriptive_title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `passport_group_id` int(11) NOT NULL,
  `show_in_tasklist` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_links`
--

INSERT INTO `user_links` (`link_id`, `name`, `target`, `descriptive_title`, `description`, `passport_group_id`, `show_in_tasklist`, `status`, `icon`, `priority`) VALUES
(1, 'Module Control', 'sysadmin/module_control.php', 'Module Control', 'Enable or disable system modules', 2, 'Yes', 'On', 'modulecontrol.png', 0),
(2, 'Set User Passports', 'sysadmin/set_user_passports.php', 'Set User Passports', 'Change the passport settings of system users', 2, 'Yes', 'On', 'passport.png', 0),
(3, 'Security Monitor', 'sysadmin/security_monitor.php', 'Security Monitor', 'Examine the system log', 2, 'Yes', 'On', 'security3.png', 0),
(4, 'Add person', 'sysadmin/add_person.php', 'Add Person', '', 2, 'No', 'On', 'form.png', 0),
(5, 'Edit person', 'sysadmin/edit_person.php', 'Edit Person', '', 2, 'No', 'On', 'form.png', 0),
(6, 'View person', 'sysadmin/listview_person.php', 'Person', '', 2, 'Yes', 'On', 'persons.png', 0),
(7, 'Delete person', 'sysadmin/delete_person.php', 'Delete Person', '', 2, 'No', 'On', 'form.png', 0),
(8, 'Add user', 'sysadmin/add_user.php', 'Add User', '', 2, 'No', 'On', 'form.png', 0),
(9, 'Edit user', 'sysadmin/edit_user.php', 'Edit User', '', 2, 'No', 'On', 'form.png', 0),
(10, 'View user', 'sysadmin/listview_user.php', 'User', '', 2, 'Yes', 'On', 'card.png', 0),
(11, 'Delete user', 'sysadmin/delete_user.php', 'Delete User', '', 2, 'No', 'On', 'form.png', 0),
(12, 'Add user role', 'sysadmin/add_user_role.php', 'Add User Role', '', 2, 'No', 'On', 'form.png', 0),
(13, 'Edit user role', 'sysadmin/edit_user_role.php', 'Edit User Role', '', 2, 'No', 'On', 'form.png', 0),
(14, 'View user role', 'sysadmin/listview_user_role.php', 'User Roles', '', 2, 'Yes', 'On', 'roles.png', 0),
(15, 'Delete user role', 'sysadmin/delete_user_role.php', 'Delete User Role', '', 2, 'No', 'On', 'form.png', 0),
(16, 'Add system settings', 'sysadmin/add_system_settings.php', 'Add System Settings', '', 2, 'No', 'On', 'form.png', 0),
(17, 'Edit system settings', 'sysadmin/edit_system_settings.php', 'Edit System Settings', '', 2, 'No', 'On', 'form.png', 0),
(18, 'View system settings', 'sysadmin/listview_system_settings.php', 'System Settings', '', 2, 'Yes', 'On', 'system_settings.png', 0),
(19, 'Delete system settings', 'sysadmin/delete_system_settings.php', 'Delete System Settings', '', 2, 'No', 'On', 'form.png', 0),
(20, 'Add user links', 'sysadmin/add_user_links.php', 'Add User Links', '', 2, 'No', 'On', 'form.png', 0),
(21, 'Edit user links', 'sysadmin/edit_user_links.php', 'Edit User Links', '', 2, 'No', 'On', 'form.png', 0),
(22, 'View user links', 'sysadmin/listview_user_links.php', 'User Links', '', 2, 'Yes', 'On', 'links.png', 0),
(23, 'Delete user links', 'sysadmin/delete_user_links.php', 'Delete User Links', '', 2, 'No', 'On', 'form.png', 0),
(24, 'Add user passport groups', 'sysadmin/add_user_passport_groups.php', 'Add User Passport Groups', '', 2, 'No', 'On', 'form.png', 0),
(25, 'Edit user passport groups', 'sysadmin/edit_user_passport_groups.php', 'Edit User Passport Groups', '', 2, 'No', 'On', 'form.png', 0),
(26, 'View user passport groups', 'sysadmin/listview_user_passport_groups.php', 'User Passport Groups', '', 2, 'Yes', 'On', 'passportgroup.png', 0),
(27, 'Delete user passport groups', 'sysadmin/delete_user_passport_groups.php', 'Delete User Passport Groups', '', 2, 'No', 'On', 'form.png', 0),
(28, 'Add system skins', 'sysadmin/add_system_skins.php', 'Add System Skins', '', 2, 'No', 'On', 'form.png', 0),
(29, 'Edit system skins', 'sysadmin/edit_system_skins.php', 'Edit System Skins', '', 2, 'No', 'On', 'form.png', 0),
(30, 'View system skins', 'sysadmin/listview_system_skins.php', 'System Skins', '', 2, 'Yes', 'On', 'system_skins.png', 0),
(31, 'Delete system skins', 'sysadmin/delete_system_skins.php', 'Delete System Skins', '', 2, 'No', 'On', 'form.png', 0),
(32, 'Reset Password', 'sysadmin/reset_password.php', 'Reset Password', '', 2, 'Yes', 'On', 'lock_big.png', 0),
(33, 'Add cobalt sst', 'sst/add_cobalt_sst.php', 'Add Cobalt SST', '', 2, 'No', 'On', 'form3.png', 0),
(34, 'Edit cobalt sst', 'sst/edit_cobalt_sst.php', 'Edit Cobalt SST', '', 2, 'No', 'On', 'form3.png', 0),
(35, 'View cobalt sst', 'sst/listview_cobalt_sst.php', 'Cobalt SST', '', 2, 'Yes', 'On', 'form3.png', 0),
(36, 'Delete cobalt sst', 'sst/delete_cobalt_sst.php', 'Delete Cobalt SST', '', 2, 'No', 'On', 'form3.png', 0),
(37, 'Add action notice', 'modules/action_notice/add_action_notice.php', 'Add Action Notice', '', 1, 'No', 'On', 'form3.png', 0),
(38, 'Edit action notice', 'modules/action_notice/edit_action_notice.php', 'Edit Action Notice', '', 1, 'No', 'On', 'form3.png', 0),
(39, 'View action notice', 'modules/action_notice/listview_action_notice.php', 'Action Notice', '', 1, 'Yes', 'On', 'form3.png', 0),
(40, 'Delete action notice', 'modules/action_notice/delete_action_notice.php', 'Delete Action Notice', '', 1, 'No', 'On', 'form3.png', 0),
(41, 'Add applicant', 'modules/applicant/applicant/add_applicant.php', 'Add Applicant', '', 3, 'No', 'On', 'form3.png', 0),
(42, 'Edit applicant', 'modules/applicant/applicant/edit_applicant.php', 'Edit Applicant', '', 3, 'No', 'On', 'form3.png', 0),
(43, 'View applicant', 'modules/applicant/applicant/listview_applicant.php', 'Applicant', '', 3, 'Yes', 'On', 'form3.png', 0),
(44, 'Delete applicant', 'modules/applicant/applicant/delete_applicant.php', 'Delete Applicant', '', 3, 'No', 'On', 'form3.png', 0),
(45, 'Add applicant exam', 'modules/applicant/applicant_exam/add_applicant_exam.php', 'Add Applicant Exam', '', 3, 'No', 'On', 'form3.png', 0),
(46, 'Edit applicant exam', 'modules/applicant/applicant_exam/edit_applicant_exam.php', 'Edit Applicant Exam', '', 3, 'No', 'On', 'form3.png', 0),
(47, 'View applicant exam', 'modules/applicant/applicant_exam/listview_applicant_exam.php', 'Applicant Exam', '', 3, 'Yes', 'On', 'form3.png', 0),
(48, 'Delete applicant exam', 'modules/applicant/applicant_exam/delete_applicant_exam.php', 'Delete Applicant Exam', '', 3, 'No', 'On', 'form3.png', 0),
(49, 'Add applicant family members', 'modules/applicant/applicant_family_members/add_applicant_family_members.php', 'Add Applicant Family Members', '', 3, 'No', 'On', 'form3.png', 0),
(50, 'Edit applicant family members', 'modules/applicant/applicant_family_members/edit_applicant_family_members.php', 'Edit Applicant Family Members', '', 3, 'No', 'On', 'form3.png', 0),
(51, 'View applicant family members', 'modules/applicant/applicant_family_members/listview_applicant_family_members.php', 'Family Members', '', 3, 'Yes', 'On', 'form3.png', 0),
(52, 'Delete applicant family members', 'modules/applicant/applicant_family_members/delete_applicant_family_members.php', 'Delete Applicant Family Members', '', 3, 'No', 'On', 'form3.png', 0),
(53, 'Add applicant interview', 'modules/applicant/applicant_interview/add_applicant_interview.php', 'Add Applicant Interview', '', 3, 'No', 'On', 'form3.png', 0),
(54, 'Edit applicant interview', 'modules/applicant/applicant_interview/edit_applicant_interview.php', 'Edit Applicant Interview', '', 3, 'No', 'On', 'form3.png', 0),
(55, 'View applicant interview', 'modules/applicant/applicant_interview/listview_applicant_interview.php', 'Applicant Interview', '', 3, 'Yes', 'On', 'form3.png', 0),
(56, 'Delete applicant interview', 'modules/applicant/applicant_interview/delete_applicant_interview.php', 'Delete Applicant Interview', '', 3, 'No', 'On', 'form3.png', 0),
(57, 'Add applicant languages proficiency', 'modules/applicant/applicant_languages_proficiency/add_applicant_languages_proficiency.php', 'Add Applicant Languages Proficiency', '', 3, 'No', 'On', 'form3.png', 0),
(58, 'Edit applicant languages proficiency', 'modules/applicant/applicant_languages_proficiency/edit_applicant_languages_proficiency.php', 'Edit Applicant Languages Proficiency', '', 3, 'No', 'On', 'form3.png', 0),
(59, 'View applicant languages proficiency', 'modules/applicant/applicant_languages_proficiency/listview_applicant_languages_proficiency.php', 'Languages Proficiency', '', 3, 'Yes', 'On', 'form3.png', 0),
(60, 'Delete applicant languages proficiency', 'modules/applicant/applicant_languages_proficiency/delete_applicant_languages_proficiency.php', 'Delete Applicant Languages Proficiency', '', 3, 'No', 'On', 'form3.png', 0),
(61, 'Add applicant license', 'modules/applicant/applicant_license/add_applicant_license.php', 'Add Applicant License', '', 3, 'No', 'On', 'form3.png', 0),
(62, 'Edit applicant license', 'modules/applicant/applicant_license/edit_applicant_license.php', 'Edit Applicant License', '', 3, 'No', 'On', 'form3.png', 0),
(63, 'View applicant license', 'modules/applicant/applicant_license/listview_applicant_license.php', 'Licenses', '', 3, 'Yes', 'On', 'form3.png', 0),
(64, 'Delete applicant license', 'modules/applicant/applicant_license/delete_applicant_license.php', 'Delete Applicant License', '', 3, 'No', 'On', 'form3.png', 0),
(65, 'Add applicant previous employers', 'modules/applicant/applicant_previous_employers/add_applicant_previous_employers.php', 'Add Applicant Previous Employers', '', 3, 'No', 'On', 'form3.png', 0),
(66, 'Edit applicant previous employers', 'modules/applicant/applicant_previous_employers/edit_applicant_previous_employers.php', 'Edit Applicant Previous Employers', '', 3, 'No', 'On', 'form3.png', 0),
(67, 'View applicant previous employers', 'modules/applicant/applicant_previous_employers/listview_applicant_previous_employers.php', 'Previous Employers', '', 3, 'Yes', 'On', 'form3.png', 0),
(68, 'Delete applicant previous employers', 'modules/applicant/applicant_previous_employers/delete_applicant_previous_employers.php', 'Delete Applicant Previous Employers', '', 3, 'No', 'On', 'form3.png', 0),
(69, 'Add applicant reference', 'modules/applicant/applicant_reference/add_applicant_reference.php', 'Add Applicant Reference', '', 3, 'No', 'On', 'form3.png', 0),
(70, 'Edit applicant reference', 'modules/applicant/applicant_reference/edit_applicant_reference.php', 'Edit Applicant Reference', '', 3, 'No', 'On', 'form3.png', 0),
(71, 'View applicant reference', 'modules/applicant/applicant_reference/listview_applicant_reference.php', 'References', '', 3, 'Yes', 'On', 'form3.png', 0),
(72, 'Delete applicant reference', 'modules/applicant/applicant_reference/delete_applicant_reference.php', 'Delete Applicant Reference', '', 3, 'No', 'On', 'form3.png', 0),
(73, 'Add applicant school attended', 'modules/applicant/applicant_school_attended/add_applicant_school_attended.php', 'Add Applicant School Attended', '', 3, 'No', 'On', 'form3.png', 0),
(74, 'Edit applicant school attended', 'modules/applicant/applicant_school_attended/edit_applicant_school_attended.php', 'Edit Applicant School Attended', '', 3, 'No', 'On', 'form3.png', 0),
(75, 'View applicant school attended', 'modules/applicant/applicant_school_attended/listview_applicant_school_attended.php', 'Schools Attended', '', 3, 'Yes', 'On', 'form3.png', 0),
(76, 'Delete applicant school attended', 'modules/applicant/applicant_school_attended/delete_applicant_school_attended.php', 'Delete Applicant School Attended', '', 3, 'No', 'On', 'form3.png', 0),
(77, 'Add applicant skills', 'modules/applicant/applicant_skills/add_applicant_skills.php', 'Add Applicant Skills', '', 3, 'No', 'On', 'form3.png', 0),
(78, 'Edit applicant skills', 'modules/applicant/applicant_skills/edit_applicant_skills.php', 'Edit Applicant Skills', '', 3, 'No', 'On', 'form3.png', 0),
(79, 'View applicant skills', 'modules/applicant/applicant_skills/listview_applicant_skills.php', 'Skills', '', 3, 'Yes', 'On', 'form3.png', 0),
(80, 'Delete applicant skills', 'modules/applicant/applicant_skills/delete_applicant_skills.php', 'Delete Applicant Skills', '', 3, 'No', 'On', 'form3.png', 0),
(81, 'Add branch', 'modules/organization/branch/add_branch.php', 'Add Branch', '', 1, 'No', 'On', 'form3.png', 0),
(82, 'Edit branch', 'modules/organization/branch/edit_branch.php', 'Edit Branch', '', 1, 'No', 'On', 'form3.png', 0),
(83, 'View branch', 'modules/organization/branch/listview_branch.php', 'Branch', '', 1, 'Yes', 'On', 'form3.png', 0),
(84, 'Delete branch', 'modules/organization/branch/delete_branch.php', 'Delete Branch', '', 1, 'No', 'On', 'form3.png', 0),
(85, 'Add branch type', 'modules/organization/branch_type/add_branch_type.php', 'Add Branch Type', '', 1, 'No', 'On', 'form3.png', 0),
(86, 'Edit branch type', 'modules/organization/branch_type/edit_branch_type.php', 'Edit Branch Type', '', 1, 'No', 'On', 'form3.png', 0),
(87, 'View branch type', 'modules/organization/branch_type/listview_branch_type.php', 'Branch Type', '', 1, 'Yes', 'On', 'form3.png', 0),
(88, 'Delete branch type', 'modules/organization/branch_type/delete_branch_type.php', 'Delete Branch Type', '', 1, 'No', 'On', 'form3.png', 0),
(89, 'Add building', 'modules/organization/building/add_building.php', 'Add Building', '', 1, 'No', 'On', 'form3.png', 0),
(90, 'Edit building', 'modules/organization/building/edit_building.php', 'Edit Building', '', 1, 'No', 'On', 'form3.png', 0),
(91, 'View building', 'modules/organization/building/listview_building.php', 'Building', '', 1, 'Yes', 'On', 'form3.png', 0),
(92, 'Delete building', 'modules/organization/building/delete_building.php', 'Delete Building', '', 1, 'No', 'On', 'form3.png', 0),
(93, 'Add calendar event', 'modules/calendar_event/add_calendar_event.php', 'Add Calendar Event', '', 1, 'No', 'On', 'form3.png', 0),
(94, 'Edit calendar event', 'modules/calendar_event/edit_calendar_event.php', 'Edit Calendar Event', '', 1, 'No', 'On', 'form3.png', 0),
(95, 'View calendar event', 'modules/calendar_event/listview_calendar_event.php', 'Calendar Event', '', 1, 'Yes', 'On', 'form3.png', 0),
(96, 'Delete calendar event', 'modules/calendar_event/delete_calendar_event.php', 'Delete Calendar Event', '', 1, 'No', 'On', 'form3.png', 0),
(97, 'Add calendar event type', 'modules/calendar_event_type/add_calendar_event_type.php', 'Add Calendar Event Type', '', 1, 'No', 'On', 'form3.png', 0),
(98, 'Edit calendar event type', 'modules/calendar_event_type/edit_calendar_event_type.php', 'Edit Calendar Event Type', '', 1, 'No', 'On', 'form3.png', 0),
(99, 'View calendar event type', 'modules/calendar_event_type/listview_calendar_event_type.php', 'Calendar Event Type', '', 1, 'Yes', 'On', 'form3.png', 0),
(100, 'Delete calendar event type', 'modules/calendar_event_type/delete_calendar_event_type.php', 'Delete Calendar Event Type', '', 1, 'No', 'On', 'form3.png', 0),
(101, 'Add clearance', 'modules/clearance/clearance/add_clearance.php', 'Add Clearance', '', 1, 'No', 'On', 'form3.png', 0),
(102, 'Edit clearance', 'modules/clearance/clearance/edit_clearance.php', 'Edit Clearance', '', 1, 'No', 'On', 'form3.png', 0),
(103, 'View clearance', 'modules/clearance/clearance/listview_clearance.php', 'Clearance', '', 1, 'Yes', 'On', 'form3.png', 0),
(104, 'Delete clearance', 'modules/clearance/clearance/delete_clearance.php', 'Delete Clearance', '', 1, 'No', 'On', 'form3.png', 0),
(105, 'Add clearance approval', 'modules/clearance/clearance_approval/add_clearance_approval.php', 'Add Clearance Approval', '', 1, 'No', 'On', 'form3.png', 0),
(106, 'Edit clearance approval', 'modules/clearance/clearance_approval/edit_clearance_approval.php', 'Edit Clearance Approval', '', 1, 'No', 'On', 'form3.png', 0),
(107, 'View clearance approval', 'modules/clearance/clearance_approval/listview_clearance_approval.php', 'Clearance Approval', '', 1, 'Yes', 'On', 'form3.png', 0),
(108, 'Delete clearance approval', 'modules/clearance/clearance_approval/delete_clearance_approval.php', 'Delete Clearance Approval', '', 1, 'No', 'On', 'form3.png', 0),
(109, 'Add clearance approver', 'modules/clearance/clearance_approver/add_clearance_approver.php', 'Add Clearance Approver', '', 1, 'No', 'On', 'form3.png', 0),
(110, 'Edit clearance approver', 'modules/clearance/clearance_approver/edit_clearance_approver.php', 'Edit Clearance Approver', '', 1, 'No', 'On', 'form3.png', 0),
(111, 'View clearance approver', 'modules/clearance/clearance_approver/listview_clearance_approver.php', 'Clearance Approver', '', 1, 'Yes', 'On', 'form3.png', 0),
(112, 'Delete clearance approver', 'modules/clearance/clearance_approver/delete_clearance_approver.php', 'Delete Clearance Approver', '', 1, 'No', 'On', 'form3.png', 0),
(113, 'Add company', 'modules/organization/company/add_company.php', 'Add Company', '', 1, 'No', 'On', 'form3.png', 0),
(114, 'Edit company', 'modules/organization/company/edit_company.php', 'Edit Company', '', 1, 'No', 'On', 'form3.png', 0),
(115, 'View company', 'modules/organization/company/listview_company.php', 'Company', '', 1, 'Yes', 'On', 'form3.png', 0),
(116, 'Delete company', 'modules/organization/company/delete_company.php', 'Delete Company', '', 1, 'No', 'On', 'form3.png', 0),
(117, 'Add company policy', 'modules/organization/company_policy/add_company_policy.php', 'Add Company Policy', '', 1, 'No', 'On', 'form3.png', 0),
(118, 'Edit company policy', 'modules/organization/company_policy/edit_company_policy.php', 'Edit Company Policy', '', 1, 'No', 'On', 'form3.png', 0),
(119, 'View company policy', 'modules/organization/company_policy/listview_company_policy.php', 'Company Policy', '', 1, 'Yes', 'On', 'form3.png', 0),
(120, 'Delete company policy', 'modules/organization/company_policy/delete_company_policy.php', 'Delete Company Policy', '', 1, 'No', 'On', 'form3.png', 0),
(121, 'Add company violation', 'modules/organization/company_violation/add_company_violation.php', 'Add Company Violation', '', 1, 'No', 'On', 'form3.png', 0),
(122, 'Edit company violation', 'modules/organization/company_violation/edit_company_violation.php', 'Edit Company Violation', '', 1, 'No', 'On', 'form3.png', 0),
(123, 'View company violation', 'modules/organization/company_violation/listview_company_violation.php', 'Company Violation', '', 1, 'Yes', 'On', 'form3.png', 0),
(124, 'Delete company violation', 'modules/organization/company_violation/delete_company_violation.php', 'Delete Company Violation', '', 1, 'No', 'On', 'form3.png', 0),
(125, 'Add department', 'modules/organization/department/add_department.php', 'Add Department', '', 1, 'No', 'On', 'form3.png', 0),
(126, 'Edit department', 'modules/organization/department/edit_department.php', 'Edit Department', '', 1, 'No', 'On', 'form3.png', 0),
(127, 'View department', 'modules/organization/department/listview_department.php', 'Department', '', 1, 'Yes', 'On', 'form3.png', 0),
(128, 'Delete department', 'modules/organization/department/delete_department.php', 'Delete Department', '', 1, 'No', 'On', 'form3.png', 0),
(129, 'Add employee', 'modules/employee/employee/add_employee.php', 'Add Employee', '', 1, 'No', 'On', 'form3.png', 0),
(130, 'Edit employee', 'modules/employee/employee/edit_employee.php', 'Edit Employee', '', 1, 'No', 'On', 'form3.png', 0),
(131, 'View employee', 'modules/employee/employee/listview_employee.php', 'Employee', '', 1, 'Yes', 'On', 'form3.png', 0),
(132, 'Delete employee', 'modules/employee/employee/delete_employee.php', 'Delete Employee', '', 1, 'No', 'On', 'form3.png', 0),
(133, 'Add employee education', 'modules/employee/employee_education/add_employee_education.php', 'Add Employee Education', '', 1, 'No', 'On', 'form3.png', 0),
(134, 'Edit employee education', 'modules/employee/employee_education/edit_employee_education.php', 'Edit Employee Education', '', 1, 'No', 'On', 'form3.png', 0),
(135, 'View employee education', 'modules/employee/employee_education/listview_employee_education.php', 'Employee Education', '', 1, 'Yes', 'On', 'form3.png', 0),
(136, 'Delete employee education', 'modules/employee/employee_education/delete_employee_education.php', 'Delete Employee Education', '', 1, 'No', 'On', 'form3.png', 0),
(137, 'Add employee evaluation', 'modules/employee/employee_evaluation/add_employee_evaluation.php', 'Add Employee Evaluation', '', 1, 'No', 'On', 'form3.png', 0),
(138, 'Edit employee evaluation', 'modules/employee/employee_evaluation/edit_employee_evaluation.php', 'Edit Employee Evaluation', '', 1, 'No', 'On', 'form3.png', 0),
(139, 'View employee evaluation', 'modules/employee/employee_evaluation/listview_employee_evaluation.php', 'Employee Evaluation', '', 1, 'Yes', 'On', 'form3.png', 0),
(140, 'Delete employee evaluation', 'modules/employee/employee_evaluation/delete_employee_evaluation.php', 'Delete Employee Evaluation', '', 1, 'No', 'On', 'form3.png', 0),
(141, 'Add employee evaluation answer', 'modules/employee/employee_evaluation_answer/add_employee_evaluation_answer.php', 'Add Employee Evaluation Answer', '', 1, 'No', 'On', 'form3.png', 0),
(142, 'Edit employee evaluation answer', 'modules/employee/employee_evaluation_answer/edit_employee_evaluation_answer.php', 'Edit Employee Evaluation Answer', '', 1, 'No', 'On', 'form3.png', 0),
(143, 'View employee evaluation answer', 'modules/employee/employee_evaluation_answer/listview_employee_evaluation_answer.php', 'Employee Evaluation Answer', '', 1, 'Yes', 'On', 'form3.png', 0),
(144, 'Delete employee evaluation answer', 'modules/employee/employee_evaluation_answer/delete_employee_evaluation_answer.php', 'Delete Employee Evaluation Answer', '', 1, 'No', 'On', 'form3.png', 0),
(145, 'Add employee evaluation template', 'modules/employee/employee_evaluation_template/add_employee_evaluation_template.php', 'Add Employee Evaluation Template', '', 1, 'No', 'On', 'form3.png', 0),
(146, 'Edit employee evaluation template', 'modules/employee/employee_evaluation_template/edit_employee_evaluation_template.php', 'Edit Employee Evaluation Template', '', 1, 'No', 'On', 'form3.png', 0),
(147, 'View employee evaluation template', 'modules/employee/employee_evaluation_template/listview_employee_evaluation_template.php', 'Employee Evaluation Template', '', 1, 'Yes', 'On', 'form3.png', 0),
(148, 'Delete employee evaluation template', 'modules/employee/employee_evaluation_template/delete_employee_evaluation_template.php', 'Delete Employee Evaluation Template', '', 1, 'No', 'On', 'form3.png', 0),
(149, 'Add employee evaluation template criteria', 'modules/employee/employee_evaluation_template_criteria/add_employee_evaluation_template_criteria.php', 'Add Employee Evaluation Template Criteria', '', 1, 'No', 'On', 'form3.png', 0),
(150, 'Edit employee evaluation template criteria', 'modules/employee/employee_evaluation_template_criteria/edit_employee_evaluation_template_criteria.php', 'Edit Employee Evaluation Template Criteria', '', 1, 'No', 'On', 'form3.png', 0),
(151, 'View employee evaluation template criteria', 'modules/employee/employee_evaluation_template_criteria/listview_employee_evaluation_template_criteria.php', 'Employee Evaluation Template Criteria', '', 1, 'Yes', 'On', 'form3.png', 0),
(152, 'Delete employee evaluation template criteria', 'modules/employee/employee_evaluation_template_criteria/delete_employee_evaluation_template_criteria.php', 'Delete Employee Evaluation Template Criteria', '', 1, 'No', 'On', 'form3.png', 0),
(153, 'Add floor', 'modules/organization/floor/add_floor.php', 'Add Floor', '', 1, 'No', 'On', 'form3.png', 0),
(154, 'Edit floor', 'modules/organization/floor/edit_floor.php', 'Edit Floor', '', 1, 'No', 'On', 'form3.png', 0),
(155, 'View floor', 'modules/organization/floor/listview_floor.php', 'Floor', '', 1, 'Yes', 'On', 'form3.png', 0),
(156, 'Delete floor', 'modules/organization/floor/delete_floor.php', 'Delete Floor', '', 1, 'No', 'On', 'form3.png', 0),
(157, 'Add hr program', 'modules/hr_program/hr_program/add_hr_program.php', 'Add Hr Program', '', 1, 'No', 'On', 'form3.png', 0),
(158, 'Edit hr program', 'modules/hr_program/hr_program/edit_hr_program.php', 'Edit Hr Program', '', 1, 'No', 'On', 'form3.png', 0),
(159, 'View hr program', 'modules/hr_program/hr_program/listview_hr_program.php', 'Hr Program', '', 1, 'Yes', 'On', 'form3.png', 0),
(160, 'Delete hr program', 'modules/hr_program/hr_program/delete_hr_program.php', 'Delete Hr Program', '', 1, 'No', 'On', 'form3.png', 0),
(161, 'Add hr program attendees', 'modules/hr_program/hr_program_attendees/add_hr_program_attendees.php', 'Add Hr Program Attendees', '', 1, 'No', 'On', 'form3.png', 0),
(162, 'Edit hr program attendees', 'modules/hr_program/hr_program_attendees/edit_hr_program_attendees.php', 'Edit Hr Program Attendees', '', 1, 'No', 'On', 'form3.png', 0),
(163, 'View hr program attendees', 'modules/hr_program/hr_program_attendees/listview_hr_program_attendees.php', 'Hr Program Attendees', '', 1, 'Yes', 'On', 'form3.png', 0),
(164, 'Delete hr program attendees', 'modules/hr_program/hr_program_attendees/delete_hr_program_attendees.php', 'Delete Hr Program Attendees', '', 1, 'No', 'On', 'form3.png', 0),
(165, 'Add performance appraisal', 'modules/performance_appraisal/add_performance_appraisal.php', 'Add Performance Appraisal', '', 1, 'No', 'On', 'form3.png', 0),
(166, 'Edit performance appraisal', 'modules/performance_appraisal/edit_performance_appraisal.php', 'Edit Performance Appraisal', '', 1, 'No', 'On', 'form3.png', 0),
(167, 'View performance appraisal', 'modules/performance_appraisal/listview_performance_appraisal.php', 'Performance Appraisal', '', 1, 'Yes', 'On', 'form3.png', 0),
(168, 'Delete performance appraisal', 'modules/performance_appraisal/delete_performance_appraisal.php', 'Delete Performance Appraisal', '', 1, 'No', 'On', 'form3.png', 0),
(169, 'Add performance appraisal goal', 'modules/performance_appraisal_goal/add_performance_appraisal_goal.php', 'Add Performance Appraisal Goal', '', 1, 'No', 'On', 'form3.png', 0),
(170, 'Edit performance appraisal goal', 'modules/performance_appraisal_goal/edit_performance_appraisal_goal.php', 'Edit Performance Appraisal Goal', '', 1, 'No', 'On', 'form3.png', 0),
(171, 'View performance appraisal goal', 'modules/performance_appraisal_goal/listview_performance_appraisal_goal.php', 'Performance Appraisal Goal', '', 1, 'Yes', 'On', 'form3.png', 0),
(172, 'Delete performance appraisal goal', 'modules/performance_appraisal_goal/delete_performance_appraisal_goal.php', 'Delete Performance Appraisal Goal', '', 1, 'No', 'On', 'form3.png', 0),
(173, 'Add personnel requisition', 'modules/personnel_requisition/add_personnel_requisition.php', 'Add Personnel Requisition', '', 1, 'No', 'On', 'form3.png', 0),
(174, 'Edit personnel requisition', 'modules/personnel_requisition/edit_personnel_requisition.php', 'Edit Personnel Requisition', '', 1, 'No', 'On', 'form3.png', 0),
(175, 'View personnel requisition', 'modules/personnel_requisition/listview_personnel_requisition.php', 'Personnel Requisition', '', 1, 'Yes', 'On', 'form3.png', 0),
(176, 'Delete personnel requisition', 'modules/personnel_requisition/delete_personnel_requisition.php', 'Delete Personnel Requisition', '', 1, 'No', 'On', 'form3.png', 0),
(177, 'Add plantilla', 'modules/plantilla/add_plantilla.php', 'Add Plantilla', '', 1, 'No', 'On', 'form3.png', 0),
(178, 'Edit plantilla', 'modules/plantilla/edit_plantilla.php', 'Edit Plantilla', '', 1, 'No', 'On', 'form3.png', 0),
(179, 'View plantilla', 'modules/plantilla/listview_plantilla.php', 'Plantilla', '', 1, 'Yes', 'On', 'form3.png', 0),
(180, 'Delete plantilla', 'modules/plantilla/delete_plantilla.php', 'Delete Plantilla', '', 1, 'No', 'On', 'form3.png', 0),
(181, 'Add resignation', 'modules/resignation/add_resignation.php', 'Add Resignation', '', 1, 'No', 'On', 'form3.png', 0),
(182, 'Edit resignation', 'modules/resignation/edit_resignation.php', 'Edit Resignation', '', 1, 'No', 'On', 'form3.png', 0),
(183, 'View resignation', 'modules/resignation/listview_resignation.php', 'Resignation', '', 1, 'Yes', 'On', 'form3.png', 0),
(184, 'Delete resignation', 'modules/resignation/delete_resignation.php', 'Delete Resignation', '', 1, 'No', 'On', 'form3.png', 0),
(185, 'Add school', 'modules/organization/school/add_school.php', 'Add School', '', 1, 'No', 'On', 'form3.png', 0),
(186, 'Edit school', 'modules/organization/school/edit_school.php', 'Edit School', '', 1, 'No', 'On', 'form3.png', 0),
(187, 'View school', 'modules/organization/school/listview_school.php', 'School', '', 1, 'Yes', 'On', 'form3.png', 0),
(188, 'Delete school', 'modules/organization/school/delete_school.php', 'Delete School', '', 1, 'No', 'On', 'form3.png', 0),
(189, 'Add site', 'modules/organization/site/add_site.php', 'Add Site', '', 1, 'No', 'On', 'form3.png', 0),
(190, 'Edit site', 'modules/organization/site/edit_site.php', 'Edit Site', '', 1, 'No', 'On', 'form3.png', 0),
(191, 'View site', 'modules/organization/site/listview_site.php', 'Site', '', 1, 'Yes', 'On', 'form3.png', 0),
(192, 'Delete site', 'modules/organization/site/delete_site.php', 'Delete Site', '', 1, 'No', 'On', 'form3.png', 0),
(193, 'Add training', 'modules/training/add_training.php', 'Add Training', '', 1, 'No', 'On', 'form3.png', 0),
(194, 'Edit training', 'modules/training/edit_training.php', 'Edit Training', '', 1, 'No', 'On', 'form3.png', 0),
(195, 'View training', 'modules/training/listview_training.php', 'Training', '', 1, 'Yes', 'On', 'form3.png', 0),
(196, 'Delete training', 'modules/training/delete_training.php', 'Delete Training', '', 1, 'No', 'On', 'form3.png', 0),
(197, 'Applicant access', 'none', 'Applicant Access', '', 3, 'No', 'On', 'none', 0),
(202, 'Other declarations', 'modules/applicant_other_declarations/process_declaration.php', 'Other Declarations', 'a', 3, 'Yes', 'On', 'form3.png', -1),
(203, 'Add applicant other declarations', 'modules/applicant_other_declarations/add_applicant_other_declarations.php', 'Add Applicant Other Declarations', '', 1, 'No', 'On', 'form3.png', 0),
(204, 'Edit applicant other declarations', 'modules/applicant_other_declarations/edit_applicant_other_declarations.php', 'Edit Applicant Other Declarations', '', 1, 'No', 'On', 'form3.png', 0),
(205, 'View applicant other declarations', 'modules/applicant_other_declarations/listview_applicant_other_declarations.php', 'Applicant Other Declarations', '', 1, 'Yes', 'On', 'form3.png', 0),
(206, 'Delete applicant other declarations', 'modules/applicant_other_declarations/delete_applicant_other_declarations.php', 'Delete Applicant Other Declarations', '', 1, 'No', 'On', 'form3.png', 0),
(207, 'Add iprf staffrequest', 'modules/iprf_staffrequest/add_iprf_staffrequest.php', 'Add Iprf Staffrequest', '', 1, 'No', 'On', 'form3.png', 0),
(208, 'Edit iprf staffrequest', 'modules/iprf_staffrequest/edit_iprf_staffrequest.php', 'Edit Iprf Staffrequest', '', 1, 'No', 'On', 'form3.png', 0),
(209, 'View iprf staffrequest', 'modules/iprf_staffrequest/listview_iprf_staffrequest.php', 'Iprf Staffrequest', '', 1, 'Yes', 'On', 'form3.png', 0),
(210, 'Delete iprf staffrequest', 'modules/iprf_staffrequest/delete_iprf_staffrequest.php', 'Delete Iprf Staffrequest', '', 1, 'No', 'On', 'form3.png', 0),
(211, 'Add iprf staffrequest applicants', 'modules/iprf_staffrequest_applicants/add_iprf_staffrequest_applicants.php', 'Add Iprf Staffrequest Applicants', '', 1, 'No', 'On', 'form3.png', 0),
(212, 'Edit iprf staffrequest applicants', 'modules/iprf_staffrequest_applicants/edit_iprf_staffrequest_applicants.php', 'Edit Iprf Staffrequest Applicants', '', 1, 'No', 'On', 'form3.png', 0),
(213, 'View iprf staffrequest applicants', 'modules/iprf_staffrequest_applicants/listview_iprf_staffrequest_applicants.php', 'Iprf Staffrequest Applicants', '', 1, 'No', 'On', 'form3.png', 0),
(214, 'Delete iprf staffrequest applicants', 'modules/iprf_staffrequest_applicants/delete_iprf_staffrequest_applicants.php', 'Delete Iprf Staffrequest Applicants', '', 1, 'No', 'On', 'form3.png', 0),
(215, 'Add upload material', 'modules/upload_material/add_upload_material.php', 'Add Upload Material', '', 1, 'No', 'On', 'form3.png', 0),
(216, 'Edit upload material', 'modules/upload_material/edit_upload_material.php', 'Edit Upload Material', '', 1, 'No', 'On', 'form3.png', 0),
(217, 'View upload material', 'modules/upload_material/listview_upload_material.php', 'Upload Material', '', 1, 'Yes', 'On', 'form3.png', 0),
(218, 'Delete upload material', 'modules/upload_material/delete_upload_material.php', 'Delete Upload Material', '', 1, 'No', 'On', 'form3.png', 0),
(219, 'Applicant access', 'none', 'Applicant Access', 'none', 3, 'No', 'On', 'form3.png', 0),
(220, 'Add applicant attachments', 'modules/applicant_attachments/add_applicant_attachments.php', 'Add Applicant Attachments', '', 3, 'No', 'On', 'form3.png', 0),
(221, 'Edit applicant attachments', 'modules/applicant_attachments/edit_applicant_attachments.php', 'Edit Applicant Attachments', '', 3, 'No', 'On', 'form3.png', 0),
(222, 'View applicant attachments', 'modules/applicant_attachments/listview_applicant_attachments.php', 'Document Attachments', '', 3, 'Yes', 'On', 'form3.png', 99),
(223, 'Delete applicant attachments', 'modules/applicant_attachments/delete_applicant_attachments.php', 'Delete Applicant Attachments', '', 3, 'No', 'On', 'form3.png', 0),
(224, 'Add applicant preferred positions', 'modules/applicant_preferred_positions/add_applicant_preferred_positions.php', 'Add Applicant Preferred Positions', '', 3, 'No', 'On', 'form3.png', 0),
(225, 'Edit applicant preferred positions', 'modules/applicant_preferred_positions/edit_applicant_preferred_positions.php', 'Edit Applicant Preferred Positions', '', 3, 'No', 'On', 'form3.png', 0),
(226, 'View applicant preferred positions', 'modules/applicant_preferred_positions/listview_applicant_preferred_positions.php', 'Preferred Positions', '', 3, 'Yes', 'On', 'form3.png', 0),
(227, 'Delete applicant preferred positions', 'modules/applicant_preferred_positions/delete_applicant_preferred_positions.php', 'Delete Applicant Preferred Positions', '', 3, 'No', 'On', 'form3.png', 0),
(228, 'Add position', 'modules/position/add_position.php', 'Add Position', '', 1, 'No', 'On', 'form3.png', 0),
(229, 'Edit position', 'modules/position/edit_position.php', 'Edit Position', '', 1, 'No', 'On', 'form3.png', 0),
(230, 'View position', 'modules/position/listview_position.php', 'Position', '', 1, 'Yes', 'On', 'form3.png', 0),
(231, 'Delete position', 'modules/position/delete_position.php', 'Delete Position', '', 1, 'No', 'On', 'form3.png', 0),
(232, 'Recruiter access', 'none', 'Recruiter Access', 'a', 1, 'No', 'On', 'form3.png', 0),
(233, 'Add city', 'modules/set_up_table/city/add_city.php', 'Add City', '', 1, 'No', 'On', 'form3.png', 0),
(234, 'Edit city', 'modules/set_up_table/city/edit_city.php', 'Edit City', '', 1, 'No', 'On', 'form3.png', 0),
(235, 'View city', 'modules/set_up_table/city/listview_city.php', 'City', '', 1, 'Yes', 'On', 'form3.png', 0),
(236, 'Delete city', 'modules/set_up_table/city/delete_city.php', 'Delete City', '', 1, 'No', 'On', 'form3.png', 0),
(237, 'Add province', 'modules/set_up_table/province/add_province.php', 'Add Province', '', 1, 'No', 'On', 'form3.png', 0),
(238, 'Edit province', 'modules/set_up_table/province/edit_province.php', 'Edit Province', '', 1, 'No', 'On', 'form3.png', 0),
(239, 'View province', 'modules/set_up_table/province/listview_province.php', 'Province', '', 1, 'Yes', 'On', 'form3.png', 0),
(240, 'Delete province', 'modules/set_up_table/province/delete_province.php', 'Delete Province', '', 1, 'No', 'On', 'form3.png', 0),
(249, 'Add languages', 'modules/set_up_table/languages/add_languages.php', 'Add Languages', '', 1, 'No', 'On', 'form3.png', 0),
(250, 'Edit languages', 'modules/set_up_table/languages/edit_languages.php', 'Edit Languages', '', 1, 'No', 'On', 'form3.png', 0),
(251, 'View languages', 'modules/set_up_table/languages/listview_languages.php', 'Languages', '', 1, 'Yes', 'On', 'form3.png', 0),
(252, 'Delete languages', 'modules/set_up_table/languages/delete_languages.php', 'Delete Languages', '', 1, 'No', 'On', 'form3.png', 0),
(253, 'Add applicant trainings', 'modules/applicant_trainings/add_applicant_trainings.php', 'Add Applicant Trainings', '', 1, 'No', 'On', 'form3.png', 0),
(254, 'Edit applicant trainings', 'modules/applicant_trainings/edit_applicant_trainings.php', 'Edit Applicant Trainings', '', 1, 'No', 'On', 'form3.png', 0),
(255, 'View applicant trainings', 'modules/applicant_trainings/listview_applicant_trainings.php', 'Applicant Trainings', '', 1, 'Yes', 'On', 'form3.png', 0),
(256, 'Delete applicant trainings', 'modules/applicant_trainings/delete_applicant_trainings.php', 'Delete Applicant Trainings', '', 1, 'No', 'On', 'form3.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_passport`
--

CREATE TABLE `user_passport` (
  `username` varchar(255) NOT NULL,
  `link_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_passport`
--

INSERT INTO `user_passport` (`username`, `link_id`) VALUES
('applicant1', 42),
('applicant1', 49),
('applicant1', 50),
('applicant1', 51),
('applicant1', 52),
('applicant1', 57),
('applicant1', 58),
('applicant1', 59),
('applicant1', 60),
('applicant1', 61),
('applicant1', 62),
('applicant1', 63),
('applicant1', 64),
('applicant1', 65),
('applicant1', 66),
('applicant1', 67),
('applicant1', 68),
('applicant1', 69),
('applicant1', 70),
('applicant1', 71),
('applicant1', 72),
('applicant1', 73),
('applicant1', 74),
('applicant1', 75),
('applicant1', 76),
('applicant1', 77),
('applicant1', 78),
('applicant1', 79),
('applicant1', 80),
('applicant1', 197),
('applicant1', 202),
('applicant1', 203),
('applicant1', 204),
('applicant1', 220),
('applicant1', 221),
('applicant1', 222),
('applicant1', 223),
('applicant1', 224),
('applicant1', 225),
('applicant1', 227),
('applicant1', 228),
('applicant1', 229),
('applicant1', 231),
('applicant1', 253),
('applicant1', 254),
('applicant1', 255),
('applicant1', 256),
('markdanico.fernandez1@gmail.com', 42),
('markdanico.fernandez1@gmail.com', 49),
('markdanico.fernandez1@gmail.com', 50),
('markdanico.fernandez1@gmail.com', 51),
('markdanico.fernandez1@gmail.com', 52),
('markdanico.fernandez1@gmail.com', 57),
('markdanico.fernandez1@gmail.com', 58),
('markdanico.fernandez1@gmail.com', 59),
('markdanico.fernandez1@gmail.com', 60),
('markdanico.fernandez1@gmail.com', 61),
('markdanico.fernandez1@gmail.com', 62),
('markdanico.fernandez1@gmail.com', 63),
('markdanico.fernandez1@gmail.com', 64),
('markdanico.fernandez1@gmail.com', 65),
('markdanico.fernandez1@gmail.com', 66),
('markdanico.fernandez1@gmail.com', 67),
('markdanico.fernandez1@gmail.com', 68),
('markdanico.fernandez1@gmail.com', 69),
('markdanico.fernandez1@gmail.com', 70),
('markdanico.fernandez1@gmail.com', 71),
('markdanico.fernandez1@gmail.com', 72),
('markdanico.fernandez1@gmail.com', 73),
('markdanico.fernandez1@gmail.com', 74),
('markdanico.fernandez1@gmail.com', 75),
('markdanico.fernandez1@gmail.com', 76),
('markdanico.fernandez1@gmail.com', 77),
('markdanico.fernandez1@gmail.com', 78),
('markdanico.fernandez1@gmail.com', 79),
('markdanico.fernandez1@gmail.com', 80),
('markdanico.fernandez1@gmail.com', 197),
('markdanico.fernandez1@gmail.com', 202),
('markdanico.fernandez1@gmail.com', 203),
('markdanico.fernandez1@gmail.com', 204),
('markdanico.fernandez1@gmail.com', 220),
('markdanico.fernandez1@gmail.com', 221),
('markdanico.fernandez1@gmail.com', 222),
('markdanico.fernandez1@gmail.com', 223),
('markdanico.fernandez1@gmail.com', 224),
('markdanico.fernandez1@gmail.com', 225),
('markdanico.fernandez1@gmail.com', 227),
('markdanico.fernandez1@gmail.com', 228),
('markdanico.fernandez1@gmail.com', 229),
('markdanico.fernandez1@gmail.com', 231),
('markdanico.fernandez1@gmail.com', 253),
('markdanico.fernandez1@gmail.com', 254),
('markdanico.fernandez1@gmail.com', 255),
('markdanico.fernandez1@gmail.com', 256),
('markdanico.fernandez@gmail.com', 42),
('markdanico.fernandez@gmail.com', 49),
('markdanico.fernandez@gmail.com', 50),
('markdanico.fernandez@gmail.com', 51),
('markdanico.fernandez@gmail.com', 52),
('markdanico.fernandez@gmail.com', 57),
('markdanico.fernandez@gmail.com', 58),
('markdanico.fernandez@gmail.com', 59),
('markdanico.fernandez@gmail.com', 60),
('markdanico.fernandez@gmail.com', 61),
('markdanico.fernandez@gmail.com', 62),
('markdanico.fernandez@gmail.com', 63),
('markdanico.fernandez@gmail.com', 64),
('markdanico.fernandez@gmail.com', 65),
('markdanico.fernandez@gmail.com', 66),
('markdanico.fernandez@gmail.com', 67),
('markdanico.fernandez@gmail.com', 68),
('markdanico.fernandez@gmail.com', 69),
('markdanico.fernandez@gmail.com', 70),
('markdanico.fernandez@gmail.com', 71),
('markdanico.fernandez@gmail.com', 72),
('markdanico.fernandez@gmail.com', 73),
('markdanico.fernandez@gmail.com', 74),
('markdanico.fernandez@gmail.com', 75),
('markdanico.fernandez@gmail.com', 76),
('markdanico.fernandez@gmail.com', 77),
('markdanico.fernandez@gmail.com', 78),
('markdanico.fernandez@gmail.com', 79),
('markdanico.fernandez@gmail.com', 80),
('markdanico.fernandez@gmail.com', 197),
('markdanico.fernandez@gmail.com', 202),
('markdanico.fernandez@gmail.com', 203),
('markdanico.fernandez@gmail.com', 204),
('markdanico.fernandez@gmail.com', 220),
('markdanico.fernandez@gmail.com', 221),
('markdanico.fernandez@gmail.com', 222),
('markdanico.fernandez@gmail.com', 223),
('markdanico.fernandez@gmail.com', 224),
('markdanico.fernandez@gmail.com', 225),
('markdanico.fernandez@gmail.com', 227),
('markdanico.fernandez@gmail.com', 228),
('markdanico.fernandez@gmail.com', 229),
('markdanico.fernandez@gmail.com', 231),
('markdanico.fernandez@gmail.com', 253),
('markdanico.fernandez@gmail.com', 254),
('markdanico.fernandez@gmail.com', 255),
('markdanico.fernandez@gmail.com', 256),
('markdanico.fernandezz@gmail.com', 42),
('markdanico.fernandezz@gmail.com', 49),
('markdanico.fernandezz@gmail.com', 50),
('markdanico.fernandezz@gmail.com', 51),
('markdanico.fernandezz@gmail.com', 52),
('markdanico.fernandezz@gmail.com', 57),
('markdanico.fernandezz@gmail.com', 58),
('markdanico.fernandezz@gmail.com', 59),
('markdanico.fernandezz@gmail.com', 60),
('markdanico.fernandezz@gmail.com', 61),
('markdanico.fernandezz@gmail.com', 62),
('markdanico.fernandezz@gmail.com', 63),
('markdanico.fernandezz@gmail.com', 64),
('markdanico.fernandezz@gmail.com', 65),
('markdanico.fernandezz@gmail.com', 66),
('markdanico.fernandezz@gmail.com', 67),
('markdanico.fernandezz@gmail.com', 68),
('markdanico.fernandezz@gmail.com', 69),
('markdanico.fernandezz@gmail.com', 70),
('markdanico.fernandezz@gmail.com', 71),
('markdanico.fernandezz@gmail.com', 72),
('markdanico.fernandezz@gmail.com', 73),
('markdanico.fernandezz@gmail.com', 74),
('markdanico.fernandezz@gmail.com', 75),
('markdanico.fernandezz@gmail.com', 76),
('markdanico.fernandezz@gmail.com', 77),
('markdanico.fernandezz@gmail.com', 78),
('markdanico.fernandezz@gmail.com', 79),
('markdanico.fernandezz@gmail.com', 80),
('markdanico.fernandezz@gmail.com', 197),
('markdanico.fernandezz@gmail.com', 202),
('markdanico.fernandezz@gmail.com', 203),
('markdanico.fernandezz@gmail.com', 204),
('markdanico.fernandezz@gmail.com', 220),
('markdanico.fernandezz@gmail.com', 221),
('markdanico.fernandezz@gmail.com', 222),
('markdanico.fernandezz@gmail.com', 223),
('markdanico.fernandezz@gmail.com', 224),
('markdanico.fernandezz@gmail.com', 225),
('markdanico.fernandezz@gmail.com', 227),
('markdanico.fernandezz@gmail.com', 228),
('markdanico.fernandezz@gmail.com', 229),
('markdanico.fernandezz@gmail.com', 231),
('markdanico.fernandezz@gmail.com', 253),
('markdanico.fernandezz@gmail.com', 254),
('markdanico.fernandezz@gmail.com', 255),
('markdanico.fernandezz@gmail.com', 256),
('recruiter1', 43),
('recruiter1', 213),
('recruiter1', 215),
('recruiter1', 216),
('recruiter1', 217),
('recruiter1', 232),
('reyeskimberly018@gmail.com', 42),
('reyeskimberly018@gmail.com', 49),
('reyeskimberly018@gmail.com', 50),
('reyeskimberly018@gmail.com', 51),
('reyeskimberly018@gmail.com', 52),
('reyeskimberly018@gmail.com', 57),
('reyeskimberly018@gmail.com', 58),
('reyeskimberly018@gmail.com', 59),
('reyeskimberly018@gmail.com', 60),
('reyeskimberly018@gmail.com', 61),
('reyeskimberly018@gmail.com', 62),
('reyeskimberly018@gmail.com', 63),
('reyeskimberly018@gmail.com', 64),
('reyeskimberly018@gmail.com', 65),
('reyeskimberly018@gmail.com', 66),
('reyeskimberly018@gmail.com', 67),
('reyeskimberly018@gmail.com', 68),
('reyeskimberly018@gmail.com', 69),
('reyeskimberly018@gmail.com', 70),
('reyeskimberly018@gmail.com', 71),
('reyeskimberly018@gmail.com', 72),
('reyeskimberly018@gmail.com', 73),
('reyeskimberly018@gmail.com', 74),
('reyeskimberly018@gmail.com', 75),
('reyeskimberly018@gmail.com', 76),
('reyeskimberly018@gmail.com', 77),
('reyeskimberly018@gmail.com', 78),
('reyeskimberly018@gmail.com', 79),
('reyeskimberly018@gmail.com', 80),
('reyeskimberly018@gmail.com', 197),
('reyeskimberly018@gmail.com', 202),
('reyeskimberly018@gmail.com', 203),
('reyeskimberly018@gmail.com', 204),
('reyeskimberly018@gmail.com', 220),
('reyeskimberly018@gmail.com', 221),
('reyeskimberly018@gmail.com', 222),
('reyeskimberly018@gmail.com', 223),
('reyeskimberly018@gmail.com', 224),
('reyeskimberly018@gmail.com', 225),
('reyeskimberly018@gmail.com', 227),
('reyeskimberly018@gmail.com', 228),
('reyeskimberly018@gmail.com', 229),
('reyeskimberly018@gmail.com', 231),
('reyeskimberly018@gmail.com', 253),
('reyeskimberly018@gmail.com', 254),
('reyeskimberly018@gmail.com', 255),
('reyeskimberly018@gmail.com', 256),
('root', 1),
('root', 2),
('root', 3),
('root', 4),
('root', 5),
('root', 6),
('root', 7),
('root', 8),
('root', 9),
('root', 10),
('root', 11),
('root', 12),
('root', 13),
('root', 14),
('root', 15),
('root', 16),
('root', 17),
('root', 18),
('root', 19),
('root', 20),
('root', 21),
('root', 22),
('root', 23),
('root', 24),
('root', 25),
('root', 26),
('root', 27),
('root', 28),
('root', 29),
('root', 30),
('root', 31),
('root', 32),
('root', 33),
('root', 34),
('root', 35),
('root', 36),
('root', 37),
('root', 38),
('root', 39),
('root', 40),
('root', 41),
('root', 42),
('root', 43),
('root', 44),
('root', 45),
('root', 46),
('root', 47),
('root', 48),
('root', 49),
('root', 50),
('root', 51),
('root', 52),
('root', 53),
('root', 54),
('root', 55),
('root', 56),
('root', 57),
('root', 58),
('root', 59),
('root', 60),
('root', 61),
('root', 62),
('root', 63),
('root', 64),
('root', 65),
('root', 66),
('root', 67),
('root', 68),
('root', 69),
('root', 70),
('root', 71),
('root', 72),
('root', 73),
('root', 74),
('root', 75),
('root', 76),
('root', 77),
('root', 78),
('root', 79),
('root', 80),
('root', 81),
('root', 82),
('root', 83),
('root', 84),
('root', 85),
('root', 86),
('root', 87),
('root', 88),
('root', 89),
('root', 90),
('root', 91),
('root', 92),
('root', 93),
('root', 94),
('root', 95),
('root', 96),
('root', 97),
('root', 98),
('root', 99),
('root', 100),
('root', 101),
('root', 102),
('root', 103),
('root', 104),
('root', 105),
('root', 106),
('root', 107),
('root', 108),
('root', 109),
('root', 110),
('root', 111),
('root', 112),
('root', 113),
('root', 114),
('root', 115),
('root', 116),
('root', 117),
('root', 118),
('root', 119),
('root', 120),
('root', 121),
('root', 122),
('root', 123),
('root', 124),
('root', 125),
('root', 126),
('root', 127),
('root', 128),
('root', 129),
('root', 130),
('root', 131),
('root', 132),
('root', 133),
('root', 134),
('root', 135),
('root', 136),
('root', 137),
('root', 138),
('root', 139),
('root', 140),
('root', 141),
('root', 142),
('root', 143),
('root', 144),
('root', 145),
('root', 146),
('root', 147),
('root', 148),
('root', 149),
('root', 150),
('root', 151),
('root', 152),
('root', 153),
('root', 154),
('root', 155),
('root', 156),
('root', 157),
('root', 158),
('root', 159),
('root', 160),
('root', 161),
('root', 162),
('root', 163),
('root', 164),
('root', 165),
('root', 166),
('root', 167),
('root', 168),
('root', 169),
('root', 170),
('root', 171),
('root', 172),
('root', 173),
('root', 174),
('root', 175),
('root', 176),
('root', 177),
('root', 178),
('root', 179),
('root', 180),
('root', 181),
('root', 182),
('root', 183),
('root', 184),
('root', 185),
('root', 186),
('root', 187),
('root', 188),
('root', 189),
('root', 190),
('root', 191),
('root', 192),
('root', 193),
('root', 194),
('root', 195),
('root', 196),
('root', 197),
('root', 202),
('root', 203),
('root', 204),
('root', 205),
('root', 206),
('root', 207),
('root', 208),
('root', 209),
('root', 210),
('root', 211),
('root', 212),
('root', 213),
('root', 214),
('smic.email.tester@gmail.com', 42),
('smic.email.tester@gmail.com', 49),
('smic.email.tester@gmail.com', 50),
('smic.email.tester@gmail.com', 51),
('smic.email.tester@gmail.com', 52),
('smic.email.tester@gmail.com', 57),
('smic.email.tester@gmail.com', 58),
('smic.email.tester@gmail.com', 59),
('smic.email.tester@gmail.com', 60),
('smic.email.tester@gmail.com', 61),
('smic.email.tester@gmail.com', 62),
('smic.email.tester@gmail.com', 63),
('smic.email.tester@gmail.com', 64),
('smic.email.tester@gmail.com', 65),
('smic.email.tester@gmail.com', 66),
('smic.email.tester@gmail.com', 67),
('smic.email.tester@gmail.com', 68),
('smic.email.tester@gmail.com', 69),
('smic.email.tester@gmail.com', 70),
('smic.email.tester@gmail.com', 71),
('smic.email.tester@gmail.com', 72),
('smic.email.tester@gmail.com', 73),
('smic.email.tester@gmail.com', 74),
('smic.email.tester@gmail.com', 75),
('smic.email.tester@gmail.com', 76),
('smic.email.tester@gmail.com', 77),
('smic.email.tester@gmail.com', 78),
('smic.email.tester@gmail.com', 79),
('smic.email.tester@gmail.com', 80),
('smic.email.tester@gmail.com', 197),
('smic.email.tester@gmail.com', 202),
('smic.email.tester@gmail.com', 203),
('smic.email.tester@gmail.com', 204),
('smic.email.tester@gmail.com', 220),
('smic.email.tester@gmail.com', 221),
('smic.email.tester@gmail.com', 222),
('smic.email.tester@gmail.com', 223),
('smic.email.tester@gmail.com', 224),
('smic.email.tester@gmail.com', 225),
('smic.email.tester@gmail.com', 227),
('smic.email.tester@gmail.com', 228),
('smic.email.tester@gmail.com', 229),
('smic.email.tester@gmail.com', 231),
('smic.email.tester@gmail.com', 253),
('smic.email.tester@gmail.com', 254),
('smic.email.tester@gmail.com', 255),
('smic.email.tester@gmail.com', 256),
('sumimurayoshi28@gmail.com', 42),
('sumimurayoshi28@gmail.com', 49),
('sumimurayoshi28@gmail.com', 50),
('sumimurayoshi28@gmail.com', 51),
('sumimurayoshi28@gmail.com', 52),
('sumimurayoshi28@gmail.com', 57),
('sumimurayoshi28@gmail.com', 58),
('sumimurayoshi28@gmail.com', 59),
('sumimurayoshi28@gmail.com', 60),
('sumimurayoshi28@gmail.com', 61),
('sumimurayoshi28@gmail.com', 62),
('sumimurayoshi28@gmail.com', 63),
('sumimurayoshi28@gmail.com', 64),
('sumimurayoshi28@gmail.com', 65),
('sumimurayoshi28@gmail.com', 66),
('sumimurayoshi28@gmail.com', 67),
('sumimurayoshi28@gmail.com', 68),
('sumimurayoshi28@gmail.com', 69),
('sumimurayoshi28@gmail.com', 70),
('sumimurayoshi28@gmail.com', 71),
('sumimurayoshi28@gmail.com', 72),
('sumimurayoshi28@gmail.com', 73),
('sumimurayoshi28@gmail.com', 74),
('sumimurayoshi28@gmail.com', 75),
('sumimurayoshi28@gmail.com', 76),
('sumimurayoshi28@gmail.com', 77),
('sumimurayoshi28@gmail.com', 78),
('sumimurayoshi28@gmail.com', 79),
('sumimurayoshi28@gmail.com', 80),
('sumimurayoshi28@gmail.com', 197),
('sumimurayoshi28@gmail.com', 202),
('sumimurayoshi28@gmail.com', 203),
('sumimurayoshi28@gmail.com', 204),
('sumimurayoshi28@gmail.com', 220),
('sumimurayoshi28@gmail.com', 221),
('sumimurayoshi28@gmail.com', 222),
('sumimurayoshi28@gmail.com', 223),
('sumimurayoshi28@gmail.com', 224),
('sumimurayoshi28@gmail.com', 225),
('sumimurayoshi28@gmail.com', 227),
('sumimurayoshi28@gmail.com', 228),
('sumimurayoshi28@gmail.com', 229),
('sumimurayoshi28@gmail.com', 231),
('sumimurayoshi28@gmail.com', 253),
('sumimurayoshi28@gmail.com', 254),
('sumimurayoshi28@gmail.com', 255),
('sumimurayoshi28@gmail.com', 256);

-- --------------------------------------------------------

--
-- Table structure for table `user_passport_groups`
--

CREATE TABLE `user_passport_groups` (
  `passport_group_id` int(11) NOT NULL,
  `passport_group` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL,
  `icon` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_passport_groups`
--

INSERT INTO `user_passport_groups` (`passport_group_id`, `passport_group`, `priority`, `icon`) VALUES
(1, 'Default', 0, 'blue_folder3.png'),
(2, 'Admin', 0, 'preferences-system.png'),
(3, 'Applicant Information', 1, 'blue_folder3.png');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `role_id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`role_id`, `role`, `description`) VALUES
(1, 'Super Admin', 'Super admin role with 100% system privileges'),
(2, 'System Admin', 'System admin role with all sysadmin permissions'),
(3, 'Applicant', 'Applicant'),
(4, 'Recruiter Access', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `user_role_links`
--

CREATE TABLE `user_role_links` (
  `role_id` int(11) NOT NULL,
  `link_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role_links`
--

INSERT INTO `user_role_links` (`role_id`, `link_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(1, 39),
(1, 40),
(1, 41),
(1, 42),
(1, 43),
(1, 44),
(1, 45),
(1, 46),
(1, 47),
(1, 48),
(1, 49),
(1, 50),
(1, 51),
(1, 52),
(1, 53),
(1, 54),
(1, 55),
(1, 56),
(1, 57),
(1, 58),
(1, 59),
(1, 60),
(1, 61),
(1, 62),
(1, 63),
(1, 64),
(1, 65),
(1, 66),
(1, 67),
(1, 68),
(1, 69),
(1, 70),
(1, 71),
(1, 72),
(1, 73),
(1, 74),
(1, 75),
(1, 76),
(1, 77),
(1, 78),
(1, 79),
(1, 80),
(1, 81),
(1, 82),
(1, 83),
(1, 84),
(1, 85),
(1, 86),
(1, 87),
(1, 88),
(1, 89),
(1, 90),
(1, 91),
(1, 92),
(1, 93),
(1, 94),
(1, 95),
(1, 96),
(1, 97),
(1, 98),
(1, 99),
(1, 100),
(1, 101),
(1, 102),
(1, 103),
(1, 104),
(1, 105),
(1, 106),
(1, 107),
(1, 108),
(1, 109),
(1, 110),
(1, 111),
(1, 112),
(1, 113),
(1, 114),
(1, 115),
(1, 116),
(1, 117),
(1, 118),
(1, 119),
(1, 120),
(1, 121),
(1, 122),
(1, 123),
(1, 124),
(1, 125),
(1, 126),
(1, 127),
(1, 128),
(1, 129),
(1, 130),
(1, 131),
(1, 132),
(1, 133),
(1, 134),
(1, 135),
(1, 136),
(1, 137),
(1, 138),
(1, 139),
(1, 140),
(1, 141),
(1, 142),
(1, 143),
(1, 144),
(1, 145),
(1, 146),
(1, 147),
(1, 148),
(1, 149),
(1, 150),
(1, 151),
(1, 152),
(1, 153),
(1, 154),
(1, 155),
(1, 156),
(1, 157),
(1, 158),
(1, 159),
(1, 160),
(1, 161),
(1, 162),
(1, 163),
(1, 164),
(1, 165),
(1, 166),
(1, 167),
(1, 168),
(1, 169),
(1, 170),
(1, 171),
(1, 172),
(1, 173),
(1, 174),
(1, 175),
(1, 176),
(1, 177),
(1, 178),
(1, 179),
(1, 180),
(1, 181),
(1, 182),
(1, 183),
(1, 184),
(1, 185),
(1, 186),
(1, 187),
(1, 188),
(1, 189),
(1, 190),
(1, 191),
(1, 192),
(1, 193),
(1, 194),
(1, 195),
(1, 196),
(1, 197),
(1, 202),
(1, 203),
(1, 204),
(1, 205),
(1, 206),
(1, 207),
(1, 208),
(1, 209),
(1, 210),
(1, 211),
(1, 212),
(1, 213),
(1, 214),
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(2, 8),
(2, 9),
(2, 10),
(2, 11),
(2, 12),
(2, 13),
(2, 14),
(2, 15),
(2, 16),
(2, 17),
(2, 18),
(2, 19),
(2, 20),
(2, 21),
(2, 22),
(2, 23),
(2, 24),
(2, 25),
(2, 26),
(2, 27),
(2, 28),
(2, 29),
(2, 30),
(2, 31),
(2, 32),
(2, 33),
(2, 34),
(2, 35),
(2, 36),
(3, 42),
(3, 49),
(3, 50),
(3, 51),
(3, 52),
(3, 57),
(3, 58),
(3, 59),
(3, 60),
(3, 61),
(3, 62),
(3, 63),
(3, 64),
(3, 65),
(3, 66),
(3, 67),
(3, 68),
(3, 69),
(3, 70),
(3, 71),
(3, 72),
(3, 73),
(3, 74),
(3, 75),
(3, 76),
(3, 77),
(3, 78),
(3, 79),
(3, 80),
(3, 197),
(3, 202),
(3, 203),
(3, 204),
(3, 220),
(3, 221),
(3, 222),
(3, 223),
(3, 224),
(3, 225),
(3, 227),
(3, 228),
(3, 229),
(3, 231),
(3, 253),
(3, 254),
(3, 255),
(3, 256),
(4, 43),
(4, 213),
(4, 215),
(4, 216),
(4, 217),
(4, 232);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `action_notice`
--
ALTER TABLE `action_notice`
  ADD PRIMARY KEY (`action_notice_id`),
  ADD KEY `fk_action_notice_employee1_idx` (`employee_id`);

--
-- Indexes for table `applicant`
--
ALTER TABLE `applicant`
  ADD PRIMARY KEY (`applicant_id`);

--
-- Indexes for table `applicant_attachments`
--
ALTER TABLE `applicant_attachments`
  ADD PRIMARY KEY (`applicant_attachment_id`);

--
-- Indexes for table `applicant_exam`
--
ALTER TABLE `applicant_exam`
  ADD PRIMARY KEY (`applicant_exam_id`),
  ADD KEY `fk_applicant_exam_applicant1_idx` (`applicant_id`);

--
-- Indexes for table `applicant_family_members`
--
ALTER TABLE `applicant_family_members`
  ADD PRIMARY KEY (`applicant_family_member_id`);

--
-- Indexes for table `applicant_interview`
--
ALTER TABLE `applicant_interview`
  ADD PRIMARY KEY (`applicant_interview_id`),
  ADD KEY `fk_applicant_interview_employee2_idx` (`interviewer_employee_id`),
  ADD KEY `fk_applicant_interview_applicant1_idx` (`applicant_id`);

--
-- Indexes for table `applicant_languages_proficiency`
--
ALTER TABLE `applicant_languages_proficiency`
  ADD PRIMARY KEY (`applicant_language_proficiency_id`);

--
-- Indexes for table `applicant_license`
--
ALTER TABLE `applicant_license`
  ADD PRIMARY KEY (`applicant_license_id`);

--
-- Indexes for table `applicant_other_declarations`
--
ALTER TABLE `applicant_other_declarations`
  ADD PRIMARY KEY (`applicant_other_declaration_id`);

--
-- Indexes for table `applicant_preferred_positions`
--
ALTER TABLE `applicant_preferred_positions`
  ADD PRIMARY KEY (`applicant_preferred_position_id`);

--
-- Indexes for table `applicant_previous_employers`
--
ALTER TABLE `applicant_previous_employers`
  ADD PRIMARY KEY (`applicant_previous_employer_id`);

--
-- Indexes for table `applicant_reference`
--
ALTER TABLE `applicant_reference`
  ADD PRIMARY KEY (`applicant_reference_id`);

--
-- Indexes for table `applicant_school_attended`
--
ALTER TABLE `applicant_school_attended`
  ADD PRIMARY KEY (`applicant_school_attended_id`);

--
-- Indexes for table `applicant_skills`
--
ALTER TABLE `applicant_skills`
  ADD PRIMARY KEY (`applicant_skill_id`);

--
-- Indexes for table `applicant_trainings`
--
ALTER TABLE `applicant_trainings`
  ADD PRIMARY KEY (`applicant_training_id`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`branch_id`),
  ADD KEY `fk_branch_company_idx` (`company_id`),
  ADD KEY `fk_branch_building1_idx` (`building_id`),
  ADD KEY `fk_branch_branch_type1_idx` (`branch_type_id`);

--
-- Indexes for table `branch_type`
--
ALTER TABLE `branch_type`
  ADD PRIMARY KEY (`branch_type_id`);

--
-- Indexes for table `building`
--
ALTER TABLE `building`
  ADD PRIMARY KEY (`building_id`),
  ADD KEY `fk_building_site1_idx` (`site_id`);

--
-- Indexes for table `calendar_event`
--
ALTER TABLE `calendar_event`
  ADD PRIMARY KEY (`calendar_event_id`),
  ADD KEY `fk_calendar_event_calendar_event_type1_idx` (`calendar_event_type_id`);

--
-- Indexes for table `calendar_event_type`
--
ALTER TABLE `calendar_event_type`
  ADD PRIMARY KEY (`calendar_event_type_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `classification`
--
ALTER TABLE `classification`
  ADD PRIMARY KEY (`classification_id`);

--
-- Indexes for table `clearance`
--
ALTER TABLE `clearance`
  ADD PRIMARY KEY (`clearance_id`),
  ADD KEY `fk_clearance_resignation1_idx` (`resignation_id`);

--
-- Indexes for table `clearance_approval`
--
ALTER TABLE `clearance_approval`
  ADD PRIMARY KEY (`clearance_approval_id`),
  ADD KEY `fk_clearance_approval_clearance1_idx` (`clearance_id`),
  ADD KEY `fk_clearance_approval_clearance_approver1_idx` (`clearance_approver_id`);

--
-- Indexes for table `clearance_approver`
--
ALTER TABLE `clearance_approver`
  ADD PRIMARY KEY (`clearance_approver_id`),
  ADD KEY `fk_clearance_approver_employee1_idx` (`employee_id`);

--
-- Indexes for table `cobalt_reporter`
--
ALTER TABLE `cobalt_reporter`
  ADD PRIMARY KEY (`module_name`,`report_name`);

--
-- Indexes for table `cobalt_sst`
--
ALTER TABLE `cobalt_sst`
  ADD PRIMARY KEY (`auto_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `company_policy`
--
ALTER TABLE `company_policy`
  ADD PRIMARY KEY (`company_policy_id`),
  ADD KEY `fk_company_policy_company_policy1_idx` (`parent_company_policy_id`);

--
-- Indexes for table `company_violation`
--
ALTER TABLE `company_violation`
  ADD PRIMARY KEY (`company_violation_id`),
  ADD KEY `fk_company_violation_employee1_idx` (`employee_id`),
  ADD KEY `fk_company_violation_company_policy1_idx` (`company_policy_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`department_id`),
  ADD KEY `fk_department_branch1_idx` (`branch_id`),
  ADD KEY `fk_department_floor1_idx` (`floor_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`employee_id`),
  ADD KEY `fk_employee_branch1_idx` (`branch_id`),
  ADD KEY `fk_employee_department1_idx` (`department_id`),
  ADD KEY `fk_employee_employee1_idx` (`approver1_employee_id`),
  ADD KEY `fk_employee_employee2_idx` (`approver2_employee_id`),
  ADD KEY `fk_employee_plantilla1_idx` (`plantilla_id`),
  ADD KEY `fk_employee_company1_idx` (`company_id`);

--
-- Indexes for table `employee_education`
--
ALTER TABLE `employee_education`
  ADD PRIMARY KEY (`employee_education_id`),
  ADD KEY `fk_employee_education_employee1_idx` (`employee_id`);

--
-- Indexes for table `employee_evaluation`
--
ALTER TABLE `employee_evaluation`
  ADD PRIMARY KEY (`employee_evaluation_id`),
  ADD KEY `fk_employee_evaluation_employee1_idx` (`employee_id`),
  ADD KEY `fk_employee_evaluation_employee_evaluation_template1_idx` (`employee_evaluation_template_id`);

--
-- Indexes for table `employee_evaluation_answer`
--
ALTER TABLE `employee_evaluation_answer`
  ADD PRIMARY KEY (`employee_evaluation_answer_id`),
  ADD KEY `fk_employee_evaluation_answer_employee_evaluation1_idx` (`employee_evaluation_id`);

--
-- Indexes for table `employee_evaluation_template`
--
ALTER TABLE `employee_evaluation_template`
  ADD PRIMARY KEY (`employee_evaluation_template_id`);

--
-- Indexes for table `employee_evaluation_template_criteria`
--
ALTER TABLE `employee_evaluation_template_criteria`
  ADD PRIMARY KEY (`employee_evaluation_template_criteria_id`),
  ADD KEY `fk_employee_evaluation_template_criteria_employee_evaluatio_idx` (`employee_evaluation_template_id`);

--
-- Indexes for table `floor`
--
ALTER TABLE `floor`
  ADD PRIMARY KEY (`floor_id`),
  ADD KEY `fk_floor_site1_idx` (`site_id`),
  ADD KEY `fk_floor_building1_idx` (`building_id`);

--
-- Indexes for table `forgot_pass_table`
--
ALTER TABLE `forgot_pass_table`
  ADD PRIMARY KEY (`forgot_pass_id`);

--
-- Indexes for table `hr_program`
--
ALTER TABLE `hr_program`
  ADD PRIMARY KEY (`hr_program_id`);

--
-- Indexes for table `hr_program_attendees`
--
ALTER TABLE `hr_program_attendees`
  ADD PRIMARY KEY (`hr_program_attendees_id`),
  ADD KEY `fk_hr_program_attendees_hr_program1_idx` (`hr_program_id`),
  ADD KEY `fk_hr_program_attendees_employee1_idx` (`employee_id`);

--
-- Indexes for table `ibloodtype`
--
ALTER TABLE `ibloodtype`
  ADD PRIMARY KEY (`BloodTypeID`);

--
-- Indexes for table `icitizenship`
--
ALTER TABLE `icitizenship`
  ADD PRIMARY KEY (`CitizenshipID`);

--
-- Indexes for table `icivilstatus`
--
ALTER TABLE `icivilstatus`
  ADD PRIMARY KEY (`CivilStatusID`);

--
-- Indexes for table `icourse`
--
ALTER TABLE `icourse`
  ADD PRIMARY KEY (`CourseID`);

--
-- Indexes for table `ieducationallevel`
--
ALTER TABLE `ieducationallevel`
  ADD PRIMARY KEY (`EducationalLevelID`);

--
-- Indexes for table `iemployeestatus`
--
ALTER TABLE `iemployeestatus`
  ADD PRIMARY KEY (`EmployeeStatusID`);

--
-- Indexes for table `igender`
--
ALTER TABLE `igender`
  ADD PRIMARY KEY (`GenderID`);

--
-- Indexes for table `ilanguage`
--
ALTER TABLE `ilanguage`
  ADD PRIMARY KEY (`LanguageID`);

--
-- Indexes for table `iprf_staffrequest`
--
ALTER TABLE `iprf_staffrequest`
  ADD PRIMARY KEY (`iprf_staffrequest_id`);

--
-- Indexes for table `iprf_staffrequest_applicants`
--
ALTER TABLE `iprf_staffrequest_applicants`
  ADD PRIMARY KEY (`iprf_staffrequest_applicant_id`);

--
-- Indexes for table `iproficiency`
--
ALTER TABLE `iproficiency`
  ADD PRIMARY KEY (`ProficiencyID`);

--
-- Indexes for table `irelationship`
--
ALTER TABLE `irelationship`
  ADD PRIMARY KEY (`RelationshipID`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `other_declarations_name_smgroup`
--
ALTER TABLE `other_declarations_name_smgroup`
  ADD PRIMARY KEY (`other_declarations_name_smgroup_id`);

--
-- Indexes for table `other_declarations_name_smic_employ`
--
ALTER TABLE `other_declarations_name_smic_employ`
  ADD PRIMARY KEY (`other_declarations_name_smic_employ_id`);

--
-- Indexes for table `performance_appraisal`
--
ALTER TABLE `performance_appraisal`
  ADD PRIMARY KEY (`performance_appraisal_id`),
  ADD KEY `fk_performance_appraisal_employee1_idx` (`employee_id`);

--
-- Indexes for table `performance_appraisal_goal`
--
ALTER TABLE `performance_appraisal_goal`
  ADD PRIMARY KEY (`performance_appraisal_goal_id`),
  ADD KEY `fk_performance_appraisal_goal_performance_appraisal1_idx` (`performance_appraisal_id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`person_id`);

--
-- Indexes for table `personnel_requisition`
--
ALTER TABLE `personnel_requisition`
  ADD PRIMARY KEY (`personnel_requisition_id`),
  ADD KEY `fk_personnel_requisition_plantilla1_idx` (`plantilla_id`),
  ADD KEY `fk_personnel_requisition_branch1_idx` (`branch_id`),
  ADD KEY `fk_personnel_requisition_department1_idx` (`department_id`),
  ADD KEY `fk_personnel_requisition_company1_idx` (`company_id`),
  ADD KEY `fk_personnel_requisition_employee1_idx` (`employee_id`);

--
-- Indexes for table `plantilla`
--
ALTER TABLE `plantilla`
  ADD PRIMARY KEY (`plantilla_id`),
  ADD KEY `fk_plantilla_plantilla1_idx` (`parent_plantilla_id`),
  ADD KEY `fk_plantilla_branch1_idx` (`branch_id`),
  ADD KEY `fk_plantilla_department1_idx` (`department_id`),
  ADD KEY `fk_plantilla_company1_idx` (`company_id`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`position_id`);

--
-- Indexes for table `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`province_id`);

--
-- Indexes for table `rank`
--
ALTER TABLE `rank`
  ADD PRIMARY KEY (`rank_id`);

--
-- Indexes for table `resignation`
--
ALTER TABLE `resignation`
  ADD PRIMARY KEY (`resignation_id`),
  ADD KEY `fk_resignation_employee1_idx` (`employee_id`);

--
-- Indexes for table `school`
--
ALTER TABLE `school`
  ADD PRIMARY KEY (`school_id`);

--
-- Indexes for table `site`
--
ALTER TABLE `site`
  ADD PRIMARY KEY (`site_id`);

--
-- Indexes for table `system_log`
--
ALTER TABLE `system_log`
  ADD PRIMARY KEY (`entry_id`);

--
-- Indexes for table `system_settings`
--
ALTER TABLE `system_settings`
  ADD PRIMARY KEY (`setting`);

--
-- Indexes for table `system_skins`
--
ALTER TABLE `system_skins`
  ADD PRIMARY KEY (`skin_id`);

--
-- Indexes for table `temp_user`
--
ALTER TABLE `temp_user`
  ADD PRIMARY KEY (`temp_user_id`);

--
-- Indexes for table `training`
--
ALTER TABLE `training`
  ADD PRIMARY KEY (`training_id`);

--
-- Indexes for table `upload_material`
--
ALTER TABLE `upload_material`
  ADD PRIMARY KEY (`upload_material_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `user_links`
--
ALTER TABLE `user_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `user_passport`
--
ALTER TABLE `user_passport`
  ADD PRIMARY KEY (`username`,`link_id`);

--
-- Indexes for table `user_passport_groups`
--
ALTER TABLE `user_passport_groups`
  ADD PRIMARY KEY (`passport_group_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `user_role_links`
--
ALTER TABLE `user_role_links`
  ADD PRIMARY KEY (`role_id`,`link_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `action_notice`
--
ALTER TABLE `action_notice`
  MODIFY `action_notice_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `applicant`
--
ALTER TABLE `applicant`
  MODIFY `applicant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `applicant_attachments`
--
ALTER TABLE `applicant_attachments`
  MODIFY `applicant_attachment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `applicant_exam`
--
ALTER TABLE `applicant_exam`
  MODIFY `applicant_exam_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `applicant_family_members`
--
ALTER TABLE `applicant_family_members`
  MODIFY `applicant_family_member_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `applicant_interview`
--
ALTER TABLE `applicant_interview`
  MODIFY `applicant_interview_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `applicant_languages_proficiency`
--
ALTER TABLE `applicant_languages_proficiency`
  MODIFY `applicant_language_proficiency_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `applicant_license`
--
ALTER TABLE `applicant_license`
  MODIFY `applicant_license_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `applicant_other_declarations`
--
ALTER TABLE `applicant_other_declarations`
  MODIFY `applicant_other_declaration_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `applicant_preferred_positions`
--
ALTER TABLE `applicant_preferred_positions`
  MODIFY `applicant_preferred_position_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `applicant_previous_employers`
--
ALTER TABLE `applicant_previous_employers`
  MODIFY `applicant_previous_employer_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `applicant_reference`
--
ALTER TABLE `applicant_reference`
  MODIFY `applicant_reference_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `applicant_school_attended`
--
ALTER TABLE `applicant_school_attended`
  MODIFY `applicant_school_attended_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `applicant_skills`
--
ALTER TABLE `applicant_skills`
  MODIFY `applicant_skill_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `applicant_trainings`
--
ALTER TABLE `applicant_trainings`
  MODIFY `applicant_training_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `branch_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `branch_type`
--
ALTER TABLE `branch_type`
  MODIFY `branch_type_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `building`
--
ALTER TABLE `building`
  MODIFY `building_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `calendar_event`
--
ALTER TABLE `calendar_event`
  MODIFY `calendar_event_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `calendar_event_type`
--
ALTER TABLE `calendar_event_type`
  MODIFY `calendar_event_type_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3394;
--
-- AUTO_INCREMENT for table `clearance`
--
ALTER TABLE `clearance`
  MODIFY `clearance_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `clearance_approval`
--
ALTER TABLE `clearance_approval`
  MODIFY `clearance_approval_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `clearance_approver`
--
ALTER TABLE `clearance_approver`
  MODIFY `clearance_approver_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cobalt_sst`
--
ALTER TABLE `cobalt_sst`
  MODIFY `auto_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `company_policy`
--
ALTER TABLE `company_policy`
  MODIFY `company_policy_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `company_violation`
--
ALTER TABLE `company_violation`
  MODIFY `company_violation_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `employee_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_education`
--
ALTER TABLE `employee_education`
  MODIFY `employee_education_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_evaluation`
--
ALTER TABLE `employee_evaluation`
  MODIFY `employee_evaluation_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_evaluation_answer`
--
ALTER TABLE `employee_evaluation_answer`
  MODIFY `employee_evaluation_answer_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_evaluation_template`
--
ALTER TABLE `employee_evaluation_template`
  MODIFY `employee_evaluation_template_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_evaluation_template_criteria`
--
ALTER TABLE `employee_evaluation_template_criteria`
  MODIFY `employee_evaluation_template_criteria_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `floor`
--
ALTER TABLE `floor`
  MODIFY `floor_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `forgot_pass_table`
--
ALTER TABLE `forgot_pass_table`
  MODIFY `forgot_pass_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `hr_program`
--
ALTER TABLE `hr_program`
  MODIFY `hr_program_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hr_program_attendees`
--
ALTER TABLE `hr_program_attendees`
  MODIFY `hr_program_attendees_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `iprf_staffrequest_applicants`
--
ALTER TABLE `iprf_staffrequest_applicants`
  MODIFY `iprf_staffrequest_applicant_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;
--
-- AUTO_INCREMENT for table `other_declarations_name_smgroup`
--
ALTER TABLE `other_declarations_name_smgroup`
  MODIFY `other_declarations_name_smgroup_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `other_declarations_name_smic_employ`
--
ALTER TABLE `other_declarations_name_smic_employ`
  MODIFY `other_declarations_name_smic_employ_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `performance_appraisal`
--
ALTER TABLE `performance_appraisal`
  MODIFY `performance_appraisal_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `performance_appraisal_goal`
--
ALTER TABLE `performance_appraisal_goal`
  MODIFY `performance_appraisal_goal_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
  MODIFY `person_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `personnel_requisition`
--
ALTER TABLE `personnel_requisition`
  MODIFY `personnel_requisition_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plantilla`
--
ALTER TABLE `plantilla`
  MODIFY `plantilla_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `province`
--
ALTER TABLE `province`
  MODIFY `province_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT for table `resignation`
--
ALTER TABLE `resignation`
  MODIFY `resignation_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `school`
--
ALTER TABLE `school`
  MODIFY `school_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2031;
--
-- AUTO_INCREMENT for table `site`
--
ALTER TABLE `site`
  MODIFY `site_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `system_log`
--
ALTER TABLE `system_log`
  MODIFY `entry_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2194;
--
-- AUTO_INCREMENT for table `system_skins`
--
ALTER TABLE `system_skins`
  MODIFY `skin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `temp_user`
--
ALTER TABLE `temp_user`
  MODIFY `temp_user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `training`
--
ALTER TABLE `training`
  MODIFY `training_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `upload_material`
--
ALTER TABLE `upload_material`
  MODIFY `upload_material_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_links`
--
ALTER TABLE `user_links`
  MODIFY `link_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=257;
--
-- AUTO_INCREMENT for table `user_passport_groups`
--
ALTER TABLE `user_passport_groups`
  MODIFY `passport_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `action_notice`
--
ALTER TABLE `action_notice`
  ADD CONSTRAINT `fk_action_notice_employee1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `building`
--
ALTER TABLE `building`
  ADD CONSTRAINT `fk_building_site1` FOREIGN KEY (`site_id`) REFERENCES `site` (`site_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `calendar_event`
--
ALTER TABLE `calendar_event`
  ADD CONSTRAINT `fk_calendar_event_calendar_event_type1` FOREIGN KEY (`calendar_event_type_id`) REFERENCES `calendar_event_type` (`calendar_event_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `clearance`
--
ALTER TABLE `clearance`
  ADD CONSTRAINT `fk_clearance_resignation1` FOREIGN KEY (`resignation_id`) REFERENCES `resignation` (`resignation_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `clearance_approval`
--
ALTER TABLE `clearance_approval`
  ADD CONSTRAINT `fk_clearance_approval_clearance1` FOREIGN KEY (`clearance_id`) REFERENCES `clearance` (`clearance_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_clearance_approval_clearance_approver1` FOREIGN KEY (`clearance_approver_id`) REFERENCES `clearance_approver` (`clearance_approver_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `clearance_approver`
--
ALTER TABLE `clearance_approver`
  ADD CONSTRAINT `fk_clearance_approver_employee1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `company_policy`
--
ALTER TABLE `company_policy`
  ADD CONSTRAINT `fk_company_policy_company_policy1` FOREIGN KEY (`parent_company_policy_id`) REFERENCES `company_policy` (`company_policy_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `company_violation`
--
ALTER TABLE `company_violation`
  ADD CONSTRAINT `fk_company_violation_company_policy1` FOREIGN KEY (`company_policy_id`) REFERENCES `company_policy` (`company_policy_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_company_violation_employee1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `fk_employee_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_employee_employee1` FOREIGN KEY (`approver1_employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_employee_employee2` FOREIGN KEY (`approver2_employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_employee_plantilla1` FOREIGN KEY (`plantilla_id`) REFERENCES `plantilla` (`plantilla_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `employee_education`
--
ALTER TABLE `employee_education`
  ADD CONSTRAINT `fk_employee_education_employee1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `employee_evaluation`
--
ALTER TABLE `employee_evaluation`
  ADD CONSTRAINT `fk_employee_evaluation_employee1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_employee_evaluation_employee_evaluation_template1` FOREIGN KEY (`employee_evaluation_template_id`) REFERENCES `employee_evaluation_template` (`employee_evaluation_template_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `employee_evaluation_answer`
--
ALTER TABLE `employee_evaluation_answer`
  ADD CONSTRAINT `fk_employee_evaluation_answer_employee_evaluation1` FOREIGN KEY (`employee_evaluation_id`) REFERENCES `employee_evaluation` (`employee_evaluation_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `employee_evaluation_template_criteria`
--
ALTER TABLE `employee_evaluation_template_criteria`
  ADD CONSTRAINT `fk_employee_evaluation_template_criteria_employee_evaluation_1` FOREIGN KEY (`employee_evaluation_template_id`) REFERENCES `employee_evaluation_template` (`employee_evaluation_template_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `floor`
--
ALTER TABLE `floor`
  ADD CONSTRAINT `fk_floor_building1` FOREIGN KEY (`building_id`) REFERENCES `building` (`building_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_floor_site1` FOREIGN KEY (`site_id`) REFERENCES `site` (`site_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `hr_program_attendees`
--
ALTER TABLE `hr_program_attendees`
  ADD CONSTRAINT `fk_hr_program_attendees_employee1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_hr_program_attendees_hr_program1` FOREIGN KEY (`hr_program_id`) REFERENCES `hr_program` (`hr_program_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `performance_appraisal`
--
ALTER TABLE `performance_appraisal`
  ADD CONSTRAINT `fk_performance_appraisal_employee1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `performance_appraisal_goal`
--
ALTER TABLE `performance_appraisal_goal`
  ADD CONSTRAINT `fk_performance_appraisal_goal_performance_appraisal1` FOREIGN KEY (`performance_appraisal_id`) REFERENCES `performance_appraisal` (`performance_appraisal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `personnel_requisition`
--
ALTER TABLE `personnel_requisition`
  ADD CONSTRAINT `fk_personnel_requisition_branch1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_personnel_requisition_employee1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_personnel_requisition_plantilla1` FOREIGN KEY (`plantilla_id`) REFERENCES `plantilla` (`plantilla_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `resignation`
--
ALTER TABLE `resignation`
  ADD CONSTRAINT `fk_resignation_employee1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
